%define name      hclib
%define base_dest /opt/app
%define src_dest  %{name}
%define src       %{name}.tar.gz

Name:           %{name} 
Version:        1.0
Release:        1%{?dist}
Summary:        HClib library
Source:         %{_topdir}/SOURCES
Requires:       glibc, libstdc++

License:        Public Domain
URL:            http://fedoraproject.org/

%description
Hockeyhurd's C++ library - HCLib


%prep
# Nothing to setup

%build
# Nothing to build 

%install
%__mkdir -p %{buildroot}/%{base_dest}/%{src_dest}

%__cp -Lrp %{_topdir}/SOURCES/* %{buildroot}/%{base_dest}/%{src_dest}


%clean
%__rm -rf $RPM_BUILD_ROOT
%__rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{base_dest}/%{src_dest}


%pre
# Nothing to do pre-install


%post
# Nothing to do post-install


%preun
# Nothing to do pre-uninstall


%postun
# Nothing to do post-uninstall


%changelog
* Thu May 13 2021 Nick Hurd <nhurd96@gmail.com>
- Initial creation

