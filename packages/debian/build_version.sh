#!/bin/bash

usage()
{
    printf "expected usage:\n\tbash build_version.sh -a \"arch\" -m \"build type\" -v \"version number\"\n"
    printf "or\n\tbash build_version.sh -h\n"
    exit -1
}

while getopts ":a:m:v:h" options; do
    case "${options}" in
        a)
            ARCH=${OPTARG}
            ;;
        h)
            usage
            ;;
        m)
            MODE=${OPTARG}
            ;;
        v)
            VERSION=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

INCLUDE_DEBUG_BUILD=0

if [ "${ARCH}" == "" ]; then
    ARCH=amd64
    echo "Defaulting to architecture: ${ARCH}"
elif [ "${ARCH}" == "amd64" ]; then
    echo "Building for architecture: ${ARCH}"
elif [ "${ARCH}" == "armhf" ]; then
    echo "Building for architecture: ${ARCH}"
else
    echo "Un-supported architecture: ${ARCH}"
fi

if [ "${VERSION}" == "" ]; then
    VERSION="1.0.0-1"
    echo "Defaulting to release version: ${VERSION}"
else
    echo "Building for release version: ${VERSION}"
fi

ROOT_DIR=hclib-${VERSION}

if [ "${MODE}" == "" ]; then
    MODE=release
    BASE_DEST=/usr/local
    NAME=${ROOT_DIR}
    PACKAGE_NAME=hclib
    echo "Defaulting to release type: ${MODE}"
elif [ "${MODE}" == "release" ]; then
    BASE_DEST=/usr/local
    NAME=${ROOT_DIR}
    PACKAGE_NAME=hclib
    echo "Building for release type: ${MODE}"
elif [ "${MODE}" == "debug" ]; then
    BASE_DEST=/usr/local
    NAME=${ROOT_DIR}-dbg
    PACKAGE_NAME=hclib-dbg
    INCLUDE_DEBUG_BUILD=1
    echo "Building for release type: ${MODE}"
elif [ "${MODE}" == "devel" ]; then
    BASE_DEST=/opt/app/hclib
    NAME=${ROOT_DIR}-devel
    PACKAGE_NAME=hclib-devel
    INCLUDE_DEBUG_BUILD=1
    echo "Building for release type: ${MODE}"
else
    echo "Un-supported release type: ${MODE}"
fi

echo "mkdir -p ${NAME}"
mkdir -p ${NAME}
echo "mkdir -p ${NAME}/DEBIAN"
mkdir -p ${NAME}/DEBIAN
echo "mkdir -p ${NAME}/${BASE_DEST}"
mkdir -p ${NAME}/${BASE_DEST}
echo "mkdir -p ${NAME}/${BASE_DEST}/doc/${NAME}"
mkdir -p ${NAME}/${BASE_DEST}/doc/${NAME}
echo "mkdir -p ${NAME}/${BASE_DEST}/lib"
mkdir -p ${NAME}/${BASE_DEST}/lib

# Grab template files.
echo "rsync -tp control_template ${NAME}/DEBIAN/control"
rsync -tp control_template ${NAME}/DEBIAN/control
echo "rsync -tp postinst_template ${NAME}/DEBIAN/postinst"
rsync -tp postinst_template ${NAME}/DEBIAN/postinst

# Pull includes folders.
echo "rsync -trp ../../include ${NAME}/${BASE_DEST}"
rsync -trp ../../include ${NAME}/${BASE_DEST}

# Pull doc folder, but place in special sub-directory
echo "rsync -trp ../../doc/* ${NAME}/${BASE_DEST}/doc/${NAME}"
rsync -trp ../../doc/* ${NAME}/${BASE_DEST}/doc/${NAME}

# Special case, move the LICENSE to the doc folder.
echo "rsync -tp ../../LICENSE ${NAME}/${BASE_DEST}/doc/${NAME}"
rsync -tp ../../LICENSE ${NAME}/${BASE_DEST}/doc/${NAME}

# Copy binaries
echo "rsync -tpL ../../build/libHCLib.so ${NAME}/${BASE_DEST}/lib"
rsync -tpL ../../build/libHCLib.so ${NAME}/${BASE_DEST}/lib

if [[ ${INCLUDE_DEBUG_BUILD} != 0 ]]; then
    echo "rsync -tpL ../../build/libHCLibd.so ${NAME}/${BASE_DEST}/lib"
    rsync -tpL ../../build/libHCLibd.so ${NAME}/${BASE_DEST}/lib
fi

echo "chmod 755 ${NAME}/DEBIAN/control"
chmod 755 ${NAME}/DEBIAN/control
echo "chmod 755 ${NAME}/DEBIAN/postinst"
chmod 755 ${NAME}/DEBIAN/postinst

# Update package information within template files.
echo "sed -i "s#arch#$ARCH#g" ${NAME}/DEBIAN/control"
sed -i "s#arch#${ARCH}#g" ${NAME}/DEBIAN/control
echo "sed -i "s#ver#$VERSION#g" ${NAME}/DEBIAN/control"
sed -i "s#ver#${VERSION}#g" ${NAME}/DEBIAN/control
echo "sed -i "s#package#${PACKAGE_NAME}#g" ${NAME}/DEBIAN/control"
sed -i "s#package#${PACKAGE_NAME}#g" ${NAME}/DEBIAN/control
echo "sed -i "s#path#$BASE_DEST#g" ${NAME}/DEBIAN/postinst"
sed -i "s#path#${BASE_DEST}#g" ${NAME}/DEBIAN/postinst

echo "dpkg-deb --build ${NAME}"
dpkg-deb --build ${NAME}

