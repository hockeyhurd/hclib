#!/bin/bash

if [ -z "$1" ]; then
    ARCH=amd64
else
    ARCH="$1"
fi

if [ -z "$2" ]; then
    VERSION=1.0.0-1
else
    VERSION="$2"
fi

ROOT_DIR=hclib-${VERSION}

echo "rm -rf ${ROOT_DIR}*"
rm -rf ${ROOT_DIR}*
echo "rm -rf ${ROOT_DIR}*.deb"
rm -rf ${ROOT_DIR}*.deb

