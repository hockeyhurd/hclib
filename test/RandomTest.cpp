#include <hclib/Random.h>

#include <gtest/gtest.h>

using namespace hclib;

static const s32 count = 10000;

template<class T>
static bool inRange(const T min, const T max, const T value)
{
    return value >= min && value <= max;
}

TEST(RandomTest, int32_t_test)
{
    RandomInt rand(0, 0xFF);

    for (s32 i = 0; i < count; ++i)
    {
        ASSERT_TRUE(inRange<s32>(0, 0xFF, rand.next()));
    }
}

TEST(RandomTest, int64_t_test)
{
    RandomLong rand(0, 0xFF);

    for (s32 i = 0; i < count; ++i)
    {
        ASSERT_TRUE(inRange<s64>(0, 0xFF, rand.next()));
    }
}

TEST(RandomTest, float32_t_test)
{
    RandomFloat rand(0, 255.0F);

    for (s32 i = 0; i < count; ++i)
    {
        ASSERT_TRUE(inRange<f32>(0, 255.0F, rand.next()));
    }
}

TEST(RandomTest, float64_t_test)
{
    RandomDouble rand(0, 255.0);

    for (s32 i = 0; i < count; ++i)
    {
        ASSERT_TRUE(inRange<f64>(0, 255.0, rand.next()));
    }
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
