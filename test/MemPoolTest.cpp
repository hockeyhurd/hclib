#include <hclib/MemPool.h>
#include <hclib/Types.h>

#include <iostream>
#include <set>

using namespace hclib;

class Test
{
    private:

        s32 value;

    public:

        Test(const s32 value = 0) : value(value)
        {
        }

        s32 getValue() const
        {
            return value;
        }

        void setValue(const s32 value)
        {
            this->value = value;
        }

        bool operator< (Test const &other) const
        {
            return value < other.value;
        }
};

int main()
{
    std::set<Test, std::less<Test>, MemPool<Test>> theSet;

    theSet.emplace(3);
    theSet.emplace(1);
    theSet.emplace(4);
    theSet.emplace(2);
    theSet.emplace(5);

    for (auto &data : theSet)
    {
        std::cout << data.getValue() << std::endl;
    }

    return 0;
}
