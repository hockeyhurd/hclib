#include <hclib/Types.h>

#include <array>
#include <utility>
#include <vector>
#include <gtest/gtest.h>

using namespace hclib;

class ToLowerTestParameterized : public ::testing::TestWithParam<std::pair<char, char>>
{
public:
};

class ToUpperTestParameterized : public ::testing::TestWithParam<std::pair<char, char>>
{
public:
};

class MinTestParameterized : public ::testing::TestWithParam<std::pair<s32, s32>>
{
public:
};

class MaxTestParameterized : public ::testing::TestWithParam<std::pair<s32, s32>>
{
public:
};

class ParseTestParameterized : public ::testing::TestWithParam<std::pair<std::string, f32>>
{
public:
};

// pair represents: (s32) capacity, (s32) value
class FillTestParameterized : public ::testing::TestWithParam<std::pair<s32, s32>>
{
public:
};

class FloatRoundTwoPlacesTestParameterized : public ::testing::TestWithParam<std::pair<f32, f32>>
{
public:
};

class DoubleRoundTwoPlacesTestParameterized : public ::testing::TestWithParam<std::pair<f64, f64>>
{
public:
};

TEST_P(ToLowerTestParameterized, toLowerTestCases)
{
    auto pair = GetParam();
    ASSERT_EQ(toLower(pair.first), pair.second);
}

TEST_P(ToUpperTestParameterized, toUpperTestCases)
{
    auto pair = GetParam();
    ASSERT_EQ(toUpper(pair.first), pair.second);
}

TEST_P(MinTestParameterized, minTestCases)
{
    const auto pair = GetParam();
    const auto result = pair.first <= pair.second ? pair.first : pair.second;

    ASSERT_EQ(result, min(pair.first, pair.second));
}

TEST_P(MaxTestParameterized, maxTestCases)
{
    const auto pair = GetParam();
    const auto result = pair.first >= pair.second ? pair.first : pair.second;

    ASSERT_EQ(result, max(pair.first, pair.second));
}

TEST(BlockCopyTest, blockCopyTest)
{
    const s32 len = 0x10;
    char arr[len];

    for (s32 i = 0; i < len; ++i)
    {
        arr[i] = 1;
        ASSERT_EQ(arr[i], 1);
    }

    blockCopy(arr, len, 2);

    for (s32 i = 0; i < len; ++i)
    {
        ASSERT_EQ(arr[i], 2);
    }
}

TEST_P(ParseTestParameterized, parseTestCases)
{
    const auto pair = GetParam();

    using ResultType = decltype(pair.second);
    ResultType parsedResult;

    const bool wasParsedCorrectly = parse<ResultType, decltype(pair.first)>(pair.first, parsedResult);

    ASSERT_TRUE(wasParsedCorrectly);
    ASSERT_EQ(parsedResult, pair.second);
}

TEST_P(FillTestParameterized, fillTestCases)
{
    const auto pair = GetParam();
    const s32 capacity = pair.first;
    const s32 value = pair.second;
    std::vector<s32> expectedVec;
    expectedVec.reserve(capacity);

    for (auto i = 0; i < capacity; ++i)
        expectedVec.push_back(value);

    ASSERT_TRUE(!expectedVec.empty());
    
    std::vector<s32> vec(capacity);
    hclib::fill(vec.begin(), vec.end(), value);
    
    ASSERT_TRUE(!vec.empty());
    ASSERT_EQ(vec.size(), expectedVec.size());

    for (std::size_t i = 0; i < expectedVec.size(); ++i)
    {
        ASSERT_EQ(expectedVec[i], vec[i]);
    }
}

TEST(ForEachTest, forEachTest)
{
    const s32 len = 0x10;
    std::array<s32, len> arr;
    std::array<s32, len> arrDoubled;

    for (s32 i = 0; i < len; ++i)
    {
        arr[i] = i;
        arrDoubled[i] = i * 2;
    }

    static const auto doubleFunc = [](s32 &value) { value *= 2; };
    forEach(arr.begin(), arr.end(), doubleFunc);
    // std::for_each(arr.begin(), arr.end(), doubleFunc);

    for (s32 i = 0; i < len; ++i)
    {
        ASSERT_EQ(arr[i], arrDoubled[i]);
    }
}

TEST(PtrSizeTest, ptrSizeTest)
{
#if CPP_VER > 2011
    HCLIB_CONSTEXPR auto calculatedSize = sizeof(void*);
    HCLIB_CONSTEXPR auto result = pointerSize();
#else
    auto calculatedSize = sizeof(void*);
    auto result = pointerSize();
#endif
    ASSERT_EQ(calculatedSize, result);
}

TEST_P(FloatRoundTwoPlacesTestParameterized, floatRoundTwoTestCases)
{
    const f32 eps = 0.001F;
    const auto pair = GetParam();
    ASSERT_NEAR(roundTwoPlaces(pair.first), pair.second, eps);
}

TEST_P(DoubleRoundTwoPlacesTestParameterized, doubleRoundTwoTestCases)
{
    const f64 eps = 0.001;
    const auto pair = GetParam();
    ASSERT_NEAR(roundTwoPlaces(pair.first), pair.second, eps);
}

INSTANTIATE_TEST_SUITE_P(
        ToLowerTests,
        ToLowerTestParameterized,
        ::testing::Values(
            std::pair<char, char>('3', '3'),
            std::pair<char, char>('c', 'c'),
            std::pair<char, char>('C', 'c'),
            std::pair<char, char>('!', '!'),
            std::pair<char, char>('3', '3')
        ));

INSTANTIATE_TEST_SUITE_P(
        ToUpperTests,
        ToUpperTestParameterized,
        ::testing::Values(
            std::pair<char, char>('3', '3'),
            std::pair<char, char>('c', 'C'),
            std::pair<char, char>('C', 'C'),
            std::pair<char, char>('!', '!'),
            std::pair<char, char>('3', '3')
        ));

INSTANTIATE_TEST_SUITE_P(
        MinTests,
        MinTestParameterized,
        ::testing::Values(
            std::pair<s32, s32>(3, 5),
            std::pair<s32, s32>(-3, 5),
            std::pair<s32, s32>(3, -5),
            std::pair<s32, s32>(-3, -5)
        ));

INSTANTIATE_TEST_SUITE_P(
        MaxTests,
        MaxTestParameterized,
        ::testing::Values(
            std::pair<s32, s32>(3, 5),
            std::pair<s32, s32>(-3, 5),
            std::pair<s32, s32>(3, -5),
            std::pair<s32, s32>(-3, -5)
        ));

INSTANTIATE_TEST_SUITE_P(
        ParseTests,
        ParseTestParameterized,
        ::testing::Values(
            std::pair<std::string, f32>("42", 42.0F),
            std::pair<std::string, f32>("-42", -42.0F),
            std::pair<std::string, f32>("3.14", 3.14F),
            std::pair<std::string, f32>("-3.14", -3.14F)
        ));

INSTANTIATE_TEST_SUITE_P(
        FillTests,
        FillTestParameterized,
        ::testing::Values(
            std::pair<s32, s32>(42, 42),
            std::pair<s32, s32>(10, -10),
            std::pair<s32, s32>(1000, 1000),
            std::pair<s32, s32>(1, 123)
        ));

INSTANTIATE_TEST_SUITE_P(
        FloatRoundTwoPlacesTests,
        FloatRoundTwoPlacesTestParameterized,
        ::testing::Values(
            std::pair<f32, f32>(f32_PI, 3.14F),
            std::pair<f32, f32>(-f32_PI, -3.14F)
        ));

INSTANTIATE_TEST_SUITE_P(
        DoubleRoundTwoPlacesTests,
        DoubleRoundTwoPlacesTestParameterized,
        ::testing::Values(
            std::pair<f64, f64>(f64_PI, 3.14),
            std::pair<f64, f64>(-f64_PI, -3.14)
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

