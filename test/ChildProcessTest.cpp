#include <hclib/ChildProcess.h>
#include <hclib/Debug.h>
#include <hclib/Types.h>

#include <gtest/gtest.h>

using namespace hclib;

#include <iostream>

TEST(ChildProcessTest, testProcess)
{
    ChildProcess proc;

    const char *argv[] = { "sleep", "2", nullptr };
    // const char *argv[] = { "sleep", "5", "" };
    s32 status = proc.create("/bin/sleep", const_cast<char**>(argv));
    ASSERT_GT(status, 0);

    EnumProcessState procStatus = proc.status();
    Debug::info("proc status:");
    Debug::info(procStatus.name());
    std::cout << "proc status:" << std::endl;
    std::cout << procStatus.name() << std::endl;

    // I would prefer if we could ASSERT_EQ '0' but in a release
    // build, the process may not be completed and therefore returns,
    // a value >= '0'.
    ASSERT_TRUE(procStatus == EnumProcessState::RUNNING || procStatus == EnumProcessState::STOPPED);

    status = proc.wait();
    ASSERT_EQ(status, 0);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
