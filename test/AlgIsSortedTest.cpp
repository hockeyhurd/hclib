#include <hclib/Types.h>
#include <hclib/Algorithm.h>
#include <hclib/Math.h>

#include <functional>
#include <list>
#include <vector>

#include <gtest/gtest.h>

using namespace hclib;
using namespace hclib::algorithm;

class AlgIsIntVecSortedLessTestParameterized : public ::testing::TestWithParam<std::vector<s32>>
{
public:
};

class AlgIsIntVecSortedGreaterTestParameterized : public ::testing::TestWithParam<std::vector<s32>>
{
public:
};

class AlgIsFloatListSortedLessTestParameterized : public ::testing::TestWithParam<std::list<f32>>
{
public:
};

class AlgIsFloatListSortedGreaterTestParameterized : public ::testing::TestWithParam<std::list<f32>>
{
public:
};

TEST_P(AlgIsIntVecSortedLessTestParameterized, intVecLessTestCases)
{
    const auto &vec = GetParam();
    ASSERT_GT(vec.size(), 1);
    ASSERT_TRUE(isSorted(vec.cbegin(), vec.cend(), std::less_equal<s32>()));
}

TEST_P(AlgIsIntVecSortedGreaterTestParameterized, intVecGreaterTestCases)
{
    const auto &vec = GetParam();
    ASSERT_GT(vec.size(), 1);
    ASSERT_TRUE(isSorted(vec.cbegin(), vec.cend(), std::greater_equal<s32>()));
}

TEST_P(AlgIsFloatListSortedLessTestParameterized, floatListLessTestCases)
{
    const auto &list = GetParam();
    ASSERT_GT(list.size(), 1);
    ASSERT_TRUE(isSorted(list.cbegin(), list.cend(), std::less_equal<f32>()));
}

TEST_P(AlgIsFloatListSortedGreaterTestParameterized, floatListGreaterTestCases)
{
    const auto &list = GetParam();
    ASSERT_GT(list.size(), 1);
    ASSERT_TRUE(isSorted(list.cbegin(), list.cend(), std::greater_equal<f32>()));
}

INSTANTIATE_TEST_SUITE_P(
        AlgIsSortedTests,
        AlgIsIntVecSortedLessTestParameterized,
        ::testing::Values(
            std::vector<s32> { 1, 2, 3, 4, 5 },
            std::vector<s32> { -5, -4, -3, -2, -1 },
            std::vector<s32> { 1, 1, 1, 1, 1 }
        ));

INSTANTIATE_TEST_SUITE_P(
        AlgIsSortedTests,
        AlgIsIntVecSortedGreaterTestParameterized,
        ::testing::Values(
            std::vector<s32> { 5, 4, 3, 2, 1 },
            std::vector<s32> { -1, -2, -3, -4, -5 },
            std::vector<s32> { 1, 1, 1, 1, 1 }
        ));

INSTANTIATE_TEST_SUITE_P(
        AlgIsSortedTests,
        AlgIsFloatListSortedLessTestParameterized,
        ::testing::Values(
            std::list<f32> { 1.0F, 2.0F, 3.0F, 4.0F, 5.0F },
            std::list<f32> { -5.0F, -4.0F, -3.0F, -2.0F, -1.0F },
            std::list<f32> { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F }
        ));

INSTANTIATE_TEST_SUITE_P(
        AlgIsSortedTests,
        AlgIsFloatListSortedGreaterTestParameterized,
        ::testing::Values(
            std::list<f32> { 5.0F, 4.0F, 3.0F, 2.0F, 1.0F },
            std::list<f32> { -1.0F, -2.0F, -3.0F, -4.0F, -5.0F },
            std::list<f32> { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F }
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
