#include <hclib/BitField.h>

#include <gtest/gtest.h>

using namespace hclib;

static bool check(const BitField &field, const u32 value)
{
    return field.getData() == value;
}

TEST(BitFieldTest, BitFieldTestGeneric)
{
    BitField bitField;

    ASSERT_TRUE(check(bitField, 0));
    bitField.add(true);
    ASSERT_TRUE(check(bitField, 1));
    bitField.add(true);
    ASSERT_TRUE(check(bitField, 3));
    bitField.add(true);
    ASSERT_TRUE(check(bitField, 7));
    bitField.add(true);

    bitField.zero();
    ASSERT_TRUE(check(bitField, 0));
    bitField.set(4, true);
    ASSERT_TRUE(check(bitField, 1 << 4));
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
