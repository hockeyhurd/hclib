#include <hclib/Types.h>
#include <hclib/Math.h>

#include <gtest/gtest.h>

using namespace hclib;

template<class T>
struct MathTestVec3
{
    T x;
    T y;
    T z;

    explicit MathTestVec3(const T x, const T y, const T z) : x(x), y(y), z(z) {}
};

/*
 * TEST_P(InterfaceTestParameterized, testCases)
 * {
 *     auto& vec = GetParam();
 *     const auto myResult = max(vec.x, vec.y, vec.z);
 *     const auto stdResult = std::max(vec.x, std::max(vec.y, vec.z));
 *     ASSERT_EQ(myResult, stdResult);
 * }
 * 
 * INSTANTIATE_TEST_CASE_P(
 *         InterfaceTests,
 *         InterfaceTestParameterized,
 *         ::testing::Values(
 *             Vec3(1, 2, 3),
 *             Vec3(6, 5, 4),
 *             Vec3(2, 8, 2)
 *         ));
 */

class AbsTestParameterized : public ::testing::TestWithParam<s32>
{
public:
};

class ClampTestParameterized : public ::testing::TestWithParam<MathTestVec3<s32>>
{
public:
};

class DistanceTestParameterized : public ::testing::TestWithParam<MathTestVec3<f32>>
{
public:
};

class ApproxTestParameterized : public ::testing::TestWithParam<MathTestVec3<f32>>
{
public:
};

TEST_P(AbsTestParameterized, absTestCases)
{
    const auto &value = GetParam();
    const auto result = math::abs(value);

    if (value < 0)
        ASSERT_EQ(result, -value);
    else
        ASSERT_EQ(result, value);
}

TEST_P(ClampTestParameterized, clampTestCases)
{
    MathTestVec3<s32> &value = const_cast<MathTestVec3<s32>&>(GetParam());
    ASSERT_NE(value.x, value.y);

    const auto &result = math::clamp<s32>(value.x, value.y, value.z);

    ASSERT_LE(value.x, result);
    ASSERT_LE(result, value.y);
}

TEST_P(DistanceTestParameterized, distanceTestCases)
{
    const f32 eps = 0.00001F;
    MathTestVec3<f32> &value = const_cast<MathTestVec3<f32>&>(GetParam());

    const auto result = math::distance(value.x, value.y);
    ASSERT_NEAR(result, value.z, eps);
}

TEST_P(ApproxTestParameterized, approxTestCases)
{
    // const f32 eps = 0.00001F;
    MathTestVec3<f32> &value = const_cast<MathTestVec3<f32>&>(GetParam());

    ASSERT_NEAR(value.x, value.y, value.z);
}

INSTANTIATE_TEST_SUITE_P(
        MathAbsTests,
        AbsTestParameterized,
        ::testing::Values(
            3,
            -3,
            0,
            -0,
            100,
            -100
        ));

INSTANTIATE_TEST_SUITE_P(
        MathClampTests,
        ClampTestParameterized,
        ::testing::Values(
            MathTestVec3<s32>(-10, 10, -100),
            MathTestVec3<s32>(-10, 10, -10),
            MathTestVec3<s32>(-10, 10, 0),
            MathTestVec3<s32>(-10, 10, 10),
            MathTestVec3<s32>(-10, 10, 100),
            MathTestVec3<s32>(0, 10, -100),
            MathTestVec3<s32>(0, 10, -10),
            MathTestVec3<s32>(0, 10, 0),
            MathTestVec3<s32>(0, 10, 10),
            MathTestVec3<s32>(0, 10, 100),
            MathTestVec3<s32>(-10, 0, -100),
            MathTestVec3<s32>(-10, 0, -10),
            MathTestVec3<s32>(-10, 0, 0),
            MathTestVec3<s32>(-10, 0, 10),
            MathTestVec3<s32>(-10, 0, 100)
        ));

INSTANTIATE_TEST_SUITE_P(
        MathDistanceTests,
        DistanceTestParameterized,
        ::testing::Values(
            MathTestVec3<f32>(3.0F, 4.0F, 5.0F),
            MathTestVec3<f32>(5.0F, 12.0F, 13.0F),
            MathTestVec3<f32>(7.0F, 24.0F, 25.0F),
            MathTestVec3<f32>(8.0F, 15.0F, 17.0F),
            MathTestVec3<f32>(9.0F, 40.0F, 41.0F),
            MathTestVec3<f32>(11.0F, 60.0F, 61.0F),
            MathTestVec3<f32>(-3.0F, 4.0F, 5.0F),
            MathTestVec3<f32>(-5.0F, 12.0F, 13.0F),
            MathTestVec3<f32>(-7.0F, 24.0F, 25.0F),
            MathTestVec3<f32>(-8.0F, 15.0F, 17.0F),
            MathTestVec3<f32>(-9.0F, 40.0F, 41.0F),
            MathTestVec3<f32>(-11.0F, 60.0F, 61.0F),
            MathTestVec3<f32>(3.0F, -4.0F, 5.0F),
            MathTestVec3<f32>(5.0F, -12.0F, 13.0F),
            MathTestVec3<f32>(7.0F, -24.0F, 25.0F),
            MathTestVec3<f32>(8.0F, -15.0F, 17.0F),
            MathTestVec3<f32>(9.0F, -40.0F, 41.0F),
            MathTestVec3<f32>(11.0F, -60.0F, 61.0F),
            MathTestVec3<f32>(-3.0F, -4.0F, 5.0F),
            MathTestVec3<f32>(-5.0F, -12.0F, 13.0F),
            MathTestVec3<f32>(-7.0F, -24.0F, 25.0F),
            MathTestVec3<f32>(-8.0F, -15.0F, 17.0F),
            MathTestVec3<f32>(-9.0F, -40.0F, 41.0F),
            MathTestVec3<f32>(-11.0F, -60.0F, 61.0F)
        ));

INSTANTIATE_TEST_SUITE_P(
        MathApproxTests,
        ApproxTestParameterized,
        ::testing::Values(
            MathTestVec3<f32>(1.0F, 1.01F, 0.01F),
            MathTestVec3<f32>(0.0001F, 0.01F, 0.01F)
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

