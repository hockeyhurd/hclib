#include <hclib/Types.h>
#include <hclib/String.h>
#include <hclib/StringBuilder.h>

#include <string>
#include <gtest/gtest.h>

using namespace hclib;

static const std::string hello = "Hello, world!";
static const std::string longString = "########################################################################################################################";

// Small string begins here

TEST(StringTest, SmallStringDefaultConstructor)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), "");
}

TEST(StringTest, SmallStringConstructorEmptyString)
{
    String str("");
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), "");
}

TEST(StringTest, SmallStringConstructorHelloWorldString)
{
    String str("Hello, world!");
    ASSERT_EQ(str.size(), 13);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), "Hello, world!");
}

TEST(StringTest, SmallStringCopyConstructor)
{
    String str0("Hello, world!");
    String str1(str0);
    ASSERT_EQ(str0.size(), str1.size());
    ASSERT_FALSE(str0.isEmpty());
    ASSERT_FALSE(str1.isEmpty());
}

TEST(StringTest, SmallStringCopyConstructorStdString)
{
    String str(hello);
    ASSERT_EQ(str.size(), hello.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringMoveConstructor)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());

    String str2(std::move(str));
    ASSERT_EQ(str2.size(), 0);
    ASSERT_TRUE(str2.isEmpty());
}

TEST(StringTest, SmallStringMoveConstructorStdString)
{
    std::string temp = hello;
    String str(std::move(temp));
    ASSERT_EQ(str.size(), hello.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringOperatorEqualsCopyOfString)
{
    String temp(hello);
    const auto tempSize = temp.size();

    String str;
    str = temp;
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringOperatorEqualsMoveFromString)
{
    String temp(hello);
    const auto tempSize = temp.size();

    String str;
    str = std::move(temp);
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringOperatorEqualsCopyOfCString)
{
    const char *cstr = "Hello, world!";
    const auto cstrSize = strlen(cstr);

    String str;
    str = cstr;
    ASSERT_EQ(str.size(), cstrSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), cstr);
}

TEST(StringTest, SmallStringOperatorEqualsCopyOfStdString)
{
    std::string temp = hello;
    const auto tempSize = temp.size();

    String str;
    str = temp;
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringOperatorEqualsMoveFromStdString)
{
    std::string temp = hello;
    const auto tempSize = temp.size();

    String str;
    str = std::move(temp);
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

TEST(StringTest, SmallStringAppendOtherString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    const auto oldSize = str.size();

    String str2(hello);
    ASSERT_EQ(str2.size(), hello.size());
    ASSERT_FALSE(str2.isEmpty());

    str += str2;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_EQ(str.size(), oldSize + str2.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), str2.c_str());
}

TEST(StringTest, SmallStringAppendCString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    const auto oldSize = str.size();

    const char *cstr = "Hello, world!";
    const auto cstrSize = strlen(cstr);

    str += cstr;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_EQ(str.size(), oldSize + cstrSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), cstr);
}

TEST(StringTest, SmallStringAppendSTDString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());

    str += hello;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), hello.c_str());
}

// Small string ends here

// Normal string begins here

TEST(StringTest, StringConstructorLongString)
{
    String str("########################################################################################################################");
    ASSERT_EQ(str.size(), 120);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), "########################################################################################################################");
}

TEST(StringTest, StringCopyConstructor)
{
    String str0("########################################################################################################################");
    String str1(str0);
    ASSERT_EQ(str0.size(), str1.size());
    ASSERT_FALSE(str0.isEmpty());
    ASSERT_FALSE(str1.isEmpty());
}

TEST(StringTest, StringCopyConstructorStdString)
{
    String str(longString);
    ASSERT_EQ(str.size(), longString.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringMoveConstructor)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());

    String str2(std::move(str));
    ASSERT_EQ(str2.size(), 0);
    ASSERT_TRUE(str2.isEmpty());
}

TEST(StringTest, StringMoveConstructorStdString)
{
    std::string temp = longString;
    String str(std::move(temp));
    ASSERT_EQ(str.size(), longString.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringOperatorEqualsCopyOfString)
{
    String temp(longString);
    const auto tempSize = temp.size();

    String str;
    str = temp;
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringOperatorEqualsMoveFromString)
{
    String temp(longString);
    const auto tempSize = temp.size();

    String str;
    str = std::move(temp);
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringOperatorEqualsCopyOfCString)
{
    const char *cstr = "########################################################################################################################";
    const auto cstrSize = strlen(cstr);

    String str;
    str = cstr;
    ASSERT_EQ(str.size(), cstrSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), cstr);
}

TEST(StringTest, StringOperatorEqualsCopyOfStdString)
{
    std::string temp = longString;
    const auto tempSize = temp.size();

    String str;
    str = temp;
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringOperatorEqualsMoveFromStdString)
{
    std::string temp = longString;
    const auto tempSize = temp.size();

    String str;
    str = std::move(temp);
    ASSERT_EQ(str.size(), tempSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

TEST(StringTest, StringAppendOtherString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    const auto oldSize = str.size();

    String str2(longString);
    ASSERT_EQ(str2.size(), longString.size());
    ASSERT_FALSE(str2.isEmpty());

    str += str2;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_EQ(str.size(), oldSize + str2.size());
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), str2.c_str());
}

TEST(StringTest, StringAppendCString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());
    const auto oldSize = str.size();

    const char *cstr = "########################################################################################################################";
    const auto cstrSize = strlen(cstr);

    str += cstr;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_EQ(str.size(), oldSize + cstrSize);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), cstr);
}

TEST(StringTest, StringAppendSTDString)
{
    String str;
    ASSERT_EQ(str.size(), 0);
    ASSERT_TRUE(str.isEmpty());

    str += longString;
    
    ASSERT_NE(str.size(), 0);
    ASSERT_FALSE(str.isEmpty());
    ASSERT_STREQ(str.c_str(), longString.c_str());
}

// Normal string ends here

TEST(StringBuilderTest, DISABLED_StringBuilderAppendInt32)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    const s32 thisLen = 10;
    for (s32 i = 0; i < thisLen; ++i)
    {
        builder.append(i);
    }

    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == "0123456789");
}

TEST(StringBuilderTest, DISABLED_StringBuilderAppendFloat32)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    const f32 piShort = 3.14592F;
    builder.append(piShort);

    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());

    // NOTE: std::to_string appends a '0' at the end.
    ASSERT_TRUE(builder.toSTDString() == "3.145920");
}

TEST(StringBuilderTest, DISABLED_StringBuilderAppendCopyStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderAppendMoveStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    auto temp = hello;
    builder.append(std::move(temp));
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderAppendCopyStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2.append(builder);
    ASSERT_EQ(builder.size(), builder2.size());
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderAppendMoveStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2.append(std::move(builder));
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderRightShiftOperatorChar)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    for (auto ch : hello)
    {
        builder << ch;
    }
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderRightShiftOperatorCopyStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderRightShiftOperatorMoveStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    auto temp = hello;
    builder << std::move(temp);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderRightShiftOperatorCopyStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2 << builder;
    ASSERT_EQ(builder.size(), builder2.size());
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderRightShiftOperatorMoveStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2 << std::move(builder);
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, DISABLED_StringBuilderSubStringHello)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    const std::string helloSubString = "Hello";
    auto subStrBuilder = builder.substring(helloSubString.size());
    ASSERT_EQ(subStrBuilder.size(), helloSubString.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == helloSubString);
}

TEST(StringBuilderTest, DISABLED_StringBuilderSubStringWorld)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    const std::string worldSubString = "world!";
    auto subStrBuilder = builder.substring(7, builder.size() - 7);
    ASSERT_EQ(subStrBuilder.size(), worldSubString.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == worldSubString);
}

TEST(StringBuilderTest, DISABLED_StringBuilderSubStringHelloWorld)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    auto subStrBuilder = builder.substring(builder.size());
    ASSERT_EQ(subStrBuilder.size(), builder.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == builder.toSTDString());
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
