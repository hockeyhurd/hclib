#include <hclib/MemStats.h>
#include <hclib/Types.h>

#include "Common.h"

using namespace hclib;

s32 main()
{
    MemStats &memStats = MemStats::instance();

    auto current = memStats.currentUsage();
    auto total = memStats.totalAllocated();
    expectTrue(current == 0);
    expectTrue(total == 0);

    s32 *ptr = new s32(42);
    current = memStats.currentUsage();
    total = memStats.totalAllocated();

    expectFalse(current == 0);
    expectFalse(total == 0);
    expectTrue(current == sizeof(s32));
    expectTrue(total == sizeof(s32));

    delete ptr;
    current = memStats.currentUsage();
    total = memStats.totalAllocated();
    expectTrue(current == 0);
    expectTrue(total == sizeof(s32));

    char *str = new char[100];
    current = memStats.currentUsage();
    total = memStats.totalAllocated();
    expectTrue(current == 100);
    expectTrue(total == 104);
    
    char *str2 = new char[100];
    current = memStats.currentUsage();
    total = memStats.totalAllocated();
    expectTrue(current == 200);
    expectTrue(total == 204);

    delete[] str;
    current = memStats.currentUsage();
    total = memStats.totalAllocated();
    expectTrue(current == 100);
    expectTrue(total == 204);
    
    delete[] str2;
    current = memStats.currentUsage();
    total = memStats.totalAllocated();
    expectTrue(current == 0);
    expectTrue(total == 204);

    return 0;
}
