#include <hclib/Types.h>
#include <hclib/Algorithm.h>

#include <string>
#include <utility>

#include <gtest/gtest.h>

using namespace hclib;
using namespace hclib::algorithm;

class BoyerMooreTestParameterized : public ::testing::TestWithParam<std::pair<std::string, std::string>>
{
public:
};

TEST_P(BoyerMooreTestParameterized, boyerMooreSearchTestCases)
{
    const auto &pair = GetParam();
    auto &text = pair.first;
    auto &pattern = pair.second;

    auto begin = text.begin();
    auto end = text.end();

    BoyerMoore bm(pattern.begin(), pattern.end());

    auto result = stringSearch(begin, end, bm);

    ASSERT_FALSE(result == end);

    for (const auto ch : pattern)
    {
        ASSERT_EQ(ch, *result);
        ++result;
    }
}

INSTANTIATE_TEST_SUITE_P(
        BoyerMooreSearcherTests,
        BoyerMooreTestParameterized,
        ::testing::Values(
            std::pair("ABCABDAB", "ABDAB"),
            std::pair("aababcabcdabcdeabcdef", "abcdef")
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

