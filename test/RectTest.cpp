#include <hclib/Rect.h>
#include <hclib/Types.h>

#include <gtest/gtest.h>

using namespace hclib;

TEST(RectTest, RectTestGeneric)
{
    Rect<s32> base(0, 0, 100, 100);

    ASSERT_TRUE(base == base);
    ASSERT_FALSE(base != base);
    ASSERT_TRUE(base == Rect<s32>(0, 0, 100, 100));
    ASSERT_FALSE(base != Rect<s32>(0, 0, 100, 100));
    ASSERT_TRUE(base != Rect<s32>(1, 1, 100, 100));
    ASSERT_FALSE(base == Rect<s32>(1, 1, 100, 100));
    ASSERT_TRUE(base.center() == Vec2<s32>(50, 50));
    ASSERT_TRUE(base.getX() == 0);
    ASSERT_TRUE(base.getY() == 0);
    ASSERT_TRUE(base.getWidth() == 100);
    ASSERT_TRUE(base.getHeight() == 100);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
