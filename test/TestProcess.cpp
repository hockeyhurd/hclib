#include <hclib/Debug.h>
#include <hclib/StringBuilder.h>
#include <hclib/Types.h>
#include <hclib/Timer.h>

#include <chrono>
#include <string>
#include <thread>

using namespace hclib;

s32 main()
{
    using namespace std::chrono_literals;

    Timer<std::chrono::high_resolution_clock> timer;
    std::this_thread::sleep_for(5s);
    // std::cout << "Slept for " << timer.seconds() << " seconds.\n";
    StringBuilder builder;
    builder.append("Slept for ");
    builder.append(std::to_string(timer.seconds()));
    builder.append(" seconds.");
    Debug::info(builder.c_str());

    return 0;
}

