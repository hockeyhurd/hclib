#include <hclib/Types.h>
#include <hclib/Optional.h>

#include <gtest/gtest.h>

#include <string>
#include <vector>

using namespace hclib;

TEST(OptionalTest, OptionalTestConstructValid)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);
}

TEST(OptionalTest, OptionalTestConstructValidByOperatorBool)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt);
    ASSERT_EQ(*opt, value);
}

TEST(OptionalTest, OptionalTestConstructValidThenReset)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    opt.reset();
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestConstructInvalid)
{
    Optional<s32> opt;
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestConstructWithNullopt)
{
    Optional<s32> opt(nullopt);
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestConstructWithExplicitNullopt)
{
    auto opt = nullopt;
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestConstructViaMakeOptional)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = make_optional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);
}

TEST(OptionalTest, OptionalTestConstructString)
{
    const std::string str = "Hello, world!";
    auto opt = makeOptional<std::string>(str);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, str);
}

TEST(OptionalTest, OptionalTestConstructVector)
{
    const std::vector<s32> vec = { 1, 2, 3 };
    auto opt = makeOptional<std::vector<s32>>(vec);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(opt->size(), vec.size());

    for (auto iterVec = vec.cbegin(), iterOptVec = opt->cbegin();
         iterVec != vec.cend() && iterOptVec != opt->cend();
         ++iterVec, ++iterOptVec)
    {
        ASSERT_EQ(*iterVec, *iterOptVec);
    }
}

TEST(OptionalTest, OptionalTestCopyConstructInt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = makeOptional<s32>(value);
    auto copy = opt;

    ASSERT_TRUE(opt.hasValue());
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(opt, copy);
    ASSERT_EQ(*opt, value);
    ASSERT_EQ(*opt, *copy);
}

TEST(OptionalTest, OptionalTestCopyConstructString)
{
    const std::string value = "Hello, world!";
    auto opt = makeOptional<std::string>(value);
    auto copy = opt;

    ASSERT_TRUE(opt.hasValue());
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(opt, copy);
    ASSERT_EQ(*opt, value);
    ASSERT_EQ(*opt, *copy);
}

TEST(OptionalTest, OptionalTestCopyConstructIntInvalid)
{
    Optional<s32> opt;
    auto copy = opt;

    ASSERT_FALSE(opt.hasValue());
    ASSERT_FALSE(copy.hasValue());
    ASSERT_EQ(opt, copy);
}

TEST(OptionalTest, OptionalTestMoveConstructInt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = makeOptional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto move = std::move(opt);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value);
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestMoveConstructString)
{
    const std::string value = "Hello, world!";
    auto opt = makeOptional<std::string>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto move = std::move(opt);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value);
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestMoveConstructIntInvalid)
{
    Optional<s32> opt;
    ASSERT_FALSE(opt.hasValue());

    auto move = std::move(opt);
    ASSERT_FALSE(move.hasValue());
}

TEST(OptionalTest, OptionalTestCopyAssignableInt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = makeOptional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto copy = makeOptional<s32>(value + 1);
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(*copy, value + 1);

    copy = opt;
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(opt, copy);
    ASSERT_EQ(*opt, *copy);
}

TEST(OptionalTest, OptionalTestCopyAssignableString)
{
    const std::string value = "Hello, world!";
    const std::string value2 = "Hello again, world!";
    auto opt = makeOptional<std::string>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto copy = makeOptional<std::string>(value2);
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(*copy, value2);

    copy = opt;
    ASSERT_TRUE(copy.hasValue());
    ASSERT_EQ(opt, copy);
    ASSERT_EQ(*opt, *copy);
}

TEST(OptionalTest, OptionalTestCopyAssignableIntInvalid)
{
    Optional<s32> opt;
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> copy;
    ASSERT_FALSE(copy.hasValue());
    ASSERT_EQ(opt, copy);

    copy = opt;
    ASSERT_FALSE(copy.hasValue());
    ASSERT_EQ(opt, copy);
}

TEST(OptionalTest, OptionalTestMoveAssignableInt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = makeOptional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto move = makeOptional<s32>(value + 1);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value + 1);

    move = std::move(opt);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value);
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestMoveAssignableString)
{
    const std::string value = "Hello, world!";
    const std::string value2 = "Hello again, world!";
    auto opt = makeOptional<std::string>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto move = makeOptional<std::string>(value2);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value2);

    move = std::move(opt);
    ASSERT_TRUE(move.hasValue());
    ASSERT_EQ(*move, value);
    ASSERT_FALSE(opt.hasValue());
}

TEST(OptionalTest, OptionalTestMoveAssignableIntInvalid)
{
    Optional<s32> opt;
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> move;
    ASSERT_FALSE(move.hasValue());
    ASSERT_EQ(opt, move);

    move = std::move(opt);
    ASSERT_FALSE(move.hasValue());
    ASSERT_EQ(opt, move);
}

TEST(OptionalTest, OptionalTestEquality)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = make_optional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = make_optional<s32>(value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);

    ASSERT_EQ(opt, opt2);
}

TEST(OptionalTest, OptionalTestInEquality)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = make_optional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    HCLIB_CONSTEXPR s32 value2 = value + 1;
    auto opt2 = make_optional<s32>(value2);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value2);

    ASSERT_NE(opt, opt2);
}

TEST(OptionalTest, OptionalTestSwapInt)
{
    const s32 value = 42;
    const s32 value2 = 43;
    auto opt = makeOptional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto swapper = makeOptional<s32>(value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value2);

    swapper.swap(opt);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value);
}

TEST(OptionalTest, OptionalTestSwapString)
{
    const std::string value = "Hello, world!";
    const std::string value2 = "Hello again, world!";
    auto opt = makeOptional<std::string>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto swapper = makeOptional<std::string>(value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value2);

    swapper.swap(opt);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value);
}

TEST(OptionalTest, OptionalTestSTDSwapInt)
{
    const s32 value = 42;
    const s32 value2 = 43;
    auto opt = makeOptional<s32>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto swapper = makeOptional<s32>(value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value2);

    std::swap(opt, swapper);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value);
}

TEST(OptionalTest, OptionalTestSTDSwapString)
{
    const std::string value = "Hello, world!";
    const std::string value2 = "Hello again, world!";
    auto opt = makeOptional<std::string>(value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto swapper = makeOptional<std::string>(value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value2);

    std::swap(opt, swapper);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value2);
    ASSERT_TRUE(swapper.hasValue());
    ASSERT_EQ(*swapper, value);
}

TEST(OptionalTest, OptionalTestLessThan)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = makeOptional<s32>(value + 1);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorIntLessThanNullopt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    Optional<s32> opt2(nullopt);
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorIntLessThanNullopt2)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(nullopt);
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestIntLessThanNullopt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = nullopt;
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestIntLessThanNullopt2)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = nullopt;
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestIntGreaterThan)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = makeOptional<s32>(value - 1);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(value - 1, opt2.value());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorIntGreaterThanNullopt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(opt.value(), value);

    Optional<s32> opt2(nullopt);
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorIntGreaterThanNullopt2)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(nullopt);
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestIntGreaterThanNullopt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(value, opt.value());

    auto opt2 = nullopt;
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestIntGreaterThanNullopt2)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = nullopt;
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorStringLessThanNullopt)
{
    const std::string value = "Hello, world!";
    Optional<std::string> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    Optional<std::string> opt2(nullopt);
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorStringLessThanNullopt2)
{
    const std::string value = "Hello, world!";
    Optional<std::string> opt(nullopt);
    ASSERT_FALSE(opt.hasValue());

    Optional<std::string> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestStringLessThanNullopt)
{
    const std::string value = "Hello, world!";
    Optional<std::string> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = nullopt;
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestStringLessThanNullopt2)
{
    const std::string value = "Hello, world!";
    auto opt = nullopt;
    ASSERT_FALSE(opt.hasValue());

    Optional<std::string> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestStringGreaterThan)
{
    const std::string value = "Hello, world!2";
    const std::string value2 = "Hello, world!";
    Optional<std::string> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(*opt, value);

    auto opt2 = makeOptional<std::string>(value2);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(value2, opt2.value());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorStringGreaterThanNullopt)
{
    const std::string value = "Hello, world!";
    Optional<std::string> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(opt.value(), value);

    Optional<std::string> opt2(nullopt);
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestConstructorStringGreaterThanNullopt2)
{
    const std::string value = "Hello, world!";
    Optional<std::string> opt(nullopt);
    ASSERT_FALSE(opt.hasValue());

    Optional<std::string> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestStringGreaterThanNullopt)
{
    HCLIB_CONSTEXPR s32 value = 42;
    Optional<s32> opt(in_place, value);
    ASSERT_TRUE(opt.hasValue());
    ASSERT_EQ(value, opt.value());

    auto opt2 = nullopt;
    ASSERT_FALSE(opt2.hasValue());
    ASSERT_FALSE(opt < opt2);
    ASSERT_FALSE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_TRUE(opt > opt2);
    ASSERT_TRUE(opt >= opt2);
}

TEST(OptionalTest, OptionalTestStringGreaterThanNullopt2)
{
    HCLIB_CONSTEXPR s32 value = 42;
    auto opt = nullopt;
    ASSERT_FALSE(opt.hasValue());

    Optional<s32> opt2(in_place, value);
    ASSERT_TRUE(opt2.hasValue());
    ASSERT_EQ(*opt2, value);
    ASSERT_TRUE(opt < opt2);
    ASSERT_TRUE(opt <= opt2);
    ASSERT_FALSE(opt == opt2);
    ASSERT_TRUE(opt != opt2);
    ASSERT_FALSE(opt > opt2);
    ASSERT_FALSE(opt >= opt2);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
