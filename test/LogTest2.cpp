#include <hclib/FileWriter.h>

using namespace hclib;

int main() {
    FileWriter writer("fw.txt");

    writer.log("Hello, world!");
    writer.endl();

    // writer.endl() << "Hello, world2!" << writer.flush();
    writer.endl();
    writer << "Hello, world2!";
    writer.flush();
    writer.endl();

    return 0;
}
