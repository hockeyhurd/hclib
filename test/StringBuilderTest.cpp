#include <hclib/Types.h>
#include <hclib/StringBuilder.h>

#include <algorithm>
#include <string>
#include <gtest/gtest.h>

using namespace hclib;

static const std::string hello = "Hello, world!";

TEST(StringBuilderTest, StringBuilderConstructor)
{
    StringBuilder builder(0x20);
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());
}

TEST(StringBuilderTest, StringBuilderCopyConstructor)
{
    StringBuilder builder(0x20);
    StringBuilder builder2(builder);
    ASSERT_EQ(builder.size(), builder2.size());
    ASSERT_TRUE(builder.isEmpty());
    ASSERT_TRUE(builder2.isEmpty());
}

TEST(StringBuilderTest, StringBuilderCopyConstructorStdString)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderMoveConstructor)
{
    StringBuilder builder(0x20);
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    StringBuilder builder2(std::move(builder));
    ASSERT_EQ(builder2.size(), 0);
    ASSERT_TRUE(builder2.isEmpty());
}

TEST(StringBuilderTest, StringBuilderMoveConstructorStdString)
{
    std::string temp = hello;
    StringBuilder builder(std::move(temp));
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderAppendChar)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    for (auto ch : hello)
    {
        builder.append(ch);
    }
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderAppendInt32)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    const s32 thisLen = 10;
    for (s32 i = 0; i < thisLen; ++i)
    {
        builder.append(i);
    }

    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == "0123456789");
}

TEST(StringBuilderTest, StringBuilderAppendFloat32)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    const f32 piShort = 3.14592F;
    builder.append(piShort);

    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());

    // NOTE: std::to_string appends a '0' at the end.
    ASSERT_TRUE(builder.toSTDString() == "3.145920");
}

TEST(StringBuilderTest, StringBuilderAppendCopyStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderAppendMoveStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    auto temp = hello;
    builder.append(std::move(temp));
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderAppendCopyStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2.append(builder);
    ASSERT_EQ(builder.size(), builder2.size());
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderAppendMoveStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder.append(hello);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2.append(std::move(builder));
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderRightShiftOperatorChar)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    for (auto ch : hello)
    {
        builder << ch;
    }
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderRightShiftOperatorCopyStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderRightShiftOperatorMoveStdString)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    auto temp = hello;
    builder << std::move(temp);
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderRightShiftOperatorCopyStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2 << builder;
    ASSERT_EQ(builder.size(), builder2.size());
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderRightShiftOperatorMoveStringBuilder)
{
    StringBuilder builder;
    ASSERT_EQ(builder.size(), 0);
    ASSERT_TRUE(builder.isEmpty());

    builder << hello;
    
    ASSERT_NE(builder.size(), 0);
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    StringBuilder builder2;
    builder2 << std::move(builder);
    ASSERT_NE(builder2.size(), 0);
    ASSERT_FALSE(builder2.isEmpty());
    ASSERT_TRUE(builder2.toSTDString() == hello);
}

TEST(StringBuilderTest, StringBuilderSubStringHello)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    const std::string helloSubString = "Hello";
    auto subStrBuilder = builder.substring(helloSubString.size());
    ASSERT_EQ(subStrBuilder.size(), helloSubString.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == helloSubString);
}

TEST(StringBuilderTest, StringBuilderSubStringWorld)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    const std::string worldSubString = "world!";
    auto subStrBuilder = builder.substring(7, builder.size() - 7);
    ASSERT_EQ(subStrBuilder.size(), worldSubString.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == worldSubString);
}

TEST(StringBuilderTest, StringBuilderSubStringHelloWorld)
{
    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(builder.toSTDString() == hello);

    auto subStrBuilder = builder.substring(builder.size());
    ASSERT_EQ(subStrBuilder.size(), builder.size());
    ASSERT_FALSE(builder.isEmpty());
    ASSERT_TRUE(subStrBuilder.toSTDString() == builder.toSTDString());
}

TEST(StringBuilderTest, StringBuilderReverse)
{
    auto reversedHello = hello;
    std::reverse(reversedHello.begin(), reversedHello.end());

    StringBuilder builder(hello);
    ASSERT_EQ(builder.size(), hello.size());
    ASSERT_FALSE(builder.isEmpty());

    builder.reverse();
    ASSERT_TRUE(builder.toSTDString() == reversedHello);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

