#include <hclib/Mutex.h>
#include <hclib/LockGuard.h>

// #include <iostream>
#include <thread>

#include <gtest/gtest.h>

using namespace hclib;

static Mutex mutex;

#if CPP_17
static SharedMutex sharedMutex;
#endif

static void test(s32 &x, const s32 id)
{
    // std::cout << "Entered: " << id << std::endl;
    UniqueLock<Mutex> lockGuard(mutex);
    // std::cout << "Lock aquired: " << id << std::endl;

    for (s32 i = 0; i < 10; ++i)
    {
        ++x;
    }

    // std::cout << "Unlocking: " << id << ", with x: " << *x << std::endl;
}

#if CPP_17
static void sharedTest(s32 &x, const s32 id)
{
    // std::cout << "Entered: " << id << std::endl;
    UniqueLock<SharedMutex> lockGuard(sharedMutex);
    // std::cout << "Lock aquired: " << id << std::endl;

    for (s32 i = 0; i < 10; ++i)
    {
        ++x;
    }

    // std::cout << "Unlocking: " << id << ", with x: " << *x << std::endl;
}
#endif

TEST(MutexTest, LockTest)
{
    s32 x = 0;

    std::thread t0(test, std::ref(x), 0);
    std::thread t1(test, std::ref(x), 1);

    t0.join();
    t1.join();

    // std::cout << "x: " << x << std::endl;
    ASSERT_EQ(x, 20);
}

#if CPP_17
TEST(MutexTest, SharedLockTest)
{
    s32 x = 0;

    std::thread t0(sharedTest, std::ref(x), 0);
    std::thread t1(sharedTest, std::ref(x), 1);

    t0.join();
    t1.join();

    // std::cout << "x: " << x << std::endl;
    ASSERT_EQ(x, 20);
}
#endif

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
