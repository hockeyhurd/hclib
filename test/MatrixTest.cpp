#include <hclib/Types.h>
#include <hclib/Matrix.h>

#include <gtest/gtest.h>

using namespace hclib;

TEST(MatrixTest, IntMatrixIdentity)
{
    const s32 len = 3;
    auto identity = Matrix<s32, len>::identity(1);

    for (auto i = 0; i < len; ++i)
    {
        ASSERT_EQ(identity[i][i], 1);
    }
}

TEST(MatrixTest, FloatMatrixIdentity)
{
    const s32 len = 3;
    auto identity = Matrix<f32, len>::identity(1.0);

    for (auto i = 0; i < len; ++i)
    {
        const f32 eps = 0.001F;
        ASSERT_NEAR(identity[i][i], 1, eps);
    }
}

// static void test2()
TEST(MatrixTest, FloatMatrixMultiplication)
{
    const f32 eps = 0.001F;
    Matrix<f32, 3> left;
    Matrix<f32, 3> right;

    ASSERT_EQ(left.size(), 3);
    ASSERT_EQ(right.size(), 3);

    // Initializing first array to:
    // [1 2 3]
    // [1 2 3]
    // [1 2 3]
    for (s32 row = 0; row < 3; ++row)
    {
        for (s32 col = 0; col < 3; ++col)
        {
            left[row][col] = static_cast<f32>(col + 1);
            ASSERT_NEAR(left[row][col], (col + 1.0F), eps);
        }
    }

    // Initializing first array to:
    // [1 1 1]
    // [2 2 2]
    // [3 3 3]
    right[0][0] = 1.0F;
    right[0][1] = 1.0F;
    right[0][2] = 1.0F;
    right[1][0] = 2.0F;
    right[1][1] = 2.0F;
    right[1][2] = 2.0F;
    right[2][0] = 3.0F;
    right[2][1] = 3.0F;
    right[2][2] = 3.0F;

    for (s32 row = 0; row < 3; ++row)
    {
        for (s32 col = 0; col < 3; ++col)
        {
            ASSERT_NEAR(right[row][col], row + 1.0F, eps);
        }
    }

    // Expected result should be
    // [14 9 8]
    // [14 9 8]
    // [14 9 8]
    auto result = left.dot(right);
    ASSERT_EQ(result.size(), 3);

    for (s32 row = 0; row < result.size(); ++row)
    {
        for (s32 col = 0; col < result.size(); ++col)
        {
            ASSERT_NEAR(result[row][col], 14.0F, eps);
        }
    }

    // ASSERT_NEAR(result[0][0], 14.0F, eps);
    // ASSERT_NEAR(result[0][1], 9.0F, eps);
    // ASSERT_NEAR(result[0][2], 8.0F, eps);
    // ASSERT_NEAR(result[1][0], 14.0F, eps);
    // ASSERT_NEAR(result[1][1], 9.0F, eps);
    // ASSERT_NEAR(result[1][2], 8.0F, eps);
    // ASSERT_NEAR(result[2][0], 14.0F, eps);
    // ASSERT_NEAR(result[2][1], 9.0F, eps);
    // ASSERT_NEAR(result[2][2], 8.0F, eps);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
