#include <hclib/Types.h>
#include <hclib/Math.h>

#include <string>

#include <gtest/gtest.h>

using namespace hclib;
using namespace hclib::math;

static const f32 eps_f32 = 0.001F;
static const f64 eps_f64 = 0.001;

TEST(MathLogTest, MathLogTest_Int32)
{
    const s32 value = 57;
    const std::string valueStr = std::to_string(value);
    s32 result = nearestPowerOfTwo(value);
    ASSERT_TRUE(result == 6);

    result = ceilPowerOfTwo(value);
    ASSERT_TRUE(result == 6);

    result = floorPowerOfTwo(value);
    ASSERT_TRUE(result == 5);
}

TEST(MathLogTest, MathLogTest_Float32)
{
    const f32 value = 57.0F;
    const std::string valueStr = std::to_string(value);
    f32 result = nearestPowerOfTwo(value);
    ASSERT_NEAR(result, 6.0F, eps_f32);

    result = ceilPowerOfTwo(value);
    ASSERT_NEAR(result, 6.0F, eps_f32);

    result = floorPowerOfTwo(value);
    ASSERT_NEAR(result, 5.0F, eps_f32);
}

TEST(MathLogTest, MathLogTest_Float64)
{
    const f64 value = 57.0;
    const std::string valueStr = std::to_string(value);
    f64 result = nearestPowerOfTwo(value);
    ASSERT_NEAR(result, 6.0, eps_f64);

    result = ceilPowerOfTwo(value);
    ASSERT_NEAR(result, 6.0, eps_f64);

    result = floorPowerOfTwo(value);
    ASSERT_NEAR(result, 5.0, eps_f64);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
