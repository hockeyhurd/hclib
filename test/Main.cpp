/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#include <iostream>
#include <thread>

#include <hclib/Debug.h>
#include <hclib/String.h>
#include <hclib/SmartPtr.h>
#include <hclib/ThreadPool.h>
#include <hclib/ConcurrentVector.h>

using namespace hclib;

s32 main(s32 argc, char **argv) {
    String str = "Hello world!";
    std::cout << str << std::endl;

    str += " again...";
    std::cout << str << std::endl;

    auto ptr = makeUnique<u32>(10);
    std::cout << *ptr << std::endl;

    auto pair = std::make_pair<u32, u32>(10, 32);

    static auto add = [](const u32 left, const u32 right) {
        auto result = left + right;
        return result;
    };

    std::cout << add(pair.first, pair.second) << std::endl;

#if 0
    ThreadPool pool(std::thread::hardware_concurrency() / 2);
    // ThreadPool pool(2);

    static auto add2 = [](const u32 left, const u32 right, u32 &output) {
        output = left + right;
    };

    static auto func = []() {
        u32 output = 0;
        add2(10, 200, output);
        std::cout << output << std::endl;
    };

    static auto func2 = []() {
        u32 output = 0;
        add2(30, 12, output);
        std::cout << output << std::endl;
    };

    pool.run(func);
    pool.run(func2);
#endif

    ConcurrentVector<u32> vec(0x10);

    for (u32 i = 0; i < 0x10; i++) {
        vec.emplace_back(i);
    }

    for (auto &data : vec) {
        std::cout << data << std::endl;
    }

    return 0;
}
