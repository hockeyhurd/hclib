#include <iostream>

#include <hclib/Types.h>
#include <hclib/File.h>

#include "Common.h"

using namespace hclib;

static void writeFile(const std::string &name, const std::string &expectString)
{
    File fileOut(name, "w");
    fileOut.write(expectString);
}

static std::string readFile(const std::string &name, const std::string &expectString)
{
    File file(name, "r");
    return file.readFile();
}

s32 main()
{
    const std::string name = "hello.txt";
    const std::string expectString = "Hello, world!";

    writeFile(name, expectString);

    const auto result = readFile(name, expectString);

    expectTrue(result == expectString);

    return 0;
}
