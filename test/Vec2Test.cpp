#include <hclib/Vec2.h>
#include <hclib/Types.h>

#include <gtest/gtest.h>

using namespace hclib;

/*template<class T>
static void printVec(const Vec2<T> &v)
{
    std::cout << '(' << v.x << ", " << v.y << ")\n";
}*/

TEST(Vec2Test, InitAndRotateTest)
{
    const f64 eps = 0.001;
    Vec2<f64> v0(1.0, 0.0);
    Vec2<f64> origin;

    ASSERT_NEAR(v0.x, 1.0, eps);
    ASSERT_NEAR(v0.y, 0.0, eps);
    ASSERT_NEAR(origin.x, 0.0, eps);
    ASSERT_NEAR(origin.y, 0.0, eps);
    // printVec(v0); 
    v0.rotate(origin, 90 * M_PI / 180.0);
    ASSERT_NEAR(v0.x, 0.0, eps);
    ASSERT_NEAR(v0.y, 1.0, eps);
    // printVec(v0); 
}

TEST(Vec2Test, InitAndAddTest)
{
    Vec2<s32> v0;
    ASSERT_EQ(v0.x, 0);
    ASSERT_EQ(v0.y, 0);

    v0 += Vec2<s32>(1, 1);
    ASSERT_EQ(v0.x, 1);
    ASSERT_EQ(v0.y, 1);
}

TEST(Vec2Test, InitAndSubtractTest)
{
    Vec2<s32> v0(1, 1);
    ASSERT_EQ(v0.x, 1);
    ASSERT_EQ(v0.y, 1);

    v0 -= Vec2<s32>(1, 1);
    ASSERT_EQ(v0.x, 0);
    ASSERT_EQ(v0.y, 0);
}

TEST(Vec2Test, InitAndMultiplyTest)
{
    Vec2<s32> v0(1, 1);
    ASSERT_EQ(v0.x, 1);
    ASSERT_EQ(v0.y, 1);

    v0 *= Vec2<s32>(2, 2);
    ASSERT_EQ(v0.x, 2);
    ASSERT_EQ(v0.y, 2);
}

TEST(Vec2Test, InitAndDivideTest)
{
    Vec2<s32> v0(2, 2);
    ASSERT_EQ(v0.x, 2);
    ASSERT_EQ(v0.y, 2);

    v0 /= Vec2<s32>(2, 2);
    ASSERT_EQ(v0.x, 1);
    ASSERT_EQ(v0.y, 1);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
