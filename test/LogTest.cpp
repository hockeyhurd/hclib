#include <hclib/Types.h>
#include <hclib/Logger.h>

#include <string>
#include <sstream>

#include <gtest/gtest.h>

using namespace hclib;

static const std::string helloWorld = "Hello, world!";
static const std::string helloWorldCmp = "[INFO]: Hello, world!";

#if defined(__clang__) || defined(_MSC_VER) || (defined(__GNUC__) && __GNUC__ >= 5)
static const s32 timestampOffset = 24; // Needed to exclude the timestamp
#else
static const s32 timestampOffset = 0; // No timestamps for older GCC versions
#endif

TEST(LogTest, LogTestSTDostringstream0)
{
    std::ostringstream os;
    Logger logger(os);

    logger << helloWorld;
    ASSERT_EQ(os.str().c_str() + timestampOffset, helloWorldCmp);
}

TEST(LogTest, LogTestSTDostringstream1)
{
    std::ostringstream os;
    Logger logger(os);

    logger.write(logger.getLevel(), helloWorld);
    ASSERT_EQ(os.str().c_str() + timestampOffset, helloWorldCmp);
}

TEST(LogTest, LogTestSTDlogger)
{
    Logger &logger = Logger::stdlogger();
    logger << "\nIf you see this, this test is NOT a false positive!\n\n";
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

