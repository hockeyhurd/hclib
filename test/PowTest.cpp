#include <hclib/Math.h>
#include <hclib/Types.h>

using namespace hclib;

#include <gtest/gtest.h>

TEST(PowTest, PowTest0)
{
    s32 x = 2;
    s32 result = math::pow(x, 3);

    ASSERT_EQ(result, 8);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
