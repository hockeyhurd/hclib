#include <hclib/Types.h>
#include <hclib/Immutable.h>

#include <gtest/gtest.h>

using namespace hclib;

TEST(ImmutableTest, ImmutableTestConstruct)
{
    const s32 value = 42;
    Immutable<s32> immutable(value);

    ASSERT_EQ(value, immutable.getData());
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

