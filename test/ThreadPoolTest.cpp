#include <hclib/Types.h>
#include <hclib/ThreadPool.h>

#include <gtest/gtest.h>

using namespace hclib;

static void sum(const s32 *data, const s32 start, const s32 end, s32 *output)
{
    for (s32 i = start; i < end; ++i)
    {
        *output += data[i];
    }
}

namespace ThreadPoolTests
{

    class Data
    {
    public:

        static const s32 len = 10000;
        s32 arr[len];

        s32 init()
        {
            s32 singleSum = 0;

            for (s32 i = 0; i < len; ++i)
            {
                arr[i] = i;
                singleSum += arr[i];
            }

            return singleSum;
        }

    };

    struct Test
    {
        s32 *data;
        s32 start, end;
        s32 *output;

        static void func(Test *test)
        {
            sum(test->data, test->start, test->end, test->output);
        }
    };
}

TEST(ThreadPoolTest, Sum100)
{
    ThreadPoolTests::Data data;
    const s32 singleSum = data.init();
    const s32 threads = 4;
    s32 sums[threads];

    for (s32 i = 0; i < threads; ++i)
        sums[i] = 0;

    ThreadPoolTests::Test tests[threads];
    ThreadPool pool(threads);

    for (s32 i = 0; i < threads; ++i)
    {
        auto &test = tests[i];
        test.data = data.arr;
        test.start = i * (ThreadPoolTests::Data::len / threads);
        test.end = (i + 1) * (ThreadPoolTests::Data::len / threads);
        test.output = &sums[i];
        pool.queueJob(ThreadPoolTests::Test::func, &tests[i]);
    }

    pool.shutdown(true);

    s32 multiSum = 0;

    for (s32 i = 0; i < threads; ++i)
        multiSum += sums[i];

    // std::cout << "Total: " << singleSum << std::endl;
    // std::cout << "Got: " << multiSum << std::endl;
    ASSERT_EQ(singleSum, multiSum);
}
