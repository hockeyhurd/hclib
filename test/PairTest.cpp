#include <hclib/Types.h>
#include <hclib/Pair.h>

#include <gtest/gtest.h>

using namespace hclib;

TEST(PairTest, PairTestGeneric)
{
    Pair<s32, s32> pair0(42, 43);
    ASSERT_TRUE(pair0.first == 42);
    ASSERT_TRUE(pair0.second == 43);

    const s32 first = 42;

    Pair<s32, s32> pair1(first, 43);
    ASSERT_TRUE(pair1.first == first);
    ASSERT_TRUE(pair1.second == 43);

    const s32 second = 43;

    Pair<s32, s32> pair2(42, second);
    ASSERT_TRUE(pair2.first == 42);
    ASSERT_TRUE(pair2.second == second);

    Pair<s32, s32> pair3(first, second);
    ASSERT_TRUE(pair3.first == first);
    ASSERT_TRUE(pair3.second == second);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
