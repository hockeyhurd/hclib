#include <hclib/Types.h>
#include <hclib/Logger.h>
#include <hclib/Optional.h>
#include <hclib/csv/CSVObject.h>
#include <hclib/csv/CSVReader.h>
#include <hclib/csv/CSVWriter.h>

#include <functional>
#include <vector>
#include <gtest/gtest.h>

using namespace hclib;
using namespace hclib::csv;

static void stringReaderCallback(std::string& str, const std::string& fromStr)
{
    str = fromStr;
}

static void stringWriterCallback(const std::string& str, std::string& toStr)
{
    toStr = str;
}

template<class T>
static Optional<CSVReader<T>> setupReader(const std::string& input, const bool dedicatedRow, std::function<void(std::string&, const std::string&)> readerCallback)
{
    CSVReader<T> csvReader(readerCallback, CSVReader<std::string>::DEFAULT_ROW_RESERVE_SIZE);
    const bool success = csvReader.read(input);

    if (success)
    {
        return hclib::makeOptional<CSVReader<T>>(std::move(csvReader));
    }

    return hclib::nullopt;
}

template<class T>
static Optional<CSVWriter<T>> setupWriter(const std::vector<std::vector<CSVObject<T>>>& rows, std::string& output)
{
    static HCLIB_CONSTEXPR std::size_t bufferSize = 1024;
    CSVWriter<T> csvWriter;
    const bool success = csvWriter.write(rows, output, bufferSize);

    if (success)
    {
        return hclib::makeOptional<CSVWriter<T>>(std::move(csvWriter));
    }

    return hclib::nullopt;
}

TEST(CSVTest, CSVObjectStringCreate)
{
    const std::string value = "test";
    CSVString csvStr(value);
    ASSERT_EQ(value, csvStr.get());
}

TEST(CSVTest, CSVObjectReaderCreate)
{
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>("", dedicatedRow, stringReaderCallback);
    ASSERT_FALSE(optCSVReader.hasValue());
}

TEST(CSVTest, CSVObjectReaderReadEmpty)
{
    const std::string value = "";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_FALSE(optCSVReader.hasValue());
}

TEST(CSVTest, CSVObjectReaderReadOne)
{
    const std::string value = "test";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 1);

    const auto& firstRow = rows.at(0);
    ASSERT_EQ(firstRow.size(), 1);
}

TEST(CSVTest, CSVObjectReaderReadTwo)
{
    const std::string value = "test,test2";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 1);

    const auto& firstRow = rows.at(0);
    ASSERT_EQ(firstRow.size(), 2);
}

TEST(CSVTest, CSVObjectReaderReadTwoXTwo)
{
    const std::string value = "test,test2\ntest3,test4";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 2);

    for (const auto& row : rows)
    {
        ASSERT_EQ(row.size(), 2);
    }
}

TEST(CSVTest, CSVObjectReaderReadTwoXOneXThree)
{
    const std::string value = "test,test2\ntest3\ntest4,test5,test6";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 3);

    ASSERT_EQ(rows[0].size(), 2);
    ASSERT_EQ(rows[1].size(), 1);
    ASSERT_EQ(rows[2].size(), 3);
}

TEST(CSVTest, CSVObjectReaderReadTwoXTwoCR)
{
    const std::string value = "test,test2\rtest3,test4";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 2);

    for (const auto& row : rows)
    {
        ASSERT_EQ(row.size(), 2);
    }
}

TEST(CSVTest, CSVObjectReaderReadTwoXOneXThreeCR)
{
    const std::string value = "test,test2\rtest3\rtest4,test5,test6";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 3);

    ASSERT_EQ(rows[0].size(), 2);
    ASSERT_EQ(rows[1].size(), 1);
    ASSERT_EQ(rows[2].size(), 3);
}

TEST(CSVTest, CSVObjectReaderReadTwoXTwoCRN)
{
    const std::string value = "test,test2\r\ntest3,test4";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 2);

    for (const auto& row : rows)
    {
        ASSERT_EQ(row.size(), 2);
    }
}

TEST(CSVTest, CSVObjectReaderReadTwoXOneXThreeCRN)
{
    const std::string value = "test,test2\r\ntest3\r\ntest4,test5,test6";
    const bool dedicatedRow = true;
    auto optCSVReader = setupReader<std::string>(value, dedicatedRow, stringReaderCallback);
    ASSERT_TRUE(optCSVReader.hasValue());

    const auto& rows = optCSVReader->getRows();
    ASSERT_EQ(rows.size(), 3);

    ASSERT_EQ(rows[0].size(), 2);
    ASSERT_EQ(rows[1].size(), 1);
    ASSERT_EQ(rows[2].size(), 3);
}

TEST(CSVTest, CSVObjectWriterWriteEmptyString)
{
    std::vector<std::vector<CSVString>> table = { { CSVString("", stringReaderCallback, stringWriterCallback) } };
    std::string output;
    auto optCSVWriter = setupWriter<std::string>(table, output);
    ASSERT_TRUE(optCSVWriter.hasValue());
    ASSERT_EQ("", output);
}

TEST(CSVTest, CSVObjectWriterWriteSingleString)
{
    const std::string input = "Hello world!";
    std::vector<std::vector<CSVString>> table = { { CSVString(input, stringReaderCallback, stringWriterCallback) } };
    std::string output;
    auto optCSVWriter = setupWriter<std::string>(table, output);
    ASSERT_TRUE(optCSVWriter.hasValue());
    ASSERT_EQ(input, output);
}

TEST(CSVTest, CSVObjectWriterWriteDoubleString)
{
    const std::string input = "Hello world!";
    std::vector<std::vector<CSVString>> table = { { CSVString(input, stringReaderCallback, stringWriterCallback), CSVString(input, stringReaderCallback, stringWriterCallback) } };
    std::string output;
    auto optCSVWriter = setupWriter<std::string>(table, output);
    ASSERT_TRUE(optCSVWriter.hasValue());

    std::string expected = input;
    expected += ',';
    expected += input;
    ASSERT_EQ(expected, output);
}

TEST(CSVTest, CSVObjectWriterWriteQuadrupleString)
{
    const std::string input = "Hello world!";
    std::vector<std::vector<CSVString>> table = { { CSVString(input, stringReaderCallback, stringWriterCallback),
                                                    CSVString(input, stringReaderCallback, stringWriterCallback),
                                                    CSVString(input, stringReaderCallback, stringWriterCallback),
                                                    CSVString(input, stringReaderCallback, stringWriterCallback) } };
    std::string output;
    auto optCSVWriter = setupWriter<std::string>(table, output);
    ASSERT_TRUE(optCSVWriter.hasValue());

    std::string expected = input;
    expected += ',';
    expected += input;
    expected += ',';
    expected += input;
    expected += ',';
    expected += input;
    ASSERT_EQ(expected, output);
}

TEST(CSVTest, CSVObjectWriterWriteQuadrupleXQuadrupleString)
{
    const std::string input = "Hello world!";
    CSVString obj(input, stringReaderCallback, stringWriterCallback);
    std::vector<std::vector<CSVString>> table;
    HCLIB_CONSTEXPR std::size_t quad = 4;

    for (std::size_t i = 0; i < quad; ++i)
    {
        table.emplace_back();
        auto& row = table.back();

        for (std::size_t j = 0; j < 4; ++j)
        {
            row.push_back(obj);
        }
    }

    std::string output;
    auto optCSVWriter = setupWriter<std::string>(table, output);
    ASSERT_TRUE(optCSVWriter.hasValue());

    std::string expected = input;
    expected += ',';
    expected += input;
    expected += ',';
    expected += input;
    expected += ',';
    expected += input;

    const std::string temp = expected;

    for (std::size_t i = 0; i < quad - 1; ++i)
    {
        expected += '\n';
        expected += temp;
    }

    ASSERT_EQ(expected, output);
}

s32 main(s32 argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

