#include <hclib/Types.h>
#include <hclib/Graph.h>

#include <iostream>

#include <gtest/gtest.h>

using namespace hclib;

TEST(GraphTest, GraphAddTest)
{
    std::cout << std::endl;

    Graph<s32> g;
    g.addEdge(1, 2);
    g.addEdge(1, 3);
    g.addEdge(3, 4);

    std::cout << g << std::endl;
}
s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

