// My includes
#include <hclib/Types.h>
#include <hclib/Histogram.h>
#include <hclib/Random.h>

// GoogleTest includes
#include <gtest/gtest.h>

// std includes
#include <array>

using namespace hclib;

template<class T1, class T2>
static bool match(const T1 &cont0, const T2 &cont1)
{
    const s32 size = static_cast<s32>(cont0.size());

    if (size != static_cast<s32>(cont1.size()))
        return false;

    for (s32 i = 0; i < size; ++i)
    {
        if (cont0[i] != cont1[i])
            return false;
    }

    return true;
}

TEST(HistogramTest, HistogramSTDarrayCompare)
{
    static const s32 HIST_SIZE = 100;

    Histogram<s32> hist(HIST_SIZE);
    Histogram<s32> hist2(HIST_SIZE);
    std::array<s32, HIST_SIZE> baseline = { 0 };
    RandomInt rand(0, HIST_SIZE - 1);

    static const s32 MAX_ITER = 10000;

    static auto incFunc = [](s32 &value)
    {
        ++value;
    };

    for (s32 i = 0; i < MAX_ITER; ++i)
    {
        const s32 value = rand.next();

        hist.increment(value);
        hist2.increment(value, incFunc);
        ++baseline[value];
    }

    ASSERT_TRUE(match(hist, baseline));
    ASSERT_TRUE(match(hist2, baseline));
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
