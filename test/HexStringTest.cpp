#include <hclib/Types.h>
#include <hclib/Algorithm.h>

#include <string>

#include <gtest/gtest.h>

using namespace hclib;
using namespace hclib::algorithm;

class HexStringTestParameterized : public ::testing::TestWithParam<s32>
{
public:
};

TEST(HexStringTest, STDstringTest)
{
    u32 value = 0;
    // auto str = toHexString<u32, std::string>(value);
    // ASSERT_EQ(str, "00000000");

    value = 1;
    // str = toHexString<u32, std::string>(value);
    auto str = toHexString<u32, std::string>(value);
    ASSERT_EQ(str, "00000001");
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
