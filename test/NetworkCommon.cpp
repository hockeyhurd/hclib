#include "NetworkCommon.h"

#include <iostream>

void printMessage(const std::string &name, const std::string &message)
{
    std::cout << "[" << name << "]: " << message << std::endl;
}

void printMessage(const std::string &name, const char *message)
{
    std::cout << "[" << name << "]: " << message << std::endl;
}

Header readHeader(Socket &socket)
{
    Header header;
    header.val = 0;
    socket.receive(header.buf, sizeof(header.val));

    return header;
}

Header readHeader(ServerSocket &socket, const ServerSocket::ClientData &clientData)
{
    Header header;
    header.val = 0;
    socket.receive(clientData, header.buf, sizeof(header.val));

    return header;
}
