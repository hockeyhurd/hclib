#include <hclib/Types.h>
#include <hclib/Range.h>

#include <gtest/gtest.h>

using namespace hclib;

template<class T>
struct RangeTestVec3
{
    T x;
    T y;
    T z;

    explicit RangeTestVec3(const T x, const T y, const T z) : x(x), y(y), z(z) {}
};

class RangeInRangeTestParameterized : public ::testing::TestWithParam<RangeTestVec3<f32>>
{
public:
};

TEST(RangeTest, rangeTestInit)
{
    Range<s32> range(0, 100);

    ASSERT_TRUE(range.getMin() == 0);
    ASSERT_TRUE(range.getMax() == 100);
    ASSERT_TRUE(range.inRange(0));
    ASSERT_TRUE(range.inRange(42));
    ASSERT_TRUE(range.inRange(99));
    ASSERT_FALSE(range.inRange(-5));
}

TEST_P(RangeInRangeTestParameterized, rangeInRangeTestCases)
{
    const auto &value = const_cast<RangeTestVec3<f32>&>(GetParam());
    Range<f32> range(value.x, value.y);

    ASSERT_TRUE(range.inRange(value.z));
}

INSTANTIATE_TEST_SUITE_P(
        RangeTests,
        RangeInRangeTestParameterized,
        ::testing::Values(
            RangeTestVec3<f32>(0.0F, 1.0F, 0.5F),
            RangeTestVec3<f32>(0.0001F, 0.01F, 0.0002F),
            RangeTestVec3<f32>(-100.0F, 100.0002F, 0.01F)
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
