#include <hclib/DataBuffer.h>
#include <hclib/Types.h>

#include <cstring>
#include <stdexcept>

#include <gtest/gtest.h>

using namespace hclib;

TEST(DataBufferTest, CreateEmptyBufferLE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);
}

TEST(DataBufferTest, CreateEmptyBufferBE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::BIG, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);
}

TEST(DataBufferTest, PackByteThenUnpackLE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const char inValue = 0xEF;
    buffer.packByte(inValue);

    auto iter = buffer.cbegin();
    const char outValue = buffer.unpackByte(iter);
    ASSERT_EQ(outValue, inValue);
}

TEST(DataBufferTest, PackByteThenUnpackBE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::BIG, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const char inValue = 0xEF;
    buffer.packByte(inValue);

    auto iter = buffer.cbegin();
    const char outValue = buffer.unpackByte(iter);
    ASSERT_EQ(outValue, inValue);
}

TEST(DataBufferTest, PackTwoU16sThenUnpackLE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u16 inValues[2] = { 0xBE, 0xEF };
    buffer.packU16(inValues[0]);
    buffer.packU16(inValues[1]);

    auto iter = buffer.cbegin();
    u16 outValue = buffer.unpackU16(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU16(iter);
    ASSERT_EQ(outValue, inValues[1]);
}

TEST(DataBufferTest, PackTwoU16sThenUnpackBE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::BIG, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u16 inValues[2] = { 0xBE, 0xEF };
    buffer.packU16(inValues[0]);
    buffer.packU16(inValues[1]);

    auto iter = buffer.cbegin();
    u16 outValue = buffer.unpackU16(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU16(iter);
    ASSERT_EQ(outValue, inValues[1]);
}

TEST(DataBufferTest, PackFourU32sThenUnpackLE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u32 inValues[4] = { 0xDEAD, 0xBEEF, 0xDEAD, 0xBEEF };
    buffer.packU32(inValues[0]);
    buffer.packU32(inValues[1]);
    buffer.packU32(inValues[2]);
    buffer.packU32(inValues[3]);

    auto iter = buffer.cbegin();
    u32 outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[1]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[2]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[3]);
}

TEST(DataBufferTest, PackFourU32sThenUnpackBE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::BIG, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u32 inValues[4] = { 0xDEAD, 0xBEEF, 0xDEAD, 0xBEEF };
    buffer.packU32(inValues[0]);
    buffer.packU32(inValues[1]);
    buffer.packU32(inValues[2]);
    buffer.packU32(inValues[3]);

    auto iter = buffer.cbegin();
    u32 outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[1]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[2]);

    outValue = buffer.unpackU32(iter);
    ASSERT_EQ(outValue, inValues[3]);
}

TEST(DataBufferTest, PackFourU64sThenUnpackLE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    // const u16 inValues[4] = { 0xDEAD, 0xBEEF, 0xDEAD, 0xBEEF };
    const u64 inValues[4] = { 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF };
    buffer.packU64(inValues[0]);
    buffer.packU64(inValues[1]);
    buffer.packU64(inValues[2]);
    buffer.packU64(inValues[3]);

    auto iter = buffer.cbegin();
    u64 outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[1]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[2]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[3]);
}

TEST(DataBufferTest, PackFourU64sThenUnpackBE)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u64 inValues[4] = { 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF, 0xDEADBEEFDEADBEEF };
    buffer.packU64(inValues[0]);
    buffer.packU64(inValues[1]);
    buffer.packU64(inValues[2]);
    buffer.packU64(inValues[3]);

    auto iter = buffer.cbegin();
    u64 outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[0]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[1]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[2]);

    outValue = buffer.unpackU64(iter);
    ASSERT_EQ(outValue, inValues[3]);
}

TEST(DataBufferTest, PackBytes)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const char* str = "Hello, world!";
    const std::size_t len = strlen(str);
    buffer.packBytes(str, len);

    ASSERT_FALSE(buffer.empty());
    ASSERT_EQ(buffer.size(), len);

    auto iter = buffer.cbegin();

    for (std::size_t i = 0; i < len; ++i)
    {
        const char ch = buffer.unpackByte(iter);
        ASSERT_EQ(str[i], ch);
    }
}

TEST(DataBufferTest, PackString)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const std::string str = "Hello, world!";
    const std::size_t len = str.size();
    buffer.packString(str);

    ASSERT_FALSE(buffer.empty());
    ASSERT_EQ(buffer.size(), len);

    auto iter = buffer.cbegin();

    for (std::size_t i = 0; i < len; ++i)
    {
        const char ch = buffer.unpackByte(iter);
        ASSERT_EQ(str[i], ch);
    }
}

TEST(DataBufferTest, Get)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const char ch = 'A';
    buffer.packByte(ch);

    std::size_t index = 0;
    auto charAt = buffer.get(index++);
    ASSERT_EQ(ch, charAt);

    ASSERT_THROW(charAt = buffer.get(index), std::out_of_range);
}

TEST(DataBufferTest, TryGet)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const char ch = 'A';
    buffer.packByte(ch);

    std::size_t index = 0;
    auto* charAt = buffer.tryGet(index++);
    ASSERT_EQ(ch, *charAt);

    charAt = buffer.tryGet(index);
    ASSERT_EQ(charAt, nullptr);
}

TEST(DataBufferTest, SetResetPosition)
{
    HCLIB_CONSTEXPR std::size_t capacity = 4096;
    DataBuffer buffer(capacity, EnumEndian::LITTLE, true);

    ASSERT_EQ(buffer.getCapacity(), capacity);
    ASSERT_TRUE(buffer.empty());
    ASSERT_EQ(buffer.size(), 0);

    const u32 value = 0xDEADBEEF;
    buffer.packU32(value);

    ASSERT_EQ(buffer.size(), sizeof(value));

    buffer.resetPosition();
    ASSERT_EQ(buffer.size(), 0);

    buffer.setPosition(sizeof(value));
    ASSERT_EQ(buffer.size(), sizeof(value));
}


s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

