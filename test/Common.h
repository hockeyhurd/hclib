#pragma once

#ifndef COMMON_H
#define COMMON_H

#include <cstdlib>
#include <iostream>
#include <mutex>

#include <hclib/Types.h>

using namespace hclib;

u64 getTestCount()
{
    static u64 testID = 0;
    static std::mutex mutex;

    std::unique_lock<std::mutex> lock(mutex);
    return testID++;
}

template<class T>
void expectTrue(const T &value)
{
    const auto testID = getTestCount();

    if (!value)
    {
        std::cout << "[ERROR Test #" << testID << "]: Expected true, got " << value << std::endl;
    }
}

template<class T>
void expectTrue(T &&value)
{
    const auto testID = getTestCount();

    if (!value)
    {
        std::cout << "[ERROR Test #" << testID << "]: Expected true, got " << value << std::endl;
    }
}

template<class T>
void expectFalse(const T &value)
{
    const auto testID = getTestCount();

    if (value)
    {
        std::cout << "[ERROR Test #" << testID << "]: Expected false, got " << value << std::endl;
    }
}

template<class T>
void expectFalse(T &&value)
{
    const auto testID = getTestCount();

    if (value)
    {
        std::cout << "[ERROR Test #" << testID << "]: Expected false, got " << value << std::endl;
    }
}

#endif //!COMMON_H
