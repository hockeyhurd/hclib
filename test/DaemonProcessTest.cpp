#include <hclib/Daemon.h>
#include <hclib/Debug.h>
#include <hclib/Types.h>

#include <gtest/gtest.h>

using namespace hclib;

#include <iostream>

// *nix only for now...
#if OS_UNIX || OS_APPLE
TEST(DaemonProcessTest, testProcess)
{
    const char *argv[] = { "sleep", "10", nullptr };
    const bool status = Daemon::create("/usr/bin/sleep", const_cast<char**>(argv));
    ASSERT_TRUE(status);
}

// Disable this for Windows (not implemented).
#else
TEST(DaemonProcessTest, DISABLED_testProcess)
{
}
#endif

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

