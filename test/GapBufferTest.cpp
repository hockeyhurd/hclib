#include <hclib/Types.h>
#include <hclib/GapBuffer.h>
#include <hclib/Logger.h>

#include <utility>
// #include <vector>

#include <gtest/gtest.h>

using namespace hclib;

TEST(GapBufferTest, GapBufferCreate)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferCreateFromCopy)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    GapBuffer gb2(gb);

    ASSERT_EQ(gb.size(), gb2.size());
    ASSERT_EQ(gb, gb2);
}

TEST(GapBufferTest, GapBufferCreateFromMove)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    GapBuffer gb2(std::move(gb));

    ASSERT_EQ(gb2.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferCopyFromSmallerOpEQsCausesNoResize)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    GapBuffer gb2(gapSize * 2, bufferSize * 4);
    ASSERT_NE(gb.size(), gb2.size());
    ASSERT_NE(gb, gb2);

    gb2 = gb;
    ASSERT_NE(gb.size(), gb2.size());
    ASSERT_EQ(gb2.size(), bufferSize * 4);
    ASSERT_NE(gb, gb2);
}

TEST(GapBufferTest, GapBufferMoveToCurrentPosChangesNothing)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    HCLIB_CONSTEXPR std::size_t curPos = 0;
    ASSERT_EQ(gb.getCurrentPosition(), curPos);
    gb.move(curPos);
    ASSERT_EQ(gb.getCurrentPosition(), curPos);
}

TEST(GapBufferTest, GapBufferMoveToPosOneChangesToOne)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    std::size_t curPos = 0;
    ASSERT_EQ(gb.getCurrentPosition(), curPos);
    gb.move(++curPos);
    ASSERT_EQ(gb.getCurrentPosition(), curPos);
}

TEST(GapBufferTest, GapBufferMoveToPosEndOfGapChangesNothing)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    ASSERT_EQ(gb.getCurrentPosition(), 0);
    gb.move(gapSize);
    ASSERT_EQ(gb.getCurrentPosition(), gapSize);
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferMoveToPosEndOfBufferChangesSize)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    ASSERT_EQ(gb.getCurrentPosition(), 0);
    gb.move(bufferSize);
    ASSERT_EQ(gb.getCurrentPosition(), bufferSize);
    ASSERT_EQ(gb.size(), bufferSize + gapSize);
}

TEST(GapBufferTest, GapBufferMoveToPosEndOfBufferThreeTimesChangesSizeThreeTimes)
{
    std::size_t gapSize = 16;
    std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    ASSERT_EQ(gb.getCurrentPosition(), 0);

    for (auto i = 0; i < 3; ++i)
    {
        gb.move(bufferSize);
        ASSERT_EQ(gb.getCurrentPosition(), bufferSize);
        ASSERT_EQ(gb.size(), bufferSize + gapSize);
    }
}

TEST(GapBufferTest, GapBufferCurrentPos)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    gb.insert('A');
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferInsertOneNoResize)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    gb.insert('A');
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferInsertOneTooManyCausesAResize)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    HCLIB_CONSTEXPR char base = 'A';
    std::size_t i;
    char value;

    for (i = 0; i < gapSize - 1; ++i)
    {
        value = static_cast<char>(base + i);
        gb.insert(value);
        ASSERT_EQ(gb.size(), bufferSize);
    }

    // Add the 'one more'
    ++i;
    value = static_cast<char>(base + i);
    gb.insert(value);
    ASSERT_EQ(gb.size(), bufferSize + gapSize);
}

TEST(GapBufferTest, GapBufferEraseFromNothingFails)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    ASSERT_FALSE(gb.erase());
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferEraseFromSomethingIsSuccess)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    gb.insert('A');
    ASSERT_EQ(gb.size(), bufferSize);
    ASSERT_TRUE(gb.erase());
    ASSERT_EQ(gb.size(), bufferSize);
}

TEST(GapBufferTest, GapBufferInsertOneTooManyCausesAResizeThenEraseAllUntilFailure)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    HCLIB_CONSTEXPR char base = 'A';
    std::size_t i;
    char value;

    for (i = 0; i < gapSize - 1; ++i)
    {
        value = static_cast<char>(base + i);
        gb.insert(value);
        ASSERT_EQ(gb.size(), bufferSize);
    }

    // Add the 'one more'
    ++i;
    value = static_cast<char>(base + i);
    gb.insert(value);
    ASSERT_EQ(gb.size(), bufferSize + gapSize);

    for (i = 0; i < gapSize; ++i)
    {
        ASSERT_EQ(gb.size(), bufferSize + gapSize);
        ASSERT_TRUE(gb.erase());
    }

    ASSERT_FALSE(gb.erase());
}

TEST(GapBufferTest, GapBufferAddFourThenSearch)
{
    HCLIB_CONSTEXPR std::size_t gapSize = 16;
    HCLIB_CONSTEXPR std::size_t bufferSize = 64;
    GapBuffer gb(gapSize, bufferSize);
    ASSERT_EQ(gb.size(), bufferSize);

    gb.insert('A');
    gb.insert('B');
    gb.insert('C');
    gb.insert('T');
    ASSERT_EQ(gb.size(), bufferSize);

    const std::string pattern = "BC";
    const std::size_t startPos = 0;
    const std::size_t resultPos = gb.search(pattern, startPos);
    ASSERT_EQ(resultPos, 1);
}

s32 main(s32 argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

