#include <hclib/Socket.h>
#include <hclib/ServerSocket.h>
#include <hclib/StringBuilder.h>
#include <hclib/Types.h>

using namespace hclib;

#include <algorithm>

union Header
{
    s32 val;
    char buf[sizeof(s32)];
};

void printMessage(const std::string &name, const std::string &message);
void printMessage(const std::string &name, const char *message);

template<class Socket>
void sendHeader(Socket &socket, const s32 len)
{
    union Header
    {
        s32 val;
        char buf[sizeof(s32)];
    };

    Header header;
    header.val = len;
    socket.send(header.buf, sizeof(header.val));
}

Header readHeader(Socket &socket);

Header readHeader(ServerSocket &socket, const ServerSocket::ClientData &clientData);
