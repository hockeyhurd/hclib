#include <hclib/String.h>
#include <hclib/StringCache.h>

#include <gtest/gtest.h>
#include <string>
#include <cstring>

using namespace hclib;

TEST(StringCacheTest, StringCacheTestGeneric)
{
    StringCache<std::string> cache;
    auto stringView = cache.intern(std::string("Hello, world!"));
    auto stringView2 = cache.intern(std::string("Hello, world!"));

    ASSERT_EQ(stringView.string().compare("Hello, world!"), 0);
    ASSERT_EQ(std::strncmp(stringView.c_str(), "Hello, world!", stringView.string().size()), 0);
    ASSERT_EQ(stringView.c_str(), stringView2.c_str());
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
