#include <hclib/Types.h>
#include <hclib/ArgParser.h>
#include <hclib/Debug.h>
#include <hclib/StringBuilder.h>

// #include <array>
#include <string>
#include <vector>

using namespace hclib;

s32 main()
{
    // std::array<char*, 2> args = { "./argParserTest", "-f" };
    // std::array<char*, 3> args = { "./argParserTest", "-o", "2.0" };
    // std::array<const char*, 4> args = { "./argParserTest", "-o", "2.0", "3.14159" };
    std::vector<const char*> args = { "./argParserTest", "-o", "some_name", "-pi", "3.14159" };
    StringBuilder builder(0x100);
    s32 count = 0;

    Debug::info("Dumping args:");
    for (const auto &arg : args)
    {
        builder.append('[');
        builder.append(count++);
        builder.append("]: ");
        builder.append(arg);

        Debug::info(builder.toSTDString());
        builder.clear();
    }
    Debug::info("Done!");

    // const s32 expectedSuccessfullParseCount = 1;
    ArgParser argParser;
#if 0
    StringArg fArgHandler;
    argParser.registerFlag("-f", &fArgHandler);
#elif 0
    PrimitiveArg<s32> oArgHandler(-1, 1);
    argParser.registerFlag("-o", &oArgHandler);
#elif 0
    PrimitiveArg<f32> oArgHandler(-1, 2);
    argParser.registerFlag("-o", &oArgHandler);
#elif 1
    StringArg oArgHandler(1, 1);
    argParser.registerFlag("-o", &oArgHandler);
    PrimitiveArg<f32> vArgHandler(-1, 1);
    argParser.registerFlag("-pi", &vArgHandler);
#endif
    const s32 expectedSuccessfullParseCount = argParser.expectedParseResult();
    const s32 argc = static_cast<s32>(args.size());
    char **argss = const_cast<char**>(args.data());
    const auto parsedFlagsCount = argParser.parse(argc, argss);

    builder.append("Parsed: ");
    builder.append(parsedFlagsCount);
    builder.append(" args successfully!");

    Debug::info(builder.toSTDString());

    if (parsedFlagsCount == expectedSuccessfullParseCount)
    {
        s32 resultCount = 0;

        {
            builder.clear();
            auto &results = oArgHandler.getResults();

            for (auto &result : results)
            {
                builder << '[' << (resultCount++) << "]: " << result << '\n';
            }
        
            Debug::info(builder.toSTDString());
        }
        {
            builder.clear();
            auto &results = vArgHandler.getResults();

            for (auto &result : results)
            {
                builder << '[' << (resultCount++) << "]: " << result << '\n';
            }
        
            Debug::info(builder.toSTDString());
        }
    }

    return 0;
}
