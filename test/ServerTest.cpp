#include <hclib/Types.h>
#include <hclib/Socket.h>
#include <hclib/ServerSocket.h>

#include <string>
#include <cstring>
#include <iostream>

#include "NetworkCommon.h"

using namespace hclib;

s32 main(s32 argc, char **argv)
{
    // args: port, name
    if (argc < 3)
    {
        std::cout << "Expected 2 args: port and name...\n";
        std::exit(EXIT_FAILURE);
    }

    const s32 port = parse<s32>(argv[1]);
    const std::string name = argv[2];
    // printMessage(name, address);
    printMessage(name, std::to_string(port));

    ServerSocket socket(port);
    socket.connect();
    const s32 bufferSize = 8192;
    char *buffer = new char[bufferSize];
    StringBuilder builder(bufferSize);
    
    auto clientData = socket.accept();

    do
    {
        Header header = readHeader(socket, clientData);
        socket.receive(clientData, buffer, std::min<s32>(bufferSize, header.val));
        buffer[bufferSize - 1] = '\0';
        builder.clear();
        builder.append("Received: ");
        builder.append(buffer);
        printMessage(name, builder.c_str());
    }
    while (std::strncmp(buffer, "exit", bufferSize) != 0);

    socket.disconnect();
    delete[] buffer;

    return 0;
}
