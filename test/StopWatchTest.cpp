#include <hclib/StopWatch.h>

#include <chrono>
#include <thread>

#include <gtest/gtest.h>

using namespace hclib;

TEST(StopWatchTest, StopWatchTest2Seconds)
{
    StopWatch<std::chrono::high_resolution_clock> timer;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // std::cout << "Slept for " << timer.seconds() << " seconds.\n";
    const f64 eps = 0.1;
    const auto elapsedTime = timer.seconds();
    ASSERT_NEAR(elapsedTime, 1, eps);
}

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
