#include <hclib/Socket.h>
#include <hclib/StringBuilder.h>
#include <hclib/Types.h>

using namespace hclib;

#include <algorithm>
#include <string>
#include <iostream>
#include <thread>

#include "NetworkCommon.h"

s32 main(s32 argc, char **argv)
{
    // args: address, port, name
    if (argc < 4)
    {
        std::cout << "Expected 3 args: address, port, and name...\n";
        std::exit(EXIT_FAILURE);
    }

    const std::string address = argv[1];
    const s32 port = parse<s32>(argv[2]);
    const std::string name = argv[3];
    printMessage(name, address);
    printMessage(name, std::to_string(port));

    Socket socket(address, port);
    socket.connect();
    std::string message = "";
    const s32 bufferSize = 8192;
    char *buffer = new char[bufferSize];
    StringBuilder builder(bufferSize);

    while (std::cin >> message)
    {
        if (message == "exit")
        {
            std::cout << "Exitting...\n";
            break;
        }

        printMessage(name, message);

        printMessage(name, "sending...");
        sendHeader(socket, message.size() + 1);
        socket.send(message.c_str(), message.size() + 1);
        printMessage(name, "sent!");
    }

    socket.disconnect();
    delete[] buffer;

    return 0;
}
