#include <hclib/Types.h>
#include <hclib/Logger.h>
#include <hclib/Sort.h>

#include <functional>
#include <vector>
#include <gtest/gtest.h>

using namespace hclib;

// static void testMerge(Logger &logger);
// static void testQuick(Logger &logger);

template<class T>
static void dump(T &container, Logger &logger)
{
    std::size_t i = 0;
    for (const auto &value : container)
    {
        logger << '[' << (i++) << "]: " << value << '\n';
    }
}

#if 1
class QuickSortTestParameterized : public ::testing::TestWithParam<std::vector<s32>>
{
public:
};

TEST_P(QuickSortTestParameterized, QuickSortTestLessParameterized)
{
    auto &vec = const_cast<std::vector<s32>&>(GetParam());
    // mergeSort(vec, std::less<s32>());
    quickSort<std::vector<s32>, s32>(vec, std::less<s32>());

    // Logger logger;
    // dump(vec, logger);

    s32 lastValue = vec[0];

    for (s32 i = 1; i < static_cast<s32>(vec.size()); ++i)
    {
        const auto value = vec[i];
        ASSERT_LE(lastValue, value);
        lastValue = value;
    }
}

TEST_P(QuickSortTestParameterized, QuickSortTestGreaterParameterized)
{
    auto &vec = const_cast<std::vector<s32>&>(GetParam());
    // mergeSort(vec, std::greater<s32>());
    quickSort<std::vector<s32>, s32>(vec, std::greater<s32>());
    
    // Logger logger;
    // dump(vec, logger);
    
    s32 lastValue = vec[0];

    for (s32 i = 1; i < static_cast<s32>(vec.size()); ++i)
    {
        const auto value = vec[i];
        ASSERT_GE(lastValue, value);
        lastValue = value;
    }
}

INSTANTIATE_TEST_SUITE_P(
        QuickSortTests,
        QuickSortTestParameterized,
        ::testing::Values(
            std::vector<s32> { 69, 96, 5, 2, 3, 10, 11, 33, 39, 27 },
            std::vector<s32> { 27, 39, 33, 11, 10, 3, 2, 5, 96, 69 }
        ));

s32 main(s32 argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#else
s32 main()
{
    Logger logger;
    // logger << "testMerge\n";
    // testMerge(logger);
    logger << "testQuick\n";
    testQuick(logger);

    return 0;
}

/* static */
void testMerge(Logger &logger)
{
    static const s32 len = 10;
    std::vector<s32> vec;
    vec.reserve(len);

    for (s32 i = 0; i < len; ++i)
    {
        vec.emplace_back(rand() % 100);
    }

    logger << "Before:\n";
    dump(vec, logger);
    logger << "After:\n";
    mergeSort(vec, std::less<s32>());
    dump(vec, logger);
}

/* static */
void testQuick(Logger &logger)
{
    static const s32 len = 10;
    std::vector<s32> vec;
    vec.reserve(len);

    for (s32 i = 0; i < len; ++i)
    {
        vec.emplace_back(rand() % 100);
    }

    logger << "Before:\n";
    dump(vec, logger);
    logger << "After:\n";
    quickSort<std::vector<s32>, s32>(vec, std::less<s32>());
    dump(vec, logger);
}
#endif

