#!/bin/bash

echo "Building"

CC="CC=g++"
CFG=""

# Get variables passed
for var in "$@"
do
    if [ ${var:0:10} == "CC=clang++" ]
    then
        CC="$var"
    elif [ ${var:0:6} == "CC=g++" ]
    then
        CC="$var"
    elif [ $var == "CFG=Debug" ]
    then
        CFG="$var"
    elif [ $var == "CFG=Release" ]
    then
        CFG="$var"
    else
        echo "Unrecognized command $var"
        exit -1
    fi
done

pushd src/ > /dev/null

make all $CC $CFG 
RESULT=$?

popd > /dev/null

echo "Done"
echo "EXITED with result: ${RESULT}"
exit ${RESULT}
