/**
 * @author hockeyhurd
 * @version 2020-12-11
 */

#pragma once

#ifndef HCLIB_GRAPH_H
#define HCLIB_GRAPH_H

#include <hclib/Types.h>

#include <iosfwd>
#include <map>
#include <set>

namespace hclib
{

    template<class T>
    class Node;

    template<class T>
    class Edge 
    {
    private:

        friend Node<T>;

        Node<T> *first;
        Node<T> *second;
        s32 weight;

    public:

        explicit Edge(Node<T> *first = nullptr, Node<T> *second = nullptr, const s32 weight = 0) : first(first), second(second), weight(weight) {}
        Edge(const Edge<T> &other) = delete;
        Edge(Edge<T> &&other) = default;
        ~Edge() = default;

        Edge<T> &operator= (const Edge<T> &other) = delete;
        Edge<T> &operator= (Edge<T> &&other) = default;

        inline Node<T> *getFirst()
        {
            return first;
        }

        inline const Node<T> *getFirst() const
        {
            return first;
        }

        inline Node<T> *getSecond()
        {
            return second;
        }

        inline const Node<T> *getSecond() const
        {
            return second;
        }

        bool operator< (const Edge<T> &other) const;
    };

    template<class T>
    class Graph;

    template<class T>
    class Node 
    {
    private:

        friend Edge<T>;
        friend Graph<T>;

        T value;
        std::set<Edge<T>> edges;

    public:

        explicit Node(const T &value);
        explicit Node(T &&value);
        Node(const Node<T> &other) = default;
        Node(Node<T> &&other) = default;
        ~Node() = default;

        Node<T> &operator= (const Node<T> &other) = default;
        Node<T> &operator= (Node<T> &&other) = default;

        inline T &getValue()
        {
            return value;
        }

        inline const T &getValue() const
        {
            return value;
        }

        void addEdge(Node<T> *other, const s32 weight = 1);

        friend std::ostream &operator<< (std::ostream &os, const Node<T> &node)
        {
            insertIndent(os, 4);
            os << node.getValue() << ":\n";
            // os << node.value;

            for (auto &edge : node.edges)
            {
                insertIndent(os, 4 * 2);
                os << edge.getSecond()->getValue();
                // os << '\n';
            }

            return os;
        }

    private:

        static void insertIndent(std::ostream &os, const s32 indentation);
    };

    // template<class K, class V>
    template<class T>
    class Graph
    {
    private:

        // using Map = std::map<K, Node<V>>;
        using Map = std::map<T, Node<T>>;

        Map map;

    public:

        explicit Graph() = default;
        // Graph(const Graph<K, V> &) = default;
        // Graph(Graph<K, V> &&) = default;
        Graph(const Graph<T> &) = default;
        Graph(Graph<T> &&) = default;
        ~Graph() = default;

        // Graph<K, V> &operator= (const Graph<K, V> &) = default;
        // Graph<K, V> &operator= (Graph<K, V> &&) = default;
        Graph<T> &operator= (const Graph<T> &) = default;
        Graph<T> &operator= (Graph<T> &&) = default;

        void addEdge(const T &first, const T &second, const s32 weight = 1);

        friend std::ostream &operator<< (std::ostream &os, const Graph<T> &graph)
        {
            std::set<const Node<T>*> visited;

            bool first = true;

            for (auto &node : graph.map)
            {
                const Node<T> *ptr = &(node.second);
                const auto findResult = visited.find(ptr);

                if (findResult == visited.end())
                {
                    if (first)
                        first = false;
                    else
                        os << '\n';

                    visited.emplace(ptr);

                    insertIndent(os, 4);
                    os << *ptr;
                }
            }

            return os;
        }

    private:

        static void insertIndent(std::ostream &os, const s32 indentation);
    };

}

#include "Graph.inl"

#endif //!HCLIB_GRAPH_H

