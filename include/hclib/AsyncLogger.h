/**
 * @author hockeyhurd
 * @version 2022-01-28
 */

#pragma once

#ifndef HCLIB_ASYNC_LOGGER_H
#define HCLIB_ASYNC_LOGGER_H

#include <hclib/Types.h>
#include <hclib/LockGuard.h>
#include <hclib/Logger.h>
#include <hclib/Mutex.h>

#if CPP_VER >= 2017
#include <string_view>
#endif

namespace hclib
{

    class HCLIB_EXPORT AsyncLogger
    {
    protected:
        Logger logger;

        using mutex_t = SharedMutex;
        mutable mutex_t mutex;

    public:

        /**
         * Constructor from an existing std::ostream.
         * 
         * @param os std::ostream reference.
         * @param level LogLevel to use.
         */
        explicit AsyncLogger(std::ostream &os = static_cast<std::ostream &>(std::cout),
                        const LogLevel level = LogLevel::INFO);

        /**
         * Constructor for writing logs to a file.
         *
         *
         * @param path std::string path to write to.
         * @param level LogLevel to use.
         */
        explicit AsyncLogger(const std::string &path, const LogLevel level = LogLevel::INFO);

        /**
         * Constructor for writing logs to a file.
         *
         * @param path std::string path to write to.
         * @param level LogLevel to use.
         */
        explicit AsyncLogger(std::string &&path, const LogLevel level = LogLevel::INFO);

        /**
         * Deleted copy constructor.
         */
        AsyncLogger(const AsyncLogger &other) = delete;

        /**
         * Move constructor.
         */
        AsyncLogger(AsyncLogger &&other);

        /**
         * Destructor.
         */
        virtual ~AsyncLogger();

        /**
         * Deleted copy assignment operator.
         */
        AsyncLogger &operator= (const AsyncLogger &other) = delete;

        /**
         * Move assignment operator.
         *
         * @param other AsyncLogger to move from.
         */
        AsyncLogger &operator= (AsyncLogger &&other);

        /**
         * Default logger (uses std::cout).
         */
        static AsyncLogger &stdlogger();

        /**
         * Gets the current LogLevel.
         *
         * @return LogLevel.
         */
        virtual LogLevel getLevel() const;

        /**
         * Set the LogLevel.
         *
         * @param level LogLevel to set to.
         */
        virtual void setLevel(const LogLevel level);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param data to write.
         */
        template<class T>
        AsyncLogger &operator<< (const T &data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param data to write.
         */
        template<class T>
        AsyncLogger &operator<< (T &&data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param level the LogLevel we are writing as.
         * @param data to write.
         */
        template<class T>
        void write(const LogLevel level, const T &data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param level the LogLevel we are writing as.
         * @param data to write.
         */
        template<class T>
        void write(const LogLevel level, T &&data);

        /**
         * Flushes the underlying std::ostream.
         *
         * @return this AsyncLogger instance.
         */
        virtual AsyncLogger &flush();

        /**
         * Writes a newline character underlying std::ostream.
         *
         * @return this AsyncLogger instance.
         */
        virtual AsyncLogger &endl();

    };

    template<class T>
    AsyncLogger &AsyncLogger::operator<< (const T &data)
    {
        write(logger.getLevel(), data);
        return *this;
    }

    template<class T>
    AsyncLogger &AsyncLogger::operator<< (T &&data)
    {
        write(logger.getLevel(), std::forward<T>(data));
        return *this;
    }

    template<class T>
    void AsyncLogger::write(const LogLevel level, const T &data)
    {
        const UniqueLock<mutex_t> guard(mutex);
        logger.write(level, data);
    }

    template<class T>
    void AsyncLogger::write(const LogLevel level, T &&data)
    {
        const UniqueLock<mutex_t> guard(mutex);
        logger.write(level, std::forward<T>(data));
    }

}

#endif //!HCLIB_ASYNC_LOGGER_H

