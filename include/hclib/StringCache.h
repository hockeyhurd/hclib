#pragma once

#ifndef HCLIB_STRINGCACHE_H
#define HCLIB_STRINGCACHE_H

#include <hclib/Types.h>

#include <set>

namespace hclib
{

    template<class StringT>
    class StringCache;

    template<class StringT>
    class StringView
    {
        friend class StringCache<StringT>;

    private:

        StringT *str;

        explicit StringView(StringT *str);

    public:

        StringView(const StringView<StringT> &) = default;
        StringView(StringView<StringT> &&) = default;
        ~StringView() = default;

        StringView &operator= (const StringView<StringT> &) = default;
        StringView &operator= (StringView<StringT> &&) = default;

        inline bool operator< (const StringView<StringT> &other) const
        {
            return *str < *other.str;
        }

        inline bool operator== (const StringView<StringT> &other) const
        {
            return str == other.str || *str == *other.str;
        }

        inline bool operator!= (const StringView<StringT> &other) const
        {
            return !(*this == other);
        }

        const StringT &string() const;
        const char *c_str() const;

        std::size_t hash() const;
    };

    template<class StringT>
    class StringCache
    {
    private:

        using Set = std::set<StringT>;
        Set cache;

    public:

        StringCache() = default;
        StringCache(const StringCache<StringT> &) = delete;
        StringCache(StringCache<StringT> &&) = default;
        ~StringCache() = default;

        StringCache<StringT> &operator= (const StringCache<StringT> &) = delete;
        StringCache<StringT> &operator= (StringCache<StringT> &&) = default;

        StringView<StringT> intern(const StringT &str);
        StringView<StringT> intern(StringT &&str);

        bool find(const StringT &str);
        bool find(StringT &&str);
        bool find(StringView<StringT> view);

        StringView<StringT> get(const StringT &str);
        StringView<StringT> get(StringT &&str);
    };

}

#include "StringCache.cpp"

#endif //!HCLIB_STRINGCACHE_H
