/**
 * A class for representing Optional objects.
 *
 * @author hockeyhurd
 * @version 2021-12-03
 */

#pragma once

#ifndef HCLIB_OPTIONAL_H
#define HCLIB_OPTIONAL_H

// HCLib includes
#include <hclib/Types.h>

#include <exception>
#include <new>
// #include <stdexcept>
#include <type_traits>

namespace hclib
{

    struct in_place_t
    {
        explicit HCLIB_CONSTEXPR_FUNC in_place_t() = default;
    };

    struct nullopt_t
    {
        explicit HCLIB_CONSTEXPR_FUNC nullopt_t(int) {}

        inline bool HCLIB_CONSTEXPR_FUNC hasValue() const HCLIB_NOEXCEPT { return false; }
    };

    class BadOptionalAccess : public std::exception
    {
    private:
        const char *message;

    public:

        /**
         * Default constructor.
         */
        BadOptionalAccess() HCLIB_NOEXCEPT;

        /**
         * Default copy constructor.
         */
        BadOptionalAccess(const char* message) HCLIB_NOEXCEPT;

        /**
         * Copy assignment operator.
         */
        BadOptionalAccess(const BadOptionalAccess &) HCLIB_NOEXCEPT = default;

        /**
         * Deleted move assignment operator.
         */
        BadOptionalAccess(BadOptionalAccess &&) HCLIB_NOEXCEPT = default;

        /**
         * Default destructor.
         */
        virtual ~BadOptionalAccess() = default;

        /**
         * Copy assignment operator.
         */
        BadOptionalAccess &operator= (const BadOptionalAccess &) HCLIB_NOEXCEPT = default;

        /**
         * Deleted move assignment operator.
         */
        BadOptionalAccess &operator= (BadOptionalAccess &&) HCLIB_NOEXCEPT = default;

        /**
         * Returns the explanatory string.
         * 
         * @returns const char*.
         */
        virtual const char* what() const HCLIB_NOEXCEPT override;
    };

#if CPP_VER == 2011
    // Do nothing
    in_place_t in_place;
    nullopt_t nullopt(0);

    // template<class T, class U>
    // bool is_same_v = std::is_same<T, U>::value;
#elif CPP_VER == 2014
    constexpr in_place_t in_place;
    constexpr nullopt_t nullopt(0);

    template<class T, class U>
    constexpr bool is_same_v = std::is_same<T, U>::value;
#elif CPP_VER > 2014
    inline constexpr in_place_t in_place;
    inline constexpr nullopt_t nullopt(0);

    template<class T, class U>
    inline constexpr bool is_same_v = std::is_same<T, U>::value;
#else
#error "Unsuported C++ version"
#endif

    template<class T>
    class Optional
    {
#if CPP_VER == 2011
        static_assert(!std::is_same<typename std::remove_cv<T>::type, nullopt_t>::value,
                "Cannot instantiate with type: nullopt_t");
#else
        static_assert(!is_same_v<typename std::remove_cv<T>::type, nullopt_t>,
                "Cannot instantiate with type: nullopt_t");
#endif
    private:
        static constexpr std::size_t sizeT = sizeof(T);

        union Storage
        {
            char dummyVariable; // Needed to silence a benign warning (C++20 extension)
            T obj;
            char buffer[sizeT];

            HCLIB_CONSTEXPR_FUNC Storage();
            ~Storage();
        };

        bool valid;
        Storage storage;

    public:

        /**
         * Default constructor.  Initializes to an invalid Optional.
         */
        HCLIB_CONSTEXPR_FUNC Optional() HCLIB_NOEXCEPT;

        /**
         * Constructor by nullopt_t.  Initializes to an invalid Optional.
         *
         * @param nullopt_t null option.
         */
        HCLIB_CONSTEXPR_FUNC Optional(nullopt_t) HCLIB_NOEXCEPT;

        /**
         * Copy constructor.
         *
         * @param other Optional<T> to copy from.
         */
        HCLIB_CONSTEXPR_FUNC Optional(const Optional<T> &other);

        /**
         * Move constructor.
         *
         * @param other Optional<T> to copy from.
         */
        HCLIB_CONSTEXPR_FUNC Optional(Optional<T> &&other) HCLIB_NOEXCEPT;

        /**
         * Constructor by forwarding arguments to valid object to be created.
         *
         * @param args the variadic arguments forwarded to creating a valid object.
         */
        template<class... Args>
        Optional(in_place_t, Args&&... args);

        /**
         * Destructor.
         */
        ~Optional();

        /**
         * Copy assignment operator.
         *
         * @param the other Optional we are copying.
         */
        HCLIB_CONSTEXPR_FUNC Optional<T> &operator= (const Optional<T> &other);

        /**
         * Move assignment operator.
         *
         * @param the other Optional we are moving from.
         */
        HCLIB_CONSTEXPR_FUNC Optional<T> &operator= (Optional<T> &&other) HCLIB_NOEXCEPT;

        /**
         * Gets the current valid state.
         *
         * @returns bool true if valid Optional, otherwhise false.
         */
        HCLIB_CONSTEXPR_FUNC bool hasValue() const HCLIB_NOEXCEPT;

        /**
         * Gets the stored value.
         * Note: This function may throw an exception if this optional does NOT
         * contain a value.
         *
         * @return T value as reference.
         */
        HCLIB_CONSTEXPR_FUNC T &value();

        /**
         * Gets the (const) stored value.
         * Note: This function may throw an exception if this optional does NOT
         * contain a value.
         *
         * @return T value as const reference.
         */
        HCLIB_CONSTEXPR_FUNC const T &value() const;

        /**
         * Gets the stored value by pointer.
         * Note: This function returns nullptr if the Optional is invalid.
         *
         * @return T value as a pointer.
         */
        HCLIB_CONSTEXPR_FUNC T *pointer();

        /**
         * Gets the stored value by (const) pointer.
         * Note: This function returns nullptr if the Optional is invalid.
         *
         * @return T value as a (const) pointer.
         */
        HCLIB_CONSTEXPR_FUNC const T *pointer() const;

        /**
         * Gets the stored value.
         * Note: This function may throw an exception if this optional does NOT
         * contain a value.
         *
         * @return T value as reference.
         */
        HCLIB_CONSTEXPR_FUNC T &operator* ();

        /**
         * Gets the (const) stored value.
         * Note: This function may throw an exception if this optional does NOT
         * contain a value.
         *
         * @return T value as const reference.
         */
        HCLIB_CONSTEXPR_FUNC const T &operator* () const;

        /**
         * Gets the stored value by pointer.
         * Note: This function returns nullptr if the Optional is invalid.
         *
         * @return T value as a pointer.
         */
        HCLIB_CONSTEXPR_FUNC T *operator-> ();

        /**
         * Gets the stored value by (const) pointer.
         * Note: This function returns nullptr if the Optional is invalid.
         *
         * @return T value as a (const) pointer.
         */
        HCLIB_CONSTEXPR_FUNC const T *operator-> () const;

        /**
         * Operator bool to check if this Optional has a value.
         *
         * @return bool result.
         */
        HCLIB_CONSTEXPR_FUNC explicit operator bool() const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object equality if valid.
         *
         * @param other the other Optional.
         * @returns true if both Optionals are valid AND equal.
         */
        HCLIB_CONSTEXPR_FUNC bool operator== (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object equality if valid.
         *
         * @param other the other Optional.
         * @returns true if both Optionals are valid AND equal.
         */
        HCLIB_CONSTEXPR_FUNC bool operator== (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for in-equality if valid.
         *
         * @param other the other Optional.
         * @returns true if both Optionals are valid AND equal.
         */
        HCLIB_CONSTEXPR_FUNC bool operator!= (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object equality if valid.
         *
         * @param other the other Optional.
         * @returns true if both Optionals are valid AND equal.
         */
        HCLIB_CONSTEXPR_FUNC bool operator!= (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than another Optional object.
         *
         * NOTE: If this is Optional and the other are BOTH valid, returns the
         * result of the comparision to the underlying object otherwise, if
         * this Optional is NOT valid and other is valid, ALWAYS return true,
         * otherwise this function ALWAYS returns false.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator< (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than another Optional object.
         *
         * NOTE: If this is Optional and the other are BOTH valid, returns the
         * result of the comparision to the underlying object otherwise, if
         * this Optional is NOT valid and other is valid, ALWAYS return true,
         * otherwise this function ALWAYS returns false.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator< (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than or equal than another Optional object.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator<= (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than or equal than another Optional object.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator<= (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than another Optional object.
         *
         * NOTE: If this is Optional and the other are BOTH valid, returns the
         * result of the comparision to the underlying object otherwise, if
         * this Optional is NOT valid and other is valid, ALWAYS return true,
         * otherwise this function ALWAYS returns false.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator> (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for less than another Optional object.
         *
         * NOTE: If this is Optional and the other are BOTH valid, returns the
         * result of the comparision to the underlying object otherwise, if
         * this Optional is NOT valid and other is valid, ALWAYS return true,
         * otherwise this function ALWAYS returns false.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator> (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for greater than or equal than another Optional object.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator>= (const Optional<T> &other) const HCLIB_NOEXCEPT;

        /**
         * Compares the underlying object for greater than or equal than another Optional object.
         *
         * @param other the other Optional.
         * @return result per the above NOTE conditions.
         */
        HCLIB_CONSTEXPR_FUNC bool operator>= (nullopt_t other) const HCLIB_NOEXCEPT;

        /**
         * Emplaces/re-/constructs the undelying object with the supplied arguments.
         *
         * @param args Arguments to emplace/re-/construct the object with.
         */
        template<class... Args>
        HCLIB_CONSTEXPR_FUNC T &emplace(in_place_t, Args&&... args);

        /**
         * Destroys the underlying object.
         */
        HCLIB_CONSTEXPR_FUNC void reset() HCLIB_NOEXCEPT;

        /**
         * Swaps the this Optional with the other provided Optional.
         *
         * @param other The other Optional we are swapping with.
         */
        HCLIB_CONSTEXPR_FUNC void swap(Optional<T> &other) HCLIB_NOEXCEPT;
    };

    /**
     * Makes an Optional of type T.
     *
     * @return Optional<T>
     */
    template<class T, class... Args>
    HCLIB_CONSTEXPR_FUNC Optional<T> make_optional(Args&&... args);

    /**
     * Makes an Optional of type T.
     *
     * @return Optional<T>
     */
    template<class T, class... Args>
    HCLIB_CONSTEXPR_FUNC Optional<T> makeOptional(Args&&... args);

    /**
     * Compares the underlying object for less than another Optional object.
     *
     * NOTE: If this is Optional and the other are BOTH valid, returns the
     * result of the comparision to the underlying object otherwise, if
     * this Optional is NOT valid and other is valid, ALWAYS return true,
     * otherwise this function ALWAYS returns false.
     *
     * @param other the other Optional.
     * @return result per the above NOTE conditions.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator< (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

    /**
     * Compares the underlying object for less than or equal than another Optional object.
     *
     * @param other the other Optional.
     * @return result per the above NOTE conditions.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator<= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

    /**
     * Compares the underlying object for less than another Optional object.
     *
     * NOTE: If this is Optional and the other are BOTH valid, returns the
     * result of the comparision to the underlying object otherwise, if
     * this Optional is NOT valid and other is valid, ALWAYS return true,
     * otherwise this function ALWAYS returns false.
     *
     * @param other the other Optional.
     * @return result per the above NOTE conditions.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator> (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

    /**
     * Compares the underlying object for less than or equal than another Optional object.
     *
     * @param other the other Optional.
     * @return result per the above NOTE conditions.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator>= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

    /**
     * Compares the underlying object equality if valid.
     *
     * @param other the other Optional.
     * @returns true if both Optionals are valid AND equal.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator== (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

    /**
     * Compares the underlying object for in-equality if valid.
     *
     * @param other the other Optional.
     * @returns true if both Optionals are valid AND equal.
     */
    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator!= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT;

}

namespace std
{
    template<class T>
    HCLIB_CONSTEXPR_FUNC void swap(hclib::Optional<T> &a, hclib::Optional<T> &b) HCLIB_NOEXCEPT
    {
#if CPP_VER >= 2017
        static_assert(std::is_nothrow_move_constructible_v<T>);
        static_assert(std::is_nothrow_swappable_v<T>);
#endif
        a.swap(b);
    }
}

#ifndef HCLIB_OPTIONAL_INL
#define HCLIB_OPTIONAL_INL
#include "Optional.inl"
#endif //!HCLIB_OPTIONAL_INL

#endif //HCLIB_OPTIONAL_H
