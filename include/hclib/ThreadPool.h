/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#pragma once

#ifndef HCLIB_THREADPOOL_H
#define HCLIB_THREADPOOL_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <queue>
#include <thread>
#include <vector>

#include <hclib/Types.h>
#include <hclib/Conditional.h>
#include <hclib/LockGuard.h>
#include <hclib/Mutex.h>

namespace hclib
{

    class HCLIB_EXPORT ThreadPool
    {
    private:

        // Queue typedef
        using Queue = std::queue<std::function<void()>>;

        // Job queue
        Queue jobs;

        // Queue lock
        Mutex mutex;
        Conditional cond;

        // Active threads
        std::vector<std::thread> threads;

        // For tracking if running.
        std::atomic_bool running;

    public:

        explicit ThreadPool(const u32 = std::thread::hardware_concurrency());
        ThreadPool(const ThreadPool &) = delete;
        ThreadPool(ThreadPool &&) = delete;

        ~ThreadPool();

        ThreadPool &operator= (const ThreadPool) = delete;
        ThreadPool &operator= (ThreadPool &&) = delete;

        inline bool isRunning() const
        {
            return running.load();
        }

        inline std::size_t threadCount() const
        {
            return threads.size();
        }

        // Shutsdown ThreadPool
        void shutdown(const bool waitForJobsToComplete = false);

        // Waits for ThreadPool to drain.
        void wait();

        template<class Function, class... Args>
        void queueJob(Function func, Args &&...args)
        {
            // Ensure we have the lock in order to write to shared data.
            UniqueLock<Mutex> guard(mutex);

            // Enqueue the job into our queue.
            // We cast to void to ignore the return type and so cppcheck doesn't complain.
            (void) jobs.push(std::bind(func, std::forward<Args>(args)...));

            // Notify a child thread that a job has been enqueued.
            cond.signal();
        }

    private:

        void init(const u32 numThreads);

        static void workerThread(ThreadPool &pool, Queue &jobs, Mutex &mutex, Conditional &cond);

    };

}

#endif //HCLIB_THREADPOOL_H
