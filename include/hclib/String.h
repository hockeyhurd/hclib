/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#pragma once

#ifndef HCLIB_STRING_H
#define HCLIB_STRING_H

#include <hclib/Types.h>

#include <limits>
#include <ostream>
#include <string>

#ifndef HCLIB_SMALL_STRING_SIZE
#define HCLIB_SMALL_STRING_SIZE 64
#endif

namespace hclib
{

    class HCLIB_EXPORT String
    {
    protected:

        s32 len;
        mutable char *str;
        mutable char smallStrBuffer[HCLIB_SMALL_STRING_SIZE];

        String(const s32 len, char *str);

    public:

        class iterator;
        class reverse_iterator;
        using const_iterator = const iterator;
        using const_reverse_iterator = const reverse_iterator;

        String();
        String(const char *cstr);
        String(const char *cstr, const s32 len);
        String(const String &other);
        String(String &&other);
        String(const std::string &other);

        virtual ~String();

        String &operator= (const char *cstr);
        String &operator= (const String &other);
        String &operator= (String &&other);
        String &operator= (const std::string &other);

        char &operator[] (s32 index);
        const char &operator[] (s32 index) const;

        inline static HCLIB_CONSTEXPR s32 smallStringSize()
        {
            return HCLIB_SMALL_STRING_SIZE;
        }

        virtual s32 size() const;
        virtual bool isEmpty() const;
        virtual bool isSmallString() const;

        virtual const char *c_str() const;
        virtual std::string toString() const;

        virtual s32 hashCode() const;

        virtual void clear();
        virtual String copy() const;

        // If index < 0 or index >= len, this function will throw std::out_of_range exception.
        virtual char &at(const s32);

        // If index < 0 or index >= len, this function will throw std::out_of_range exception.
        virtual const char &at(const s32) const;

        virtual String &toLowercase();
        virtual String &toUppercase();
        virtual bool contains(const char);
        virtual bool contains(const String &);

        virtual bool startsWith(const char c);
        virtual bool endsWith(const char c);

        virtual s64 indexOf(const char c);
        virtual s64 lastIndexOf(const char c);

        virtual void substring(String &output, const s32 start = 0, const s32 end = 1);
        virtual String substring(const s32 start = 0, const s32 end = 1);

        template<class T, T MAX_SIZE = std::numeric_limits<T>::max()>
        static T stringLength(const char *str)
        {
            T result = static_cast<T>(0);

            if (str == nullptr)
                return result;

            for (char *c = const_cast<char*>(str); *c != '\0' && result < MAX_SIZE; ++c)
            {
                ++result;
            }

            return result;
        }

        virtual String &reverse();
        virtual String reverseCopy() const;

        static s32 hashString(const char *str);

        virtual bool equals(const String &other) const;
        virtual bool equalsIgnoreCase(const String &other) const;

        static void parseInt32(String &ref, s32 value);
        static void parseUInt32(String &ref, u32 value);
        static void parseInt64(String &ref, s64 value);
        static void parseUInt64(String &ref, u64 value);

        static String parseInt32(const s32);
        static String parseUInt32(const u32);
        static String parseInt64(const s64);
        static String parseUInt64(const u64);

        virtual String operator+ (const String &other);
        virtual String operator+ (String &&other);

        virtual String &operator+= (const String &other);
        virtual String &operator+= (String &&other);

        virtual bool operator== (const String &other) const;
        virtual bool operator== (String &&other) const;
        virtual bool operator!= (const String &other) const;
        virtual bool operator!= (String &&other) const;
        virtual bool operator< (const String &other) const;
        virtual bool operator< (String &&other) const;
        virtual bool operator> (const String &other) const;
        virtual bool operator> (String &&other) const;
        virtual bool operator<= (const String &other) const;
        virtual bool operator<= (String &&other) const;
        virtual bool operator>= (const String &other) const;
        virtual bool operator>= (String &&other) const;

        virtual iterator begin() const;
        virtual iterator end() const;
        virtual const_iterator cbegin() const;
        virtual const_iterator cend() const;
        virtual reverse_iterator rbegin() const;
        virtual reverse_iterator rend() const;
        virtual const_reverse_iterator crbegin() const;
        virtual const_reverse_iterator crend() const;

        // NOTE: Should the underlying String structure
        // be expanded past/changed from it's original
        // contents at the time of iterator use, is
        // undefined.

        class HCLIB_EXPORT iterator
        {
        private:

            friend class String;

            char *ch;

            explicit iterator(char *str);

        public:

            iterator(const iterator &) = default;
            iterator(iterator &&) = default;
            ~iterator() = default;

            iterator &operator= (const iterator &) = default;
            iterator &operator= (iterator &&) = default;

            char &operator*();
            char *operator->();

            // Pre-increment
            iterator operator++ ();

            // Post-increment
            iterator operator++ (s32);

            // Pre-increment
            iterator operator-- ();

            // Post-decrement
            iterator operator-- (s32);

            bool operator== (const iterator &other);
            bool operator!= (const iterator &other);

        };

        class HCLIB_EXPORT reverse_iterator
        {
        private:

            friend class String;

            String *str;
            s32 index;

            explicit reverse_iterator(String *str, const s32 index);

        public:

            reverse_iterator(const reverse_iterator &) = default;
            reverse_iterator(reverse_iterator &&) = default;
            ~reverse_iterator() = default;

            reverse_iterator &operator= (const reverse_iterator &) = default;
            reverse_iterator &operator= (reverse_iterator &&) = default;

            char &operator*();
            char *operator->();

            // Pre-increment
            reverse_iterator operator++ ();

            // Post-increment
            reverse_iterator operator++ (s32);

            // Pre-increment
            reverse_iterator operator-- ();

            // Post-decrement
            reverse_iterator operator-- (s32);

            bool operator== (const reverse_iterator &other);
            bool operator!= (const reverse_iterator &other);

        };

    };

    HCLIB_EXPORT std::ostream &operator<< (std::ostream &os, const String &string);

}

#endif //HCLIB_STRING_H
