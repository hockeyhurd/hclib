/**
 * @author hockeyhurd
 * @version 2020-12-22
 */

#pragma once

#ifndef HCLIB_ARG_PARSER_H
#define HCLIB_ARG_PARSER_H

#include <hclib/Types.h>
#include <hclib/StringCache.h>

#include <map>
#include <string>
#include <unordered_map>
#include <vector>

namespace hclib
{
    class HCLIB_EXPORT ArgParser;

    class HCLIB_EXPORT IArg
    {
    public:

        friend ArgParser;
        using StringType = StringView<std::string>;
        using StringCacheType = StringCache<std::string>;

    protected:

        // StringType flag;
        s32 order;
        s32 numValues;

        // NOTE: order='-1' implies this arg can be anywhere on the command-line.
        //       Also, this does not include args with follow-on or numValues
        //       TODO: Create an example here.
        // explicit IArg(StringType flag, const s32 order = -1, const u32 numValues = 0);
        explicit IArg(const s32 order = -1, const s32 numValues = 0);

    public:

        virtual ~IArg() = default;

        // StringType getFlag() const;

        virtual s32 getOrder() const;
        virtual s32 getNumberOfValues() const;

        virtual bool isOrderDependent() const;

        // void registerThis(ArgParser &argParser);
        virtual bool parse(StringCacheType &stringCache, const std::vector<StringType> &options) = 0;
    };

    template<class T>
    class PrimitiveArg : public IArg
    {
    private:

        std::vector<T> results;

    public:

        explicit PrimitiveArg(const s32 order = -1, const u32 numValues = 0) : IArg(order, numValues),
            results(numValues)
        {
#if CPP_VER >= 2014
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif
        }

        ~PrimitiveArg() = default;

        inline std::vector<T> &getResults()
        {
            return results;
        }

        inline const std::vector<T> &getResults() const
        {
            return results;
        }

        bool parse(StringCacheType &stringCache, const std::vector<StringType> &options) override
        {
            // Make sure T is something trivial (ex. int, float, etc.).
            // static_assert(std::is_trivial<T>());

            // Make sure we have enough args...
            // NOTE: options.size() is an std::size_t type
            // but ArgParser will gurantee this value is bounded
            // by the range of [0, UINT32_MAX).
            if (static_cast<s32>(options.size()) != getNumberOfValues())
                return false;

            // Make sure the collection is empty
            results.clear();

            for (const auto &str : options)
            {
                // const auto &actualString = str.string();
                T result;
                // const bool parseResult = parse(actualString, result);
                const bool parseResult = commonParse<IArg::StringType, T>(str, result);

                // Stop parsing if one of the passed options is invalid.
                if (!parseResult)
                    return false;
                else
                    results.push_back(result);
            }

            return true;
        }

    private:

        // Common parse function for trivial arg classes (ex. IntArg, FloatArg, etc.).
        template<class StringType, class OutType>
        static bool commonParse(StringType str, OutType &result)
        {
            return hclib::parse(str.string(), result);
        }

    };

    class HCLIB_EXPORT StringArg : public IArg
    {
    private:

        std::vector<IArg::StringType> results;

    public:

        // explicit StringArg(IArg::StringType flag, const s32 order = -1, const u32 numValues = 0);
        explicit StringArg(const s32 order = -1, const u32 numValues = 0);
        virtual ~StringArg() = default;

        inline std::vector<IArg::StringType> &getResults()
        {
            return results;
        }

        inline const std::vector<IArg::StringType> &getResults() const
        {
            return results;
        }

        virtual bool parse(StringCacheType &stringCache, const std::vector<StringType> &options) override;
    };

    class HCLIB_EXPORT ArgParser
    {
    public:

        using StringType = StringView<std::string>;
        using StringCacheType = StringCache<std::string>;

    protected:

        StringCacheType stringCache;
        std::unordered_map<StringType, IArg*> flagMap;
        std::map<s32, IArg*> orderMap;

    public:

        ArgParser() = default;
        ArgParser(const ArgParser &) = delete;
        ArgParser(ArgParser &&) = default;
        virtual ~ArgParser() = default;

        ArgParser &operator= (const ArgParser &) = delete;
        ArgParser &operator= (ArgParser &&) = default;

        virtual u32 expectedParseResult() const;

        // This will actually perform the parsing of command-line arguments.
        // The result will be the number of args parsed successfully.
        // If an error has occurred, the result will be '-1'.
        // NOTE: This function will assume the program's name is included
        // with the parameters.
        virtual s32 parse(const s32 argc, char *argv[]);

        // Registers an IArg handler.
        // In the event of a duplicate flag, the last
        // registered IArg instance will be chosen.
        // This function returns false if flag is empty
        // OR arg is a nullptr.
        virtual bool registerFlag(const std::string &flag, IArg *arg);
        virtual bool registerFlag(std::string &&flag, IArg *arg);

    protected:

        static bool enoughArgsLeft(const s32 index, const s32 requestedArgs, const s32 argc);

        // Functions calling this function should check validity
        // of the passed values (as needed).
        virtual bool registerFlag(StringType flag, IArg *arg);
    };

}

#endif //!HCLIB_ARG_PARSER_H

