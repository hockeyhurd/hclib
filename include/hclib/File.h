#pragma once

#ifndef HCLIB_FILE_H
#define HCLIB_FILE_H

#include <hclib/Types.h>

#include <string>

#if CPP_17
#include <string_view>
#endif

namespace hclib
{


    class HCLIB_EXPORT File
    {
    public:
        enum class EnumMode
        {
            READ = 0, WRITE, APPEND
        };

    protected:

        struct HCLIB_EXPORT Buffer
        {
            s8 data;

            Buffer(const s8 data = 0);

            bool isEOF() const;
        };

        std::string path;
        EnumMode mode;
        FILE *handle;
        Buffer buffer;
        std::size_t bytesWritten;

    public:

        File(std::string &&path, const EnumMode mode);
        File(const std::string &path, const EnumMode mode);

        File(const File &) = delete;
        File(File &&);

        File &operator= (const File &) = delete;
        File &operator= (File &&other);

        virtual ~File();

        virtual const std::string &getPath() const;
        virtual std::string &getPath();
        virtual EnumMode getMode() const;
        virtual bool isEOF();
        virtual std::size_t size() const;

        virtual s32 getPermissions() const;
        virtual void setPermissions(const s32 perms);

        virtual void flush();
        virtual void close();

        virtual void write(const char data);
        virtual void write(const u32 data);
        virtual void write(const char *data, const std::size_t len);
        virtual void write(const std::string &str);
        virtual void write(std::string &&str);

#if CPP_17
        virtual void write(const std::string_view str);
#endif

        virtual s8 readByte();
        virtual s32 readInt();

        virtual std::string readFile(const std::size_t expectedSize);

        virtual bool remove();
        virtual bool rename(const std::string &name);
        virtual bool rename(std::string &&name);

        static std::string getNameWithoutExtension(const std::string &input);
        static std::string getNameWithoutExtension(std::string &&input);

        static const char *modeName(const EnumMode mode);
        static const char *accessorMode(const EnumMode mode);

#if CPP_17
        static std::string_view name_s(const EnumMode mode);
        static std::string_view accessorMode_s(const EnumMode mode);
#endif

    protected:

        void closeInternal();

    };

}

#endif // !HCLIB_FILE_H
