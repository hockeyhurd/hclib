#pragma once

#ifndef HCLIB_HISTOGRAM_H
#define HCLIB_HISTOGRAM_H

#include <hclib/Types.h>

namespace hclib
{

    template<class T>
    class Histogram
    {
    public:

        using IncrementFunc = void (*)(T&);

    private:

        s32 len;
        T *arr;

    public:

        explicit Histogram(const s32 size);
        Histogram(const Histogram<T> &other);
        Histogram(Histogram &&other);

        ~Histogram();

        Histogram<T> &operator= (const Histogram<T> &other);
        Histogram<T> &operator= (Histogram<T> &&other);

        s32 size() const;
        s32 numBytes() const;

        T *buffer() const;

        T &get(const s32 index);
        void set(const s32 index, const T &data);
        void set(const s32 index, T &&data);

        T &increment(const s32 index);
        T &increment(const s32 index, IncrementFunc func);

        // This will only perform a resize operation
        // IFF the new size is larger than the current
        // size of the buffer.
        //
        // This function returns true if the buffer
        // was successfully resized.  Otherwise, the
        // result is false.
        bool resize(const s32 size);
        void zero();

        // WARNING: Potentially unsafe function
        // since bounds are not checked...
        T &operator[] (const s32 index);
        const T &operator[] (const s32 index) const;

    private:

        // NOTE: This class assumes this buffer's size
        // is equal to the 'other' histogram.
        // Also, the internal buffer is NOT
        // a 'nullptr'.
        void copy(const Histogram<T> &other);

        void cleanup();
    };

}

#include "Histogram.cpp"

#endif //!HCLIB_HISTOGRAM_H
