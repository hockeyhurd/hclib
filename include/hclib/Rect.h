#pragma once

#ifndef HCLIB_RECT_H
#define HCLIB_RECT_H

#include <hclib/Vec2.h>

namespace hclib
{

    template<class T>
    class Rect
    {
    private:

        Vec2<T> pos;
        T width, height;

    public:

        explicit Rect(const T &x, const T &y, const T &width, const T &height);
        explicit Rect(T &&x = T(), T &&y = T(), T &&width = T(), T &&height = T());
        Rect(const Rect<T> &) = default;
        Rect(Rect<T> &&) = default;
        ~Rect() = default;

        Rect<T> &operator= (const Rect<T> &) = default;
        Rect<T> &operator= (Rect<T> &&) = default;

        T &getX();
        void setX(const T &x);
        void setX(T &&x);

        T &getY();
        void setY(const T &y);
        void setY(T &&y);

        Vec2<T> &getPos();
        void setPos(const Vec2<T> &pos);
        void setPos(Vec2<T> &&pos);

        T &getWidth();
        void setWidth(const T &width);
        void setWidth(T &&width);

        T &getHeight();
        void setHeight(const T &height);
        void setHeight(T &&height);

        T area() const;

        Vec2<T> center() const;

        bool operator== (const Rect<T> &other);
        bool operator== (Rect<T> &&other);

        bool operator!= (const Rect<T> &other);
        bool operator!= (Rect<T> &&other);
    };

}

#include <hclib/Rect.cpp>

#endif //!HCLIB_RECT_H
