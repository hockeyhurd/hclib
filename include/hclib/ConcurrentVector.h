/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#ifndef HCLIB_CONCURRENTVECTOR_H
#define HCLIB_CONCURRENTVECTOR_H

#include <hclib/Mutex.h>

#include <vector>

namespace hclib
{

    template<class T, class Allocator = std::allocator<T>>
    class ConcurrentVector
    {

    private:

        using iterator = typename std::vector<T, Allocator>::iterator;
        using const_iterator = typename std::vector<T, Allocator>::const_iterator;

        std::vector<T> vec;
        Mutex mutex;

    public:

        ConcurrentVector() = default;
        ConcurrentVector(const size_t);

        ~ConcurrentVector() = default;

        auto empty();
        auto size();

        void reserve(const size_t);
        void resize(const size_t);

        void clear();

        void push_back(const T &);
        void push_back(T &&);

        template<class ...Args>
        void emplace_back(Args &&...);

        void pop_back();

        iterator erase(iterator pos);
        iterator erase(const_iterator pos);
        iterator erase(iterator first, iterator last);
        iterator erase(const_iterator first, const_iterator last);

        void shrink_to_fit();

        T &at(const size_t);
        T &at(const size_t) const;
        T &operator[] (const size_t);

        auto begin();
        auto rbegin();
        auto end();
        auto rend();

        auto operator== (const ConcurrentVector<T, Allocator> &);
        auto operator!= (const ConcurrentVector<T, Allocator> &);
        auto operator<= (const ConcurrentVector<T, Allocator> &);
        auto operator>= (const ConcurrentVector<T, Allocator> &);
        auto operator< (const ConcurrentVector<T, Allocator> &);
        auto operator> (const ConcurrentVector<T, Allocator> &);

    };
}

#include "ConcurrentVector.cpp"

#endif //HCLIB_CONCURRENTVECTOR_H
