#include <utility>

namespace hclib
{

#define MATRIX_TEMPLATE template<class T, s32 N>
#define MATRIX_T Matrix<T, N>

MATRIX_TEMPLATE
MATRIX_T::Matrix()
{
#if CPP_14
    static_assert(N > 0, "Expected N-dimensions to greater than '0'");
#endif
}

MATRIX_TEMPLATE
MATRIX_T::Matrix(const T &defaultValue)
{
#if CPP_14
    static_assert(N > 0, "Expected N-dimensions to greater than '0'");
#endif

    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] = defaultValue;
        }
    }
}

MATRIX_TEMPLATE
MATRIX_T MATRIX_T::identity(const T &identityValue, const T &defaultValue)
{
    MATRIX_T result;

    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            if (row == col)
                result.data[row][col] = identityValue;
            else
                result.data[row][col] = defaultValue;
        }
    }

    return result;
}

MATRIX_TEMPLATE
void MATRIX_T::zero(const T &defaultValue)
{
    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] = defaultValue;
        }
    }
}

MATRIX_TEMPLATE
const T &MATRIX_T::at(const s32 row, const s32 col) const
{
    if (row < 0 || row >= N || col < 0 || col >= N)
        throw std::out_of_range("row or col out of range");

    return data[row][col];
}

MATRIX_TEMPLATE
T &MATRIX_T::at(const s32 row, const s32 col)
{
    if (row < 0 || row >= N || col < 0 || col >= N)
        throw std::out_of_range("row or col out of range");

    return data[row][col];
}

MATRIX_TEMPLATE
bool MATRIX_T::set(const s32 row, const s32 col, const T &input)
{
    if (row < 0 || row >= N || col < 0 || col >= N)
        return false;

    data[row][col] = input;

    return true;
}

MATRIX_TEMPLATE
bool MATRIX_T::set(const s32 row, const s32 col, T &&input)
{
    if (row < 0 || row >= N || col < 0 || col >= N)
        return false;

    data[row][col] = std::forward<T>(input);

    return true;
}

MATRIX_TEMPLATE
std::array<T, N> &MATRIX_T::operator[] (const std::size_t index)
{
    return data[index];
}

MATRIX_TEMPLATE
const std::array<T, N> &MATRIX_T::operator[] (const std::size_t index) const
{
    return data[index];
}

MATRIX_TEMPLATE
MATRIX_T MATRIX_T::dot(const MATRIX_T &other)
{
    using SizeT = typename std::array<T, N>::size_type;
    MATRIX_T result(static_cast<T>(0));

    for (SizeT row = 0; row < data.size(); ++row)
    {
        for (SizeT col = 0; col < data[row].size(); ++col)
        {
            for (SizeT colIndex = 0; colIndex < data.size(); ++colIndex)
            {
                result[row][col] += data[row][colIndex] * other.data[colIndex][col];
            }
        }
    }

    return result;
}

// TODO: Re-implement
#if 0
MATRIX_TEMPLATE
MATRIX_T &MATRIX_T::cross(const MATRIX_T &other)
{
    const auto lenSquared = len * len;

    for (auto i = 0; i < lenSquared; ++i)
    {
        data[i] *= other.data[i];
    }

    return *this;
}
#endif

MATRIX_TEMPLATE
MATRIX_T &MATRIX_T::operator+= (const MATRIX_T &other)
{
    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] += other.data[row][col];
        }
    }

    return *this;
}

MATRIX_TEMPLATE
MATRIX_T &MATRIX_T::operator-= (const MATRIX_T &other)
{
    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] -= other.data[row][col];
        }
    }

    return *this;
}

MATRIX_TEMPLATE
MATRIX_T &MATRIX_T::operator*= (const MATRIX_T &other)
{
    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] *= other.data[row][col];
        }
    }

    return *this;
}

MATRIX_TEMPLATE
MATRIX_T &MATRIX_T::operator/= (const MATRIX_T &other)
{
    for (s32 row = 0; row < N; ++row)
    {
        for (s32 col = 0; col < N; ++col)
        {
            data[row][col] /= other.data[row][col];
        }
    }

    return *this;
}

/*MATRIX_TEMPLATE
T &MATRIX_T::operator[] (const s32 index)
{
    return data[index];
}

MATRIX_TEMPLATE
const T &MATRIX_T::operator[] (const s32 index) const
{
    return data[index];
}*/

#undef MATRIX_TEMPLATE
#undef MATRIX_T

}
