/**
 * @author hockeyhurd
 * @version 2019-09-01
 */

#pragma once

#ifndef HCLIB_BITFIELD_H
#define HCLIB_BITFIELD_H

#include <hclib/Hashable.h>

namespace hclib
{

    class HCLIB_EXPORT BitField : public Hashable
    {

    private:

        u32 data;
        u32 index;

    public:

        explicit BitField(const u32 data = 0u) HCLIB_NOEXCEPT;
        BitField(const BitField &) HCLIB_NOEXCEPT = default;
        BitField(BitField &&) HCLIB_NOEXCEPT = default;
        ~BitField() HCLIB_NOEXCEPT = default;

        BitField &operator= (const BitField &) = default;
        BitField &operator= (BitField &&) = default;

        inline HCLIB_CONSTEXPR_FUNC u32 capacity() const HCLIB_NOEXCEPT
        {
            return sizeof(data) << 3;
        }

        u32 getData() const HCLIB_NOEXCEPT;

        bool isFull() const HCLIB_NOEXCEPT;

        bool get(u32 index) const HCLIB_NOEXCEPT;
        void set(u32 index, const bool value) HCLIB_NOEXCEPT;
        u32 add(const bool value) HCLIB_NOEXCEPT;

        void zero() HCLIB_NOEXCEPT;

        s32 hashCode() const override;

    };

}

#endif //!HCLIB_BITFIELD_H
