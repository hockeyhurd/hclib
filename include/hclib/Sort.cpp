#include <hclib/Types.h>

#include <algorithm>
#include <utility>

namespace hclib
{

    template<class T>
    static void merge(T &result, const std::size_t len, const T &left, const T &right)
    {
        std::size_t i = 0;
        std::size_t j = 0;
        std::size_t k = 0;

        while (i < len)
        {
            if (left[j] <= right[k])
            {
                result.push_back(left[j++]);
                ++i;
            }

            else
            {
                result.push_back(right[k++]);
                ++i;
            }

            // End of the left side, the result is the remaining
            // right elements.
            if (j == left.size())
            {
                for (; k < right.size(); ++k)
                {
                    result.push_back(right[k]);
                    ++i;
                }

                break;
            }

            // End of the right side, the result is the remaining
            // left elements.
            if (k == right.size())
            {
                for (; j < left.size(); ++j)
                {
                    result.push_back(left[j]);
                    ++i;
                }

                break;
            }
        }
    }

    template<class T, class Compare>
    static T mergeSort(T &container, Compare compare, const std::size_t low, const std::size_t high)
    {
        const std::size_t diff = high - low + 1;
        if (diff == 1)
        {
            T result;
            result.push_back(container[low]);
            return result;
        }

        else if (diff == 2)
        {
            T result;

            // if (container[low] <= container[high])
            if (compare(container[low], container[high]))
            {
                result.push_back(container[low]);
                result.push_back(container[high]);
            }

            else
            {
                result.push_back(container[high]);
                result.push_back(container[low]);
            }

            return result;
        }

        const std::size_t mid = (low + high) / 2;

        auto left = mergeSort(container, compare, low, mid);
        auto right = mergeSort(container, compare, mid + 1, high);

        T result;

        merge(result, diff, left, right);
        return result;
    }

    template<class T, class Compare>
    void mergeSort(T &container, Compare compare)
    {
        // If the container is empty, nothing todo...
        if (container.size() == 0)
            return;

        const std::size_t low = 0;
        const std::size_t high = container.size();
        const std::size_t mid = (low + high) / 2;

        auto left = mergeSort(container, compare, low, mid);
        auto right = mergeSort(container, compare, mid + 1, high - 1);

        container.clear();
        merge(container, high, left, right);
    }

    // template<class T, class Compare>
    template<class Container, class T, class Compare>
    static void quickSort(Container &container, Compare compare, const s64 min, const s64 max)
    {
        if (min >= max)
            return;

        // const s64 midIndex = (min + max) / 2;
        const s64 midIndex = max;
        const T &mid = container[midIndex];

        s64 i = min - 1;

        // for (s64 j = min; j < max; ++j)
        for (s64 j = min; j <= max - 1; ++j)
        {
            auto &elem = container[j];

            if (compare(elem, mid))
            // if (compare(mid, elem))
            {
                ++i;
                std::swap(container[i], elem);
            }
        }

        const s64 pi = i + 1;
        // std::swap(container[pi], container[midIndex]);
        std::swap(container[pi], container[max]);

        quickSort<Container, T, Compare>(container, compare, min, pi - 1);
        quickSort<Container, T, Compare>(container, compare, pi + 1, max);
    }

    // template<class T, class Compare>
    template<class Container, class T, class Compare>
    void quickSort(Container &container, Compare compare)
    {
        const auto size = static_cast<s64>(container.size());

        if (size > 0)
        {
            quickSort<Container, T, Compare>(container, compare, 0, size - 1);
            // quickSort<Container, T, Compare>(container, compare, 0, container.size() - 1);
        }
    }

}
