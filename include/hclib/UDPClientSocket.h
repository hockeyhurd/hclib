/*
 * Class for creating and using Posix style UDP client side sockets.
 *
 * @author hockeyhurd
 * @version 8/22/2021
 */

#pragma once

#ifndef HCLIB_UDP_CLIENT_SOCKET_H
#define HCLIB_UDP_CLIENT_SOCKET_H

#include <hclib/Types.h>
#include <hclib/Socket.h>

namespace hclib
{

    class HCLIB_EXPORT UDPClientSocket : public Socket
    {
    protected:

        std::string address;
        s32 port;
        s32 ttl;

    public:

        /**
         * Constructs a UDPClientSocket object.  This makes no attempt to validate
         * the input values though it is assumed the address is real and port > 0
         * is valid otherwise, invalid.
         *
         * @param address std::string address of the server to send data to.
         * @param port s32 port number of the server to send data to.
         * @param ttl sets the time to live.
         */
        UDPClientSocket(const std::string &address, const s32 port, const u8 ttl = 64);

        /**
         * Constructs a UDPClientSocket object.  This makes no attempt to validate
         * the input values though it is assumed the address is real and port > 0
         * is valid otherwise, invalid.
         *
         * @param address std::string address of the server to send data to.
         * @param port s32 port number of the server to send data to.
         * @param ttl sets the time to live.
         */
        UDPClientSocket(std::string &&address, const s32 port, const u8 ttl = 64);

        /**
         * Deleted copy constructor per parent Socket class requirement.
         */
        UDPClientSocket(const UDPClientSocket &other) = delete;

        /**
         * Default move constructor.
         */
        UDPClientSocket(UDPClientSocket &&) = default;

        /**
         * Default destructor.
         */
        virtual ~UDPClientSocket() = default;

        /**
         * Deleted copy assignment operator per parent Socket class requirement.
         */
        UDPClientSocket &operator= (const UDPClientSocket &) = delete;

        /**
         * Default move assignment operator.
         */
        UDPClientSocket &operator= (UDPClientSocket &&) = default;

        /**
         * Gets the address of the server we are sending data to.
         *
         * @return std::string address.
         */
        virtual const std::string &getAddress() const;

        /**
         * Gets the address of the server we are sending data to.
         *
         * @return std::string address.
         */
        virtual std::string &getAddress();

        /**
         * Sets the address of this socket.
         * Note: It is up to the caller to ensure the socket has
         * been closed first otherwise this could lead to
         * undefined behavior.
         *
         * @param address std::string address of the server we are
         * attempting to data to.
         */
        virtual void setAddress(const std::string &address);

        /**
         * Sets the address of this socket.
         * Note: It is up to the caller to ensure the socket has
         * been closed first otherwise this could lead to
         * undefined behavior.
         *
         * @param address std::string address of the server we are
         * attempting to send data to.
         */
        virtual void setAddress(std::string &&address);

        /**
         * Gets the port we are sending data to.
         *
         * @return s32 port number.
         */
        virtual s32 getPort() const;

        /**
         * Sets the port number of the destination server to send data to.
         * Note: It is up to the caller to ensure the socket has
         * been closed first otherwise this could lead to
         * undefined behavior.
         *
         * @param port s32 port number.
         */
        virtual void setPort(const s32 port);

        /**
         * Gets the time to live (TTL) assigned to this UDPServerSocket.
         *
         * @return s32 time to live.
         */
        virtual s32 getTTL() const HCLIB_NOEXCEPT;

        /**
         * Sets the TTL if the socket option can be set.
         *
         * @param ttl u8 time to live value to set.
         */
        virtual void setTTL(const u8 ttl);

        /**
         * Checks the internal socket handle to determine if we are initialized.
         * to the server or not.
         *
         * @return bool true if initialized, otherwise bool false.
         */
        virtual bool isSetup() const;

        /**
         * Attempts to open a socket capable of sending data to the server over Posix
         * UDP.  If this fails, an std::runtime_error exception shall be thrown.
         */
        virtual void open();

        /**
         * Attempts to close the socket socket.
         * If this fails, an std::runtime_error exception shall be thrown.
         */
        virtual void close() override;

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer Data buffer to send.
         * @param len The size of the buffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(const char *buffer, const std::size_t len) const override;

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer DataBuffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(const DataBuffer &buffer) const override;

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer DataBuffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(DataBuffer &&buffer) const override;

        /**
         * Receives a data buffer from the destination.
         *
         * @param buffer Data buffer to put data into.
         * @param len The length of the buffer/max number of bytes able to receive.
         *
         * @return The s32 result of the operation.
         */
        virtual s32 receive(char *buffer, const s32 len) const override;

        /**
         * Receives a data buffer from the destination.
         *
         * @param buffer DataBuffer to put data into.
         * @param len The length of the buffer/max number of bytes able to receive.
         *
         * @return The s32 result of the operation.
         */
        virtual s32 receive(DataBuffer &buffer, const s32 len) const override;

    protected:

        /**
         * Actually sets the TTL socket options.
         */
        virtual void setTTLInternal();
    };

}

#endif //!HCLIB_UDP_CLIENT_SOCKET_H

