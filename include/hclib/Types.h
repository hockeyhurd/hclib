/**
 * @author hockeyhurd
 * @version 2019-02-15
 */

#pragma once

#ifndef HCLIB_TYPES_H
#define HCLIB_TYPES_H

#if defined(_DEBUG) || defined(DEBUG_BUILD)
#define DebugMode 1u
#else
#define DebugMode 0u
#endif

#ifdef __cplusplus
#define TYPES_CPP 1
#else
#define TYPES_CPP 0
#endif

#ifdef __APPLE__
#define OS_APPLE 1
#define OS_WIN 0
#define OS_UNIX 1
#elif WIN32 || _WIN64
#define OS_APPLE 0
#define OS_WIN 1
#define OS_UNIX 0
#elif __unix__
#define OS_APPLE 0
#define OS_WIN 0
#define OS_UNIX 1
#endif

#include <cstdlib>
#include <cstdint>
#include <cfloat>
#include <sstream>

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#if OS_WIN
// #include <math.h>
#include <cmath>
#else
#include <cmath>
#endif
#endif

#if defined(__GNUC__)
#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED(func) func
#endif

#ifndef HCLIB_EXPORT
#if OS_WIN
#define HCLIB_IMPORT __declspec(dllimport)
#define HCLIB_EXPORT __declspec(dllexport)
#else
#define HCLIB_IMPORT
#define HCLIB_EXPORT
#endif
#endif

#ifndef True
#define True 1
#endif

#ifndef False
#define False 0
#endif

#if (__cplusplus == 199711L) // C++98
#define CPP_98 1
#define CPP_11 0
#define CPP_14 0
#define CPP_17 0
#define CPP_20 0
#define CPP_VER 1998
#elif (__cplusplus == 201103L) // C++11
#define CPP_98 1
#define CPP_11 1
#define CPP_14 0
#define CPP_17 0
#define CPP_20 0
#define CPP_VER 2011
#elif (__cplusplus == 201402L) // C++14
#define CPP_98 1
#define CPP_11 1
#define CPP_14 1
#define CPP_17 0
#define CPP_20 0
#define CPP_VER 2014
#elif (__cplusplus == 201703L || __cplusplus > 201402L) // C++17
#define CPP_98 1
#define CPP_11 1
#define CPP_14 1
#define CPP_17 1
#define CPP_20 0
#define CPP_VER 2017
#elif (__cplusplus == 202002L) // C++20
#define CPP_98 1
#define CPP_11 1
#define CPP_14 1
#define CPP_17 1
#define CPP_20 1
#define CPP_VER 2020
#else // C++ (pre-98)
#define CPP_98 0
#define CPP_11 0
#define CPP_14 0
#define CPP_17 0
#define CPP_20 0
#define CPP_VER 1997
#endif

#if CPP_14
#define HCLIB_NOEXCEPT noexcept
#define HCLIB_CONSTEXPR constexpr
#define HCLIB_CONSTEXPR_FUNC constexpr
#elif CPP_11
#define HCLIB_NOEXCEPT noexcept
#define HCLIB_CONSTEXPR constexpr
#define HCLIB_CONSTEXPR_FUNC
#else
#define HCLIB_NOEXCEPT
#define HCLIB_CONSTEXPR
#define HCLIB_CONSTEXPR_FUNC
#endif

namespace hclib
{

    typedef std::uint8_t u8;
    typedef std::uint16_t u16;
    typedef std::uint32_t u32;
    typedef std::uint64_t u64;

    typedef std::int8_t s8;
    typedef std::int16_t s16;
    typedef std::int32_t s32;
    typedef std::int64_t s64;

    typedef u32 B32;
    typedef u32 b32;

    typedef std::size_t pint;

    typedef float f32;
    typedef double f64;

    typedef float r32;
    typedef double r64;

    const f32 f32_PI = 3.14159265358979323846264338327950288F;
    const f64 f64_PI = 3.14159265358979323846264338327950288;

    const f32 f32_2_PI = 6.28318530717958647692528676655900576F;
    const f64 f64_2_PI = 6.28318530717958647692528676655900576;

    const f32 f32_HALF_PI = 1.57079632679489661923132169163975144F;
    const f64 f64_HALF_PI = 1.57079632679489661923132169163975144;

    const f32 f32_QUARTER_PI = 0.78539816339744830961566084581987572F;
    const f64 f64_QUARTER_PI = 0.78539816339744830961566084581987572;

    const f32 f32_EIGHTH_PI = 0.39269908169872415480783042290993786F;
    const f64 f64_EIGHTH_PI = 0.39269908169872415480783042290993786;

    const f32 f32_e = 2.71828182845904523536F;
    const f64 f64_e = 2.71828182845904523536;

#ifndef ENUMENDIAN
#define ENUMENDIAN
    enum class EnumEndian
    {
        LITTLE = 0, BIG = 1
    };
#endif

    inline HCLIB_CONSTEXPR_FUNC u32 pointerSize() HCLIB_NOEXCEPT
    {
        return sizeof(pint);
    }

    inline HCLIB_CONSTEXPR_FUNC char toLower(char &c) HCLIB_NOEXCEPT
    {
        return c >= 'A' && c <= 'Z' ? c += 0x20 : c;
    }

    inline HCLIB_CONSTEXPR_FUNC char toUpper(char &c) HCLIB_NOEXCEPT
    {
        return c >= 'a' && c <= 'z' ? c -= 0x20 : c;
    }

    HCLIB_EXPORT f32 roundTwoPlaces(f32);
    HCLIB_EXPORT f64 roundTwoPlaces(f64);

    template<class T>
    inline HCLIB_CONSTEXPR_FUNC const T &min(const T &left, const T &right)
    {
        return left <= right ? left : right;
    }

    template<class T>
    inline HCLIB_CONSTEXPR_FUNC const T &max(const T &left, const T &right)
    {
        return left >= right ? left : right;
    }

    template<class T, class Iter>
    T distance(Iter begin, Iter end)
    {
        T result = static_cast<T>(0);
        for (auto iter = begin; iter != end; ++iter)
            ++result;

        return result;
    }

    template<class Iter, class T>
    void fill(Iter begin, Iter end, const T &value)
    {
        for (; begin != end; ++begin)
        {
            *begin = value;
        }
    }

    template<class Iter, class Func>
    Func forEach(Iter begin, Iter end, Func func)
    {
        for (auto iter = begin; iter != end; ++iter)
            func(*iter);

        return func;
    }

    HCLIB_EXPORT void blockCopy(void *buf, const std::size_t size, const char value = '\0');

    template<class T, class Str>
    bool parse(const Str &str, T &result)
    {
        std::istringstream stream(str);
        stream >> result;

        return stream.good() || stream.eof();
    }
}

#endif //HCLIB_TYPES_H
