#include <exception>

namespace hclib
{

    class UnGuardedException : public std::exception
    {
    public:

        virtual const char *what() const throw()
        {
            return "Data not guarded exception";
        }
    };

#define LOCK_TEMPLATE template<class T>
#define LOCK_OBJ Lock<T>

#if CPP_17
#define RWLOCK_TEMPLATE template<class T>
#define RWLOCK_OBJ RWLock<T>
#endif

LOCK_TEMPLATE
std::pair<std::unique_lock<std::mutex>, T*> LOCK_OBJ::get()
{
    return std::make_pair(std::unique_lock<std::mutex>(mutex), &value);
}

LOCK_TEMPLATE
void LOCK_OBJ::set(const T &value)
{
    std::unique_lock<std::mutex> guard(mutex);
    this->value = value;
}

LOCK_TEMPLATE
void LOCK_OBJ::set(T &&value)
{
    std::unique_lock<std::mutex> guard(mutex);
    this->value = std::forward<T>(value);
}

#if CPP_17

LOCK_TEMPLATE
std::pair<std::shared_lock<std::shared_mutex>, T*> RWLOCK_OBJ::get()
{
    return std::make_pair(std::shared_lock<std::shared_mutex>(mutex), &value);
}

LOCK_TEMPLATE
void RWLOCK_OBJ::set(const T &value)
{
    std::unique_lock<std::shared_mutex> guard(mutex);
    this->value = value;
}

LOCK_TEMPLATE
void RWLOCK_OBJ::set(T &&value)
{
    std::unique_lock<std::shared_mutex> guard(mutex);
    this->value = std::forward<T>(value);
}

#endif

#undef LOCK_TEMPLATE
#undef LOCK_OBJ

#if CPP_17
#undef RWLOCK_TEMPLATE
#undef RWLOCK_OBJ
#endif

}
