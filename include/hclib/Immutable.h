/**
 * Immutable class for storing immutable data.
 *
 * @author hockeyhurd
 * @version 2022-02-13
 */

#pragma once

#ifndef HCLIB_IMMUTABLE_H
#define HCLIB_IMMUTABLE_H

// HCLib includes
#include <hclib/Types.h>

namespace hclib
{
    template<class T>
    class Immutable
    {
    private:
        const T data;

    public:

        /**
         * Default constructor.
         */
        HCLIB_CONSTEXPR_FUNC Immutable() HCLIB_NOEXCEPT;

        /**
         * Copy constructor.
         * 
         * @param other other Immutable<T>.
         */
        HCLIB_CONSTEXPR_FUNC explicit Immutable(const Immutable<T> &other) = default;

        /**
         * Move constructor.
         * 
         * @param other other Immutable<T>.
         */
        HCLIB_CONSTEXPR_FUNC explicit Immutable(Immutable<T> &&other) HCLIB_NOEXCEPT = default;

        /**
         * Copy coonstructor of the data.
         * 
         * @param other other Immutable<T>.
         */
        HCLIB_CONSTEXPR_FUNC explicit Immutable(const T &data);

        /**
         * Move coonstructor of the data.
         * 
         * @param other other Immutable<T>.
         */
        HCLIB_CONSTEXPR_FUNC explicit Immutable(T &&data) HCLIB_NOEXCEPT;

        /**
         * Default destructor.
         */
        ~Immutable() = default;

        /**
         * Deleted copy assignment.
         */
        HCLIB_CONSTEXPR_FUNC Immutable &operator= (const Immutable<T> &other) = delete;

        /**
         * Deleted move assignment.
         */
        HCLIB_CONSTEXPR_FUNC Immutable &operator= (Immutable<T> &&other) = delete;

        /**
         * Deleted copy assignment,
         */
        HCLIB_CONSTEXPR_FUNC Immutable &operator= (const T &data) = delete;

        /**
         * Deleted move assignment,
         */
        HCLIB_CONSTEXPR_FUNC Immutable &operator= (T &&data) = delete;

        /**
         * Gets the underlying data as immutable const ref.
         *
         * @return const ref T.
         */
        HCLIB_CONSTEXPR_FUNC const T &getData() const HCLIB_NOEXCEPT;
    };
}

#ifndef HCLIB_IMMUTABLE_INL
#define HCLIB_IMMUTABLE_INL
#include <hclib/Immutable.inl>
#endif //!HCLIB_IMMUTABLE_INL

#endif //!HCLIB_IMMUTABLE_H

