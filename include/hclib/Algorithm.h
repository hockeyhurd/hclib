/**
 * @author hockeyhurd
 * @version 2020-11-06
 */

#pragma once

#ifndef HCLIB_ALGORITHM_H
#define HCLIB_ALGORITHM_H

#include <hclib/Types.h>

#include <array>

namespace hclib
{
    namespace algorithm 
    {

        template<class Iter>
        class BoyerMoore
        {
        private:
            Iter patternBegin;
            Iter patternEnd;
            s64 patternSize;

        public:
            BoyerMoore(Iter patternBegin, Iter patternEnd);
            BoyerMoore(const BoyerMoore &) = default;
            BoyerMoore(BoyerMoore &&) = default;
            ~BoyerMoore() = default;

            BoyerMoore &operator= (const BoyerMoore &) = default;
            BoyerMoore &operator= (BoyerMoore &&) = default;

            Iter search(Iter begin, Iter end);

        private:

            template<class T, std::size_t CAPACITY>
            void setupDeltaTable(std::array<T, CAPACITY> &table) const;

        };

        template<class Iter, class Searcher>
        Iter stringSearch(Iter begin, Iter end, Searcher &searcher)
        {
            return searcher.search(begin, end);
        }

        template<class Iter, class Compare>
        bool isSorted(Iter begin, Iter end, Compare compare);

        template<class T, class String>
        String toHexString(const T &value);

        template<class T, class String>
        String toHexString(T &&value);

    }
}

#include <hclib/Algorithm.inl>

#endif //!HCLIB_ALGORITHM_H

