/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#ifndef HCLIB_HASHABLE_H
#define HCLIB_HASHABLE_H

#include <hclib/Types.h>

namespace hclib
{

    class HCLIB_EXPORT Hashable
    {

    protected:

        Hashable() = default;

    public:

        virtual ~Hashable() = default;

        virtual s32 hashCode() const = 0;
        virtual s32 operator% (const s32) const;

    };
}


#endif //HCLIB_HASHABLE_H
