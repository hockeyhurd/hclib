/**
 * @author hockeyhurd
 * @version 2019-02-17
 */

#ifndef HCLIB_CONCURRENTQUEUE_H
#define HCLIB_CONCURRENTQUEUE_H

#include <queue>

#include <hclib/Mutex.h>

namespace hclib
{

    template<typename T>
    class ConcurrentQueue
    {

    private:

        Mutex mutex;
        std::queue<T> queue;

    public:

        ConcurrentQueue() = default;
        ConcurrentQueue(const ConcurrentQueue<T> &) = delete;
        ConcurrentQueue(ConcurrentQueue<T> &&) = default;

        ConcurrentQueue<T> &operator= (const ConcurrentQueue<T> &) = delete;
        ConcurrentQueue<T> &operator= (ConcurrentQueue<T> &&);

        b32 isEmpty();

        void push(const T &);
        void push(T &&);

        T &top();

        void pop();

    };

}

#include "ConcurrentQueue.cpp"

#endif //HCLIB_CONCURRENTQUEUE_H
