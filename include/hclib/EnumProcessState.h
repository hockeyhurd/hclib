/**
 * @author hockeyhurd
 * @version 2021-10-26
 */

#pragma once

#ifndef HCLIB_ENUM_PROCESS_STATE_H
#define HCLIB_ENUM_PROCESS_STATE_H

// My includes
#include <hclib/Types.h>

namespace hclib
{
    class HCLIB_EXPORT EnumProcessState
    {
    private:
        s32 value;
        const char *mName;

        EnumProcessState(const s32 value, const char *name);

    public:

        // TODO: Support more??
        static const EnumProcessState UNKNOWN;
        static const EnumProcessState DEFAULT;
        static const EnumProcessState RUNNING;
        static const EnumProcessState STOPPED;

        EnumProcessState(const EnumProcessState &) = default;
        EnumProcessState(EnumProcessState &&) = default;
        ~EnumProcessState() = default;

        EnumProcessState &operator= (const EnumProcessState &) = default;
        EnumProcessState &operator= (EnumProcessState &&) = default;

        s32 ordinal() const;
        const char *name() const;

        static EnumProcessState getState(const s32 value);

        bool operator== (const EnumProcessState &other) const;
        bool operator== (EnumProcessState &&other) const;
        bool operator!= (const EnumProcessState &other) const;
        bool operator!= (EnumProcessState &&other) const;
    };
}

#endif

