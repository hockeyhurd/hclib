/**
 * A class for easy gap buffering.
 *
 * @author hockeyhurd
 * @version 2023-10-19
 */

#pragma once

#ifndef HCLIB_GAPBUFFER_H
#define HCLIB_GAPBUFFER_H

// HCLib includes
#include <hclib/Types.h>

namespace hclib
{

    class GapBuffer
    {
    public:

        /**
         * Default constructor.
         */
        GapBuffer(std::size_t gapSize, std::size_t bufferSize);

        /**
         * Constructor with an initial string value.
         *
         * @param gapSize the initial gap size as std::size_t.
         * @param str the std::string to initialize to.
         */
        GapBuffer(std::size_t gapSize, const std::string& str);

        /**
         * Default copy constructor.
         */
        GapBuffer(const GapBuffer&);

        /**
         * Default move constructor.
         */
        GapBuffer(GapBuffer&&) HCLIB_NOEXCEPT;

        /**
         * Default destructor.
         */
        virtual ~GapBuffer();

        /**
         * Copy assignment operator.
         */
        GapBuffer& operator= (const GapBuffer&);

        /**
         * Move assignment operator.
         */
        GapBuffer& operator= (GapBuffer&&) HCLIB_NOEXCEPT;

        /**
         * Gets the raw char buffer.
         *
         * @return pointer to the start of the char buffer.
         */
        virtual const char* data() const HCLIB_NOEXCEPT;

        /**
         * Gets the position in the buffer.
         *
         * @return std::size_t.
         */
        virtual std::size_t getCurrentPosition() const HCLIB_NOEXCEPT;

        /**
         * Gets the size of the GapBuffer.
         * Note: this is the total size of the buffer's structure,
         *       not to be confused with the capacity since
         *       the buffer expands upon the gap's size becoming '0'.
         *
         * @return std::size_t.
         */
        virtual std::size_t size() const HCLIB_NOEXCEPT;

        /**
         * Nullifies the underlying buffer.
         */
        virtual void clear() HCLIB_NOEXCEPT;

        /**
         * Erases the character at the current position within the char buffer.
         *
         * @return true on success, else false.
         */
        virtual bool erase() HCLIB_NOEXCEPT;

        /**
         * Erases the character at the given position within the char buffer.
         * Note: This is effectively calling move() followed by erase().
         *
         * @param pos std::size_t.
         * @return true on success, else false.
         */
        virtual bool erase(std::size_t pos) HCLIB_NOEXCEPT;

        /**
         * Inserts a character at the current position within the char buffer.
         */
        virtual void insert(char value);

        /**
         * Inserts a character at the given position within the char buffer.
         * Note: This is effectively calling move() followed by insert().
         *
         * @param value char.
         * @param pos std::size_t.
         */
        virtual void insert(char value, std::size_t pos);

        /**
         * Moves the current position within the buffer.
         *
         * @param pos std::size_t.
         */
        virtual void move(std::size_t pos) HCLIB_NOEXCEPT;

        /**
         * Reserves an initial capacity for the buffer conditionally resizing.
         *
         * @param capacity the new std::size_t capacity of the char buffer.
         */
        virtual void reserve(std::size_t capacity);

        /**
         * Searches the buffer for a string of text.
         *
         * @param text std::string text.
         * @param startPos std::size_t optional starting position.
         * @return std::size_t position if found else returns equivalent to std::string::npos (-1).
         */
        virtual std::size_t search(const std::string& text, std::size_t startPos = 0) const;

        /**
         * Gets an std::string representation of the buffer, skipping over the gap.
         *
         * @return std::string.
         */
        virtual std::string toString() const;

        /**
         * Equivalency operator overload.
         *
         * @param other GapBuffer to compare against.
         */
        bool operator== (const GapBuffer& other) const HCLIB_NOEXCEPT;

        /**
         * Non-equivalency operator overload.
         *
         * @param other GapBuffer to compare against.
         */
        bool operator!= (const GapBuffer& other) const HCLIB_NOEXCEPT;

    protected:

        /**
         * Gets the current size of the gap in the GapBuffer.
         *
         * @return std::size_t.
         */
        virtual std::size_t getCurrentGapSize() const HCLIB_NOEXCEPT;

        /**
         * Resizes the gap in the GapBuffer.
         */
        virtual void resizeBuffer();

    protected:

        static const char emptyFiller;

        std::size_t gapSize;
        char* buffer;
        char* startGap;
        char* endGap;
        char* endBuffer;
    };
}

#endif //!HCLIB_GAPBUFFER_H

