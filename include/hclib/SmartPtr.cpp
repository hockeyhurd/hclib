/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

// #include <smartptr.h>

#include <utility>

#define TEMPLATE template<typename T>
#define TEMPLATE_ARGS template<typename ... Args>

namespace hclib
{

    TEMPLATE
    PType<T>::PType(const b32 isArray) : isArray(isArray)
    {

    }

    TEMPLATE
    SharedPtr<T>::SharedPtr(UniquePtr <T> &uniquePtr) : PType<T>(uniquePtr.isArray),
                                                        ptr(uniquePtr.ptr), refCount(new std::size_t(1))
    {

        uniquePtr.ptr = nullptr;
    }

    TEMPLATE
    SharedPtr<T>::SharedPtr() : ptr(nullptr), refCount(nullptr)
    {

    }

    TEMPLATE
    SharedPtr<T>::SharedPtr(T *ptr) : ptr(ptr != nullptr ? ptr : nullptr),
                                            refCount(new std::size_t(ptr != nullptr ? 1 : 0))
    {


    }

    TEMPLATE
    SharedPtr<T>::SharedPtr(const std::size_t arraySize) : PType<T>(True), ptr(new T[arraySize]),
                                                    refCount(new std::size_t(1))
    {
    }

    TEMPLATE
    SharedPtr<T>::SharedPtr(const SharedPtr<T> &other) : PType<T>(other.isArray), ptr(other.ptr),
                                                          refCount(other.refCount)
    {
        // if (ptr != other.ptr)
        (*refCount)++;
    }

    TEMPLATE
    SharedPtr<T>::SharedPtr(SharedPtr <T> &&ref) : PType<T>(ref.isArray), ptr(ref.ptr), refCount(ref.refCount)
    {
        ref.isArray = False;
        ref.ptr = nullptr;
        ref.refCount = nullptr;
    }

    TEMPLATE
    SharedPtr <T> &SharedPtr<T>::operator=(const T &data)
    {
        *ptr = data;

        return *this;
    }

    TEMPLATE
    SharedPtr <T> &SharedPtr<T>::operator=(const SharedPtr<T> &other)
    {
        if (ptr != other.ptr)
        {
            clear();

            PType<T>::isArray = other.isArray;
            ptr = other.ptr;
            refCount = other.refCount;
            (*refCount)++;
        }

        return *this;
    }

    TEMPLATE
    SharedPtr <T> &SharedPtr<T>::operator=(SharedPtr <T> &&ref)
    {
        PType<T>::isArray = ref.isArray;
        ptr = ref.ptr;
        refCount = ref.refCount;

        ref.isArray = False;
        ref.ptr = nullptr;
        ref.refCount = nullptr;

        return *this;
    }

    TEMPLATE
    SharedPtr<T>::~SharedPtr()
    {
        if (ptr != nullptr && refCount != nullptr)
            clear();
    }

    TEMPLATE
    void SharedPtr<T>::copy(const PType <T> *other)
    {
        PType <T> *otherPtr = const_cast<PType <T> *>(other);
        // PType<T>::isArray = otherPtr->isArray;
        ptr = const_cast<T *>(otherPtr->getPtr());
    }

    TEMPLATE
    TEMPLATE_ARGS
    SharedPtr <T> SharedPtr<T>::makeShared(Args &&...args)
    {
        return SharedPtr<T>(new T(std::forward<Args>(args)...));
    }

    template<typename T, typename ... Args>
    SharedPtr <T> makeShared(Args &&...args)
    {
        return SharedPtr<T>::makeShared(std::forward<Args>(args)...);
    }

    TEMPLATE
    void SharedPtr<T>::clear()
    {
        const b32 shouldDelete = (const b32) !(--(*refCount));

        // This is the last reference, clean-up allocations accordingly.
        if (shouldDelete)
        {
            if (!PType<T>::isArray)
                delete ptr;
            else
                delete[] ptr;

            delete refCount;
        }

        // Clear local pointers regardless.
        ptr = nullptr;
        refCount = nullptr;
    }

    TEMPLATE
    WeakPtr<T>::WeakPtr(T *ptr) : ptr(ptr)
    {

    }

    TEMPLATE
    WeakPtr<T>::WeakPtr(const WeakPtr <T> &other) : PType<T>(other.isArray), ptr(other.ptr)
    {

    }

    TEMPLATE
    WeakPtr<T>::WeakPtr(WeakPtr <T> &&ref) : PType<T>(ref.isArray), ptr(ref.ptr)
    {
        ref.ptr = nullptr;
    }

    TEMPLATE
    WeakPtr<T>::WeakPtr(const SharedPtr <T> &sharedPtr) : PType<T>(sharedPtr.isArray),
                                                          ptr(sharedPtr.ptr)
    {

    }

    TEMPLATE
    WeakPtr <T> &WeakPtr<T>::operator=(const T &data)
    {
        *ptr = data;

        return *this;
    }

    TEMPLATE
    WeakPtr <T> &WeakPtr<T>::operator=(const WeakPtr<T> &other)
    {
        PType<T>::isArray = other.isArray;
        ptr = other.ptr;

        return *this;
    }

    TEMPLATE
    WeakPtr <T> &WeakPtr<T>::operator=(WeakPtr <T> &&ref)
    {
        PType<T>::isArray = ref.isArray;
        ptr = ref.ptr;
        ref.ptr = nullptr;

        return *this;
    }

    TEMPLATE
    WeakPtr <T> &WeakPtr<T>::operator=(const SharedPtr <T> &other)
    {
        PType<T>::isArray = other.isArray;
        ptr = other.ptr;

        return *this;
    }

    TEMPLATE
    WeakPtr<T>::~WeakPtr()
    {
        clear();
    }

    TEMPLATE
    void WeakPtr<T>::copy(const PType <T> *other)
    {
        // PType<T>::isArray = other->isArray;
        ptr = const_cast<T *>(const_cast<PType <T> *>(other)->getPtr());
    }

    TEMPLATE
    void WeakPtr<T>::clear()
    {
        ptr = nullptr;
    }

    TEMPLATE
    UniquePtr<T>::UniquePtr() : ptr(nullptr)
    {
    }

    TEMPLATE
    UniquePtr<T>::UniquePtr(T *ptr) : ptr(ptr)
    {
    }

    TEMPLATE
    UniquePtr<T>::UniquePtr(const std::size_t arraySize) : PType<T>(True), ptr(new T[arraySize])
    {
    }

    TEMPLATE
    UniquePtr<T>::UniquePtr(UniquePtr <T> &&ref) : PType<T>(ref.isArray), ptr(ref.ptr)
    {
        ref.ptr = nullptr;
        ref.isArray = False;
    }

    TEMPLATE
    UniquePtr <T> &UniquePtr<T>::operator=(const T &data)
    {
        *ptr = data;

        return *this;
    }

    TEMPLATE
    UniquePtr <T> &UniquePtr<T>::operator=(UniquePtr<T> &&ref)
    {
        ptr = ref.ptr;
        PType<T>::isArray = ref.isArray;
        ref.ptr = nullptr;
        ref.isArray = False;

        return *this;
    }

    TEMPLATE
    UniquePtr<T>::~UniquePtr()
    {
        clear();
    }

    TEMPLATE
    void UniquePtr<T>::clear()
    {
        if (ptr != nullptr)
        {
            if (!PType<T>::isArray)
                delete ptr;
            else
                delete[] ptr;

            ptr = nullptr;
        }
    }

    TEMPLATE
    SharedPtr <T> UniquePtr<T>::transferTo()
    {
        return SharedPtr<T>(*this);
    }

    TEMPLATE
    TEMPLATE_ARGS
    UniquePtr <T> UniquePtr<T>::makeUnique(Args &&...args)
    {
        return UniquePtr<T>(new T(std::forward<Args>(args)...));
    }

    TEMPLATE
    UniquePtr <T> UniquePtr<T>::makeUnique(const std::size_t arraySize)
    {
        return UniquePtr<T>(arraySize);
    }

    template<typename T, typename ... Args>
    UniquePtr <T> makeUnique(Args &&...args)
    {
        return UniquePtr<T>::makeUnique(std::forward<Args>(args)...);
    }

    template<typename T>
    UniquePtr <T> makeUnique(const std::size_t arraySize)
    {
        return UniquePtr<T>::makeUnique(arraySize);
    }

}
