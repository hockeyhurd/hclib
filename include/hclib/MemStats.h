#pragma once

#ifndef HCLIB_MEM_STATS_H
#define HCLIB_MEM_STATS_H

#include <hclib/Lock.h>
#include <hclib/Types.h>

#include <iosfwd>

namespace hclib
{

    class MemStats
    {
    protected:

#if CPP_17 || CPP_20
        template<class T>
        using LockT = RWLock<T>;
#else
        template<class T>
        using LockT = Lock<T>;
#endif

        LockT<std::size_t> current;
        LockT<std::size_t> total;

        /**
         * Default constructor.
         */
        MemStats();

    public:

        /**
         * Default destructor.
         */
        virtual ~MemStats() = default;

        /**
         * Default instance.
         */
        static MemStats &instance();

        /**
         * Default instance.
         *
         * @return std::size_t current heap usage.
         */
        virtual std::size_t currentUsage();

        /**
         * Default instance.
         *
         * @return std::size_t current heap usage.
         */
        virtual std::size_t totalAllocated();

        /*
         * Prints memory state.
         */
        virtual std::ostream &print(std::ostream &os);

        /*
         * Prints memory state.
         *
         * @param os std::ostream stream to write to.
         * @param memStats MemStats current.
         */
        friend std::ostream &operator<< (std::ostream &os, MemStats &memStats)
        {
            memStats.print(os);
            return os;
        }

        /**
         * Allocates memory.
         *
         * @param size std::size_t of memory to allocate.
         */
        virtual void *allocate(const std::size_t size);

        /**
         * De-allocates memory.
         *
         * @param ptr Pointer to free.
         */
        virtual void deallocate(void *ptr);
    };
}

#endif //!HCLIB_MEM_STATS_H
