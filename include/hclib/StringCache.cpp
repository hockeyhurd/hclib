// #include "StringCache.h"
#include <functional>
#include <memory>

#define HCLIB_TEMPLATE template<class StringT>
#define STRING_VIEW StringView<StringT>
#define STRING_CACHE StringCache<StringT>

namespace std
{
    template<class StringT>
    struct hash<hclib::StringView<StringT>>
    {
        std::size_t operator()(hclib::StringView<StringT> const& str) const
        {
            return str.hash();
        }
    };
}

namespace hclib
{

    HCLIB_TEMPLATE
    STRING_VIEW::StringView(StringT *str) : str(str)
    {
    }

    HCLIB_TEMPLATE
    const StringT &STRING_VIEW::string() const
    {
        return *str;
    }

    HCLIB_TEMPLATE
    const char *STRING_VIEW::c_str() const
    {
        return str->c_str();
    }

    HCLIB_TEMPLATE
    std::size_t STRING_VIEW::hash() const
    {
        return std::hash<decltype(str)>()(str);
    }

    HCLIB_TEMPLATE
    STRING_VIEW STRING_CACHE::intern(const StringT &str)
    {
        auto pairResult = cache.insert(str);
        auto resultIter = pairResult.first;
        StringT *addr = const_cast<StringT*>(&(*resultIter));
        return STRING_VIEW(addr);
    }

    HCLIB_TEMPLATE
    STRING_VIEW STRING_CACHE::intern(StringT &&str)
    {
        // auto pairResult = cache.emplace(str);
        auto pairResult = cache.insert(std::forward<StringT>(str));
        auto resultIter = pairResult.first;
        StringT *addr = const_cast<StringT*>(&(*resultIter));
        return STRING_VIEW(addr);
    }

    HCLIB_TEMPLATE
    bool STRING_CACHE::find(const StringT &str)
    {
        return cache.find(str) != cache.end();
    }

    HCLIB_TEMPLATE
    bool STRING_CACHE::find(StringT &&str)
    {
        return cache.find(str) != cache.end();
    }

    HCLIB_TEMPLATE
    bool STRING_CACHE::find(StringView<StringT> view)
    {
        return find(view.string());
    }

    HCLIB_TEMPLATE
    StringView<StringT> STRING_CACHE::get(const StringT &str)
    {
        auto findResult = cache.find(str);
        return STRING_VIEW(&(*findResult));
    }

    HCLIB_TEMPLATE
    StringView<StringT> STRING_CACHE::get(StringT &&str)
    {
        auto findResult = cache.find(str);
        return STRING_VIEW(&(*findResult));
    }

}

#undef HCLIB_TEMPLATE
#undef STRING_VIEW
#undef STRING_CACHE
