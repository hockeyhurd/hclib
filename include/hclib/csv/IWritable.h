/**
 * An interface/pure virtual (base) class for writing csv values.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

#pragma once

#ifndef HCLIB_CSV_IWRITABLE_H
#define HCLIB_CSV_IWRITABLE_H

#include <hclib/Types.h>

namespace hclib
{
namespace csv
{

    class IWritable
    {
    public:

        virtual ~IWritable() = default;

        virtual void write(std::string& valueAsString) const = 0;
    };
}
}

#endif //!HCLIB_CSV_IWRITABLE_H

