/**
 * A class for ease of writing csv files.
 *
 * @author hockeyhurd
 * @version 2023-10-07
 */

// HCLib includes
#include <hclib/File.h>

// std includes
#include <utility>

#define TEMPLATE template<class T>
#define CLASS_T CSVWriter<T>

namespace hclib
{
namespace csv
{
    /* static*/
    TEMPLATE
    const std::size_t CLASS_T::DEFAULT_BUFFER_SIZE = 1024;

    TEMPLATE
    bool CLASS_T::write(const std::vector<std::vector<CSVObject<T>>>& rows, std::string& str, std::size_t bufferSize)
    {
        bool result = true;
        std::ostringstream os;
        std::string tempString;
        tempString.reserve(bufferSize);

        const std::size_t len = rows.size();
        const std::size_t lenMinusOne = len - 1;

        for (std::size_t i = 0; i < len; ++i)
        {
            const auto& row = rows[i];
            const std::size_t rowLen = row.size();
            const std::size_t rowLenMinusOne = rowLen - 1;

            for (std::size_t j = 0; j < rowLen; ++j)
            {
                tempString.clear();
                row[j].write(tempString);
                os << tempString;

                if (j < rowLenMinusOne)
                {
                    os << ',';
                }
            }

            if (i < lenMinusOne)
            {
                os << '\n';
            }
        }

        str = os.str();
        return result;
    }

    TEMPLATE
    bool CLASS_T::writeToFile(const std::vector<std::vector<CSVObject<T>>>& rows, const std::string& filepath, std::size_t bufferSize)
    {
        std::string tempString;
        const bool result = write(rows, tempString, bufferSize);

        if (!result)
        {
            return result;
        }

        File file(filepath, File::EnumMode::WRITE);
        file.write(tempString);
        file.close();

        return result;
    }
}
}

