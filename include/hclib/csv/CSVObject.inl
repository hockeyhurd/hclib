/**
 * A class for representing a single csv object/value.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

#define TEMPLATE template<class T>
#define CLASS_T CSVObject<T>

#define TEMPLATE_SPECIALIZATION template<>
#define STR_T CSVObject<std::string>

namespace hclib
{
namespace csv
{
    TEMPLATE
    CLASS_T::CSVObject(const T& value, std::function<void(T&, const std::string&)> readerCallback,
                       std::function<void(const T&, std::string&)> writerCallback) : IReadable(), IWritable(),
        value(value), readerCallback(readerCallback), writerCallback(writerCallback)
    {
        static_assert(std::is_copy_constructible<T>::value, "Type T is not copy constructible");
    }

    TEMPLATE
    CLASS_T::CSVObject(T&& value, std::function<void(T&, const std::string&)> readerCallback,
                       std::function<void(const T&, std::string&)> writerCallback) : IReadable(), IWritable(),
        value(value), readerCallback(readerCallback), writerCallback(writerCallback)
    {
        static_assert(std::is_move_constructible<T>::value, "Type T is not move constructible");
    }

    TEMPLATE
    CLASS_T::CSVObject(const CSVObject<T>& other) : value(other.value),
        readerCallback(other.readerCallback), writerCallback(other.writerCallback)

    {
        static_assert(std::is_copy_constructible<T>::value, "Type T is not copy constructible");
    }

    TEMPLATE
    CLASS_T::CSVObject(CSVObject<T>&& other) : value(std::forward<T>(other.value)),
        readerCallback(other.readerCallback), writerCallback(other.writerCallback)

    {
        static_assert(std::is_move_constructible<T>::value, "Type T is not move constructible");
    }

    TEMPLATE
    CLASS_T::~CSVObject()
    {
        static_assert(std::is_destructible<T>::value, "Type T is not destructible");
    }

    TEMPLATE
    CLASS_T& CLASS_T::operator= (const CLASS_T& other)
    {
        static_assert(std::is_copy_assignable<T>::value, "Type T is not copy assignable");

        if (this != &other)
        {
            value = other.value;
            readerCallback = other.readerCallback;
            writerCallback = other.writerCallback;
        }

        return *this;
    }

    TEMPLATE
    CLASS_T& CLASS_T::operator= (CLASS_T&& other)
    {
        static_assert(std::is_move_assignable<T>::value, "Type T is not move assignable");

        if (this != &other)
        {
            value = std::forward<T>(other.value);
            readerCallback = other.readerCallback;
            writerCallback = other.writerCallback;
        }

        return *this;
    }

    TEMPLATE
    T& CLASS_T::get() HCLIB_NOEXCEPT
    {
        return value;
    }

    TEMPLATE
    const T& CLASS_T::get() const HCLIB_NOEXCEPT
    {
        return value;
    }

    TEMPLATE
    /* virtual */
    void CLASS_T::read(const std::string& valueAsString) /* override */
    {
        if (readerCallback != nullptr)
        {
            readerCallback(value, valueAsString);
        }
    }

    TEMPLATE
    /* virtual */
    void CLASS_T::write(std::string& valueAsString) const /* override */
    {
        if (writerCallback != nullptr)
        {
            writerCallback(value, valueAsString);
        }
    }
}
}

#undef TEMPLATE
#undef TEMPLATE_SPECIALIZATION
#undef CLASS_T
#undef STR_T

