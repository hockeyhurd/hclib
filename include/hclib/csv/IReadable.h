/**
 * An interface/pure virtual (base) class for reading csv values.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

#pragma once

#ifndef HCLIB_CSV_IREADABLE_H
#define HCLIB_CSV_IREADABLE_H

#include <hclib/Types.h>

namespace hclib
{
namespace csv
{

    class IReadable
    {
    public:

        virtual ~IReadable() = default;

        virtual void read(const std::string& valueAsString) = 0;
    };
}
}

#endif //!HCLIB_CSV_IREADABLE_H

