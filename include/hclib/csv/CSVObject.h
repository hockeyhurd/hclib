/**
 * A class for representing a single csv object/value.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

#pragma once

#ifndef HCLIB_CSV_OBJECT_H
#define HCLIB_CSV_OBJECT_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/csv/IReadable.h>
#include <hclib/csv/IWritable.h>

// std includes
#include <functional>
#include <string>

namespace hclib
{
namespace csv
{

    /**
     * A default CSV reader that no-ops.
     */
    template<class T>
    void csvDefaultReader(T&, const std::string&)
    {
        // Intentionally left blank
    }

    /**
     * A default CSV writer that no-ops.
     */
    template<class T>
    void csvDefaultWriter(const T&, std::string&)
    {
        // Intentionally left blank
    }

    template<class T>
    class CSVObject : public IReadable, public IWritable
    {
    public:

        /**
         * Default constructor that defaults to default reader and writer callbacks.
         *
         * @param readerCallback the optional reader callback function.
         * @param writerCallback the optional writer callback function.
         */
        explicit CSVObject(std::function<void(T&, const std::string&)> readerCallback = csvDefaultReader<T>, std::function<void(const T&, std::string&)> writerCallback = csvDefaultWriter<T>);

        /**
         * Default constructor that defaults to default reader and writer callbacks.
         * A copy of the provided object will be made.
         *
         * @param readerCallback the optional reader callback function.
         * @param writerCallback the optional writer callback function.
         */
        explicit CSVObject(const T& value, std::function<void(T&, const std::string&)> readerCallback = csvDefaultReader<T>, std::function<void(const T&, std::string&)> writerCallback = csvDefaultWriter<T>);

        /**
         * Default constructor that defaults to default reader and writer callbacks.
         * The provided object will be forwarded to the appropriate constructor.
         *
         * @param readerCallback the optional reader callback function.
         * @param writerCallback the optional writer callback function.
         */
        explicit CSVObject(T&& value, std::function<void(T&, const std::string&)> readerCallback = csvDefaultReader<T>, std::function<void(const T&, std::string&)> writerCallback = csvDefaultWriter<T>);

        /**
         * Default copy constructor.
         */
        CSVObject(const CSVObject<T>&);

        /**
         * Default move constructor.
         */
        CSVObject(CSVObject<T>&&);

        /**
         * Default destructor.
         */
        ~CSVObject();

        /**
         * Copy assignment operator.
         */
        CSVObject<T>& operator= (const CSVObject<T>&);

        /**
         * Move assignment operator.
         */
        CSVObject<T>& operator= (CSVObject<T>&&);

        /**
         * Gets the underlying value as a reference.
         *
         * @return T&.
         */
        T& get() HCLIB_NOEXCEPT;

        /**
         * Gets the underlying value as a constt reference.
         *
         * @return const T&.
         */
        const T& get() const HCLIB_NOEXCEPT;

        /**
         * Reads a string and converts it to the internal object's representation.
         *
         * @param valueAsString std::string.
         */
        void read(const std::string& valueAsString) override;

        /**
         * Writes and converts the internal object's representation to a string.
         *
         * @param valueAsString std::string.
         */
        void write(std::string& valueAsString) const override;

    private:

        T value;
        std::function<void(T&, const std::string&)> readerCallback;
        std::function<void(const T&, std::string&)> writerCallback;
    };

    using CSVString = CSVObject<std::string>;
}
}

#ifndef HCLIB_CSV_OBJECT_INL
#define HCLIB_CSV_OBJECT_INL
#include <hclib/csv/CSVObject.inl>
#endif //!HCLIB_CSV_OBJECT_INL

#endif //!HCLIB_CSV_OBJECT_H

