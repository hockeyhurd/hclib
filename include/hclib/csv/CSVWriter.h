/**
 * A class for ease of writing csv files.
 *
 * @author hockeyhurd
 * @version 2023-10-07
 */

#pragma once

#ifndef HCLIB_CSV_WRITER_H
#define HCLIB_CSV_WRITER_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/csv/CSVObject.h>

// std includes
#include <string>
#include <vector>

namespace hclib
{
namespace csv
{

    template<class T>
    class HCLIB_EXPORT CSVWriter
    {
    public:

        /**
         * Default Constructor.
         */
        CSVWriter() HCLIB_NOEXCEPT = default;

        /**
         * Default copy constructor.
         */
        CSVWriter(const CSVWriter<T>& other) HCLIB_NOEXCEPT = default;

        /**
         * Default move constructor.
         */
        CSVWriter(CSVWriter<T>&& other) HCLIB_NOEXCEPT = default;

        /**
         * Destructor.
         */
        ~CSVWriter() = default;

        /**
         * Copy assignment operator.
         */
        CSVWriter<T>& operator= (const CSVWriter<T>& other) HCLIB_NOEXCEPT = default;

        /**
         * Move assignment operator.
         */
        CSVWriter<T>& operator= (CSVWriter<T>&& other) HCLIB_NOEXCEPT = default;

        /**
         * Writes a std::string in CSV format.
         *
         * @param rows a vector of vector or CSVObjects to write.
         * @param str the std::string to parse.
         * @param bufferSize the recommended bufferSize to reserve per row.
         */
        bool write(const std::vector<std::vector<CSVObject<T>>>& rows, std::string& str, std::size_t bufferSize = CSVWriter<T>::DEFAULT_BUFFER_SIZE);

        /**
         * Writes a file to CSV format.
         *
         * @param rows a vector of vector or CSVObjects to write.
         * @param str the std::string path to a file to parse.
         * @param bufferSize the recommended bufferSize to reserve per row.
         */
        bool writeToFile(const std::vector<std::vector<CSVObject<T>>>& rows, const std::string& filepath, std::size_t bufferSize = CSVWriter<T>::DEFAULT_BUFFER_SIZE);

    public:

        /**
         * The default buffer size of the CSV represented table.
         */
        static const std::size_t DEFAULT_BUFFER_SIZE;
    };
}
}

#ifndef HCLIB_CSV_WRITER_INL
#define HCLIB_CSV_WRITER_INL
#include <hclib/csv/CSVWriter.inl>
#endif //!HCLIB_CSV_WRITER_INL

#endif //!HCLIB_CSV_WRITER_H

