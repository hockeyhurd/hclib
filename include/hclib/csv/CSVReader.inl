/**
 * A class for ease of reading csv files.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

// HCLib includes
#include <hclib/File.h>

// std includes
#include <memory>
#include <utility>

#define TEMPLATE template<class T>
#define CLASS_T CSVReader<T>

namespace hclib
{
namespace csv
{
    /* static*/
    TEMPLATE
    const std::size_t CLASS_T::DEFAULT_MIN_BUFFER_SIZE = 32;

    /* static*/
    TEMPLATE
    const std::size_t CLASS_T::DEFAULT_BUFFER_SIZE = 1024;

    /* static*/
    TEMPLATE
    const std::size_t CLASS_T::DEFAULT_ROW_RESERVE_SIZE = 64;

    TEMPLATE
    CLASS_T::CSVReader(std::function<void(T&, const std::string&)> readerCallback, std::size_t reserveSize) :
        rows(), readerCallback(readerCallback)
    {
        rows.reserve(reserveSize);

        // Start with at least one row
        rows.emplace_back();
    }

    TEMPLATE
    CLASS_T::CSVReader(const CLASS_T& other) : rows(other.rows), readerCallback(other.readerCallback)
    {
        static_assert(std::is_copy_constructible<std::vector<T>>::value, "std::vector<T> is not copy constructible");
    }

    TEMPLATE
    CLASS_T::CSVReader(CLASS_T&& other) : rows(std::move(other.rows)), readerCallback(std::move(other.readerCallback))
    {
        static_assert(std::is_move_constructible<std::vector<T>>::value, "std::vector<T> is not move constructible");
    }

    TEMPLATE
    CLASS_T::~CSVReader()
    {
        static_assert(std::is_destructible<std::vector<T>>::value, "std::vector<T> is not destructible");
    }

    TEMPLATE
    CLASS_T& CLASS_T::operator= (const CLASS_T& other)
    {
        static_assert(std::is_copy_assignable<std::vector<T>>::value, "std::vector<T> is not copy assignable");

        if (this != &other)
        {
            rows = other.rows;
            readerCallback = other.readerCallback;
        }

        return *this;
    }

    TEMPLATE
    CLASS_T& CLASS_T::operator= (CLASS_T&& other)
    {
        static_assert(std::is_move_assignable<std::vector<T>>::value, "std::vector<T> is not move assignable");

        if (this != &other)
        {
            rows = std::move(other.rows);
            readerCallback = std::move(other.readerCallback);
        }

        return *this;
    }

    TEMPLATE
    const std::vector<CSVString>* CLASS_T::getTitleRow() const HCLIB_NOEXCEPT
    {
        return rows.empty() ? nullptr : &rows[0];
    }

    TEMPLATE
    const std::vector<std::vector<CSVObject<T>>>& CLASS_T::getRows() const HCLIB_NOEXCEPT
    {
        return rows;
    }

    TEMPLATE
    bool CLASS_T::read(const std::string& str, std::size_t bufferSize)
    {
        clear();

        // Make the bufferSize with a reasonable min value.
        bufferSize = std::max<std::size_t>(bufferSize, DEFAULT_MIN_BUFFER_SIZE);

        // Always need a row to start with for parsing
        rows.emplace_back();

        const bool success = parse(str, bufferSize);
        return success;
    }

    TEMPLATE
    bool CLASS_T::readFromFile(const std::string& filepath, std::size_t bufferSize)
    {
        File file(filepath, File::EnumMode::READ);
        const std::size_t fileSize = file.size();
        const std::string contents = file.readFile(fileSize);
        file.close();

        const bool result = read(contents, bufferSize);
        return result;
    }

    TEMPLATE
    void CLASS_T::addRow()
    {
        rows.emplace_back();
        auto& curRow = rows.back();
        curRow.reserve(CSVReader<T>::DEFAULT_ROW_RESERVE_SIZE);
    }

    TEMPLATE
    void CLASS_T::addValue(std::string& buffer)
    {
        CSVObject<T> csvObj(T(), readerCallback);
        csvObj.read(buffer);

        // buffer already read, clear it for the next value.
        buffer.clear();

        auto& curRow = rows.back();
        curRow.emplace_back(std::move(csvObj));
    }

    TEMPLATE
    void CLASS_T::clear()
    {
        rows.clear();
    }

    TEMPLATE
    bool CLASS_T::parse(const std::string& contents, std::size_t bufferSize)
    {
        static const auto inBounds = [](const std::size_t index, const std::size_t len) HCLIB_NOEXCEPT -> bool
        {
            return index < len;
        };

        bool success = true;
        std::string buffer;
        buffer.reserve(bufferSize);

        const std::size_t len = contents.size();
        std::size_t index = 0;

        while (inBounds(index, len))
        {
            char curChar = contents[index++];

            switch (curChar)
            {
            case static_cast<char>(EOF):
                break;
            case ',':
            {
                addValue(buffer);
            }
                break;
            case '\n':
                addValue(buffer);
                addRow();
                break;
            case '\r':
            {
                // Special case where it could be a platofrm like Windows where they use "\r\n".
                if (inBounds(index, len))
                {
                    curChar = contents[index];

                    if (curChar == '\n')
                    {
                        ++index;
                    }
                }

                addValue(buffer);
                addRow();
            }
                break;
            default:
                buffer += curChar;
                break;
            }
        }

        // If there was something in the buffer, add it to the table??
        if (!buffer.empty())
        {
            auto& curRow = rows.back();
            curRow.emplace_back(std::move(buffer));
        }

        success = rows.size() >= 1 && rows[0].size() > 0;
        return success;
    }
}
}

#undef TEMPLATE
#undef CLASS_T

