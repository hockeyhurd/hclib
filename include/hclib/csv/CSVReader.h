/**
 * A class for ease of reading csv files.
 *
 * @author hockeyhurd
 * @version 2023-09-07
 */

#pragma once

#ifndef HCLIB_CSV_READER_H
#define HCLIB_CSV_READER_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/csv/CSVObject.h>

// std includes
#include <functional>
#include <string>
#include <vector>

namespace hclib
{
namespace csv
{

    template<class T>
    class HCLIB_EXPORT CSVReader
    {
    public:

        /**
         * Constructor.
         *
         * @param readerCallback Callback function for how to read CSVObjects.
         * @param reserveSize Optional callback to give a hint of the number of objects in a given row.
         */
        explicit CSVReader(std::function<void(T&, const std::string&)> readerCallback = csvDefaultReader<T>,
                                              std::size_t reserveSize = CSVReader<T>::DEFAULT_ROW_RESERVE_SIZE);

        /**
         * Copy constructor.
         */
        CSVReader(const CSVReader<T>& other);

        /**
         * Move constructor.
         */
        CSVReader(CSVReader<T>&& other);

        /**
         * Destructor.
         */
        ~CSVReader();

        /**
         * Copy assignment operator.
         */
        CSVReader<T>& operator= (const CSVReader<T>& other);

        /**
         * Move assignment operator.
         */
        CSVReader<T>& operator= (CSVReader<T>&& other);

        /**
         * Gets a pointer to the first row of the CSV table.
         *
         * @return pointer to the first row.
         */
        const std::vector<CSVString>* getTitleRow() const HCLIB_NOEXCEPT;

        /**
         * Gets a reference to the CSV table represented as a vector of vectors.
         *
         * @return reference to the vector of vectors.
         */
        const std::vector<std::vector<CSVObject<T>>>& getRows() const HCLIB_NOEXCEPT;

        /**
         * Reads a std::string and parses it as a CSV.
         *
         * @param str the std::string to parse.
         * @param bufferSize the recommended bufferSize to reserve per row.
         */
        bool read(const std::string& str, std::size_t bufferSize = CSVReader<T>::DEFAULT_BUFFER_SIZE);

        /**
         * Reads a file and parses it as a CSV.
         *
         * @param str the std::string path to a file to parse.
         * @param bufferSize the recommended bufferSize to reserve per row.
         */
        bool readFromFile(const std::string& filepath, std::size_t bufferSize = CSVReader<T>::DEFAULT_BUFFER_SIZE);

    private:

        /**
         * Helper function to add a new (empty) row to the table.
         */
        void addRow();

        /**
         * Adds the result of the callback to the readerCallback and clears the buffer.
         *
         * @param buffer the std::string to operate on.
         */
        void addValue(std::string& buffer);

        /**
         * Clears the internal representation of the CSV table.
         */
        void clear();

        /**
         * Internal helper function to parse the CSV.
         *
         * @param contents the std::string representation of the table.
         * @param bufferSize the optional expected size of the table.
         */
        bool parse(const std::string& contents, std::size_t bufferSize = CSVReader<T>::DEFAULT_BUFFER_SIZE);

    public:

        /**
         * The default buffer size of the CSV represented table.
         */
        static const std::size_t DEFAULT_BUFFER_SIZE;

        /**
         * The minimum size of the buffer to be reserved for the table.
         */
        static const std::size_t DEFAULT_MIN_BUFFER_SIZE;

        /**
         * The default buffer size of the CSV represented table for a given row.
         */
        static const std::size_t DEFAULT_ROW_RESERVE_SIZE;

    private:

        /**
         * The CSV represented as a vector of vector of CSVObjects.
         */
        std::vector<std::vector<CSVObject<T>>> rows;

        /**
         * The callback function for reading CSVObjects.
         */
        std::function<void(T&, const std::string&)> readerCallback;
    };
}
}

#ifndef HCLIB_CSV_READER_INL
#define HCLIB_CSV_READER_INL
#include <hclib/csv/CSVReader.inl>
#endif //!HCLIB_CSV_READER_INL

#endif //!HCLIB_CSV_READER_H

