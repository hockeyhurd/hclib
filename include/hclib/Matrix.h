/**
 * @author hockeyhurd
 * @version 2019-12-05
 */

#pragma once

#ifndef HCLIB_MATRIX_H
#define HCLIB_MATRIX_H

#include <hclib/Types.h>

#include <array>

namespace hclib
{

template<class T, s32 N = 3>
class Matrix
{
private:

    std::array<std::array<T, N>, N> data;

public:

    Matrix();
    explicit Matrix(const T &defaultValue);
    Matrix(const Matrix<T, N> &) = default;
    Matrix(Matrix<T, N> &&) = default;
    ~Matrix() = default;

    Matrix<T, N> &operator= (const Matrix<T, N> &) = default;
    Matrix<T, N> &operator= (Matrix<T, N> &&) = default;

    static Matrix<T, N> identity(const T &identityValue, const T &defaultValue = T());

    HCLIB_CONSTEXPR s32 size() const
    {
        return N;
    }

    void zero(const T &defaultValue = T());

    T &at(const s32 row, const s32 col);
    const T &at(const s32 row, const s32 col) const;
    bool set(const s32 row, const s32 col, const T &input);
    bool set(const s32 row, const s32 col, T &&input);

    Matrix<T, N> dot(const Matrix<T, N> &other);
    // TODO: Re-implement
    // Matrix<T, N> &cross(const Matrix<T, N> &other);

    std::array<T, N> &operator[] (const std::size_t index);
    const std::array<T, N> &operator[] (const std::size_t index) const;

    Matrix<T, N> &operator+= (const Matrix<T, N> &other);
    Matrix<T, N> &operator-= (const Matrix<T, N> &other);
    Matrix<T, N> &operator*= (const Matrix<T, N> &other);
    Matrix<T, N> &operator/= (const Matrix<T, N> &other);

private:

    // T &operator[] (const s32 index);
    // const T &operator[] (const s32 index) const;
};

}

#include "Matrix.cpp"

#endif //!HCLIB_MATRIX_H
