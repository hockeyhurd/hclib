/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#pragma once

#ifndef HCLIB_DEBUG_H
#define HCLIB_DEBUG_H

#include <hclib/Types.h>

#include <iostream>

namespace hclib
{

    class HCLIB_EXPORT Debug
    {

    public:

        enum LogLevel
        {
            LL_INFO, LL_WARN, LL_ERROR
        };

    private:

        // static const char *logLevelStr[3];

    public:

        Debug() = delete;
        ~Debug() = default;

    private:

        template<class T>
        static void log(const LogLevel logLevel, const T &data)
        {
#if DebugMode
            static const char *logLevelStr[3] = { "INFO", "WARN", "ERROR" };
            std::cout << '[' << logLevelStr[logLevel] << "]: " << data << std::endl;
#endif
        }

        template<class T>
        static void log(const LogLevel logLevel, T &&data)
        {
#if DebugMode
            static const char *logLevelStr[3] = { "INFO", "WARN", "ERROR" };
            std::cout << '[' << logLevelStr[logLevel] << "]: " << std::forward<T>(data) << std::endl;
#endif
        }

    public:

        template<class T>
        static void info(const T &data)
        {
            log(LogLevel::LL_INFO, data);
        }

        template<class T>
        static void info(T &&data)
        {
            log(LogLevel::LL_INFO, std::forward<T>(data));
        }

        template<class T>
        static void warn(const T &data)
        {
            log(LogLevel::LL_WARN, data);
        }

        template<class T>
        static void warn(T &&data)
        {
            log(LogLevel::LL_WARN, std::forward<T>(data));
        }

        template<class T>
        static void error(const T &data)
        {
            log(LogLevel::LL_ERROR, data);
        }

        template<class T>
        static void error(T &&data)
        {
            log(LogLevel::LL_ERROR, std::forward<T>(data));
        }

        static void assertTrue(const b32);
        static void assertFalse(const b32);

    };

}

#endif //HCLIB_DEBUG_H
