/**
 * @author hockeyhurd
 * @version 2019-08-30
 */

#pragma once

#ifndef HCLIB_STRINGBUILDER_H
#define HCLIB_STRINGBUILDER_H

#include <hclib/Types.h>
#include <hclib/StringCache.h>

#include <string>
#include <vector>

#if CPP_VER >= 2017
#include <string_view>
#endif

#if CPP_VER >= 2014
#include <memory>
#endif

namespace hclib
{

    class HCLIB_EXPORT String;

    class HCLIB_EXPORT StringBuilder
    {
    public:

        using value_type = std::string::value_type;
        using Vec = std::vector<value_type>;
        using size_type = Vec::size_type;

    protected:
        Vec vec;

    public:

        explicit StringBuilder(const std::size_t capacity = 0x40);
        explicit StringBuilder(const std::string &str);
        explicit StringBuilder(std::string &&str);

#if CPP_VER >= 2017
        explicit StringBuilder(const std::string_view str);
#endif

        StringBuilder(const StringBuilder &other);
        StringBuilder(StringBuilder &&other);
        virtual ~StringBuilder() = default;

        StringBuilder &operator= (const StringBuilder &other);
        StringBuilder &operator= (StringBuilder &&other);
        StringBuilder &operator= (const std::string &other);
        StringBuilder &operator= (std::string &&other);

#if CPP_VER >= 2017
        StringBuilder &operator= (const std::string_view str);
#endif

        virtual size_type size() const;
        virtual bool isEmpty() const;

        virtual void reserve(const size_type capacity);

        virtual void append(const value_type c);
        virtual void append(const value_type *str);
        virtual void append(const std::string &str);
        virtual void append(std::string &&str);
        virtual void append(const String &str);
        virtual void append(String &&str);
        virtual void append(const StringBuilder &str);

        virtual void append(const bool value);
        virtual void append(const s16 value);
        virtual void append(const u16 value);
        virtual void append(const s32 value);
        virtual void append(const u32 value);
        virtual void append(const s64 value);
        virtual void append(const u64 value);

#if OS_APPLE
        virtual void append(const std::size_t value);
        virtual void append(const ssize_t value);
#endif
        virtual void append(const f32 value);
        virtual void append(const f64 value);

        template<class T>
        void append(StringView<T> str)
        {
            append(str.string());
        }

        template<class T>
        StringBuilder &operator<< (const T &str)
        {
            append(str);
            return *this;
        }

        template<class T>
        StringBuilder &operator<< (T &&str)
        {
            append(std::forward<T>(str));
            return *this;
        }

        virtual StringBuilder substring(const size_type length);
        virtual StringBuilder substring(const size_type start, const size_type length);

        virtual void clear();

        virtual String toString() const;
        virtual std::string toSTDString() const;
        // virtual const char *c_str() const;
        virtual char *copy() const;

#if CPP_VER >= 2020
        virtual std::shared_ptr<char[]> copyShared() const;
#endif
#if CPP_VER >= 2014
        virtual std::unique_ptr<char[]> copyUnique() const;
#endif

        virtual std::vector<char> copyVector() const;

        /**
         * Reverses the underlying datastructure.
         */
        virtual void reverse();

    protected:

        // This function is explicitly for primitive types only
        // i.e. s32, f64, u64, etc.
        template<class T>
        void primitiveAppend(const T value)
        {
#if CPP_14
            // Assert this is for a primitive type only!
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif
            append(std::to_string(value));
        }

    };

}

#endif //!HCLIB_STRINGBUILDER_H
