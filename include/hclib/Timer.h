/**
 * A class for simplifying Timers supporting periodic and not.
 *
 * @author hockeyhurd
 * @version 03/19/2023
 */

#pragma once

#ifndef HCLIB_TIMER_H
#define HCLIB_TIMER_H

// HCLib includes
#include <hclib/Types.h>

// std includes
#include <chrono>

typedef void (*TimerEvent)();

namespace hclib
{
    class Timer
    {
    public:

        using Clock = std::chrono::high_resolution_clock;
        using TimePoint = Clock::time_point;

        /**
         * Constructor
         *
         * @param durationMicro the duration in microseconds of time for the Timer.
         * @param eventHandler the event handler callback when the Timer expires.
         * @param periodic bool flag to enable a periodic Timer.
         */
        Timer(s64 durationMicro, TimerEvent eventHandler, bool periodic = false) HCLIB_NOEXCEPT;

        /**
         * Deleted copy constructor.
         */
        Timer(const Timer&) = delete;

        /**
         * Default move constructor.
         */
        Timer(Timer&&) HCLIB_NOEXCEPT = default;

        /**
         * Default destructor.
         */
        virtual ~Timer() = default;

        /**
         * Deleted copy constructor.
         */
        Timer& operator= (const Timer&) = delete;

        /**
         * Default move constructor.
         */
        Timer& operator= (Timer&&) HCLIB_NOEXCEPT = default;

        /**
         * Gets the duration microseconds of time.
         *
         * @return s64 duration in microseconds.
         */
        s64 getDuration() const HCLIB_NOEXCEPT;

        /**
         * Gets the UUID associated with this Timer.
         */
        u32 getUUID() const HCLIB_NOEXCEPT;

        /**
         * Gets whether the Timer is periodic or not.
         *
         * @return bool.
         */
        bool isPeriodic() const HCLIB_NOEXCEPT;

        /**
         * Checks whether the Timer has expired.
         *
         * @return bool.
         */
        bool expired() const HCLIB_NOEXCEPT;

        /**
         * Checks if the Timer has expired and conditionally calls it's event handler
         * as needed.
         *
         * @return bool true if executed, else false.
         */
        bool executeIfExpired();

        /**
         * Resets the Timer to a new T0 (T-zero).
         */
        void reset() HCLIB_NOEXCEPT;

    private:

        /**
         * Helper function to get the current TimePoint.
         *
         * @return current TimePoint.
         */
        static TimePoint currentTime() HCLIB_NOEXCEPT;

    private:

        // The duration in microseconds of the Timer.
        s64 durationMicro;

        // The callback function to callback.
        TimerEvent eventHandler;

        // Flag to indicate whether this Timer runs indefinitely at some frequency
        // and until the program exits or the Timer is stopped/removed.
        bool periodic;

        // The UUID of the Timer.
        u32 uuid;

        // The starting TimePoint for when this Timer was created.
        TimePoint startTime;
    };
}

#endif //!HCLIB_TIMER_H

