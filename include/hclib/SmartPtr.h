/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#ifndef HCLIB_SMARTPTR_H
#define HCLIB_SMARTPTR_H

#include <hclib/Types.h>

#include <iostream>

namespace hclib {

    template<typename T>
    class PType
    {

    protected:

        b32 isArray;

        PType(const b32 = False);

    public:

        virtual ~PType() = default;

        inline b32 getIsArray() const
        {
            return isArray;
        }

        virtual b32 isNULL() const = 0;

        virtual const T *getPtr() const = 0;

        virtual T &getValue() const = 0;

        virtual void copy(const PType<T> *) = 0;
    };

    template<typename T>
    class SharedPtr;

    template<typename T>
    class WeakPtr;

    template<typename T>
    class UniquePtr;

    template<typename T>
    class SharedPtr : public PType<T>
    {

        friend WeakPtr<T>;
        friend UniquePtr<T>;

    private:

        T *ptr;
        mutable std::size_t *refCount;

        // Special constructor only accessibly locally and from
        // UniquePtr class.
        explicit SharedPtr(UniquePtr<T> &);

    public:

        SharedPtr();

        SharedPtr(T *);

        explicit SharedPtr(const std::size_t);

        SharedPtr(const SharedPtr<T> &);

        SharedPtr(SharedPtr<T> &&);

        SharedPtr<T> &operator=(const T &);

        SharedPtr<T> &operator=(const SharedPtr<T> &);

        SharedPtr<T> &operator=(SharedPtr<T> &&);

        virtual ~SharedPtr();

        inline std::size_t getRefCount() const
        {
            return *refCount;
        }

        inline b32 isNULL() const
        {
            return (b32)(ptr == nullptr);
        }

        inline const T *getPtr() const
        {
            return ptr;
        }

        inline T &getValue() const
        {
            return *ptr;
        }

        inline T &operator*() const
        {
            return *ptr;
        }

        inline T *operator->() const
        {
            return ptr;
        }

        void copy(const PType<T> *other);

        template<typename ... Args>
        static SharedPtr<T> makeShared(Args &&...);

        friend std::ostream &operator<<(std::ostream &os, const SharedPtr<T> &sp)
        {
            if (!sp.isNULL())
            {
                os << *sp.ptr;
            }

            return os;
        }

    private:

        void clear();

    };

    template<typename T>
    class WeakPtr : public PType<T>
    {

    private:

        T *ptr;

    public:

        WeakPtr(T *);

        WeakPtr(const WeakPtr<T> &);

        explicit WeakPtr(const SharedPtr<T> &);

        WeakPtr(WeakPtr<T> &&);

        WeakPtr<T> &operator=(const T &);

        WeakPtr<T> &operator=(const WeakPtr<T> &);

        WeakPtr<T> &operator=(const SharedPtr<T> &);

        WeakPtr<T> &operator=(WeakPtr<T> &&);

        virtual ~WeakPtr();

        inline b32 isNULL() const
        {
            return (b32)(ptr == nullptr);
        }

        inline const T *getPtr() const
        {
            return ptr;
        }

        inline T &getValue() const
        {
            return *ptr;
        }

        inline T &operator*() const
        {
            return *ptr;
        }

        inline T *operator->() const
        {
            return ptr;
        }

        void copy(const PType<T> *other);

        friend std::ostream &operator<<(std::ostream &os, const WeakPtr<T> &sp)
        {
            if (!sp.isNULL())
            {
                os << *sp.ptr;
            }

            return os;
        }

    private:

        void clear();

    };

    template<typename T>
    class UniquePtr : public PType<T>
    {

        friend SharedPtr<T>;

    private:

        T *ptr;

    public:

        UniquePtr();

        UniquePtr(T *);

        explicit UniquePtr(const std::size_t);

        UniquePtr(UniquePtr<T> &&);

        UniquePtr(const UniquePtr<T> &) = delete;

        UniquePtr<T> &operator=(const T &);

        UniquePtr<T> &operator=(const UniquePtr<T> &) = delete;

        UniquePtr<T> &operator=(UniquePtr<T> &&);

        virtual ~UniquePtr();

        inline b32 isNULL() const
        {
            return (b32)(ptr == nullptr);
        }

        inline const T *getPtr() const
        {
            return ptr;
        }

        inline T &getValue() const
        {
            return *ptr;
        }

        inline T &operator*() const
        {
            return *ptr;
        }

        inline T *operator->() const
        {
            return ptr;
        }

        DEPRECATED(void copy(const PType<T> *other))
        {
        }

        SharedPtr<T> transferTo();

        template<typename ... Args>
        static UniquePtr<T> makeUnique(Args &&...);

        static UniquePtr<T> makeUnique(const std::size_t);

        friend std::ostream &operator<<(std::ostream &os, const UniquePtr<T> &sp)
        {
            if (!sp.isNULL())
            {
                os << *sp.ptr;
            }

            return os;
        }

    private:

        void clear();

    };

    template<typename T, typename ... Args>
    SharedPtr<T> makeShared(Args &&...);

    template<typename T, typename ... Args>
    UniquePtr<T> makeUnique(Args &&...);

    template<typename T>
    UniquePtr<T> makeUnique(const std::size_t);

}

#include "SmartPtr.cpp"

#endif //HCLIB_SMARTPTR_H
