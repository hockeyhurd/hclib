#pragma once

#ifndef HCLIB_TRIVIAL_SERIALIZABLE_H
#define HCLIB_TRIVIAL_SERIALIZABLE_H

#include <hclib/Types.h>
#include <hclib/ISerializable.h>

namespace hclib
{

    template<class T>
    class TrivialSerializable : public ISerializable
    {
    protected:

        union Value
        {
            T type;
            char buf[sizeof(T)];

            Value(const T &value) : type(value) {}
            Value(T &&value) : type(std::foreward<T>(value)) {}
            ~Value() = default;
        };

        Value value;

    public:

        TrivialSerializable() = default;

        TrivialSerializable(const T &value) HCLIB_NOEXCEPT : value(value)
        {
        }

        TrivialSerializable(T &&value) HCLIB_NOEXCEPT : value(value)
        {
        }

        virtual ~TrivialSerializable() HCLIB_NOEXCEPT = default;

        TrivialSerializable<T> &operator= (const TrivialSerializable<T> &other) HCLIB_NOEXCEPT
        {
            value = other.value;
            return *this;
        }

        TrivialSerializable<T> &operator= (TrivialSerializable<T> &&other) HCLIB_NOEXCEPT
        {
            value = std::move(other.value);
            return *this;
        }

        virtual s32 sbyteCount() const HCLIB_NOEXCEPT override
        {
#if CPP_14
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif
            return static_cast<s32>(sizeof(T));
        }

        virtual std::size_t byteCount() const HCLIB_NOEXCEPT override
        {
#if CPP_14
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif
            return static_cast<std::size_t>(sizeof(T));
        }

        virtual void toBytes(DataBuffer &buffer) override
        {
#if CPP_14
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif

            for (s32 i = 0; i < sbyteCount(); ++i)
            {
                buffer.add(value.buf[i]);
            }
        }

        virtual void fromBytes(DataBuffer &buffer) override
        {
#if CPP_14
            static_assert(std::is_trivial<T>::value, "Expected primitive value to pass trivial type test (i.e. 'std::is_trivial<T>::value')");
#endif

            for (s32 i = sbyteCount() - 1; i >= 0; --i)
            {
                value.buf[i] = buffer.removeLast();
            }
        }
    };

}

#endif //!HCLIB_TRIVIAL_SERIALIZABLE_H

