#pragma once

#ifndef HCLIB_ISERIALIZABLE_H
#define HCLIB_ISERIALIZABLE_H

#include <hclib/Types.h>
#include <hclib/DataBuffer.h>

namespace hclib
{

    class DataBuffer;

    class ISerializable
    {
    protected:

        ISerializable() = default;

    public:

        virtual ~ISerializable() = default;

        virtual s32 sbyteCount() const HCLIB_NOEXCEPT = 0;
        virtual std::size_t byteCount() const HCLIB_NOEXCEPT = 0;

        virtual void toBytes(DataBuffer &buffer) = 0;
        virtual void fromBytes(DataBuffer &buffer) = 0;

    };

}

#endif //!HCLIB_ISERIALIZABLE_H

