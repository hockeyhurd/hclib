/**
 * @author hockeyhurd
 * @version 2020-08-16
 */

#pragma once

#ifndef HCLIB_RANGE_H
#define HCLIB_RANGE_H

namespace hclib
{

    template<class T>
    class Range
    {
    private:
        T min;
        T max;

    public:
        explicit Range(const T &min, const T &max);
        explicit Range(T &&min = T(), T &&max = T());
        Range(const Range<T> &other) = default;
        Range(Range<T> &&other) = default;

        ~Range() = default;

        Range<T> copy() const;

        Range<T> &operator= (const Range<T> &) = default;
        Range<T> &operator= (Range<T> &&) = default;

        bool operator== (const Range<T> &other) const;
        bool operator== (Range<T> &&other) const;
        bool operator!= (const Range<T> &other) const;
        bool operator!= (Range<T> &&other) const;

        T &getMin();
        const T &getMin() const;
        void setMin(const T &min);
        void setMin(T &&min);

        T &getMax();
        const T &getMax() const;
        void setMax(const T &max);
        void setMax(T &&max);

        bool inRange(const T &value) const;
        bool inRange(T &&value) const;

    };

}

#include "Range.cpp"

#endif //!HCLIB_RANGE_H
