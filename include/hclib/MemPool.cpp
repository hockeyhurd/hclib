#include <memory>
#include <new>

namespace hclib
{

#define MEMPOOL_TEMPLATE template<class T, size_t CAPACITY>
#define MEMPOOL_CLASS MemPool<T, CAPACITY>

    MEMPOOL_TEMPLATE
    MEMPOOL_CLASS::value_type *MEMPOOL_CLASS::allocate(const size_t n)
    {
    }

    MEMPOOL_TEMPLATE
    void MEMPOOL_CLASS::deallocate(MEMPOOL_CLASS::value_type *ptr, const size_t n)
    {
    }

#undef MEMPOOL_TEMPLATE
#undef MEMPOOL_CLASS

}
