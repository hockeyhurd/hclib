#include <hclib/Types.h>

#include <functional>
#include <utility>

namespace hclib
{

#define HCLIB_VEC2_TEMPLATE template<class T>
#define HCLIB_VEC2_OBJ Vec2<T>

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ::Vec2() noexcept : Vec2(0, 0)
    {
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ::Vec2(const T &x, const T &y) noexcept : x(x), y(y)
    {
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ::Vec2(T &&x, T &&y) noexcept : x(std::forward<T>(x)), y(std::forward<T>(y))
    {
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::copy() const noexcept
    {
        return HCLIB_VEC2_OBJ(x, y);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator+= (const HCLIB_VEC2_OBJ &other) noexcept
    {
        x += other.x;
        y += other.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator+= (HCLIB_VEC2_OBJ &&other) noexcept
    {
        x += std::forward<T>(other.x);
        y += std::forward<T>(other.y);

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator+= (const T &value) noexcept
    {
        x += value;
        y += value;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator+= (T &&value) noexcept
    {
        return (*this += std::ref(value));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator-= (const HCLIB_VEC2_OBJ &other) noexcept
    {
        x -= other.x;
        y -= other.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator-= (HCLIB_VEC2_OBJ &&other) noexcept
    {
        x -= std::forward<T>(other.x);
        y -= std::forward<T>(other.y);

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator-= (const T &value) noexcept
    {
        x -= value;
        y -= value;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator-= (T &&value) noexcept
    {
        return (*this -= std::ref(value));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator*= (const HCLIB_VEC2_OBJ &other) noexcept
    {
        x *= other.x;
        y *= other.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator*= (HCLIB_VEC2_OBJ &&other) noexcept
    {
        x *= std::forward<T>(other.x);
        y *= std::forward<T>(other.y);

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator*= (const T &value) noexcept
    {
        x *= value;
        y *= value;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator*= (T &&value) noexcept
    {
        return (*this *= std::ref(value));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator/= (const HCLIB_VEC2_OBJ &other)
    {
        x /= other.x;
        y /= other.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator/= (HCLIB_VEC2_OBJ &&other)
    {
        x /= std::forward<T>(other.x);
        y /= std::forward<T>(other.y);

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator/= (const T &value)
    {
        x /= value;
        y /= value;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::operator/= (T &&value)
    {
        return (*this /= std::ref(value));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator+ (const HCLIB_VEC2_OBJ &other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x + other.x, y + other.y);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator+ (HCLIB_VEC2_OBJ &&other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x + std::forward<T>(other.x), y + std::forward<T>(other.y));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator+ (const T &value) const noexcept
    {
        return HCLIB_VEC2_OBJ(x + value, y + value);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator+ (T &&value) const noexcept
    {
        auto result = *this;
        result += std::forward<T>(value);
        return result;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator- (const HCLIB_VEC2_OBJ &other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x - other.x, y - other.y);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator- (HCLIB_VEC2_OBJ &&other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x - std::forward<T>(other.x), y - std::forward<T>(other.y));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator- (const T &value) const noexcept
    {
        return HCLIB_VEC2_OBJ(x - value, y - value);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator- (T &&value) const noexcept
    {
        auto result = *this;
        result -= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator* (const HCLIB_VEC2_OBJ &other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x * other.x, y * other.y);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator* (HCLIB_VEC2_OBJ &&other) const noexcept
    {
        return HCLIB_VEC2_OBJ(x * std::forward<T>(other.x), y * std::forward<T>(other.y));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator* (const T &value) const noexcept
    {
        return HCLIB_VEC2_OBJ(x * value, y * value);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator* (T &&value) const noexcept
    {
        auto result = *this;
        result *= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator/ (const HCLIB_VEC2_OBJ &other) const
    {
        return HCLIB_VEC2_OBJ(x / other.x, y / other.y);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator/ (HCLIB_VEC2_OBJ &&other) const
    {
        return HCLIB_VEC2_OBJ(x / std::forward<T>(other.x), y / std::forward<T>(other.y));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator/ (const T &value) const
    {
        return HCLIB_VEC2_OBJ(x / value, y / value);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::operator/ (T &&value) const
    {
        auto result = *this;
        result /= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC2_TEMPLATE
    bool HCLIB_VEC2_OBJ::operator== (const HCLIB_VEC2_OBJ &other) const noexcept
    {
        return x == other.x && y == other.y;
    }

    HCLIB_VEC2_TEMPLATE
    bool HCLIB_VEC2_OBJ::operator== (HCLIB_VEC2_OBJ &&other) const noexcept
    {
        return x == other.x && y == other.y;
    }

    HCLIB_VEC2_TEMPLATE
    bool HCLIB_VEC2_OBJ::operator!= (const HCLIB_VEC2_OBJ &other) const noexcept
    {
        return !(*this == other);
    }

    HCLIB_VEC2_TEMPLATE
    bool HCLIB_VEC2_OBJ::operator!= (HCLIB_VEC2_OBJ &&other) const noexcept
    {
        return !(*this == other);
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::rotate(const HCLIB_VEC2_OBJ &origin, const T &angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::rotate(const HCLIB_VEC2_OBJ &origin, T &&angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::rotate(HCLIB_VEC2_OBJ &&origin, const T &angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ &HCLIB_VEC2_OBJ::rotate(HCLIB_VEC2_OBJ &&origin, T &&angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC2_TEMPLATE
    T HCLIB_VEC2_OBJ::distance(const HCLIB_VEC2_OBJ &other) noexcept
    {
        T xx = x - other.x;
        xx *= xx;
        T yy = y - other.y;
        yy *= yy;

        return std::sqrt(xx + yy);
    }

    HCLIB_VEC2_TEMPLATE
    T HCLIB_VEC2_OBJ::distance(HCLIB_VEC2_OBJ &&other) noexcept
    {
        return distance(std::ref(other));
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::midpoint(const HCLIB_VEC2_OBJ &other) noexcept
    {
        Vec2<T> result((other.x - x) / 2, (other.y - y) / 2);
        result += *this;

        return result;
    }

    HCLIB_VEC2_TEMPLATE
    HCLIB_VEC2_OBJ HCLIB_VEC2_OBJ::midpoint(HCLIB_VEC2_OBJ &&other) noexcept
    {
        return midpoint(std::ref(other));
    }

#undef HCLIB_VEC2_TEMPLATE
#undef HCLIB_VEC2_OBJ

}
