#include <hclib/Debug.h>

#include <iostream>

#define HCLIB_EDGE_TEMPLATE template<class T>
#define HCLIB_NODE_TEMPLATE template<class T>
#define HCLIB_GRAPH_TEMPLATE template<class T>

namespace hclib
{

    inline static void internalInsertIndentation(std::ostream &os, const s32 indentation)
    {
        for (s32 i = 0; i < indentation; ++i)
            os << ' ';
    }

    HCLIB_EDGE_TEMPLATE 
    bool Edge<T>::operator< (const Edge<T> &other) const
    {
        // if (weight == other.weight)
        // {
            if (first->value == other.first->value)
            {
                if (second->value == other.second->value)
                    return first->value < other.second->value;
                return second->value < other.second->value;
            }

            return first->value < other.first->value;
        // }

        // return weight < other.weight;
    }

    HCLIB_NODE_TEMPLATE
    Node<T>::Node(const T &value) : value(value)
    {
    }

    HCLIB_NODE_TEMPLATE
    Node<T>::Node(T &&value) : value(std::forward<T>(value))
    {
    }

    HCLIB_NODE_TEMPLATE
    void Node<T>::addEdge(Node<T> *other, const s32 weight)
    {
        Edge<T> newEdge(this, other, weight);
        const auto findResult = edges.find(newEdge);

        if (findResult == edges.end())
        {
            edges.emplace(std::move(newEdge));
        }
    }

    /* static */
    HCLIB_NODE_TEMPLATE
    void Node<T>::insertIndent(std::ostream &os, const s32 indentation)
    {
        internalInsertIndentation(os, indentation);
    }

    HCLIB_GRAPH_TEMPLATE
    void Graph<T>::addEdge(const T &first, const T &second, const s32 weight)
    {
        auto findFirst = map.find(first);
        if (findFirst == map.end())
        {
            Debug::info("first not found");
            map.emplace(first, Node<T>(first));
            findFirst = map.find(first);
        }

        Node<T> *firstNode = &(findFirst->second);
        Debug::info("firstNode: " + std::to_string(firstNode->value));

        auto findSecond = map.find(second);
        if (findSecond == map.end())
        {
            Debug::info("second not found");
            map.emplace(second, Node<T>(second));
            findSecond = map.find(second);
        }

        Node<T> *secondNode = &findSecond->second;
        Debug::info("secondNode: " + std::to_string(secondNode->value));

        firstNode->addEdge(secondNode, weight);
        secondNode->addEdge(firstNode, weight);
    }

    /* static */
    HCLIB_GRAPH_TEMPLATE
    void Graph<T>::insertIndent(std::ostream &os, const s32 indentation)
    {
        internalInsertIndentation(os, indentation);
    }
}
