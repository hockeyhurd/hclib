#include <utility>

#define PAIR_TEMPLATE template<class T1, class T2>
#define PAIR_T Pair<T1, T2>

namespace hclib
{

    PAIR_TEMPLATE
    PAIR_T::Pair(const T1 &first, const T2 &second) : first(first), second(second)
    {
    }

    PAIR_TEMPLATE
    PAIR_T::Pair(T1 &&first, const T2 &second) : first(std::forward<T1>(first)), second(second)
    {
    }

    PAIR_TEMPLATE
    PAIR_T::Pair(const T1 &first, T2 &&second) : first(first), second(std::forward<T2>(second))
    {
    }

    PAIR_TEMPLATE
    PAIR_T::Pair(T1 &&first, T2 &&second) : first(std::forward<T1>(first)), second(std::forward<T2>(second))
    {
    }

    PAIR_TEMPLATE
    PAIR_T PAIR_T::makePair(const T1 &first, const T2 &second)
    {
        return PAIR_T(first, second);
    }

    PAIR_TEMPLATE
    PAIR_T PAIR_T::makePair(T1 &&first, const T2 &second)
    {
        return PAIR_T(std::forward<T1>(first), second);
    }

    PAIR_TEMPLATE
    PAIR_T PAIR_T::makePair(const T1 &first, T2 &&second)
    {
        return PAIR_T(first, std::forward<T2>(second));
    }

    PAIR_TEMPLATE
    PAIR_T PAIR_T::makePair(T1 &&first, T2 &&second)
    {
        return PAIR_T(std::forward<T1>(first), std::forward<T2>(second));
    }

    PAIR_TEMPLATE
    PAIR_T makePair(const T1 &first, const T2 &second)
    {
        return PAIR_T(first, second);
    }

    PAIR_TEMPLATE
    PAIR_T makePair(T1 &&first, const T2 &second)
    {
        return PAIR_T(std::forward<T1>(first), second);
    }

    PAIR_TEMPLATE
    PAIR_T makePair(const T1 &first, T2 &&second)
    {
        return PAIR_T(first, std::forward<T2>(second));
    }

    PAIR_TEMPLATE
    PAIR_T makePair(T1 &&first, T2 &&second)
    {
        return PAIR_T(std::forward<T1>(first), std::forward<T2>(second));
    }

}

#undef PAIR_TEMPLATE
#undef PAIR_T
