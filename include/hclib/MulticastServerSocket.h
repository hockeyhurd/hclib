/*
 * Class for creating and using Posix style multicast server side sockets.
 *
 * @author hockeyhurd
 * @version 10/31/2021
 */

#pragma once

#ifndef HCLIB_MULTICAST_SERVER_SOCKET_H
#define HCLIB_MULTICAST_SERVER_SOCKET_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/UDPServerSocket.h>

// std includes
#include <string>

namespace hclib
{
    class HCLIB_EXPORT MulticastServerSocket : public UDPServerSocket
    {
    protected:
        std::string address;
        std::string nic;

    public:

        /**
         * Constructs a UDPServerSocket with the given port.
         *
         * @param port port number to bind this socket to.
         * @param reuse indicates this port can be reused.
         * @param address the multicast group address to use.
         * @param nic the local NIC to use.
         */
        MulticastServerSocket(const s32 port, const bool reuse, const std::string &address, const std::string &nic);

        /**
         * Constructs a UDPServerSocket with the given port.
         *
         * @param port port number to bind this socket to.
         * @param reuse indicates this port can be reused.
         * @param address the multicast group address to use.
         * @param nic the local NIC to use.
         */
        MulticastServerSocket(const s32 port, const bool reuse, std::string &&address, std::string &&nic);

        /**
         * Deleted copy constructor.
         */
        MulticastServerSocket(const MulticastServerSocket &) = delete;

        /**
         * Move constructor.
         */
        MulticastServerSocket(MulticastServerSocket &&) = default;

        /**
         * Destructor
         */
        virtual ~MulticastServerSocket();

        /**
         * Deleted copy assignment operator.
         */
        MulticastServerSocket &operator= (const MulticastServerSocket &) = delete;

        /**
         * Move assignment operator.
         */
        MulticastServerSocket &operator= (MulticastServerSocket &&) = default;

        /**
         * Gets the group multicast address.
         * 
         * @returns std::string.
         */
        virtual std::string &getGroupAddress();

        /**
         * Gets the group multicast address.
         * 
         * @returns std::string.
         */
        virtual const std::string &getGroupAddress() const;

        /**
         * Gets the local NIC.
         * 
         * @returns std::string.
         */
        virtual std::string &getNIC();

        /**
         * Gets the local NIC.
         * 
         * @returns std::string.
         */
        virtual const std::string &getNIC() const;

        /**
         * Closes the MulticastServerSocket.
         * Note: The derived class is not responsible for cleaning-up the socket
         * or socket handle.  This base class will handle this for you.
         */
        virtual void close() override;

        /**
         * Attempts to join the multicast group.
         */
        virtual void joinGroup();

        /**
         * Attempts to leave the multicast group.
         */
        virtual void leaveGroup();

    protected:

        /**
         * Sets the socket options.
         */
        virtual void setSocketOptions() override;

        /**
         * Actually sets the TTL socket options.
         */
        virtual void setTTLInternal() override;

        /**
         * Gets the IP address associated with the provided NIC.
         */ 
        std::string getIPFromNIC();
    };
}

#endif //!HCLIB_MULTICAST_SERVER_SOCKET_H

