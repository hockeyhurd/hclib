/**
 * A class for manging Timers.
 *
 * @author hockeyhurd
 * @version 03/19/2023
 */

#pragma once

#ifndef HCLIB_TIMER_MANAGER_H
#define HCLIB_TIMER_MANAGER_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/Timer.h>

// std includes
#include <atomic>
#include <mutex>
#include <queue>
#include <thread>
#include <unordered_set>

namespace hclib
{
    // Forward declarations
    class ThreadPool;

    class TimerManager
    {
    public:

        /**
         * Constructor
         *
         * @param frameIntervalMicro the max frame time for checking all timers.
         *        Note: This is a signed 64-bit int for compatibility with std::chrono.
         *        This value will be stored as an absolute value to ensure it is positive.
         *        It is recommended this value to be an "order of magnitude" smaller than
         *        the queue of timers. Also, the more Timers being managed and whether
         *        a ThreadPool is used or not, will also influence this value.
         *        For basic statistics, use the 'getOverrunCount' function to see if
         *        your interval is too high.
         * @param threadPool optional ThreadPool where an expired Timer can be
         *        scheduled and subsequently called asynchronously.
         */
        explicit TimerManager(const s64 frameIntervalMicro, ThreadPool* threadPool = nullptr) HCLIB_NOEXCEPT;

        /**
         * Deleted copy constructor.
         */
        TimerManager(const TimerManager&) = delete;

        /**
         * Default move constructor.
         */
        TimerManager(TimerManager&&) HCLIB_NOEXCEPT = delete;

        /**
         * Default destructor.
         */
        ~TimerManager();

        /**
         * Deleted copy constructor.
         */
        TimerManager& operator= (const TimerManager&) = delete;

        /**
         * Default move constructor.
         */
        TimerManager& operator= (TimerManager&&) HCLIB_NOEXCEPT = delete;

        /**
         * Gets the number of overruns that have occurred.
         * Note: The actual overrun count could theoretically be
         * higher if the frequency is high enough and the calling code
         * runs long enough. This is because 'std::size_t' overflow is NOTE
         * accounted for.
         *
         * @return std::size_t count.
         */
        std::size_t getOverrunCount() const HCLIB_NOEXCEPT;

        /**
         * Creates a new Timer and returns pointer to the created Timer.
         *
         * @param duration the duration of time for the Timer.
         * @param eventHandler the event handler callback when the Timer expires.
         * @param periodic bool flag to enable a periodic Timer.
         * @return pointer to the created Timer.
         */
        Timer* createTimer(u32 duration, TimerEvent eventHandler, bool periodic);

        /**
         * Removes the Timer from this TimerManager.
         *
         * @param timer pointer to the Timer to remove.
         * @return bool true if removed, else false.
         */
        bool removeTimer(Timer* timer);

        /**
         * Stops the TimerManager.
         */
        void stop();

    private:

        /**
         * The timer loop to run in the background thread.
         *
         * @param instance the instance of the TimerManager.
         */
        static void timerThreadLoop(TimerManager* instance);

    private:

        // The max frame interval for the manager to poll the thread queue.
        s64 frameIntervalMicro;

        // An optional ThreadPool supplied by the consumer of this object.
        ThreadPool* threadPool;

        // A counter for the number of overruns.
        std::atomic<std::size_t> overruns;

        // The queue for our Timers.
        std::queue<Timer*> timerQueue;

        // The mutex for our Timers.
        std::mutex queueMutex;

        // For keeping track UUID to Timer mappings.
        std::unordered_set<u32> timerSet;

        // The mutex for our timerMap.
        std::mutex setMutex;

        // Flag to indicate whether this TimerManager is running or not.
        std::atomic_bool running;

        // The background std::thread where the TimerManager will run.
        std::thread thread;
    };
}

#endif //!HCLIB_TIMER_MANAGER_H

