#pragma once

#ifndef HCLIB_TCP_SERVER_SOCKET_H
#define HCLIB_TCP_SERVER_SOCKET_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/Socket.h>

// System includes
#if OS_WIN
#include <WinSock2.h>

#pragma comment(lib, "Ws2_32.lib")
#else
#include <netinet/in.h>
#include <sys/select.h>
#endif

// std includes
#include <memory>

namespace hclib
{

    class HCLIB_EXPORT DataBuffer;
    class HCLIB_EXPORT TCPServerSocket;

    class HCLIB_EXPORT TCPServerSocket
    {
    protected:

        Socket mSocket;
        s32 port;
        bool reuse;
        fd_set readFileDescriptorSet;

    public:

        /**
         * Constructs a TCPServerSocket with the given port.
         *
         * @param port port number to bind this socket to.
         * @param reuse indicates this port can be reused.
         */
        explicit TCPServerSocket(const s32 port, const bool reuse = false);

        /**
         * Deleted copy constructor.
         */
        TCPServerSocket(const TCPServerSocket &) = delete;

        /**
         * Move constructor.
         */
        TCPServerSocket(TCPServerSocket &&other);

        /**
         * Destructor
         */
        virtual ~TCPServerSocket();

        /**
         * Deleted copy assignment operator.
         */
        TCPServerSocket &operator= (const TCPServerSocket &) = delete;

        /**
         * Move assignment operator.
         */
        TCPServerSocket &operator= (TCPServerSocket &&other);

        /**
         * Gets the port assigned to this TCPServerSocket.
         *
         * @return s32 port number.
         */
        virtual s32 getPort() const;

        /**
         * Flag for determining if the socket is opened and ready
         * to accept incoming connections.
         *
         * @return bool true if socket is open, else bool false.
         */
        virtual bool isSetup() const;

        /**
         * Opens the TCPServerSocket and binds the socket to the specified port.
         */
        virtual void open();

        /**
         * Closes the TCPServerSocket.
         * Note: The derived class is not responsible for cleaning-up the socket
         * or socket handle.  This base class will handle this for you.
         */
        virtual void close();

        /**
         * Sets the socket option indicating this address/port should be reusable
         * once the socket is closed.
         */
        virtual void makeReusable();

        /**
         * Accepts the incoming socket.
         * Note: If an error occurs, an std::runtime_error will be thrown,
         * otherwise this function will block until the incoming socket connection
         * arrives.
         *
         * @return The accepted Socket object pointer.
         */
        virtual std::unique_ptr<Socket> accept();

        /**
         * If time pointer is NULL, this function will return
         * immediately, else will return after the set time.
         *
         * NOTE: This function assumes a passed non-nullptr
         *       struct is properly formed.
         *
         * @return true if socket is ready to be 'accept'ed,
         *         else returns false.
         */
        virtual bool poll(timeval *time = nullptr);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The buffer of data to send to the client.
         * @param len The length of the buffer in bytes to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        static s32 send(const Socket *clientSocket, const char *buffer, const std::size_t len);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The DataBuffer to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        static s32 send(const Socket *clientSocket, const DataBuffer &buffer);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The DataBuffer to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        static s32 send(const Socket *clientSocket, DataBuffer &&buffer);

        /**
         * Conveinence receive function.  Essentially just performs a "clientSocket.receive(...)".
         *
         * @param clientSocket The client socket to receive data from.
         * @param buffer The buffer of data to write to from the receiving client.
         * @param len The length of data to receive from the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes received.
         */
        static s32 receive(const Socket *clientSocket, char *buffer, const s32 len);

        /**
         * Conveinence receive function.  Essentially just performs a "clientSocket.receive(...)".
         *
         * @param clientSocket The client socket to receive data from.
         * @param buffer The DataBuffer to write to from the receiving client.
         * @param len The length of data to receive from the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes received.
         */
        static s32 receive(const Socket *clientSocket, DataBuffer &buffer, const s32 len);

    protected:

        /**
         * Binds the socket to the given address and port.
         */
        virtual s32 bind();

        /**
         * Sets up our FD_Set used for polling for available sockets.
         */
        virtual void setupFDSet();

    private:

        /**
         * Internal 'close' function to cleanup our socket properly regardless
         * of the derived class's actions.
         */
        void closeInternal();
    };

}

#endif //!HCLIB_TCP_SERVER_SOCKET_H

