/**
 * Generic RAII wrapper guards for mutex objects.
 *
 * @author hockeyhurd
 * @version 2022-01-29
 */

#pragma once

#ifndef HCLIB_LOCK_GUARD_H
#define HCLIB_LOCK_GUARD_H

#include <hclib/Types.h>
#include <hclib/Mutex.h>

namespace hclib
{

    template<class T>
    class HCLIB_EXPORT UniqueLock
    {
    private:

        T &mutex;
        bool locked;

    public:

        /**
         * Constructor that auto-locks the mutex.
         *
         * @param mutex the mutex to aquire.
         */
        explicit UniqueLock(T &mutex);

        /**
         * Constructor that conditionally locks the mutex.
         *
         * @param mutex the mutex to aquire.
         * @param defer if true, the mutex will lock, else NOOP.
         */
        UniqueLock(T &mutex, const bool defer);

        /**
         * Deleted copy constructor.
         */
        UniqueLock(const UniqueLock<T> &) = delete;

        /**
         * Deleted move constructor.
         */
        UniqueLock(UniqueLock &&) = delete;

        /**
         * Destructor
         */
        ~UniqueLock();

        /**
         * Deleted copy assignment operator.
         */
        UniqueLock<T> &operator= (const UniqueLock<T> &) = delete;

        /**
         * Deleted move assignment operator.
         */
        UniqueLock<T> &operator= (UniqueLock<T> &&) = delete;

        /**
         * Gets the mutex by reference.
         */
        T &getMutex();

        /**
         * Gets the mutex by const reference.
         */
        const T &getMutex() const;

        /**
         * Locks the mutex.
         */
        void lock();

        /**
         * Attempts to lock the mutex.
         *
         * @return bool true if mutex was locked, else false.
         */
        bool tryLock();

        /**
         * Unlocks the mutex.
         */
        void unlock();
    };

    template<class T>
    class HCLIB_EXPORT SharedLock
    {
    private:

        T &mutex;
        bool locked;

    public:

        /**
         * Constructor that auto-locks the mutex.
         *
         * @param mutex the mutex to aquire.
         */
        explicit SharedLock(T &mutex);

        /**
         * Constructor that conditionally locks the mutex.
         *
         * @param mutex the mutex to aquire.
         * @param defer if true, the mutex will lock, else NOOP.
         */
        SharedLock(T &mutex, const bool defer);

        /**
         * Deleted copy constructor.
         */
        SharedLock(const SharedLock<T> &) = delete;

        /**
         * Deleted move constructor.
         */
        SharedLock(SharedLock &&) = delete;

        /**
         * Destructor
         */
        ~SharedLock();

        SharedLock<T> &operator= (const SharedLock<T> &) = delete;

        /**
         * Deleted move assigment operator.
         */
        SharedLock<T> &operator= (SharedLock<T> &&) = delete;

        T &getMutex();
        const T &getMutex() const;

        void lock();
        bool tryLock();
        void unlock();
    };
}

#ifndef HCLIB_LOCK_GUARD_INL
#define HCLIB_LOCK_GUARD_INL
#include "LockGuard.inl"
#endif //!HCLIB_LOCK_GUARD_INL

#endif //!HCLIB_LOCK_GUARD_H

