#pragma once

#ifndef HCLIB_LOCK_H
#define HCLIB_LOCK_H

#include <hclib/Types.h>

#include <utility>
#include <mutex>

#if CPP_17
#include <shared_mutex>
#endif

namespace hclib
{

    enum class LockType
    {
        UNIQUE, SHARED
    };

    template<class T>
    class Lock
    {
    private:

        T value;
        std::mutex mutex;

    public:

        template<class ...Args>
        explicit Lock(Args &&...args) : value(std::forward<Args>(args)...)
        {
        }

        Lock(const Lock<T> &) = delete;
        Lock(Lock<T> &&);
        ~Lock() = default;

        Lock<T> &operator= (const Lock<T> &) = delete;
        Lock<T> &operator= (Lock<T> &&);

        std::pair<std::unique_lock<std::mutex>, T*> get();

        void set(const T &value);
        void set(T &&value);
    };

#if CPP_17
    template<class T>
    class RWLock
    {
    private:

        T value;
        std::shared_mutex mutex;

    public:

        template<class ...Args>
        explicit RWLock(Args &&...args) : value(std::forward<Args>(args)...)
        {
        }

        RWLock(const RWLock<T> &) = delete;
        RWLock(RWLock<T> &&);
        ~RWLock() = default;

        RWLock<T> &operator= (const RWLock<T> &) = delete;
        RWLock<T> &operator= (RWLock<T> &&);

        std::pair<std::shared_lock<std::shared_mutex>, T*> get();

        void set(const T &value);
        void set(T &&value);
    };
#else
    template<class T>
    using RWLock = Lock<T>;
#endif

}

#include "Lock.cpp"

#endif //!HCLIB_LOCK_H
