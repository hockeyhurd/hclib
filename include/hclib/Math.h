/**
 * @author hockeyhurd
 * @version 2019-08-30
 */

#pragma once

#ifndef HCLIB_MATH_H
#define HCLIB_MATH_H

#include <hclib/Types.h>

#include <cmath>
#include <utility>

namespace hclib
{

    namespace math
    {
        template<class T>
        inline T abs(T value)
        {
            return value < static_cast<T>(0) ? -value : value;
        }

        template<class T>
        inline T &clamp(const T &min, const T &max, T &value)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;

            return value;
        }

        template<class T>
        inline T distance(const T x, const T y)
        {
            return static_cast<T>(std::sqrt((x * x) + (y * y)));
        }

        template<class T>
        inline bool isApprox(const T first, const T second, const T eps)
        {
            return abs(first - second) <= eps;
        }

        template<class T>
        T pow(const T base, const T exponent)
        {
            T result = base;

            for (T i = static_cast<T>(0); i < exponent - static_cast<T>(1); ++i)
            {
                result *= base;
            }

            return result;
        }

        template<>
        inline f32 pow<f32>(const f32 base, const f32 exponent)
        {
            return powf(base, exponent);
        }

        template<>
        inline f64 pow<f64>(const f64 base, const f64 exponent)
        {
            return std::pow(base, exponent);
        }

        template<class T>
        T nearestPowerOfTwo(const T value)
        {
            using namespace std;
            return static_cast<T>(round(log2(value)));
        }

        template<>
        f32 nearestPowerOfTwo<f32>(const f32 value);

        template<class T>
        T floorPowerOfTwo(const T value)
        {
            using namespace std;
            return static_cast<T>(floor(log2(value)));
        }

        template<>
        f32 floorPowerOfTwo<f32>(const f32 value);

        template<class T>
        T ceilPowerOfTwo(const T value)
        {
            using namespace std;
            return static_cast<T>(ceil(std::log2(value)));
        }

        template<>
        f32 ceilPowerOfTwo<f32>(const f32 value);

    }

}

#endif //!HCLIB_MATH_H
