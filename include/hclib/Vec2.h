#pragma once

#ifndef HCLIB_VEC2_H
#define HCLIB_VEC2_H

namespace hclib
{

    template<class T>
    struct Vec2
    {
        T x;
        T y;

        Vec2() noexcept;
        Vec2(const T &x, const T &y) noexcept;
        Vec2(T &&x, T &&y) noexcept;
        Vec2(const Vec2<T> &other) noexcept = default;
        Vec2(Vec2<T> &&other) noexcept = default;

        ~Vec2() = default;

        Vec2<T> copy() const noexcept;

        Vec2<T> &operator= (const Vec2<T> &) noexcept = default;
        Vec2<T> &operator= (Vec2<T> &&) noexcept = default;

        Vec2<T> &operator+= (const Vec2<T> &other) noexcept;
        Vec2<T> &operator+= (Vec2<T> &&other) noexcept;
        Vec2<T> &operator+= (const T &other) noexcept;
        Vec2<T> &operator+= (T &&value) noexcept;
        Vec2<T> &operator-= (const Vec2<T> &other) noexcept;
        Vec2<T> &operator-= (Vec2<T> &&other) noexcept;
        Vec2<T> &operator-= (const T &value) noexcept;
        Vec2<T> &operator-= (T &&value) noexcept;
        Vec2<T> &operator*= (const Vec2<T> &other) noexcept;
        Vec2<T> &operator*= (Vec2<T> &&other) noexcept;
        Vec2<T> &operator*= (const T &value) noexcept;
        Vec2<T> &operator*= (T &&value) noexcept;
        Vec2<T> &operator/= (const Vec2<T> &other);
        Vec2<T> &operator/= (Vec2<T> &&other);
        Vec2<T> &operator/= (const T &value);
        Vec2<T> &operator/= (T &&value);

        Vec2<T> operator+ (const Vec2<T> &other) const noexcept;
        Vec2<T> operator+ (Vec2<T> &&other) const noexcept;
        Vec2<T> operator+ (const T &value) const noexcept;
        Vec2<T> operator+ (T &&value) const noexcept;
        Vec2<T> operator- (const Vec2<T> &other) const noexcept;
        Vec2<T> operator- (Vec2<T> &&other) const noexcept;
        Vec2<T> operator- (const T &value) const noexcept;
        Vec2<T> operator- (T &&value) const noexcept;
        Vec2<T> operator* (const Vec2<T> &other) const noexcept;
        Vec2<T> operator* (Vec2<T> &&other) const noexcept;
        Vec2<T> operator* (const T &value) const noexcept;
        Vec2<T> operator* (T &&value) const noexcept;
        Vec2<T> operator/ (const Vec2<T> &other) const;
        Vec2<T> operator/ (Vec2<T> &&other) const;
        Vec2<T> operator/ (const T &value) const;
        Vec2<T> operator/ (T &&value) const;

        bool operator== (const Vec2<T> &other) const noexcept;
        bool operator== (Vec2<T> &&other) const noexcept;
        bool operator!= (const Vec2<T> &other) const noexcept;
        bool operator!= (Vec2<T> &&other) const noexcept;

        Vec2<T> &rotate(const Vec2<T> &origin, const T &angle) noexcept;
        Vec2<T> &rotate(const Vec2<T> &origin, T &&angle) noexcept;
        Vec2<T> &rotate(Vec2<T> &&origin, const T &angle) noexcept;
        Vec2<T> &rotate(Vec2<T> &&origin, T &&angle) noexcept;

        T distance(const Vec2<T> &other) noexcept;
        T distance(Vec2<T> &&other) noexcept;

        Vec2<T> midpoint(const Vec2<T> &other) noexcept;
        Vec2<T> midpoint(Vec2<T> &&other) noexcept;

    };

}

#include "Vec2.cpp"

#endif //!HCLIB_VEC2_H
