#pragma once

#ifndef HCLIB_DATA_BUFFER_H
#define HCLIB_DATA_BUFFER_H

#include <hclib/Types.h>

#include <cstdlib>
#include <memory>
#include <vector>

namespace hclib
{

    class HCLIB_EXPORT DataBuffer
    {
    public:

        template<class T>
        class Iterator;
        template<class T>
        class ReverseIterator;

        using iterator = Iterator<char>;
        using const_iterator = Iterator<const char>;
        using reverse_iterator = ReverseIterator<char>;
        using const_reverse_iterator = ReverseIterator<const char>;

        /**
         * Default constructor with optional parameters.
         *
         * @param capacity the capacity of the internal buffer.
         * @param endian the endian form of the data.
         * @param doZero option to zero the internal buffer after allocation.
         */
        explicit DataBuffer(const std::size_t capacity = 0x40, const EnumEndian endian = EnumEndian::LITTLE, const bool doZero = false);

        /**
         * Copy constructor.
         */
        DataBuffer(const DataBuffer&);

        /**
         * Move constructor.
         */
        DataBuffer(DataBuffer&&) HCLIB_NOEXCEPT;

        /**
         * Destructor.
         */
        virtual ~DataBuffer();

        /**
         * Copy assignment operator.
         */
        DataBuffer& operator= (const DataBuffer&);

        /**
         * Move assignment operator.
         */
        DataBuffer& operator= (DataBuffer&&) HCLIB_NOEXCEPT;

        /**
         * Gets the capacity of this DataBuffer.
         *
         * @return std::size_t.
         */
        virtual std::size_t getCapacity() const HCLIB_NOEXCEPT;

        /**
         * Gets whether this DataBuffer is empty or not.
         *
         * @return bool.
         */
        virtual bool empty() const HCLIB_NOEXCEPT;

        /**
         * Gets the size of this DataBuffer.
         *
         * @return std::size_t.
         */
        virtual std::size_t size() const HCLIB_NOEXCEPT;

        /**
         * Gets a reference to the char/byte at the provided index.
         *
         * @param index the std::size_t index into the DataBuffer.
         * @return reference to the char.
         */
        char& operator[] (const std::size_t index);

        /**
         * Gets a const reference to the char/byte at the provided index.
         *
         * @param index the std::size_t index into the DataBuffer.
         * @return const reference to the char.
         */
        const char& operator[] (const std::size_t index) const;

        /**
         * Gets an iterator to the beginning of the internal buffer.
         *
         * @return iterator.
         */
        DataBuffer::iterator begin() HCLIB_NOEXCEPT;

        /**
         * Gets a const iterator to the beginning of the internal buffer.
         *
         * @return const_iterator.
         */
        DataBuffer::const_iterator cbegin() const HCLIB_NOEXCEPT;

        /**
         * Gets a reverse iterator to the beginning of the internal buffer.
         *
         * @return reverse_iterator.
         */
        DataBuffer::reverse_iterator rbegin() HCLIB_NOEXCEPT;

        /**
         * Gets a const reverse iterator to the beginning of the internal buffer.
         *
         * @return const_reverse_iterator.
         */
        DataBuffer::const_reverse_iterator crbegin() const HCLIB_NOEXCEPT;

        /**
         * Gets an iterator to the end of the internal buffer.
         *
         * @return iterator.
         */
        DataBuffer::iterator end() HCLIB_NOEXCEPT;

        /**
         * Gets a const iterator to the end of the internal buffer.
         *
         * @return const_iterator.
         */
        DataBuffer::const_iterator cend() const HCLIB_NOEXCEPT;

        /**
         * Gets a reverse iterator to the end of the internal buffer.
         *
         * @return reverse_iterator.
         */
        DataBuffer::reverse_iterator rend() HCLIB_NOEXCEPT;

        /**
         * Gets a const reverse iterator to the end of the internal buffer.
         *
         * @return const_reverse_iterator.
         */
        DataBuffer::const_reverse_iterator crend() const HCLIB_NOEXCEPT;

        /**
         * Gets a pointer to the beginning of the internal buffer.
         *
         * @return pointer to the interal char buffer.
         */
        virtual char* data() HCLIB_NOEXCEPT;

        /**
         * Gets a const pointer to the beginning of the internal buffer.
         *
         * @return const pointer to the interal char buffer.
         */
        virtual const char* data() const HCLIB_NOEXCEPT;

        /**
         * Creates an identical copy of this DataBuffer.
         *
         * @return DataBuffer.
         */
        virtual DataBuffer copy();

        /**
         * Creates a new char array as a copy to the internal buffer.
         *
         * @return pointer to the start of the char array.
         */
        virtual char* copy(const std::size_t offset = 0);

        /**
         * Creates a new std::vector as a copy to the internal buffer.
         *
         * @return std::vector.
         */
        virtual std::vector<char> copyVector(const std::size_t offset = 0);

        /**
         * Creates a new unique_ptr to a char array as a copy to the internal buffer.
         *
         * @return std::unique_ptr char array.
         */
        virtual std::unique_ptr<char[]> copyUnique(const std::size_t offset = 0);

#if CPP_20
        /**
         * Creates a new shared_ptr to a char array as a copy to the internal buffer.
         *
         * @return std::shared_ptr char array.
         */
        virtual std::shared_ptr<char[]> copyShared(const std::size_t offset = 0);
#endif

        /**
         * Gets a reference to the char/byte at the offset index into the internal buffer.
         *
         * @param index std::size_t.
         * @return char reference.
         */
        virtual char& get(const std::size_t index);

        /**
         * Gets a const reference to the char/byte at the offset index into the internal buffer.
         *
         * @param index std::size_t.
         * @return char const reference.
         */
        virtual const char& get(const std::size_t index) const;

        /**
         * Gets a pointer to the char/byte at the offset index into the internal buffer.
         * Note: If the index is beyond the capacity of the interal buffer, this
         *       function shall reutrn 'nullptr'.
         *
         * @param index std::size_t.
         * @return char pointer.
         */
        virtual char* tryGet(const std::size_t index) HCLIB_NOEXCEPT;

        /**
         * Gets a const pointer to the char/byte at the offset index into the internal buffer.
         * Note: If the index is beyond the capacity of the interal buffer, this
         *       function shall reutrn 'nullptr'.
         *
         * @param index std::size_t.
         * @return const char pointer.
         */
        virtual const char* tryGet(const std::size_t index) const HCLIB_NOEXCEPT;

        /**
         * Sets a char/byte at a given index into the internal buffer.
         * Note: If the index is beyond the capacity of the internal buffer,
         *       this function shall return false.
         *
         * @return bool true if successful, else false.
         */
        virtual bool set(const char value, const std::size_t index);

        /**
         * Clears the buffer by setting each char/byte to the passed in value.
         * Note: By using the default parameter, this function is effectively
         *       the same as calling 'zero'.
         *
         * @param defaultValue the char/byte to clear/set.
         */
        virtual void clear(const char defaultValue = static_cast<char>(0)) HCLIB_NOEXCEPT;

        /**
         * Sets each char/byte in the internal buffer to zero.
         */
        virtual void zero() HCLIB_NOEXCEPT;

        /**
         * Resets the internal index in which calling any of the 'pack' family
         * of functions use.
         */
        virtual void resetPosition() HCLIB_NOEXCEPT;

        /**
         * Sets the internal index position.
         *
         * @param pos std::size_t position.
         * @return true if position was set, else false.
         */
        virtual bool setPosition(const std::size_t pos) HCLIB_NOEXCEPT;

        /**
         * Ensures the minimum pre-allocated size of the buffer.
         *
         * @param reserveSize the std::size_t to pre-allocate.
         */
        virtual void reserve(const std::size_t reserveSize);

        /**
         * Packs a char/byte of data into the buffer.
         *
         * @param value the char/byte to pack.
         */
        virtual void packByte(const char value);

        /**
         * Packs a char array of data into the buffer.
         *
         * @param data the char array to pack.
         * @param len the length of the array.
         */
        virtual void packBytes(const char* data, const std::size_t len);

        /**
         * Packs an std::string into the buffer.
         *
         * @param str the std::string to pack.
         */
        virtual void packString(const std::string& str);

        /**
         * Packs a u16 into the buffer.
         *
         * @param value the u16 to pack.
         */
        virtual void packU16(u16 value);

        /**
         * Packs a u32 into the buffer.
         *
         * @param value the u32 to pack.
         */
        virtual void packU32(u32 value);

        /**
         * Packs a u64 into the buffer.
         *
         * @param value the u64 to pack.
         */
        virtual void packU64(u64 value);

        /**
         * Packs a f32 into the buffer.
         *
         * @param value the f32 to pack.
         */
        virtual void packF32(f32 value);

        /**
         * Packs a f64 into the buffer.
         *
         * @param value the f64 to pack.
         */
        virtual void packF64(f64 value);

        /**
         * Unpacks a char/byte of data at the current iterator's position.
         *
         * @param iter iterator.
         * @return char/byte.
         */
        virtual char unpackByte(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a char/byte of data at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return char/byte.
         */
        virtual char unpackByte(const_iterator& iter) const HCLIB_NOEXCEPT;

        /**
         * Unpacks a u16 at the current iterator's position.
         *
         * @param iter iterator.
         * @return u16.
         */
        virtual u16 unpackU16(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a u16 at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return u16.
         */
        virtual u16 unpackU16(const_iterator& iter) const HCLIB_NOEXCEPT;

        /**
         * Unpacks a u32 at the current iterator's position.
         *
         * @param iter iterator.
         * @return u32.
         */
        virtual u32 unpackU32(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a u32 at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return u32.
         */
        virtual u32 unpackU32(const_iterator& iter) const HCLIB_NOEXCEPT;

        /**
         * Unpacks a u64 at the current iterator's position.
         *
         * @param iter iterator.
         * @return u64.
         */
        virtual u64 unpackU64(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a u64 at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return u64.
         */
        virtual u64 unpackU64(const_iterator& iter) const HCLIB_NOEXCEPT;

        /**
         * Unpacks a f32 at the current iterator's position.
         *
         * @param iter iterator.
         * @return f32.
         */
        virtual f32 unpackF32(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a f32 at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return f32.
         */
        virtual f32 unpackF32(const_iterator& iter) const HCLIB_NOEXCEPT;

        /**
         * Unpacks a f64 at the current iterator's position.
         *
         * @param iter iterator.
         * @return f64.
         */
        virtual f64 unpackF64(iterator& iter) HCLIB_NOEXCEPT;

        /**
         * Unpacks a f64 at the current iterator's position.
         *
         * @param iter const_iterator.
         * @return f64.
         */
        virtual f64 unpackF64(const_iterator& iter) const HCLIB_NOEXCEPT;

    public:

        template<class T>
        class Iterator
        {
        private:

            friend class DataBuffer;

            explicit Iterator(const DataBuffer* buffer, T* pos) HCLIB_NOEXCEPT :
                instance(buffer), pos(pos) {}

        public:

            Iterator(const Iterator<T>&) HCLIB_NOEXCEPT = default;
            Iterator(Iterator<T>&&) HCLIB_NOEXCEPT = default;
            ~Iterator() = default;

            Iterator<T>& operator= (const Iterator<T>&) HCLIB_NOEXCEPT = default;
            Iterator<T>& operator= (Iterator<T>&&) HCLIB_NOEXCEPT = default;

            // Prefix
            Iterator<T>& operator++ () HCLIB_NOEXCEPT
            {
                const T* endPos = instance->buffer + instance->capacity;

                if (pos < endPos)
                {
                    ++pos;
                }

                return *this;
            }

            Iterator<T>& operator-- () HCLIB_NOEXCEPT
            {
                if (pos >= instance->buffer)
                {
                    --pos;
                }

                return *this;
            }

            // Postfix
            Iterator<T>& operator++ (s32) HCLIB_NOEXCEPT
            {
                const T* endPos = instance->buffer + instance->capacity;

                if (pos < endPos)
                {
                    ++pos;
                }

                return *this;
            }

            Iterator<T>& operator-- (s32) HCLIB_NOEXCEPT
            {
                if (pos >= instance->buffer)
                {
                    --pos;
                }

                return *this;
            }

            bool operator== (const Iterator<T>& other) const HCLIB_NOEXCEPT
            {
                return instance == other.instance && pos == other.pos;
            }

            bool operator!= (const Iterator<T>& other) const HCLIB_NOEXCEPT
            {
                return !(*this == other);
            }

        private:
            const DataBuffer* instance;
            T* pos;
        };

        template<class T>
        class ReverseIterator
        {
        private:

            friend class DataBuffer;

            explicit ReverseIterator(const DataBuffer* buffer, T* pos) HCLIB_NOEXCEPT :
                instance(buffer), pos(pos) {}

        public:

            ReverseIterator(const ReverseIterator<T>&) HCLIB_NOEXCEPT = default;
            ReverseIterator(ReverseIterator<T>&&) HCLIB_NOEXCEPT = default;
            ~ReverseIterator() = default;

            ReverseIterator<T>& operator= (const ReverseIterator<T>&) HCLIB_NOEXCEPT = default;
            ReverseIterator<T>& operator= (ReverseIterator<T>&&) HCLIB_NOEXCEPT = default;

            // Prefix
            ReverseIterator<T>& operator++ () HCLIB_NOEXCEPT
            {
                if (pos >= instance->buffer)
                {
                    --pos;
                }

                return *this;
            }

            ReverseIterator<T>& operator-- () HCLIB_NOEXCEPT
            {
                const T* endPos = instance->buffer + instance->capacity;

                if (pos < endPos)
                {
                    ++pos;
                }

                return *this;
            }

            // Postfix
            ReverseIterator<T>& operator++ (s32) HCLIB_NOEXCEPT
            {
                if (pos >= instance->buffer)
                {
                    --pos;
                }

                return *this;
            }

            ReverseIterator<T>& operator-- (s32) HCLIB_NOEXCEPT
            {
                const T* endPos = instance->buffer + instance->capacity;

                if (pos < endPos)
                {
                    ++pos;
                }

                return *this;
            }

            bool operator== (const ReverseIterator<T>& other) const HCLIB_NOEXCEPT
            {
                return instance == other.instance && pos == other.pos;
            }

            bool operator!= (const ReverseIterator<T>& other) const HCLIB_NOEXCEPT
            {
                return !(*this == other);
            }

        private:
            const DataBuffer* instance;
            T* pos;
        };

    protected:

        /**
         * Internal function for resizing the internal buffer.
         *
         * @param numMoreBytes the std::size_t number of bytes.
         */
        virtual void resizeIfNeeded(const std::size_t numMoreBytes);

    protected:

        // The capacity of this internal buffer.
        std::size_t capacity;

        // The endianess of the internal buffer.
        EnumEndian endian;

        // The index of where the next byte shall be packed from.
        std::size_t index;

        // The internal char/byte buffer.
        char* buffer;
    };

}

#endif //!HCLIB_DATA_BUFFER_H
