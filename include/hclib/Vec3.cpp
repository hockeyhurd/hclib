#include <hclib/Types.h>

#include <functional>
#include <utility>

namespace hclib
{

#define HCLIB_VEC3_TEMPLATE template<class T>
#define HCLIB_VEC3_OBJ Vec3<T>

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ::Vec3() noexcept : Vec3(0, 0, 0)
    {
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ::Vec3(const T &x, const T &y, const T &z) noexcept : x(x), y(y), z(z)
    {
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ::Vec3(T &&x, T &&y, T &&z) noexcept : x(std::forward<T>(x)), y(std::forward<T>(y)), z(std::forward<T>(z))
    {
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::copy() const noexcept
    {
        return HCLIB_VEC3_OBJ(x, y, z);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator+= (const HCLIB_VEC3_OBJ &other) noexcept
    {
        x += other.x;
        y += other.y;
        z += other.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator+= (HCLIB_VEC3_OBJ &&other) noexcept
    {
        x += std::forward<T>(other.x);
        y += std::forward<T>(other.y);
        z += std::forward<T>(other.z);

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator+= (const T &value) noexcept
    {
        x += value;
        y += value;
        z += value;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator+= (T &&value) noexcept
    {
        return (*this += std::ref(value));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator-= (const HCLIB_VEC3_OBJ &other) noexcept
    {
        x -= other.x;
        y -= other.y;
        z -= other.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator-= (HCLIB_VEC3_OBJ &&other) noexcept
    {
        x -= std::forward<T>(other.x);
        y -= std::forward<T>(other.y);
        z -= std::forward<T>(other.z);

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator-= (const T &value) noexcept
    {
        x -= value;
        y -= value;
        z -= value;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator-= (T &&value) noexcept
    {
        return (*this -= std::ref(value));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator*= (const HCLIB_VEC3_OBJ &other) noexcept
    {
        x *= other.x;
        y *= other.y;
        z *= other.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator*= (HCLIB_VEC3_OBJ &&other) noexcept
    {
        x *= std::forward<T>(other.x);
        y *= std::forward<T>(other.y);
        z *= std::forward<T>(other.z);

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator*= (const T &value) noexcept
    {
        x *= value;
        y *= value;
        z *= value;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator*= (T &&value) noexcept
    {
        return (*this *= std::ref(value));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator/= (const HCLIB_VEC3_OBJ &other)
    {
        x /= other.x;
        y /= other.y;
        z /= other.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator/= (HCLIB_VEC3_OBJ &&other)
    {
        x /= std::forward<T>(other.x);
        y /= std::forward<T>(other.y);
        z /= std::forward<T>(other.z);

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator/= (const T &value)
    {
        x /= value;
        y /= value;
        z /= value;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::operator/= (T &&value)
    {
        return (*this /= std::ref(value));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator+ (const HCLIB_VEC3_OBJ &other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x + other.x, y + other.y, z + other.z);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator+ (HCLIB_VEC3_OBJ &&other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x + std::forward<T>(other.x), y + std::forward<T>(other.y), z + std::forward<T>(other.z));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator+ (const T &value) const noexcept
    {
        return HCLIB_VEC3_OBJ(x + value, y + value, z + value);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator+ (T &&value) const noexcept
    {
        auto result = *this;
        result += std::forward<T>(value);
        return result;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator- (const HCLIB_VEC3_OBJ &other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x - other.x, y - other.y, z - other.z);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator- (HCLIB_VEC3_OBJ &&other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x - std::forward<T>(other.x), y - std::forward<T>(other.y), z - std::forward<T>(other.z));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator- (const T &value) const noexcept
    {
        return HCLIB_VEC3_OBJ(x - value, y - value, z - value);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator- (T &&value) const noexcept
    {
        auto result = *this;
        result -= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator* (const HCLIB_VEC3_OBJ &other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x * other.x, y * other.y, z * other.z);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator* (HCLIB_VEC3_OBJ &&other) const noexcept
    {
        return HCLIB_VEC3_OBJ(x * std::forward<T>(other.x), y * std::forward<T>(other.y), z * std::forward<T>(other.z));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator* (const T &value) const noexcept
    {
        return HCLIB_VEC3_OBJ(x * value, y * value, z * value);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator* (T &&value) const noexcept
    {
        auto result = *this;
        result *= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator/ (const HCLIB_VEC3_OBJ &other) const
    {
        return HCLIB_VEC3_OBJ(x / other.x, y / other.y, z / other.z);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator/ (HCLIB_VEC3_OBJ &&other) const
    {
        return HCLIB_VEC3_OBJ(x / std::forward<T>(other.x), y / std::forward<T>(other.y), z / std::forward<T>(other.z));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator/ (const T &value) const
    {
        return HCLIB_VEC3_OBJ(x / value, y / value, z / value);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::operator/ (T &&value) const
    {
        auto result = *this;
        result /= std::forward<T>(value);
        return result;
    }

    HCLIB_VEC3_TEMPLATE
    bool HCLIB_VEC3_OBJ::operator== (const HCLIB_VEC3_OBJ &other) const noexcept
    {
        return x == other.x && y == other.y && z == other.z;
    }

    HCLIB_VEC3_TEMPLATE
    bool HCLIB_VEC3_OBJ::operator== (HCLIB_VEC3_OBJ &&other) const noexcept
    {
        return x == other.x && y == other.y && z == other.z;
    }

    HCLIB_VEC3_TEMPLATE
    bool HCLIB_VEC3_OBJ::operator!= (const HCLIB_VEC3_OBJ &other) const noexcept
    {
        return !(*this == other);
    }

    HCLIB_VEC3_TEMPLATE
    bool HCLIB_VEC3_OBJ::operator!= (HCLIB_VEC3_OBJ &&other) const noexcept
    {
        return !(*this == other);
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXY(const HCLIB_VEC3_OBJ &origin, const T &angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXY(const HCLIB_VEC3_OBJ &origin, T &&angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXY(HCLIB_VEC3_OBJ &&origin, const T &angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXY(HCLIB_VEC3_OBJ &&origin, T &&angle) noexcept
    {
        x -= origin.x;
        y -= origin.y;

        T newX = x * std::cos(angle) - y * std::sin(angle);
        T newY = y * std::cos(angle) + x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        y = std::forward<T>(newY) + origin.y;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXZ(const HCLIB_VEC3_OBJ &origin, const T &angle) noexcept
    {
        x -= origin.x;
        z -= origin.z;

        T newX = x * std::cos(angle) + z * std::sin(angle);
        T newZ = z * std::cos(angle) - x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXZ(const HCLIB_VEC3_OBJ &origin, T &&angle) noexcept
    {
        x -= origin.x;
        z -= origin.z;

        T newX = x * std::cos(angle) + z * std::sin(angle);
        T newZ = z * std::cos(angle) - x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXZ(HCLIB_VEC3_OBJ &&origin, const T &angle) noexcept
    {
        x -= origin.x;
        z -= origin.z;

        T newX = x * std::cos(angle) + z * std::sin(angle);
        T newZ = z * std::cos(angle) - x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateXZ(HCLIB_VEC3_OBJ &&origin, T &&angle) noexcept
    {
        x -= origin.x;
        z -= origin.z;

        T newX = x * std::cos(angle) + z * std::sin(angle);
        T newZ = z * std::cos(angle) - x * std::sin(angle);

        x = std::forward<T>(newX) + origin.x;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateYZ(const HCLIB_VEC3_OBJ &origin, const T &angle) noexcept
    {
        y -= origin.y;
        z -= origin.z;

        T newY = y * std::cos(angle) - z * std::sin(angle);
        T newZ = z * std::cos(angle) + y * std::sin(angle);

        y = std::forward<T>(newY) + origin.y;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateYZ(const HCLIB_VEC3_OBJ &origin, T &&angle) noexcept
    {
        y -= origin.y;
        z -= origin.z;

        T newY = y * std::cos(angle) - z * std::sin(angle);
        T newZ = z * std::cos(angle) + y * std::sin(angle);

        y = std::forward<T>(newY) + origin.y;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateYZ(HCLIB_VEC3_OBJ &&origin, const T &angle) noexcept
    {
        y -= origin.y;
        z -= origin.z;

        T newY = y * std::cos(angle) - z * std::sin(angle);
        T newZ = z * std::cos(angle) + y * std::sin(angle);

        y = std::forward<T>(newY) + origin.y;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::rotateYZ(HCLIB_VEC3_OBJ &&origin, T &&angle) noexcept
    {
        y -= origin.y;
        z -= origin.z;

        T newY = y * std::cos(angle) - z * std::sin(angle);
        T newZ = z * std::cos(angle) + y * std::sin(angle);

        y = std::forward<T>(newY) + origin.y;
        z = std::forward<T>(newZ) + origin.z;

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::transform(const Matrix<T, 3> &matrix) noexcept
    {
        x = x * matrix[0][0] + y * matrix[0][1] + z * matrix[0][2];
        y = x * matrix[1][0] + y * matrix[1][1] + z * matrix[1][2];
        z = x * matrix[2][0] + y * matrix[2][1] + z * matrix[2][2];

        return *this;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ &HCLIB_VEC3_OBJ::transform(Matrix<T, 3> &&matrix) noexcept
    {
        return transform(std::ref(matrix));
    }

    HCLIB_VEC3_TEMPLATE
    T HCLIB_VEC3_OBJ::distance(const HCLIB_VEC3_OBJ &other) noexcept
    {
        T xx = x - other.x;
        xx *= xx;
        T yy = y - other.y;
        yy *= yy;
        T zz = z - other.z;
        zz *= zz;

        return std::sqrt(xx + yy + zz);
    }

    HCLIB_VEC3_TEMPLATE
    T HCLIB_VEC3_OBJ::distance(HCLIB_VEC3_OBJ &&other) noexcept
    {
        return distance(std::ref(other));
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::midpoint(const HCLIB_VEC3_OBJ &other) noexcept
    {
        Vec3<T> result((other.x - x) / 2, (other.y - y) / 2, (other.z - z) / 2);
        result += *this;

        return result;
    }

    HCLIB_VEC3_TEMPLATE
    HCLIB_VEC3_OBJ HCLIB_VEC3_OBJ::midpoint(HCLIB_VEC3_OBJ &&other) noexcept
    {
        return midpoint(std::ref(other));
    }

#undef HCLIB_VEC3_TEMPLATE
#undef HCLIB_VEC3_OBJ

}
