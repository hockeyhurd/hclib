#pragma once

#ifndef HCLIB_ASYNC_INPUT_STREAM_H
#define HCLIB_ASYNC_INPUT_STREAM_H

#include <hclib/Types.h>

#include <atomic>
#include <functional>

#include <iosfwd>
namespace std
{
    class thread;
}

namespace hclib
{

    template<class T = char>
    class AsyncInputStream 
    {
    private:
        std::basic_istream<T> &inputStream;
        u32 sleepTimeMS;
        std::function<void(T)> inputCallbackFunc;
        std::atomic_bool running;
        std::thread *thread;

    public:
        explicit AsyncInputStream(const u32 sleepTimeMS = 10);
        explicit AsyncInputStream(std::basic_istream<T> &inputStream, const u32 sleepTimeMS = 10);
        AsyncInputStream(const AsyncInputStream<T> &other) = delete;
        AsyncInputStream(AsyncInputStream<T> &&other) = default;

        ~AsyncInputStream();

        AsyncInputStream<T> &operator= (const AsyncInputStream<T> &) = delete;
        AsyncInputStream<T> &operator= (AsyncInputStream<T> &&) = default;

        u32 getSleepTimeMS() const;
        // void setSleepTimeMS(const u32 sleepTimeMS);

        void start();
        void stop();

        /**
         * Undefined behavior if you change this functor after this
         * instance was started.
         */
        void registerOnInputCallback(std::function<void(T)> func);

    private:

        static void defaultOnInputCallback(T data);
        static void checkUserInput(AsyncInputStream<T> &inst);
    };

}

#include "AsyncInputStream.inl"

#endif //!HCLIB_ASYNC_INPUT_STREAM_H
