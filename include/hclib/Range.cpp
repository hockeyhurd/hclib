/**
 * @author hockeyhurd
 * @version 2020-08-16
 */

#include <functional>
#include <utility>

#define RANGE_TEMPLATE template<class T>
#define RANGE_T Range<T>

namespace hclib
{

    RANGE_TEMPLATE
    RANGE_T::Range(const T &min, const T &max) : min(min), max(max)
    {
    }

    RANGE_TEMPLATE
    RANGE_T::Range(T &&min, T &&max) : min(std::forward<T>(min)), max(std::forward<T>(max))
    {
    }

    RANGE_TEMPLATE
    bool RANGE_T::operator== (const RANGE_T &other) const
    {
        return min == other.min && max == other.max;
    }

    RANGE_TEMPLATE
    bool RANGE_T::operator== (RANGE_T &&other) const
    {
        return (*this == std::ref(other));
    }

    RANGE_TEMPLATE
    bool RANGE_T::operator!= (const RANGE_T &other) const
    {
        return !(*this == other);
    }

    RANGE_TEMPLATE
    bool RANGE_T::operator!= (RANGE_T &&other) const
    {
        return (*this != std::ref(other));
    }

    RANGE_TEMPLATE
    T &RANGE_T::getMin()
    {
        return min;
    }

    RANGE_TEMPLATE
    const T &RANGE_T::getMin() const
    {
        return min;
    }

    RANGE_TEMPLATE
    void RANGE_T::setMin(const T &min)
    {
        this->min = min;
    }

    RANGE_TEMPLATE
    void RANGE_T::setMin(T &&min)
    {
        this->min = std::forward<T>(min);
    }

    RANGE_TEMPLATE
    T &RANGE_T::getMax()
    {
        return max;
    }

    RANGE_TEMPLATE
    const T &RANGE_T::getMax() const
    {
        return max;
    }

    RANGE_TEMPLATE
    void RANGE_T::setMax(const T &max)
    {
        this->max = max;
    }

    RANGE_TEMPLATE
    void RANGE_T::setMax(T &&max)
    {
        this->max = std::forward<T>(max);
    }

    RANGE_TEMPLATE
    bool RANGE_T::inRange(const T &value) const
    {
        return min <= value && value <= max;
    }

    RANGE_TEMPLATE
    bool RANGE_T::inRange(T &&value) const
    {
        return inRange(std::ref(value));
    }

}

#undef RANGE_TEMPLATE
#undef RANGE_T
