#include <iostream>
#include <thread>
#include <utility>

#include <hclib/Logger.h>

namespace hclib
{

template<class StringType, class T>
static void printDebug(const StringType &msg, const T &value)
{
    static auto &logger = Logger::stdlogger();
    logger << "***** msg: " << msg << ", value: " << static_cast<s32>(value) << '\n';
}

#define TEMPLATE_ASYNC_INPUT_STREAM template<class T>
#define ASYNC_INPUT_STREAM_T AsyncInputStream<T>

    TEMPLATE_ASYNC_INPUT_STREAM
    ASYNC_INPUT_STREAM_T::AsyncInputStream(const u32 sleepTimeMS) : inputStream(std::cin),
        sleepTimeMS(sleepTimeMS), inputCallbackFunc(defaultOnInputCallback),
        running(false), thread(nullptr)
    {
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    ASYNC_INPUT_STREAM_T::AsyncInputStream(std::basic_istream<T> &inputStream, const u32 sleepTimeMS) :
        inputStream(inputStream), sleepTimeMS(sleepTimeMS), inputCallbackFunc(defaultOnInputCallback),
        running(false), thread(nullptr)
    {
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    ASYNC_INPUT_STREAM_T::~AsyncInputStream()
    {
        if (thread != nullptr)
        {
            thread->join();
            delete thread;
            thread = nullptr;
        }
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    u32 ASYNC_INPUT_STREAM_T::getSleepTimeMS() const
    {
        return sleepTimeMS;
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    void ASYNC_INPUT_STREAM_T::start()
    {
        // Check if already running
        if (running)
            return;

        running = true;
        thread = new std::thread(checkUserInput, std::ref(*this));
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    void ASYNC_INPUT_STREAM_T::stop()
    {
        // Check if already running
        if (!running)
            return;

        running = false;
        thread->join();
        delete thread;
        thread = nullptr;
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    void ASYNC_INPUT_STREAM_T::registerOnInputCallback(std::function<void(T)> func)
    {
        this->inputCallbackFunc = func;
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    /* static */
    void ASYNC_INPUT_STREAM_T::defaultOnInputCallback(T data)
    {
        // Do nothing...
    }

    TEMPLATE_ASYNC_INPUT_STREAM
    /* static */
    void ASYNC_INPUT_STREAM_T::checkUserInput(AsyncInputStream<T> &inst)
    {
        do
        {
            const s32 peekValue = inst.inputStream.peek();
            printDebug("-\\_/T\\_/      :", peekValue);
            // if ((peekValue = inst.inputStream.peek()) == EOF)
            if (peekValue == EOF)
            {
                printDebug("peeked char", peekValue);
                // std::this_thread::yield();
                std::this_thread::sleep_for(std::chrono::milliseconds(inst.getSleepTimeMS()));
            }

            else
            {
                const T getValue = inst.inputStream.get();
                inst.inputCallbackFunc(getValue);
            }
        }
        while (inst.running);
    }

}

