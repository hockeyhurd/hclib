/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#define HCLIB_CONCURRENT_VECTOR_TEMPLATE template<class T, class Allocator>
#define HCLIB_CONCURRENT_VECTOR ConcurrentVector<T, Allocator>

namespace hclib
{

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    HCLIB_CONCURRENT_VECTOR::ConcurrentVector(const size_t capacity)
    {
        vec.reserve(capacity);
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::empty()
    {
        const auto guard = mutex.lockGuard();
        auto result = vec.empty();

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::size()
    {
        const auto guard = mutex.lockGuard();
        auto size = vec.size();

        return size;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::reserve(const size_t capacity)
    {
        const auto guard = mutex.lockGuard();
        vec.reserve(capacity);
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::resize(const size_t capacity)
    {
        const auto guard = mutex.lockGuard();
        vec.resize(capacity);
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::clear()
    {
        const auto guard = mutex.lockGuard();
        vec.clear();
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::push_back(const T &data)
    {
        const auto guard = mutex.lockGuard();
        vec.push_back(data);
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::push_back(T &&data)
    {
        const auto guard = mutex.lockGuard();
        vec.push_back(std::forward<T>(data));
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    template<class ...Args>
    void HCLIB_CONCURRENT_VECTOR::emplace_back(Args &&...args)
    {
        const auto guard = mutex.lockGuard();
        vec.emplace_back(std::forward<T>(args)...);
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::pop_back()
    {
        const auto guard = mutex.lockGuard();
        vec.pop_back();
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    typename HCLIB_CONCURRENT_VECTOR::iterator HCLIB_CONCURRENT_VECTOR::erase(typename HCLIB_CONCURRENT_VECTOR::iterator pos)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec.erase(pos);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    typename HCLIB_CONCURRENT_VECTOR::iterator HCLIB_CONCURRENT_VECTOR::erase(typename HCLIB_CONCURRENT_VECTOR::const_iterator pos)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec.erase(pos);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    typename HCLIB_CONCURRENT_VECTOR::iterator HCLIB_CONCURRENT_VECTOR::erase(typename HCLIB_CONCURRENT_VECTOR::iterator first,
            typename HCLIB_CONCURRENT_VECTOR::iterator last)
    {

        const auto guard = mutex.lockGuard();
        auto result = vec.erase(first, last);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    typename HCLIB_CONCURRENT_VECTOR::iterator HCLIB_CONCURRENT_VECTOR::erase(typename HCLIB_CONCURRENT_VECTOR::const_iterator first,
                                                                              typename HCLIB_CONCURRENT_VECTOR::const_iterator last)
    {

        const auto guard = mutex.lockGuard();
        auto result = vec.erase(first, last);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    void HCLIB_CONCURRENT_VECTOR::shrink_to_fit()
    {
        const auto guard = mutex.lockGuard();
        vec.shrink_to_fit();
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    T &HCLIB_CONCURRENT_VECTOR::at(const size_t index)
    {
        const auto guard = mutex.lockGuard();
        T &result = vec.at(index);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    T &HCLIB_CONCURRENT_VECTOR::at(const size_t index) const
    {
        const auto guard = mutex.lockGuard();
        T &result = vec.at(index);

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    T &HCLIB_CONCURRENT_VECTOR::operator[] (const size_t index)
    {
        const auto guard = mutex.lockGuard();
        T &result = vec[index];

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::begin()
    {
        const auto guard = mutex.lockGuard();
        auto iter = vec.begin();

        return iter;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::rbegin()
    {
        const auto guard = mutex.lockGuard();
        auto iter = vec.rbegin();

        return iter;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::rend()
    {
        const auto guard = mutex.lockGuard();
        auto iter = vec.rend();

        return iter;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::end()
    {
        const auto guard = mutex.lockGuard();
        auto iter = vec.end();

        return iter;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator== (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec == other;

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator!= (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec != other;

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator<= (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec <= other;

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator>= (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec >= other;

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator< (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec < other;

        return result;
    }

    HCLIB_CONCURRENT_VECTOR_TEMPLATE
    auto HCLIB_CONCURRENT_VECTOR::operator> (const HCLIB_CONCURRENT_VECTOR &other)
    {
        const auto guard = mutex.lockGuard();
        auto result = vec > other;

        return result;
    }

}
