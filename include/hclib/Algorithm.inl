#include <stdexcept>

namespace hclib
{
    namespace algorithm 
    {

        template<class Iter, class Compare>
        bool isSorted(Iter begin, Iter end, Compare compare)
        {
            auto last = begin;
            ++begin;

            for (; begin != end; ++begin)
            {
                const bool result = compare(*last, *begin);
                if (!result)
                    return false;

                last = begin;
            }

            return true;
        }

        template<class Iter>
        BoyerMoore<Iter>::BoyerMoore(Iter patternBegin, Iter patternEnd) :
            patternBegin(patternBegin), patternEnd(patternEnd),
            patternSize(distance<s64>(patternBegin, patternEnd))
        {
            if (patternSize <= 0)
                throw std::runtime_error("Pattern size is <= 0");
        }

        template<class Iter>
        template<class T, std::size_t CAPACITY>
        void BoyerMoore<Iter>::setupDeltaTable(std::array<T, CAPACITY> &table) const
        {
            table.fill(-1);

            std::size_t index = 0;
            for (auto iter = patternBegin; iter != patternEnd; ++iter)
            {
                const auto ch = *iter;
                table[ch] = index++;
            }
        }

        template<class Iter>
        Iter BoyerMoore<Iter>::search(Iter begin, Iter end)
        {
            const auto textSize = distance<s64>(begin, end);
            if (patternSize > textSize)
                return end;

            HCLIB_CONSTEXPR std::size_t tableSize = sizeof(char) << 8;
            std::array<s32, tableSize> deltaTable;
            setupDeltaTable<s32, tableSize>(deltaTable);

            Iter iter = begin;

            // TODO: Do we need to error check if size is 0 or something like that??
            // Ignore for now...
            auto textIndex = iter + patternSize - 1;

            while (textIndex <= end)
            {
                auto copyTextIndex = textIndex;
                auto patternIndex = patternEnd - 1;
                auto patternIntIndex = patternSize - 1;

                while (patternIntIndex >= 0 && *copyTextIndex == *patternIndex)
                {
                    // Found our result
                    if (patternIntIndex == 0)
                        return copyTextIndex;

                    --copyTextIndex;
                    --patternIndex;
                    --patternIntIndex;
                }

                // Keep searching and leveraging our jump/delta table.
                const auto currentCharInText = *copyTextIndex;

                // Lookup where
                const auto indexInDeltaTable = deltaTable[currentCharInText];

                // Not in table, shift to beginning of pattern.
                if (indexInDeltaTable < 0)
                {
                    for (auto i = 0; i < patternIntIndex + 1; ++i)
                        ++textIndex;
                }

                else
                {
                    auto delta = patternIntIndex - indexInDeltaTable;
                    for (auto i = 0; i < delta; ++i)
                        ++textIndex;
                }
            }

            return end;
        }

        template<class T, class String>
        String toHexString(const T &value)
        {
#if CPP_VER >= 2014
            static_assert(std::is_integral<T>::value, "Expected value to be an integer type");
#endif

            static char hexTable[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                                       '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

            String output = "";
            // unint64_t max string as base 10: 18446744073709551615
            output.reserve(0x20);
            T copy = value;

            // Get the digits as chars
            for (s32 i = 0; i < static_cast<s32>(sizeof(T)); ++i)
            {
                T digit = static_cast<char>(copy & 0x0F);
                T hexDigit = hexTable[digit];
                copy >>= 4;
                output += hexDigit;

                digit = static_cast<char>(copy & 0x0F);
                hexDigit = hexTable[digit];
                copy >>= 4;
                output += hexDigit;
            }

            const auto size = output.size();
            const auto halfSize = size / 2;

            // The previous step inserted them in reverse order.
            // Now we need to reverse again.
            for (T i = 0; i < halfSize; ++i)
            {
                const s32 otherIndex = size - 1 - i;
                std::swap(output[i], output[otherIndex]);
            }

            return output;
        }

        template<class T, class String>
        String toHexString(T &&value)
        {
            return toHexString(value);
        }

    }
}
