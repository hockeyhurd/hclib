/**
 * A class for checking elapsed time from the creation of this class instance.
 *
 * @author hockeyhurd
 * @version 04/01/2023
 */

#pragma once

#ifndef HCLIB_STOPWATCH_H
#define HCLIB_STOPWATCH_H

#include <hclib/Types.h>

#include <chrono>

namespace hclib
{

    template<class Clock = std::chrono::high_resolution_clock>
    class StopWatch
    {
    private:
        std::chrono::time_point<Clock> start;

    public:

        /*
         * Default constructor.
         */
        explicit StopWatch();

        /*
         * Default copy constructor.
         */
        StopWatch(const StopWatch<Clock>&) = default;

        /*
         * Default move constructor.
         */
        StopWatch(StopWatch<Clock>&&) = default;

        /*
         * Default destructor.
         */
        ~StopWatch() = default;

        /*
         * Default copy assignment operator.
         */
        StopWatch<Clock>& operator= (const StopWatch<Clock>&) = default;

        /*
         * Default move assignment operator.
         */
        StopWatch<Clock>& operator= (StopWatch<Clock>&&) = default;

    private:

        template<class T>
        s64 elapsedTime() const
        {
            const auto now = Clock::now();
            return std::chrono::duration_cast<T>(now - start).count();
        }

    public:

        /*
         * Gets the elapsed time in nanoseconds.
         *
         * @return Elapsed time in nanoseconds.
         */
        s64 nanoseconds() const;

        /*
         * Gets the elapsed time in microseconds.
         *
         * @return Elapsed time in microseconds.
         */
        s64 microseconds() const;

        /*
         * Gets the elapsed time in milliseconds.
         *
         * @return Elapsed time in milliseconds.
         */
        s64 milliseconds() const;

        /*
         * Gets the elapsed time in seconds.
         *
         * @return Elapsed time in seconds.
         */
        s64 seconds() const;

        /*
         * Gets the elapsed time in minutes.
         *
         * @return Elapsed time in minutes.
         */
        s64 minutes() const;

        /*
         * Gets the elapsed time in hours.
         *
         * @return Elapsed time in hours.
         */
        s64 hours() const;

#if CPP_VER >= 2020
        /*
         * Gets the elapsed time in days.
         *
         * @return Elapsed time in days.
         */
        s64 days() const;
#endif

        /*
         * Resets the start time.
         */
        void reset();
    };
}

#include "StopWatch.cpp"

#endif //!HCLIB_STOPWATCH_H
