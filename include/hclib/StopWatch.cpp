/**
 * A class for checking elapsed time from the creation of this class instance.
 *
 * @author hockeyhurd
 * @version 04/01/2023
 */

namespace hclib
{

    template<class Clock>
    StopWatch<Clock>::StopWatch() : start(Clock::now())
    {
    }

    template<class Clock>
    s64 StopWatch<Clock>::nanoseconds() const
    {
        return elapsedTime<std::chrono::nanoseconds>();
    }

    template<class Clock>
    s64 StopWatch<Clock>::microseconds() const
    {
        return elapsedTime<std::chrono::microseconds>();
    }

    template<class Clock>
    s64 StopWatch<Clock>::milliseconds() const
    {
        return elapsedTime<std::chrono::milliseconds>();
    }

    template<class Clock>
    s64 StopWatch<Clock>::seconds() const
    {
        return elapsedTime<std::chrono::seconds>();
    }

    template<class Clock>
    s64 StopWatch<Clock>::minutes() const
    {
        return elapsedTime<std::chrono::minutes>();
    }

    template<class Clock>
    s64 StopWatch<Clock>::hours() const
    {
        return elapsedTime<std::chrono::hours>();
    }

#if CPP_VER >= 2020
    template<class Clock>
    s64 StopWatch<Clock>::days() const
    {
        return elapsedTime<std::chrono::days>();
    }
#endif

    template<class Clock>
    void StopWatch<Clock>::reset()
    {
        start = Clock::now();
    }

}
