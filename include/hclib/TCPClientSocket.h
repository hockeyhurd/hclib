#pragma once

#ifndef HCLIB_TCP_CLIENT_SOCKET_H
#define HCLIB_TCP_CLIENT_SOCKET_H

#include <hclib/Types.h>
#include <hclib/Socket.h>

namespace hclib
{

    class HCLIB_EXPORT TCPClientSocket : public Socket
    {
    protected:

        std::string address;
        s32 port;

    public:

        /**
         * Constructs a TCPClientSocket object.  This makes no attempt to validate
         * the input values though it is assumed the address is real and port > 0
         * is valid otherwise, invalid.
         *
         * @param address std::string address of the server to connect to.
         * @param port s32 port number of the server to connect to.
         */
        TCPClientSocket(const std::string &address, const s32 port);

        /**
         * Constructs a TCPClientSocket object.  This makes no attempt to validate
         * the input values though it is assumed the address is real and port > 0
         * is valid otherwise, invalid.
         *
         * @param address std::string address of the server to connect to.
         * @param port s32 port number of the server to connect to.
         */
        TCPClientSocket(std::string &&address, const s32 port);

        /**
         * Deleted copy constructor per parent Socket class requirement.
         */
        TCPClientSocket(const TCPClientSocket &other) = delete;

        /**
         * Default move constructor.
         */
        TCPClientSocket(TCPClientSocket &&) = default;

        /**
         * Default destructor.
         */
        virtual ~TCPClientSocket() = default;

        /**
         * Deleted copy assignment operator per parent Socket class requirement.
         */
        TCPClientSocket &operator= (const TCPClientSocket &) = delete;

        /**
         * Default move assignment operator.
         */
        TCPClientSocket &operator= (TCPClientSocket &&) = default;

        /**
         * Gets the address of the server we are connecting to.
         *
         * @return std::string address.
         */
        virtual const std::string &getAddress() const;

        /**
         * Gets the address of the server we are connecting to.
         *
         * @return std::string address.
         */
        virtual std::string &getAddress();

        /**
         * Sets the address of this socket.
         * Note: It is up to the caller to ensure the socket has
         * been disconnected first otherwise this could lead to
         * undefined behavior.
         *
         * @param address std::string address of the server we are
         * attempting to connect to.
         */
        virtual void setAddress(const std::string &address);

        /**
         * Sets the address of this socket.
         * Note: It is up to the caller to ensure the socket has
         * been disconnected first otherwise this could lead to
         * undefined behavior.
         *
         * @param address std::string address of the server we are
         * attempting to connect to.
         */
        virtual void setAddress(std::string &&address);

        /**
         * Gets the port we are connecting to.
         *
         * @return s32 port number.
         */
        virtual s32 getPort() const;

        /**
         * Sets the port number of the destination server to connect to.
         * Note: It is up to the caller to ensure the socket has
         * been disconnected first otherwise this could lead to
         * undefined behavior.
         *
         * @param port s32 port number.
         */
        virtual void setPort(const s32 port);

        /**
         * Checks the internal socket handle to determine if we are connected
         * to the server or not.
         *
         * @return bool true if connected, otherwise bool false.
         */
        virtual bool isConnected() const;

        /**
         * Attempts to connect to the server.  If this fails,
         * an std::runtime_error exception shall be thrown.
         */
        virtual void connect();

        /**
         * Attempts to disconnect with the server.  If this fails,
         * an std::runtime_error exception shall be thrown.
         */
        virtual void disconnect();

        /**
         * Overrides the base Socket class's implementation and essentially
         * calls the 'disconnect' function.
         */
        virtual void close() override;
    };

}

#endif //!HCLIB_TCP_CLIENT_SOCKET_H

