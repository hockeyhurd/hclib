/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#pragma once

#ifndef HCLIB_CALLABLE_H
#define HCLIB_CALLABLE_H

#include <hclib/Types.h>

#include <functional>

namespace hclib
{

    class HCLIB_EXPORT Callable
    {

    protected:

        std::function<void()> func;

    public:

        template<class Func, class ...Args>
        explicit Callable(Func &&func, Args &&...args) : func(std::bind(func, std::forward<Args>(args)...))
        {
        }

        explicit Callable(std::function<void()> &&);

        virtual ~Callable() = default;

        virtual void operator()();

    };

}

#endif //HCLIB_CALLABLE_H
