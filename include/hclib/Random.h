/**
 * @author hockeyhurd
 * @version 2020-02-22
 */

#pragma once

#ifndef HCLIB_RANDOM_H
#define HCLIB_RANDOM_H

#include <hclib/Types.h>

#include <random>

namespace hclib
{

    class HCLIB_EXPORT RandomInt
    {
    private:

        s32 lowerBound, upperBound;
        std::default_random_engine generator;
        std::uniform_int_distribution<s32> distribution;

    public:

        RandomInt(const s32 lowerBound = 0, const s32 upperBound = 0xFFFF);
        RandomInt(const RandomInt &) = default;
        RandomInt(RandomInt &&) = default;
        ~RandomInt() = default;

        RandomInt &operator= (const RandomInt &) = default;
        RandomInt &operator= (RandomInt &&) = default;

        inline s32 getLowerBound() const
        {
            return lowerBound;
        }

        inline s32 getUpperBound() const
        {
            return upperBound;
        }

        s32 next();
    };

    class HCLIB_EXPORT RandomLong
    {
    private:

        s64 lowerBound, upperBound;
        std::default_random_engine generator;
        std::uniform_int_distribution<s64> distribution;

    public:

        RandomLong(const s64 lowerBound = 0, const s64 upperBound = 0xFFFF);
        RandomLong(const RandomLong &) = default;
        RandomLong(RandomLong &&) = default;
        ~RandomLong() = default;

        RandomLong &operator= (const RandomLong &) = default;
        RandomLong &operator= (RandomLong &&) = default;

        inline s64 getLowerBound() const
        {
            return lowerBound;
        }

        inline s64 getUpperBound() const
        {
            return upperBound;
        }

        s64 next();
    };

    class HCLIB_EXPORT RandomFloat
    {
    private:

        f32 lowerBound, upperBound;
        std::default_random_engine generator;
        std::uniform_real_distribution<f32> distribution;

    public:

        RandomFloat(const f32 lowerBound = 0.0F, const f32 upperBound = 1000.0F);
        RandomFloat(const RandomFloat &) = default;
        RandomFloat(RandomFloat &&) = default;
        ~RandomFloat() = default;

        RandomFloat &operator= (const RandomFloat &) = default;
        RandomFloat &operator= (RandomFloat &&) = default;

        inline f32 getLowerBound() const
        {
            return lowerBound;
        }

        inline f32 getUpperBound() const
        {
            return upperBound;
        }

        f32 next();
    };

    class HCLIB_EXPORT RandomDouble
    {
    private:

        f64 lowerBound, upperBound;
        std::default_random_engine generator;
        std::uniform_real_distribution<f64> distribution;

    public:

        RandomDouble(const f64 lowerBound = 0.0, const f64 upperBound = 1000.0);
        RandomDouble(const RandomDouble &) = default;
        RandomDouble(RandomDouble &&) = default;
        ~RandomDouble() = default;

        RandomDouble &operator= (const RandomDouble &) = default;
        RandomDouble &operator= (RandomDouble &&) = default;

        inline f64 getLowerBound() const
        {
            return lowerBound;
        }

        inline f64 getUpperBound() const
        {
            return upperBound;
        }

        f64 next();
    };

}

#endif //!HCLIB_RANDOM_H
