/*
 * Class for creating and using Posix style UDP server side sockets.
 *
 * @author hockeyhurd
 * @version 8/22/2021
 */

#pragma once

#ifndef HCLIB_UDP_SERVER_SOCKET_H
#define HCLIB_UDP_SERVER_SOCKET_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/Socket.h>

// System includes
#if OS_WIN
#include <WinSock2.h>

#pragma comment(lib, "Ws2_32.lib")
#else
#include <netinet/in.h>
#include <sys/select.h>
#endif

namespace hclib
{

    class HCLIB_EXPORT DataBuffer;
    class HCLIB_EXPORT UDPServerSocket;

    class HCLIB_EXPORT UDPServerSocket
    {
    protected:

        Socket mSocket;
        s32 port;
        bool reuse;
        s32 ttl;
        fd_set readFileDescriptorSet;

    public:

        /**
         * Constructs a UDPServerSocket with the given port.
         *
         * @param port port number to bind this socket to.
         * @param reuse indicates this port can be reused.
         * @param ttl sets the time to live.
         */
        explicit UDPServerSocket(const s32 port, const bool reuse = false, const u8 ttl = 64);

        /**
         * Deleted copy constructor.
         */
        UDPServerSocket(const UDPServerSocket &) = delete;

        /**
         * Move constructor.
         */
        UDPServerSocket(UDPServerSocket &&other);

        /**
         * Destructor
         */
        virtual ~UDPServerSocket();

        /**
         * Deleted copy assignment operator.
         */
        UDPServerSocket &operator= (const UDPServerSocket &) = delete;

        /**
         * Move assignment operator.
         */
        UDPServerSocket &operator= (UDPServerSocket &&other);

        /**
         * Gets the port assigned to this UDPServerSocket.
         *
         * @return s32 port number.
         */
        virtual s32 getPort() const HCLIB_NOEXCEPT;

        /**
         * Gets the time to live (TTL) assigned to this UDPServerSocket.
         *
         * @return s32 time to live.
         */
        virtual s32 getTTL() const HCLIB_NOEXCEPT;

        /**
         * Sets the TTL if the socket option can be set.
         *
         * @param ttl u8 time to live value to set.
         */
        virtual void setTTL(const u8 ttl);

        /**
         * Flag for determining if the socket is opened and ready
         * to accept incoming connections.
         *
         * @return bool true if socket is open, else bool false.
         */
        virtual bool isSetup() const HCLIB_NOEXCEPT;

        /**
         * Opens the UDPServerSocket and binds the socket to the specified port.
         */
        virtual void open();

        /**
         * Closes the UDPServerSocket.
         * Note: The derived class is not responsible for cleaning-up the socket
         * or socket handle.  This base class will handle this for you.
         */
        virtual void close();

        /**
         * Sets the socket option indicating this address/port should be reusable
         * once the socket is closed.
         */
        virtual void makeReusable();

        /**
         * If time pointer is NULL, this function will return
         * immediately, else will return after the set time.
         *
         * NOTE: This function assumes a passed non-nullptr
         *       struct is properly formed.
         *
         * @return true if socket is ready to be 'accept'ed,
         *         else returns false.
         */
        virtual bool poll(timeval *time = nullptr);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The buffer of data to send to the client.
         * @param len The length of the buffer in bytes to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        virtual s32 send(const Socket *clientSocket, const char *buffer, const std::size_t len);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The DataBuffer to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        virtual s32 send(const Socket *clientSocket, const DataBuffer &buffer);

        /**
         * Conveinence send function.  Essentially just performs a "clientSocket.send(...)".
         *
         * @param clientSocket The client socket to send data to.
         * @param buffer The DataBuffer to send to the client.
         * @return -1 if nullptrs are passed in or length is <= 0, else the number of bytes sent.
         */
        virtual s32 send(const Socket *clientSocket, DataBuffer &&buffer);

        /**
         * Conveinence receive function.  Essentially just performs a "clientSocket.receive(...)".
         *
         * @param buffer The buffer of data to write to from the receiving client.
         * @param len The length of data to receive from the client.
         * @param bytesReceived Optional param for getting the number of bytes received.
         * @return The client's socket.
         */
        virtual std::unique_ptr<Socket> receive(char *buffer, const s32 len, s32 *bytesReceived = nullptr);

        /**
         * Conveinence receive function.  Essentially just performs a "clientSocket.receive(...)".
         *
         * @param buffer The buffer of data to write to from the receiving client.
         * @param len The length of data to receive from the client.
         * @param bytesReceived Optional param for getting the number of bytes received.
         * @return The client's socket.
         */
        virtual std::unique_ptr<Socket> receive(DataBuffer &buffer, const s32 len, s32 *bytesReceived = nullptr);

    protected:

        /**
         * Binds the socket to the given address and port.
         */
        virtual s32 bind();

        /**
         * Sets the socket options.
         */
        virtual void setSocketOptions();

        /**
         * Actually sets the TTL socket options.
         */
        virtual void setTTLInternal();

        /**
         * Sets up our FD_Set used for polling for available sockets.
         */
        virtual void setupFDSet();

    private:

        /**
         * Internal 'close' function to cleanup our socket properly regardless
         * of the derived class's actions.
         */
        void closeInternal();
    };

}

#endif //!HCLIB_UDP_SERVER_SOCKET_H

