#pragma once

#ifndef HCLIB_PAIR_H
#define HCLIB_PAIR_H

#include <hclib/Types.h>

namespace hclib
{

    template<class T1, class T2>
    struct Pair
    {
        T1 first;
        T2 second;

        Pair(const T1 &first, const T2 &second);
        Pair(T1 &&first, const T2 &second);
        Pair(const T1 &first, T2 &&second);
        Pair(T1 &&first, T2 &&second);

        static Pair<T1, T2> makePair(const T1 &first, const T2 &second);
        static Pair<T1, T2> makePair(T1 &&first, const T2 &second);
        static Pair<T1, T2> makePair(const T1 &first, T2 &&second);
        static Pair<T1, T2> makePair(T1 &&first, T2 &&second);
    };

    template<class T1, class T2>
    Pair<T1, T2> makePair(const T1 &first, const T2 &second);

    template<class T1, class T2>
    Pair<T1, T2> makePair(T1 &&first, const T2 &second);

    template<class T1, class T2>
    Pair<T1, T2> makePair(const T1 &first, T2 &&second);

    template<class T1, class T2>
    Pair<T1, T2> makePair(T1 &&first, T2 &&second);

}

#include "Pair.cpp"

#endif //!HCLIB_PAIR_H
