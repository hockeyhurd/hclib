/**
 * Immutable class for storing immutable data.
 *
 * @author hockeyhurd
 * @version 2022-02-13
 */

#ifndef IMMUTABLE_TEMPLATE
#define IMMUTABLE_TEMPLATE template <class T>
#endif //!IMMUTABLE_TEMPLATE

#ifndef IMMUTABLE_CLASS
#define IMMUTABLE_CLASS Immutable<T>
#endif //!IMMUTABLE_CLASS

namespace hclib
{

    IMMUTABLE_TEMPLATE
    HCLIB_CONSTEXPR_FUNC IMMUTABLE_CLASS::Immutable() HCLIB_NOEXCEPT
    {
    }

    IMMUTABLE_TEMPLATE
    HCLIB_CONSTEXPR_FUNC IMMUTABLE_CLASS::Immutable(const T &data)
        : data(data)
    {
    }

    IMMUTABLE_TEMPLATE
    HCLIB_CONSTEXPR_FUNC IMMUTABLE_CLASS::Immutable(T &&data) HCLIB_NOEXCEPT
        : data(std::move(data))
    {
    }

    IMMUTABLE_TEMPLATE
    HCLIB_CONSTEXPR_FUNC const T &IMMUTABLE_CLASS::getData() const HCLIB_NOEXCEPT
    {
        return data;
    }

}

#undef IMMUTABLE_TEMPLATE
#undef IMMUTABLE_CLASS

