/**
 * @author hockeyhurd
 * @version 2020-08-23
 */

#pragma once

#ifndef HCLIB_SORT_H
#define HCLIB_SORT_H

// #include <functional>

namespace hclib
{

    template<class T, class Compare>
    void mergeSort(T &container, Compare compare);

    template<class Container, class T, class Compare>
    void quickSort(Container &container, Compare compare);

}

#include "Sort.cpp"

#endif //!HCLIB_SORT_H
