#pragma once

#ifndef HCLIB_SOCKET_H
#define HCLIB_SOCKET_H

// My includes
#include <hclib/Types.h>

// std includes
#include <memory>

#if OS_WIN
// Nothing for now
#else
#include <netinet/in.h>
#endif

// Forward declarations:
struct sockaddr_in;

namespace hclib
{

    // HCLib forward declarations.
    class HCLIB_EXPORT DataBuffer;
    class HCLIB_EXPORT MulticastServerSocket;
    class HCLIB_EXPORT TCPServerSocket;
    class HCLIB_EXPORT UDPServerSocket;

    /**
     * Enumeration values for various socket types to be supported by HCLib.
     */
    class EnumSocketType
    {
    private:
        s32 type;

        // Non-owning, weak-like pointer.
        std::string *name;

        /**
         * Private constructor for creating the EnumSocketType.
         */
        EnumSocketType(const s32 type, std::string *name);

    public:

        /**
         * Enumeration for TCP socket type.
         */
        static EnumSocketType TCP;

        /**
         * Enumeration for UDP socket type.
         */
        static EnumSocketType UDP;

        /**
         * Enumeration for RAW socket type.
         */
        static EnumSocketType RAW;

        /**
         * Default copy constructor.
         */
        EnumSocketType(const EnumSocketType &) = default;

        /**
         * Default move constructor.
         */
        EnumSocketType(EnumSocketType &&) = default;

        /**
         * Default destructor.
         */
        ~EnumSocketType() = default;

        /**
         * Default copy assignment operator.
         */
        EnumSocketType &operator= (const EnumSocketType &) = default;

        /**
         * Default move assignment operator.
         */
        EnumSocketType &operator= (EnumSocketType &&) = default;

        /**
         * Gets the C BSD socket value.
         *
         * @return s32 BSD socket value.
         */
        s32 getType() const;

        /**
         * Gets the name of the socket type.
         *
         * @return std::string socket name.
         */
        const std::string &getName() const;
    };

    /**
     * Simple wrapper around C BSD sockets.
     */
    class HCLIB_EXPORT Socket
    {
        // TODO: See if we can remove some of these classes we are friending.
        friend class MulticastServerSocket;
        friend class TCPServerSocket;
        friend class UDPServerSocket;

    protected:

        s32 sock;
        std::unique_ptr<sockaddr_in> handle;
        EnumSocketType socketType;

        /**
         * Protected constructor only used by derived classes.
         *
         * @param sock The socket.
         * @param handle The handle (Note: this class assumes ownership to this handle).
         * @param socketType The type of socket (TCP, UDP, etc.).
         */
        explicit Socket(const s32 sock = -1, std::unique_ptr<sockaddr_in> &&handle = nullptr,
                const EnumSocketType &socketType = EnumSocketType::TCP);

    public:

        /**
         * Delete copy constructor.
         */
        Socket(const Socket &) = delete;

        /**
         * Move constructor.
         *
         * @param other Socket to move from.
         */
        Socket(Socket &&other);

        /**
         * Default destructor.
         */
        virtual ~Socket() = default;

        /**
         * Delete copy assignment operator.
         */
        Socket &operator= (const Socket &) = delete;

        /**
         * Move assignment operator.
         *
         * @param other Socket to move from.
         */
        Socket &operator= (Socket &&other);

        /**
         * Gets the type of socket this instance is.
         *
         * @return EnumSocketType type of socket.
         */
        virtual EnumSocketType getSocketType() const;

        /**
         * Sets the socket type.
         * Note: This assumes the socket is NOT in a connected state.
         * Otherwise, this is undefined behavior.
         *
         * @param socketType The type of socket to set.
         */
        virtual void setSocketType(const EnumSocketType &socketType);

        /**
         * Closes the socket.
         */
        virtual void close();

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer Data buffer to send.
         * @param len The size of the buffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(const char *buffer, const std::size_t len) const;

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer DataBuffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(const DataBuffer &buffer) const;

        /**
         * Sends a data buffer to the destination.
         *
         * @param buffer DataBuffer to send.
         *
         * @return The s32 result of sending the data.
         */
        virtual s32 send(DataBuffer &&buffer) const;

        /**
         * Receives a data buffer from the destination.
         *
         * @param buffer Data buffer to put data into.
         * @param len The length of the buffer/max number of bytes able to receive.
         *
         * @return The s32 result of the operation.
         */
        virtual s32 receive(char *buffer, const s32 len) const;

        /**
         * Receives a data buffer from the destination.
         *
         * @param buffer DataBuffer to put data into.
         * @param len The length of the buffer/max number of bytes able to receive.
         *
         * @return The s32 result of the operation.
         */
        virtual s32 receive(DataBuffer &buffer, const s32 len) const;

        /**
         * Specialize std::swap for this class.
         */
        friend void swap(Socket &x, Socket &y)
        {
            std::swap(x.sock, y.sock);
            auto tempHandle = std::move(x.handle);
            x.handle = std::move(y.handle);
            y.handle = std::move(tempHandle);
        }
    };
}

#endif //!HCLIB_SOCKET_H

