/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#ifndef HCLIB_THREADPOOL_H
#define HCLIB_THREADPOOL_H

#include <atomic>
#include <condition_variable>
#include <thread>
#include <vector>

#include <ConcurrentQueue.h>
#include <Callable.h>

namespace hclib {

    class HCLIB_EXPORT ThreadPool {

    private:

        using Queue = ConcurrentQueue<std::function<void()>>;

        std::atomic<b32> running;
        std::mutex mutex;
        std::condition_variable cond;
        std::vector<std::thread> threads;
        std::vector<b32> threadStates;
        Queue jobs;

    public:

        ThreadPool(const u32 = std::thread::hardware_concurrency());
        ThreadPool(const ThreadPool &) = delete;
        ThreadPool(ThreadPool &&);

        ~ThreadPool();

        ThreadPool &operator= (const ThreadPool) = delete;
        ThreadPool &operator= (ThreadPool &&);

        inline b32 isRunning() const {
            return (b32) running;
        }

        inline Queue &getQueue() {
            return jobs;
        }

        // TODO: temp...
        inline std::mutex &getMutex() {
            return mutex;
        }

        inline std::condition_variable &getCond() {
            return cond;
        }

        void shutdown();

        void wait();

        void run(std::function<void()> &&);

        // void run(Callable &&);

        void setState(const u32, const b32);

    private:

        void init(const u32);

#if 0
        template<class Functor, class... Args>
        std::thread *requestThread(Functor &&func, Args &&... args) {
#if 0
            for (u32 i = 0; i < threads.size(); i++) {
                if (!threads[i].active) {
                    std::thread newThread(func, (args) ...);

                    threads[i].thread = std::move(newThread);
                    threads[i].active = True;

                    return &threads[i].thread;
                }
            }
#else
            for (auto &handle : threads) {
                if (!handle.active) {
                    // std::thread newThread(func, (args) ...);
                    std::thread newThread([&](typename std::decay<Functor>::type &&f, typename std::decay<Args>::type &&... args)
                                                 {
                                                     try
                                                     {
                                                         std::bind(f, args...)();
                                                     }
                                                     catch (...)
                                                     {
                                                         // exceptionPtr = std::current_exception();
                                                     }

                                                 }, std::forward<Functor>(func), std::forward<Args>(args)...);

                    handle.thread = std::move(newThread);
                    handle.active = True;

                    return &handle.thread;
                }
            }
#endif

            return nullptr;
        }
#endif

    };

}

#endif //HCLIB_THREADPOOL_H
