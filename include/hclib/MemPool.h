#pragma once

#ifndef HCLIB_MEMPOOL_H
#define HCLIB_MEMPOOL_H

#include "SmartPtr.h"
#include <cstdlib>

namespace hclib
{

#define MEMPOOL_TEMPLATE template<class T, size_t CAPACITY>

    template<class T, size_t CAPACITY = 4096>
    class MemPool
    {

    public:

        using value_type = T;



        MemPool() = default;

        template<class U, size_t CAPACITY>
        MemPool(MemPool<U, CAPACITY> const &other)
        {
        }

        value_type *allocate(const size_t n);

        void deallocate(value_type *ptr, const size_t n);

        inline size_t max_size() const noexcept
        {
            // return std::numeric_limits(size_type>::max() / sizeof(value_type);
            return CAPACITY * sizeof(value_type);
        }

    };

    template<class T, size_t CAP, class U, size_t CAP2>
    bool operator== (MemPool<T, CAP> const &, MemPool<U, CAP2> const &)
    {
        return false;
    }

    template<class T, size_t CAP, class U, size_t CAP2>
    bool operator!= (MemPool<T, CAP> const &left, MemPool<U, CAP2> const &right)
    {
        return !(left == right);
    }

#undef MEMPOOL_TEMPLATE

}

#include "MemPool.cpp"

#endif //!HCLIB_MEMPOOL_H
