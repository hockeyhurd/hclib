/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#pragma once

#ifndef HCLIB_MUTEX_H
#define HCLIB_MUTEX_H

#include <hclib/Types.h>

#include <pthread.h>

namespace hclib
{

    class HCLIB_EXPORT Mutex
    {
    protected:

        using mutex_t = pthread_mutex_t;
        mutex_t mutex;

    public:

        /**
         * Default constructor.
         */
        Mutex();

        /**
         * Deleted copy constructor.
         */
        Mutex(const Mutex &) = delete;

        /**
         * Deleted move constructor.
         */
        Mutex(Mutex &&) = delete;

        /**
         * Destructor.
         */
        virtual ~Mutex();

        /**
         * Deleted copy assignment operator.
         */
        Mutex &operator=(const Mutex &) = delete;

        /**
         * Deleted move assignment operator.
         */
        Mutex &operator=(Mutex &&) = delete;

        /**
         * Gets a pointer to the underlying mutex.
         *
         * @return mutex_t pointer which is guranteed to NOT be null.
         */
        virtual mutex_t *getMutex() HCLIB_NOEXCEPT;

        /**
         * Gets a const pointer to the underlying mutex.
         */
        virtual const mutex_t *getMutex() const HCLIB_NOEXCEPT;

        /**
         * Locks the mutex.
         */
        virtual void lock();

        /**
         * Attempts to lock the mutex.
         *
         * @return bool true if the mutex was aquired, else false.
         */
        virtual bool tryLock();

        /**
         * Attempts to lock the mutex.
         * Note: This version is compatibility with std::lock.
         *
         * @return bool true if the mutex was aquired, else false.
         */
        virtual bool try_lock();

        /**
         * Unlocks the mutex.
         */
        virtual void unlock();

    };

    class HCLIB_EXPORT SharedMutex
    {
    protected:
        using mutex_t = pthread_rwlock_t;
        mutex_t mutex;

    public:

        /**
         * Default constructor.
         */
        SharedMutex();

        /**
         * Deleted copy constructor.
         */
        SharedMutex(const SharedMutex &) = delete;

        /**
         * Deleted move constructor.
         */
        SharedMutex(SharedMutex &&) = delete;

        /**
         * Destructor.
         */
        virtual ~SharedMutex();

        /**
         * Deleted copy assignment operator.
         */
        SharedMutex &operator= (const SharedMutex &) = delete;

        /**
         * Deleted move assignment operator.
         */
        SharedMutex &operator= (SharedMutex &&) = delete;

        /**
         * Gets a pointer to the underlying mutex.
         */
        virtual mutex_t *getMutex() HCLIB_NOEXCEPT;

        /**
         * Gets a const pointer to the underlying mutex.
         */
        virtual const mutex_t *getMutex() const HCLIB_NOEXCEPT;

        /**
         * Locks the mutex.
         */
        virtual void lock();

        /**
         * Locks the mutex for read-only.
         */
        virtual void readLock();

        /**
         * Locks the mutex for write-only.
         */
        virtual void writeLock();

        /**
         * Attempts to aquire the mutex for read-only.
         *
         * @return true if the mutex was aquired for write-only.
         */

        /**
         * Attempts to aquire the mutex for write-only.
         *
         * @return true if the mutex was aquired for write-only.
         */
        virtual bool tryWriteLock();

        /**
         * Attempts to aquire the mutex for read-only.
         *
         * @return true if the mutex was aquired for write-only.
         */
        virtual bool tryReadLock();

        /**
         * Attempts to lock the mutex for write-only.
         * Note: This version is compatibility with std::lock.
         *
         * @return bool true if the mutex was aquired, else false.
         */
        virtual bool try_lock();

        /**
         * Unlocks the mutex.
         */
        virtual void unlock();
    };

}

#endif //HCLIB_MUTEX_H
