/**
 * @author hockeyhurd
 * @version 2019-02-17
 */

#define HCLIB_QUEUE_TEMPLATE template<typename T>
#define HCLIB_QUEUE ConcurrentQueue<T>

namespace hclib
{

    HCLIB_QUEUE_TEMPLATE
    HCLIB_QUEUE &HCLIB_QUEUE::operator= (HCLIB_QUEUE &&other)
    {
        const auto guard = mutex.lockGuard(true);
        const auto otherGuard = other.mutex.lockGuard(true);
        std::lock(guard, otherGuard);

        queue = std::move(other.queue);

        return *this;
    }

    HCLIB_QUEUE_TEMPLATE
    b32 HCLIB_QUEUE::isEmpty()
    {
        const auto guard = mutex.lockGuard();
        const b32 result = (b32) queue.empty();

        return result;
    }

    HCLIB_QUEUE_TEMPLATE
    void HCLIB_QUEUE::push(const T &data)
    {
        const auto guard = mutex.lockGuard();
        queue.push(data);
    }

    HCLIB_QUEUE_TEMPLATE
    void HCLIB_QUEUE::push(T &&data)
    {
        const auto guard = mutex.lockGuard();
        queue.emplace(std::forward<T>(data));
    }

    HCLIB_QUEUE_TEMPLATE
    T &HCLIB_QUEUE::top()
    {
        const auto guard = mutex.lockGuard();
        auto &result = queue.front();

        return result;
    }

    HCLIB_QUEUE_TEMPLATE
    void HCLIB_QUEUE::pop()
    {
        const auto guard = mutex.lockGuard();
        queue.pop();
    }

}

#undef HCLIB_QUEUE_TEMPLATE
#undef HCLIB_QUEUE
