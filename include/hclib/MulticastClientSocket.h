/*
 * Class for creating and using Posix style multicast server side sockets.
 *
 * @author hockeyhurd
 * @version 10/31/2021
 */

#pragma once

#ifndef HCLIB_MULTICAST_CLIENT_SOCKET_H
#define HCLIB_MULTICAST_CLIENT_SOCKET_H

// HCLib includes
#include <hclib/Types.h>
#include <hclib/UDPClientSocket.h>

// std includes
#include <string>

namespace hclib
{
    class HCLIB_EXPORT MulticastClientSocket : public UDPClientSocket
    {
    protected:
        std::string nic;

    public:

        /**
         * Constructs a UDPClientSocket with the given port.
         *
         * @param address the multicast group address to use.
         * @param port port number to bind this socket to.
         * @param ttl sets the time to live.
         * @param nic the local NIC to use.
         */
        MulticastClientSocket(const std::string &address, const s32 port, const u8 ttl, const std::string &nic);

        /**
         * Constructs a UDPClientSocket with the given port.
         *
         * @param address the multicast group address to use.
         * @param port port number to bind this socket to.
         * @param ttl sets the time to live.
         * @param nic the local NIC to use.
         */
        MulticastClientSocket(std::string &&address, const s32 port, const u8 ttl, std::string &&nic);

        /**
         * Deleted copy constructor.
         */
        MulticastClientSocket(const MulticastClientSocket &) = delete;

        /**
         * Move constructor.
         */
        MulticastClientSocket(MulticastClientSocket &&) = default;

        /**
         * Destructor
         */
        virtual ~MulticastClientSocket() = default;

        /**
         * Deleted copy assignment operator.
         */
        MulticastClientSocket &operator= (const MulticastClientSocket &) = delete;

        /**
         * Move assignment operator.
         */
        MulticastClientSocket &operator= (MulticastClientSocket &&) = default;

        /**
         * Gets the group multicast address.
         * 
         * @returns std::string.
         */
        virtual std::string &getGroupAddress();

        /**
         * Gets the group multicast address.
         * 
         * @returns std::string.
         */
        virtual const std::string &getGroupAddress() const;

        /**
         * Gets the local NIC.
         * 
         * @returns std::string.
         */
        virtual std::string &getNIC();

        /**
         * Gets the local NIC.
         * 
         * @returns std::string.
         */
        virtual const std::string &getNIC() const;

        /**
         * Attempts to open a socket capable of sending data to the server over Posix
         * multicast.  If this fails, an std::runtime_error exception shall be thrown.
         * Note: This will join the multicast group as well.
         */
        virtual void open() override;

        /**
         * Attempts to close the socket socket.
         * If this fails, an std::runtime_error exception shall be thrown.
         * Note: This will leave the multicast group as well.
         */
        virtual void close() override;

    protected:

        /**
         * Actually sets the TTL socket options.
         */
        virtual void setTTLInternal() override;

        /**
         * Gets the IP address associated with the provided NIC.
         */ 
        std::string getIPFromNIC();
    };
}

#endif //!HCLIB_MULTICAST_CLIENT_SOCKET_H

