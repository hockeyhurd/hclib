#pragma once

#ifndef HCLIB_FILE_WRITER_H
#define HCLIB_FILE_WRITER_H

#include <hclib/Types.h>

#include <fstream>
#include <string>
#include <utility>

namespace hclib
{

    class HCLIB_EXPORT FileWriter
    {
    private:

        std::string path;
        std::ofstream os;

    public:

        explicit FileWriter(const std::string &path, const std::ios_base::openmode mode = std::ios_base::out);
        explicit FileWriter(std::string &&path, const std::ios_base::openmode mode = std::ios_base::out);
        FileWriter(const FileWriter &) = delete;
        FileWriter(FileWriter &&) = default;
        virtual ~FileWriter();

        FileWriter &operator= (const FileWriter &) = delete;
        FileWriter &operator= (FileWriter &&) = default;

        virtual const std::string &getPath() const;

        template<class T>
        void log(const T &data)
        {
            os << data;
        }

        template<class T>
        void log(T &&data)
        {
            os << std::forward<T>(data);
        }

        template<class T>
        FileWriter &operator<< (const T &data)
        {
            log(data);
            return *this;
        }

        template<class T>
        FileWriter &operator<< (T &&data)
        {
            log(std::forward<T>(data));
            return *this;
        }

        virtual FileWriter &flush();
        virtual FileWriter &endl();
    };
}

#endif
