/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#pragma once

#ifndef HCLIB_CONDITIONAL_H
#define HCLIB_CONDITIONAL_H

// HCLib includes
#include <hclib/LockGuard.h>
#include <hclib/Mutex.h>


// System includes
#include <pthread.h>

namespace hclib
{

    class HCLIB_EXPORT Conditional
    {
    protected:

        using cond_t = pthread_cond_t;
        cond_t cond;

    public:

        /**
         * Default constructor.
         */
        Conditional();

        /**
         * Deleted copy constructor.
         */
        Conditional(const Conditional &) = delete;

        /**
         * Deleted move constructor.
         */
        Conditional(Conditional &&) = delete;

        /**
         * Destructor.
         */
        virtual ~Conditional();

        /**
         * Deleted copy assigment operator.
         */
        Conditional &operator= (const Conditional &) = delete;

        /**
         * Deleted copy assigment operator.
         */
        Conditional &operator= (Conditional &&) = delete;

        /**
         * Waits for the mutex to lock with predicated function.
         *
         * @param lock the mutex to lock.
         * @param func the predicate function.
         */
        template<class Predicated>
        void wait(UniqueLock<Mutex> &lock, Predicated pred)
        {
            while (!pred())
            {
                wait(lock);
            }
        }

        /**
         * Waits for the mutex to lock.
         *
         * @param mutex the mutex to lock.
         */
        virtual void wait(UniqueLock<Mutex> &lock);

        /**
         * Signals the underlying conditional.
         * Note: It is expected the mutex is already aquired by this thread.
         */
        virtual void signal();

        /**
         * Broadcasts signal to all waiting conditionals.
         * Note: It is expected the mutex is already aquired by this thread.
         */
        virtual void broadcast();

    };

}

#endif //HCLIB_CONDITIONAL_H
