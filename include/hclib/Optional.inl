/**
 * A class for representing Optional objects.
 *
 * @author hockeyhurd
 * @version 2021-12-03
 */

#define TEMPLATE template<class T>
#define TEMPLATE_CLASS Optional<T>

namespace hclib
{

    BadOptionalAccess::BadOptionalAccess() HCLIB_NOEXCEPT : message(nullptr)
    {
    }

    BadOptionalAccess::BadOptionalAccess(const char *message) HCLIB_NOEXCEPT : message(message)
    {
    }

    /* virtual */
    const char *BadOptionalAccess::what() const HCLIB_NOEXCEPT /* override */
    {
        return message != nullptr ? message : "bad optional access";
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::Storage::Storage() :
        dummyVariable('\0') // Initialize the dummy variable to silence a benign warning (c++20 extension)
    {
    }

    TEMPLATE
    TEMPLATE_CLASS::Storage::~Storage()
    {
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::Optional() HCLIB_NOEXCEPT : valid(false)
    {
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::Optional(nullopt_t) HCLIB_NOEXCEPT : valid(false)
    {
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::Optional(const TEMPLATE_CLASS &other) : valid(other.valid)
    {
        static_assert(std::is_copy_constructible<T>::value,
                "Type does not satisfy std::is_copy_constructible");

        if (valid)
        {
            new (&storage.obj) T(other.value());
        }
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::Optional(TEMPLATE_CLASS &&other) HCLIB_NOEXCEPT : valid(other.valid)
    {
        static_assert(std::is_move_constructible<T>::value,
                "Type does not satisfy std::is_move_constructible");

        if (valid)
        {
            new (&storage.obj) T(std::move(other.value()));
            other.valid = false; // Must be after so we don't break the move.
        }
    }

    TEMPLATE
    template<class... Args>
    TEMPLATE_CLASS::Optional(in_place_t, Args&&... args) : valid(false)
    {
        new (&storage.obj) T(std::forward<Args>(args)...);
        valid = true;
    }

    TEMPLATE
    TEMPLATE_CLASS::~Optional()
    {
        reset();
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS& TEMPLATE_CLASS::operator= (const TEMPLATE_CLASS &other)
    {
        static_assert(std::is_copy_assignable<T>::value,
                "Type does not satisfy std::is_copy_assignable");

        // Reset ourselves as needed.
        if (valid)
        {
            reset();
        }

        // Copy the 'other' instance.
        if (other.valid)
        {
            valid = other.valid;
            new (&storage.obj) T(other.value());
        }

        return *this;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS& TEMPLATE_CLASS::operator= (TEMPLATE_CLASS &&other) HCLIB_NOEXCEPT
    {
        static_assert(std::is_move_assignable<T>::value,
                "Type does not satisfy std::is_move_assignable");

        // Reset ourselves as needed.
        if (valid)
        {
            reset();
        }

        // Copy the 'other' instance.
        if (other.valid)
        {
            valid = other.valid;
            new (&storage.obj) T(std::move(other.value()));
            other.valid = false; // Must be after so we don't break the move.
        }

        return *this;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::hasValue() const HCLIB_NOEXCEPT
    {
        return valid;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC T &TEMPLATE_CLASS::value()
    {
        if (valid)
        {
            return *reinterpret_cast<T*>(storage.buffer);
        }

        throw BadOptionalAccess("Optional is not valid");
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC const T &TEMPLATE_CLASS::value() const
    {
        if (valid)
        {
            return *reinterpret_cast<T*>(const_cast<char*>(storage.buffer));
        }

        throw BadOptionalAccess("Optional is not valid");
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC T *TEMPLATE_CLASS::pointer()
    {
        if (valid)
        {
            return reinterpret_cast<T*>(storage.buffer);
        }

        return nullptr;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC const T *TEMPLATE_CLASS::pointer() const
    {
        if (valid)
        {
            return reinterpret_cast<T*>(const_cast<char*>(storage.buffer));
        }

        return nullptr;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC T &TEMPLATE_CLASS::operator* ()
    {
        if (valid)
        {
            return value();
        }

        throw BadOptionalAccess("Optional is not valid");
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC const T &TEMPLATE_CLASS::operator* () const
    {
        if (valid)
        {
            return value();
        }

        throw BadOptionalAccess("Optional is not valid");
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC T *TEMPLATE_CLASS::operator-> ()
    {
        return valid ? pointer() : nullptr;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC const T *TEMPLATE_CLASS::operator-> () const
    {
        return valid ? pointer() : nullptr;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC TEMPLATE_CLASS::operator bool() const HCLIB_NOEXCEPT
    {
        return hasValue();
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator== (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return (!valid && valid == other.valid) || (valid && other.valid && value() == other.value());
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator== (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return !valid;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator!= (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return !(*this == other);
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator!= (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return !(*this == other);
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator< (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return (!valid && other.valid) || (valid && other.valid && value() < other.value());
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator< (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return !valid; 
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator<= (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return *this < other || *this == other;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator<= (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return *this < other || *this == other;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator> (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return !(*this <= other);
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator> (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return valid;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator>= (const TEMPLATE_CLASS &other) const HCLIB_NOEXCEPT
    {
        return *this > other || *this == other;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool TEMPLATE_CLASS::operator>= (nullopt_t other) const HCLIB_NOEXCEPT
    {
        return *this > other || *this == other;
    }

    TEMPLATE
    template<class... Args>
    HCLIB_CONSTEXPR_FUNC T &TEMPLATE_CLASS::emplace(in_place_t, Args&&... args)
    {
        if (valid)
        {
            reset();
        }

        new (&storage.obj) T(std::forward<Args>(args)...);
        valid = true;
        return value();
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC void TEMPLATE_CLASS::reset() HCLIB_NOEXCEPT
    {
        if (valid)
        {
            storage.obj.~T();
            valid = false;
        }
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC void TEMPLATE_CLASS::swap(TEMPLATE_CLASS &other) HCLIB_NOEXCEPT
    {
#if CPP_VER >= 2017
        static_assert(std::is_nothrow_move_constructible_v<T>);
        static_assert(std::is_nothrow_swappable_v<T>);
#endif

        TEMPLATE_CLASS temp = std::move(other);
        other = std::move(*this);
        *this = std::move(temp);
    }

    template<class T, class... Args>
    HCLIB_CONSTEXPR_FUNC Optional<T> make_optional(Args&&... args)
    {
        return Optional<T>(in_place, std::forward<Args>(args)...);
    }

    template<class T, class... Args>
    HCLIB_CONSTEXPR_FUNC Optional<T> makeOptional(Args&&... args)
    {
        return Optional<T>(in_place, std::forward<Args>(args)...);
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool operator< (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs > lhs;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool operator<= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs >= lhs;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool operator> (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs < lhs;
    }

    TEMPLATE
    HCLIB_CONSTEXPR_FUNC bool operator>= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs <= lhs;
    }

    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator== (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs == lhs;
    }

    template<class T>
    HCLIB_CONSTEXPR_FUNC bool operator!= (nullopt_t lhs, const Optional<T> &rhs) HCLIB_NOEXCEPT
    {
        return rhs != lhs;
    }

}

#undef TEMPLATE
#undef TEMPLATE_CLASS

