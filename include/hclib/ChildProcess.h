/**
 * @author hockeyhurd
 * @version 2020-03-21
 */

#pragma once

#ifndef HCLIB_CHILD_PROCESS_H
#define HCLIB_CHILD_PROCESS_H

// My includes
#include <hclib/EnumProcessState.h>
#include <hclib/Types.h>

// std includes
#include <initializer_list>

// OS specific includes
#if OS_WIN
#include <windows.h>
#endif

namespace hclib
{

    class HCLIB_EXPORT ChildProcess
    {
    protected:

        struct Handle
        {
#if OS_WIN
            PROCESS_INFORMATION pid;
            STARTUPINFO si;
            bool valid;
#else
            s32 pid;
#endif

            Handle();

            s32 getPID() const;
            bool isValid() const;
        };

        Handle handle;

    public:

        typedef void(*SignalHandler)(s32);

        ChildProcess() = default;
        ChildProcess(const ChildProcess &) = delete;
        ChildProcess(ChildProcess &&) = default;
        virtual ~ChildProcess() = default;

        ChildProcess &operator= (const ChildProcess &) = delete;
        ChildProcess &operator= (ChildProcess &&) = default;

        /**
         * Gets the process ID.
         *
         * @return s32 pid.
         */
        virtual s32 getPID() const;

        /*
         * Sets up signal handlers.
         */
        virtual void setupSignalHandler(std::initializer_list<s32> signals, SignalHandler signalHandler);

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return s32 pid.
         */
        virtual s32 create(const char *path, char *const argv[], char *const envp[]);

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return s32 pid.
         */
        template<class Str>
        s32 create(const Str &path, char *const argv[], char *const envp[])
        {
            return create(path.c_str(), argv, envp);
        }

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return s32 pid.
         */
        template<class Str>
        s32 create(Str &&path, char *const argv[], char *const envp[])
        {
            return create(path.c_str(), argv, envp);
        }

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         *
         * @return s32 pid.
         */
        virtual s32 create(const char *path, char *const argv[]);

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         *
         * @return s32 pid.
         */
        template<class Str>
        s32 create(const Str &path, char *const argv[])
        {
            return create(path.c_str(), argv);
        }

        /**
         * Creates the child process.
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return s32 pid.
         */
        template<class Str>
        s32 create(Str &&path, char *const argv[])
        {
            return create(path.c_str(), argv);
        }

#if OS_UNIX || OS_APPLE
        /**
         * Daemonizes this ChildProcess.
         * Once this function has completed, this ChildProcess is no longer valid.
         */
        virtual s32 daemonize();
#endif

        /**
         * Attempts to kill the child process.
         *
         * @param signal the s32 signal to kill the process with.
         *               Note: On Win32, this will be the exit code. 
         * @return s32 zero on success, otherwise the error code.
         */
        virtual s32 kill(const s32 signal);

        /**
         * Gets the status of the child process.
         *
         * @return EnumProcessState status.
         */
        virtual EnumProcessState status() const;

        /**
         * Waits for the child process.
         *
         * @param waitType see man 'waitpid' for values to pass.
         *
         * @return s32 wait status; returns '-1' on error.
         */
#if OS_WIN
        virtual s32 wait(const u32 waitType = INFINITE);
#else
        virtual s32 wait(const u32 waitType = 0);
#endif

    private:

#if OS_WIN
        static s32 createProcessWindows(Handle &handle, const char* path, char* const argv[]);
#endif
    };

}

#endif //!HCLIB_CHILD_PROCESS_H
