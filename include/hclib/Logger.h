/**
 * @author hockeyhurd
 * @version 2019-09-03
 */

#pragma once

#ifndef HCLIB_LOGGER_H
#define HCLIB_LOGGER_H

#include <hclib/Types.h>

#include <string>
#include <iostream>
#include <ostream>
#include <stdexcept>

// Prior to GCC version 5, get_time and put_time not supported.
#if defined(__clang__) || defined(_MSC_VER) || (defined(__GNUC__) && __GNUC__ >= 5)
#include <ctime>
#include <iomanip>
#endif

#if CPP_VER >= 2011
#include <chrono>
#endif

#if CPP_VER >= 2017
#include <string_view>
#endif

namespace hclib
{

    /**
     * Simple enumeration for representing different LogLevels.
     */
    enum class LogLevel
    {
        ERROR = 0, WARN, INFO, DEBUG
    };

    /**
     * Gets the ordinal position of the LogLevel to lookup.
     *
     * @param level LogLevel to lookup.
     */
#if CPP_VER < 2014
    s32 ordinal(const LogLevel level);
#else
    HCLIB_CONSTEXPR_FUNC s32 ordinal(const LogLevel level)
    {
        switch (level)
        {
        case LogLevel::ERROR:
            return 0;
        case LogLevel::WARN:
            return 1;
        case LogLevel::INFO:
            return 2;
        case LogLevel::DEBUG:
            return 3;
        default:
            // Should be un-reachable.
            throw std::runtime_error("Invalid enumeration value");
        }

        // Should be un-reachable.
        return -1;
    }
#endif

    /**
     * Gets the name from enumeration.
     *
     * @param level LogLevel to lookup.
     */
#if CPP_VER < 2014
    const char *getName(const LogLevel level);
#else
    HCLIB_CONSTEXPR_FUNC const char *getName(const LogLevel level)
    {
        HCLIB_CONSTEXPR const char *lookupTable[] = { "ERROR", "WARN", "INFO", "DEBUG" };
        const s32 index = ordinal(level);
        return lookupTable[index];
    }
#endif

#if CPP_VER >= 2017

    /**
     * Gets the name from enumeration.
     *
     * @param level LogLevel to lookup.
     */
    HCLIB_CONSTEXPR_FUNC std::string_view getViewName(const LogLevel level)
    {
        return getName(level);
    }
#endif

    class HCLIB_EXPORT Logger
    {

    protected:

        std::ostream *os;
        LogLevel level;
        bool owned;
        bool seenEndl;
        bool printedSomething;

    public:

        /**
         * Constructor from an existing std::ostream.
         * 
         * @param os std::ostream reference.
         * @param level LogLevel to use.
         */
        explicit Logger(std::ostream &os = static_cast<std::ostream &>(std::cout),
                        const LogLevel level = LogLevel::INFO);

        /**
         * Constructor for writing logs to a file.
         *
         *
         * @param path std::string path to write to.
         * @param level LogLevel to use.
         */
        explicit Logger(const std::string &path, const LogLevel level = LogLevel::INFO);

        /**
         * Constructor for writing logs to a file.
         *
         * @param path std::string path to write to.
         * @param level LogLevel to use.
         */
        explicit Logger(std::string &&path, const LogLevel level = LogLevel::INFO);

        /**
         * Deleted copy constructor.
         */
        Logger(const Logger &other) = delete;

        /**
         * Move constructor.
         */
        Logger(Logger &&other);

        /**
         * Destructor.
         */
        virtual ~Logger();

        /**
         * Deleted copy assignment operator.
         */
        Logger &operator= (const Logger &other) = delete;

        /**
         * Move assignment operator.
         *
         * @param other Logger to move from.
         */
        Logger &operator= (Logger &&other);

        /**
         * Default logger (uses std::cout).
         */
        static Logger &stdlogger();

        /**
         * Gets the current LogLevel.
         *
         * @return LogLevel.
         */
        virtual LogLevel getLevel() const;

        /**
         * Set the LogLevel.
         *
         * @param level LogLevel to set to.
         */
        virtual void setLevel(const LogLevel level);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param data to write.
         */
        template<class T>
        Logger &operator<< (const T &data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param data to write.
         */
        template<class T>
        Logger &operator<< (T &&data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param level the LogLevel we are writing as.
         * @param data to write.
         */
        template<class T>
        void write(const LogLevel level, const T &data);

        /**
         * Writes the data to the underlying std::ostream.
         *
         * @param level the LogLevel we are writing as.
         * @param data to write.
         */
        template<class T>
        void write(const LogLevel level, T &&data);

        /**
         * Flushes the underlying std::ostream.
         *
         * @return this Logger instance.
         */
        virtual Logger &flush();

        /**
         * Writes a newline character underlying std::ostream.
         *
         * @return this Logger instance.
         */
        virtual Logger &endl();

    };

    template<class T>
    Logger &Logger::operator<< (const T &data)
    {
        write(level, data);
        return *this;
    }

    template<class T>
    Logger &Logger::operator<< (T &&data)
    {
        write(level, std::forward<T>(data));
        return *this;
    }

    template<class T>
    void Logger::write(const LogLevel level, const T &data)
    {
        if (os != nullptr && ordinal(level) <= ordinal(this->level))
        {
            // Prior to GCC version 5, get_time and put_time not supported. So we skip timestamp support.
#if defined(__clang__) || defined(_MSC_VER) || (defined(__GNUC__) && __GNUC__ >= 5)
    #if CPP_VER < 2011
            if (seenEndl)
            {
                seenEndl = false;
                std::time_t timeNow;
                timeNow = std::time(nullptr);
                *os << std::put_time(std::localtime(&timeNow), "%m-%d-%Y %OH:%OM:%OS")
                    << " [" << getName(level) << "]: ";
            }
    #else
            if (seenEndl)
            {
                seenEndl = false;
                const auto timeNow = std::chrono::system_clock::now();
                const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow.time_since_epoch()) % 1000;
                const auto timerTime_t = std::chrono::system_clock::to_time_t(timeNow);
                const std::tm toLocalTime = *std::localtime(&timerTime_t);

                *os << std::put_time(&toLocalTime, "%m-%d-%Y %OH:%OM:%OS"); // HH:MM:SS
                *os << '.' << std::setfill('0') << std::setw(3) << ms.count();
                *os << " [" << getName(level) << "]: ";
            }
    #endif //!CPP_VER <= 11
        *os << data;
#else
            *os << '[' << getName(level) << "]: "
                << data;
#endif //!defined(__clang__) || defined(_MSC_VER) || (defined(__GNUC__) && __GNUC__ >= 5)
            printedSomething = true;
        }
    }

    template<class T>
    void Logger::write(const LogLevel level, T &&data)
    {
        if (os != nullptr && ordinal(level) <= ordinal(this->level))
        {
            // Prior to GCC version 5, get_time and put_time not supported. So we skip timestamp support.
#if defined(__clang__) || defined(_MSC_VER) || (defined(__GNUC__) && __GNUC__ >= 5)
    #if CPP_VER < 2011
            if (seenEndl)
            {
                seenEndl = false;
                std::time_t timeNow;
                timeNow = std::time(nullptr);
                *os << std::put_time(std::localtime(&timeNow), "%m-%d-%Y %OH:%OM:%OS")
                    << " [" << getName(level) << "]: ";
            }
    #else
            if (seenEndl)
            {
                seenEndl = false;
                const auto timeNow = std::chrono::system_clock::now();
                const auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow.time_since_epoch()) % 1000;
                const auto timerTime_t = std::chrono::system_clock::to_time_t(timeNow);
                const std::tm toLocalTime = *std::localtime(&timerTime_t);

                *os << std::put_time(&toLocalTime, "%m-%d-%Y %OH:%OM:%OS"); // HH:MM:SS
                *os << '.' << std::setfill('0') << std::setw(3) << ms.count();
                *os << " [" << getName(level) << "]: ";
            }

    #endif //!CPP_VER <= 11
        *os << std::forward<T>(data);
#else
            *os << '[' << getName(level) << "]: "
                << std::forward<T>(data);
#endif
            printedSomething = true;
        }
    }

}

#endif //!HCLIB_LOGGER_H

