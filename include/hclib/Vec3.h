#pragma once

#ifndef HCLIB_VEC3_H
#define HCLIB_VEC3_H

#include <hclib/Matrix.h>

namespace hclib
{

    template<class T>
    struct Vec3
    {
        T x;
        T y;
        T z;

        Vec3() noexcept;
        Vec3(const T &x, const T &y, const T &z) noexcept;
        Vec3(T &&x, T &&y, T &&z) noexcept;
        Vec3(const Vec3<T> &other) noexcept = default;
        Vec3(Vec3<T> &&other) noexcept = default;

        ~Vec3() = default;

        Vec3<T> copy() const noexcept;

        Vec3<T> &operator= (const Vec3<T> &) noexcept = default;
        Vec3<T> &operator= (Vec3<T> &&) noexcept = default;

        Vec3<T> &operator+= (const Vec3<T> &other) noexcept;
        Vec3<T> &operator+= (Vec3<T> &&other) noexcept;
        Vec3<T> &operator+= (const T &other) noexcept;
        Vec3<T> &operator+= (T &&value) noexcept;
        Vec3<T> &operator-= (const Vec3<T> &value) noexcept;
        Vec3<T> &operator-= (Vec3<T> &&other) noexcept;
        Vec3<T> &operator-= (const T &value) noexcept;
        Vec3<T> &operator-= (T &&value) noexcept;
        Vec3<T> &operator*= (const Vec3<T> &other) noexcept;
        Vec3<T> &operator*= (Vec3<T> &&other) noexcept;
        Vec3<T> &operator*= (const T &value) noexcept;
        Vec3<T> &operator*= (T &&value) noexcept;
        Vec3<T> &operator/= (const Vec3<T> &other);
        Vec3<T> &operator/= (Vec3<T> &&other);
        Vec3<T> &operator/= (const T &value);
        Vec3<T> &operator/= (T &&value);

        Vec3<T> operator+ (const Vec3<T> &other) const noexcept;
        Vec3<T> operator+ (Vec3<T> &&other) const noexcept;
        Vec3<T> operator+ (const T &value) const noexcept;
        Vec3<T> operator+ (T &&value) const noexcept;
        Vec3<T> operator- (const Vec3<T> &other) const noexcept;
        Vec3<T> operator- (Vec3<T> &&other) const noexcept;
        Vec3<T> operator- (const T &value) const noexcept;
        Vec3<T> operator- (T &&value) const noexcept;
        Vec3<T> operator* (const Vec3<T> &other) const noexcept;
        Vec3<T> operator* (Vec3<T> &&other) const noexcept;
        Vec3<T> operator* (const T &value) const noexcept;
        Vec3<T> operator* (T &&value) const noexcept;
        Vec3<T> operator/ (const Vec3<T> &other) const;
        Vec3<T> operator/ (Vec3<T> &&other) const;
        Vec3<T> operator/ (const T &value) const;
        Vec3<T> operator/ (T &&value) const;

        bool operator== (const Vec3<T> &other) const noexcept;
        bool operator== (Vec3<T> &&other) const noexcept;
        bool operator!= (const Vec3<T> &other) const noexcept;
        bool operator!= (Vec3<T> &&other) const noexcept;

        Vec3<T> &rotateXY(const Vec3<T> &origin, const T &angle) noexcept;
        Vec3<T> &rotateXY(const Vec3<T> &origin, T &&angle) noexcept;
        Vec3<T> &rotateXY(Vec3<T> &&origin, const T &angle) noexcept;
        Vec3<T> &rotateXY(Vec3<T> &&origin, T &&angle) noexcept;

        Vec3<T> &rotateXZ(const Vec3<T> &origin, const T &angle) noexcept;
        Vec3<T> &rotateXZ(const Vec3<T> &origin, T &&angle) noexcept;
        Vec3<T> &rotateXZ(Vec3<T> &&origin, const T &angle) noexcept;
        Vec3<T> &rotateXZ(Vec3<T> &&origin, T &&angle) noexcept;

        Vec3<T> &rotateYZ(const Vec3<T> &origin, const T &angle) noexcept;
        Vec3<T> &rotateYZ(const Vec3<T> &origin, T &&angle) noexcept;
        Vec3<T> &rotateYZ(Vec3<T> &&origin, const T &angle) noexcept;
        Vec3<T> &rotateYZ(Vec3<T> &&origin, T &&angle) noexcept;

        Vec3<T> &transform(const Matrix<T, 3> &matrix) noexcept;
        Vec3<T> &transform(Matrix<T, 3> &&matrix) noexcept;

        T distance(const Vec3<T> &other) noexcept;
        T distance(Vec3<T> &&other) noexcept;

        Vec3<T> midpoint(const Vec3<T> &other) noexcept;
        Vec3<T> midpoint(Vec3<T> &&other) noexcept;

    };

}

#include "Vec3.cpp"

#endif //!HCLIB_VEC3_H
