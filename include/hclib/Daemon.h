/**
 * @author hockeyhurd
 * @version 2021-10-26
 */

#pragma once

#ifndef HCLIB_DAEMON_H
#define HCLIB_DAEMON_H

// My includes
#include <hclib/EnumProcessState.h>
#include <hclib/Types.h>

namespace hclib
{

    // *nix only for now...
#if OS_UNIX || OS_APPLE
    class HCLIB_EXPORT Daemon
    {
    public:

        Daemon() = delete;
        Daemon(const Daemon &) = delete;
        Daemon(Daemon &&) = delete;
        ~Daemon() = delete;

        Daemon &operator= (const Daemon &) = delete;
        Daemon &operator= (Daemon &&) = delete;

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return bool success/failure.
         */
        static bool create(const char *path, char *const argv[], char *const envp[]);

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return bool success/failure.
         */
        template<class Str>
        static bool create(const Str &path, char *const argv[], char *const envp[])
        {
            return create(path.c_str(), argv, envp);
        }

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         * @param envp environment for the executable.
         *
         * @return bool success/failure.
         */
        template<class Str>
        static bool create(Str &&path, char *const argv[], char *const envp[])
        {
            return create(path.c_str(), argv, envp);
        }

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         *
         * @return bool success/failure.
         */
        static bool create(const char *path, char *const argv[]);

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         *
         * @return bool success/failure.
         */
        template<class Str>
        static bool create(const Str &path, char *const argv[])
        {
            return create(path.c_str(), argv);
        }

        /**
         * Creates the daemon process.
         * This function only returns whether the first child process was created
         * successfully since we have no way of indicating whether the 'grandchild'
         * process was created or not (i.e. no return value).
         *
         * @param path path to executable.
         * @param argv arguments for the executable.
         *
         * @return bool success/failure.
         */
        template<class Str>
        static bool create(Str &&path, char *const argv[])
        {
            return create(path.c_str(), argv);
        }
    };
#endif

}

#endif //!HCLIB_DAEMON_H

