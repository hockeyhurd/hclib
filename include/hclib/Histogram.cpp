#include <cstring>
#include <stdexcept>
#include <utility>

namespace hclib
{

    template<class T>
    Histogram<T>::Histogram(const s32 len) : len(len), arr(new T[len])
    {
        zero();
    }

    template<class T>
    Histogram<T>::Histogram(const Histogram<T> &other) : len(len), arr(new T[len])
    {
        copy(other);
    }

    template<class T>
    Histogram<T>::Histogram(Histogram<T> &&other) : len(len), arr(other.arr)
    {
        other.len = 0;
        other.arr = nullptr;
    }

    template<class T>
    Histogram<T>::~Histogram()
    {
        cleanup();
    }

    template<class T>
    Histogram<T> &Histogram<T>::operator= (const Histogram<T> &other)
    {
        // Check to see if the buffer is
        // already the same len.  If so,
        // we do not need to allocate new memory!
        // Simply, just copy the other Histogram's
        // buffer.
        if (len == other.len && arr != nullptr)
        {
            copy();
        }

        // Check to see if the buffer is larger than the copying target.
        else if (len > other.len && arr != nullptr)
        {
            // Copy up until the other buffer's len
            s32 i;
            for (i = 0; i < other.len; ++i)
            {
                arr[i] = other.arr[i];
            }

            // Now zero out the rest
            for (; i < len; ++i)
            {
                arr[i] = T();
            }
        }

        else
        {
            // Cleanup (potentially) our insufficient buffer.
            cleanup();

            // Need to hard copy everything...
            len = other.len;
            arr = new T[len];

            copy(other);
        }

        return *this;
    }

    template<class T>
    Histogram<T> &Histogram<T>::operator= (Histogram<T> &&other)
    {
        // Cleanup (potentially) our insufficient buffer.
        cleanup();

        len = other.len;
        arr = other.arr;
        other.arr = nullptr;

        return *this;
    }

    template<class T>
    s32 Histogram<T>::size() const
    {
        return len;
    }

    template<class T>
    s32 Histogram<T>::numBytes() const
    {
        return len * sizeof(T);
    }

    template<class T>
    T *Histogram<T>::buffer() const
    {
        return arr;
    }

    template<class T>
    T &Histogram<T>::get(const s32 index)
    {
        if (index < 0 || index >= len)
            throw std::out_of_range("Index is negative or larger than the buffer's size.");

        return arr[index];
    }

    template<class T>
    void Histogram<T>::set(const s32 index, const T &data)
    {
        if (index < 0 || index >= len)
            throw std::out_of_range("Index is negative or larger than the buffer's size.");

        arr[index] = data;
    }

    template<class T>
    void Histogram<T>::set(const s32 index, T &&data)
    {
        if (index < 0 || index >= len)
            throw std::out_of_range("Index is negative or larger than the buffer's size.");

        arr[index] = std::move(data);
    }

    template<class T>
    T &Histogram<T>::increment(const s32 index)
    {
        if (index < 0 || index >= len)
            throw std::out_of_range("Index is negative or larger than the buffer's size.");

        T &value = arr[index];
        ++value;

        return value;
    }

    template<class T>
    T &Histogram<T>::increment(const s32 index, IncrementFunc func)
    {
        if (index < 0 || index >= len)
            throw std::out_of_range("Index is negative or larger than the buffer's size.");

        T &value = arr[index];
        func(value);

        return value;
    }

    template<class T>
    bool Histogram<T>::resize(const s32 len)
    {
        // Verify the new len is greater than
        // our current len.  Otherwise,
        // we return false.
        if (this->len >= len)
            return false;

        else if (arr == nullptr)
        {
            this->len = len;
            arr = new T[len];
            zero();

            return true;
        }

        T *buffer = new T[len];

        for (s32 i = 0; i < this->len; ++i)
        {
            buffer[i] = arr[i];
        }

        this->len = len;
        delete[] arr;
        arr = buffer;

        return true;
    }

    template<class T>
    void Histogram<T>::zero()
    {
        std::memset(arr, 0, len * sizeof(T));
    }

    template<class T>
    T &Histogram<T>::operator[] (const s32 index)
    {
        return arr[index];
    }

    template<class T>
    const T &Histogram<T>::operator[] (const s32 index) const
    {
        return arr[index];
    }

    template<class T>
    void Histogram<T>::copy(const Histogram<T> &other)
    {
        for (s32 i = 0; i < len; ++i)
        {
            arr[i] = other.arr[i];
        }
    }

    template<class T>
    void Histogram<T>::cleanup()
    {
        delete[] arr;
        arr = nullptr;
    }

}
