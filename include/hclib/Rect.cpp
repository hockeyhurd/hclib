#include <utility>

namespace hclib
{

#define HCLIB_RECT_TEMPLATE template<class T>
#define HCLIB_RECT_T Rect<T>

    HCLIB_RECT_TEMPLATE
    HCLIB_RECT_T::Rect(const T &x, const T &y, const T &width, const T &height) : pos(x, y), width(width), height(height)
    {
    }

    HCLIB_RECT_TEMPLATE
    HCLIB_RECT_T::Rect(T &&x, T &&y, T &&width, T &&height) : pos(std::forward<T>(x), std::forward<T>(y)), width(std::forward<T>(width)), height(std::forward<T>(height))
    {
    }

    HCLIB_RECT_TEMPLATE
    T &HCLIB_RECT_T::getX()
    {
        return pos.x;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setX(const T &x)
    {
        this->pos.x = x;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setX(T &&x)
    {
        this->pos.x = std::forward<T>(x);
    }

    HCLIB_RECT_TEMPLATE
    T &HCLIB_RECT_T::getY()
    {
        return pos.y;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setY(const T &y)
    {
        this->pos.y = y;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setY(T &&y)
    {
        this->pos.y = std::forward<T>(y);
    }

    HCLIB_RECT_TEMPLATE
    Vec2<T> &HCLIB_RECT_T::getPos()
    {
        return pos;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setPos(const Vec2<T> &pos)
    {
        this->pos = pos;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setPos(Vec2<T> &&pos)
    {
        this->pos = std::move(pos);
    }

    HCLIB_RECT_TEMPLATE
    T &HCLIB_RECT_T::getWidth()
    {
        return width;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setWidth(const T &width)
    {
        this->width = width;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setWidth(T &&width)
    {
        this->width = std::forward<T>(width);
    }

    HCLIB_RECT_TEMPLATE
    T &HCLIB_RECT_T::getHeight()
    {
        return height;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setHeight(const T &height)
    {
        this->height = height;
    }

    HCLIB_RECT_TEMPLATE
    void HCLIB_RECT_T::setHeight(T &&height)
    {
        this->height = std::forward<T>(height);
    }

    HCLIB_RECT_TEMPLATE
    T HCLIB_RECT_T::area() const
    {
        return width * height;
    }

    HCLIB_RECT_TEMPLATE
    Vec2<T> HCLIB_RECT_T::center() const
    {
        return Vec2<T>(pos.x + width / 2, pos.y + height / 2);
    }

    HCLIB_RECT_TEMPLATE
    bool HCLIB_RECT_T::operator== (const HCLIB_RECT_T &other)
    {
        return pos == other.pos && width == other.width && height == other.height;
    }

    HCLIB_RECT_TEMPLATE
    bool HCLIB_RECT_T::operator== (HCLIB_RECT_T &&other)
    {
        return pos == other.pos && width == other.width && height == other.height;
    }

    HCLIB_RECT_TEMPLATE
    bool HCLIB_RECT_T::operator!= (const HCLIB_RECT_T &other)
    {
        return !(*this == other);
    }

    HCLIB_RECT_TEMPLATE
    bool HCLIB_RECT_T::operator!= (HCLIB_RECT_T &&other)
    {
        return !(*this == other);
    }

#undef HCLIB_RECT_TEMPLATE 
#undef HCLIB_RECT_T

}
