#include <hclib/LockGuard.h>

#define LOCK_GUARD_TEMPLATE template<class T>
#define UNIQUE_LOCK_CLASS UniqueLock<T>
#define SHARED_LOCK_CLASS SharedLock<T>

namespace hclib
{

    LOCK_GUARD_TEMPLATE
    UNIQUE_LOCK_CLASS::UniqueLock(T &mutex) : mutex(mutex), locked(false)
    {
        lock();
    }

    LOCK_GUARD_TEMPLATE
    UNIQUE_LOCK_CLASS::UniqueLock(T &mutex, const bool defer) : mutex(mutex), locked(false)
    {
        if (!defer)
        {
            lock();
        }
    }

    LOCK_GUARD_TEMPLATE
    UNIQUE_LOCK_CLASS::~UniqueLock()
    {
        unlock();
    }

    LOCK_GUARD_TEMPLATE
    T &UNIQUE_LOCK_CLASS::getMutex()
    {
        return mutex;
    }

    LOCK_GUARD_TEMPLATE
    const T &UNIQUE_LOCK_CLASS::getMutex() const
    {
        return mutex;
    }

    LOCK_GUARD_TEMPLATE
    void UNIQUE_LOCK_CLASS::lock()
    {
        if (!locked)
        {
            locked = true;
            mutex.lock();
        }
    }

    LOCK_GUARD_TEMPLATE
    bool UNIQUE_LOCK_CLASS::tryLock()
    {
        if (!locked)
        {
            locked = mutex.tryLock();
        }

        return locked;
    }

    LOCK_GUARD_TEMPLATE
    void UNIQUE_LOCK_CLASS::unlock()
    {
        if (locked)
        {
            mutex.unlock();
            locked = false;
        }
    }

    LOCK_GUARD_TEMPLATE
    SHARED_LOCK_CLASS::SharedLock(T &mutex) : mutex(mutex), locked(false)
    {
        lock();
    }

    LOCK_GUARD_TEMPLATE
    SHARED_LOCK_CLASS::SharedLock(T &mutex, const bool defer) : mutex(mutex), locked(false)
    {
        if (!defer)
        {
            lock();
        }
    }

    LOCK_GUARD_TEMPLATE
    SHARED_LOCK_CLASS::~SharedLock()
    {
        unlock();
    }

    LOCK_GUARD_TEMPLATE
    T &SHARED_LOCK_CLASS::getMutex()
    {
        return mutex;
    }

    LOCK_GUARD_TEMPLATE
    const T &SHARED_LOCK_CLASS::getMutex() const
    {
        return mutex;
    }

    LOCK_GUARD_TEMPLATE
    void SHARED_LOCK_CLASS::lock()
    {
        if (!locked)
        {
            locked = true;
            mutex.readLock();
        }
    }

    LOCK_GUARD_TEMPLATE
    bool SHARED_LOCK_CLASS::tryLock()
    {
        if (!locked)
        {
            locked = mutex.tryReadLock();
        }

        return locked;
    }

    LOCK_GUARD_TEMPLATE
    void SHARED_LOCK_CLASS::unlock()
    {
        if (locked)
        {
            mutex.unlock();
            locked = false;
        }
    }
}

