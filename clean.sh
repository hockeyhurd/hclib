#!/bin/bash

echo "Cleaning"

function clean() {
    make -s CFG=Debug clean
    make -s CFG=Release clean
}

pushd src > /dev/null

time clean

popd > /dev/null

echo "Done"
