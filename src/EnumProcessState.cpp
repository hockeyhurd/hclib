// My includes
#include <hclib/EnumProcessState.h>

// std include
#include <functional>

// OS specific includes
#if OS_WIN
#ifndef WIFEXITED
#define WIFEXITED(x) ((x) != 3)
#endif // !WIFEXITED

#ifndef WEXITSTATUS
#define WEXITSTATUS(x) (x)
#endif // !WEXITSTATUS

// TODO: Figure this out...
#ifndef WIFSTOPPED
#define WIFSTOPPED(x) (x)
#endif // !WIFSTOPPED

#else
#include <sys/wait.h>
#endif

namespace hclib
{

    /* static */
    const EnumProcessState EnumProcessState::UNKNOWN(-1, "UNKNOWN");
    /* static */
    const EnumProcessState EnumProcessState::DEFAULT(0, "DEFAULT");
    /* static */
    const EnumProcessState EnumProcessState::RUNNING(1, "RUNNING");
    /* static */
    const EnumProcessState EnumProcessState::STOPPED(2, "STOPPED");

    EnumProcessState::EnumProcessState(const s32 value, const char *name) : value(value), mName(name)
    {
    }

    s32 EnumProcessState::ordinal() const
    {
        return value;
    }

    const char *EnumProcessState::name() const
    {
        return mName;
    }

    /* static */
    EnumProcessState EnumProcessState::getState(const s32 value)
    {
        if (WIFEXITED(value) || WIFSTOPPED(value))
        {
            return EnumProcessState::STOPPED;
        }

        const s32 signaled = WIFSIGNALED(value);

        // If we were signaled and the value is one of these, then report status as STOPPED.
        if (signaled && (value == 9 || value == 15 || value == 19))
        {
            return EnumProcessState::STOPPED;
        }

        return EnumProcessState::RUNNING;
    }

    bool EnumProcessState::operator== (const EnumProcessState &other) const
    {
        return value == other.value;
    }

    bool EnumProcessState::operator== (EnumProcessState &&other) const
    {
        return value == std::ref(other.value);
    }

    bool EnumProcessState::operator!= (const EnumProcessState &other) const
    {
        return !(*this == other);
    }

    bool EnumProcessState::operator!= (EnumProcessState &&other) const
    {
        return value != std::ref(other.value);
    }
}

