/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#include <hclib/Debug.h>

#include <cassert>

namespace hclib
{

    // const char *Debug::logLevelStr[3] = { "INFO", "WARN", "ERROR" };

    void Debug::assertTrue(const b32 value)
    {
        assert(value);
    }

    void Debug::assertFalse(const b32 value)
    {
        assert(!value);
    }

}
