/**
 * A class for easy gap buffering.
 *
 * @author hockeyhurd
 * @version 2023-10-19
 */

// HCLib includes
#include <hclib/GapBuffer.h>
#include <hclib/Algorithm.h>

// std includes
#include <cstring>
#include <sstream>
#include <stdexcept>

namespace hclib
{
    /* static */
    const char GapBuffer::emptyFiller = '\0';

    GapBuffer::GapBuffer(std::size_t gapSize, std::size_t bufferSize) :
        gapSize(gapSize), buffer(nullptr), startGap(nullptr), endGap(nullptr), endBuffer(nullptr)
    {
        if (gapSize >= bufferSize)
        {
            std::ostringstream os;
            os << "gapSize (" << gapSize << ") >= bufferSize " << bufferSize << ')';
            throw std::invalid_argument(os.str());
        }

        buffer = new char[bufferSize];
        startGap = buffer;
        endGap = startGap + gapSize - 1;
        endBuffer = buffer + bufferSize;
    }

    GapBuffer::GapBuffer(std::size_t gapSize, const std::string& str) : gapSize(gapSize),
        buffer(nullptr), startGap(nullptr), endGap(nullptr), endBuffer(nullptr)
    {
        const auto strSize = str.size();
        const std::size_t capacity = strSize + gapSize;
        buffer = new char[capacity];
        std::memcpy(buffer, str.data(), strSize);

        startGap = buffer + strSize;
        endGap = startGap + gapSize;
        endBuffer = buffer + capacity;
    }

    GapBuffer::GapBuffer(const GapBuffer& other) : gapSize(other.gapSize), buffer(nullptr),
        startGap(nullptr), endGap(nullptr), endBuffer(nullptr)
    {
        if (other.buffer == nullptr)
        {
            return;
        }

        const std::size_t bufferSize = other.size();
        buffer = new char[bufferSize];
        startGap = buffer;
        endGap = startGap + gapSize - 1;
        endBuffer = buffer + bufferSize;
        std::memcpy(buffer, other.buffer, bufferSize);
    }

    GapBuffer::GapBuffer(GapBuffer&& other) HCLIB_NOEXCEPT : gapSize(other.gapSize),
        buffer(other.buffer), startGap(other.startGap), endGap(other.endGap),
        endBuffer(other.endBuffer)
    {
        other.gapSize = 0;
        other.buffer = other.startGap = other.endGap = other.endBuffer = nullptr;
    }

    /* virtual */
    GapBuffer::~GapBuffer()
    {
        gapSize = 0;

        if (buffer != nullptr)
        {
            delete[] buffer;
            buffer = startGap = endGap = endBuffer = nullptr;
        }
    }

    GapBuffer& GapBuffer::operator= (const GapBuffer& other)
    {
        if (this == &other)
        {
            return *this;
        }

        gapSize = other.gapSize;
        const auto curSize = size();
        const auto otherSize = other.size();
        const auto otherPos = other.getCurrentPosition();
        const auto otherGapSize = other.getCurrentGapSize();

        // Buffer sufficiently sized, good to reuse.
        if (curSize >= otherSize)
        {
            std::memcpy(buffer, other.buffer, otherSize);

            // Zero fill any extra bytes
            if (curSize > otherSize)
            {
                const std::size_t diff = curSize - otherSize;
                std::memset(buffer + otherSize, GapBuffer::emptyFiller, diff);
            }
        }

        // Our buffer is too small. Resize and copy.
        else
        {
            if (buffer != nullptr)
            {
                delete[] buffer;
            }

            buffer = new char[otherSize];
            endBuffer = buffer + otherSize;
            std::memcpy(buffer, other.buffer, otherSize);
        }

        // Fix pointers.
        startGap = buffer + otherPos;
        endGap = startGap + otherGapSize;

        return *this;
    }

    GapBuffer& GapBuffer::operator= (GapBuffer&& other) HCLIB_NOEXCEPT
    {
        if (this == &other)
        {
            return *this;
        }

        if (buffer != nullptr)
        {
            delete[] buffer;
        }

        gapSize = other.gapSize;
        other.gapSize = 0;

        buffer = other.buffer;
        other.buffer = nullptr;

        startGap = other.startGap;
        other.startGap = nullptr;

        endGap = other.endGap;
        other.endGap = nullptr;

        endBuffer = other.endBuffer;
        other.endBuffer = nullptr;

        return *this;
    }

    /* virtual */
    const char* GapBuffer::data() const HCLIB_NOEXCEPT
    {
        return buffer;
    }

    /* virtual */
    std::size_t GapBuffer::getCurrentPosition() const HCLIB_NOEXCEPT
    {
        const auto result = static_cast<std::size_t>(startGap - buffer);
        return result;
    }

    /* virtual */
    std::size_t GapBuffer::size() const HCLIB_NOEXCEPT
    {
        if (buffer == nullptr)
        {
            return 0;
        }

        const auto result = static_cast<std::size_t>(endBuffer - buffer);
        return result;
    }

    /* virtual */
    void GapBuffer::clear() HCLIB_NOEXCEPT
    {
        if (buffer == nullptr)
        {
            return;
        }

        const auto curSize = size();
        std::memset(buffer, GapBuffer::emptyFiller, curSize);

        startGap = buffer;
        endGap = buffer + gapSize;
    }

    /* virtual */
    bool GapBuffer::erase() HCLIB_NOEXCEPT
    {
        if (buffer == nullptr || buffer == startGap)
        {
            return false;
        }

        *startGap = GapBuffer::emptyFiller;
        --startGap;
        return true;
    }

    /* virtual */
    bool GapBuffer::erase(std::size_t pos) HCLIB_NOEXCEPT
    {
        move(pos);
        const bool result = erase();
        return result;
    }

    /* virtual */
    void GapBuffer::insert(char value)
    {
        if (buffer == nullptr)
        {
            return;
        }

        // Out of space
        else if (getCurrentGapSize() == 0)
        {
            resizeBuffer();
        }

        *startGap = value;
        ++startGap;
    }

    /* virtual */
    void GapBuffer::insert(char value, std::size_t pos)
    {
        move(pos);
        insert(value);
    }

    /* virtual */
    void GapBuffer::move(std::size_t pos) HCLIB_NOEXCEPT
    {
        if (buffer == nullptr)
        {
            return;
        }

        const auto curPos = getCurrentPosition();

        // Already in position or past the buffer's size
        if (curPos == pos)
        {
            return;
        }

        const auto curSize = size();

        // If we know the gap would be past the buffer, then resize and shift by calling recursively.
        if (pos + gapSize > curSize)
        {
            resizeBuffer();
            move(pos);
            return;
        }

        // Move left.
        else if (pos < curPos)
        {
            const auto* endPos = &buffer[pos];

            do
            {
                --startGap;
                *endGap = *startGap;
                --endGap;
            }
            while (endPos < startGap);
        }

        // Move right.
        else
        {
            const auto* endPos = &buffer[pos];

            do
            {
                ++endGap;
                *startGap = *endGap;
                ++startGap;
            }
            while (endPos > startGap);
        }
    }

    /* virtual */
    void GapBuffer::reserve(std::size_t capacity)
    {
        const auto curSize = size();

        if (capacity < curSize)
        {
            return;
        }

        const auto curPos = getCurrentPosition();
        auto* newBuffer = new char[capacity];

        if (buffer == nullptr)
        {
            buffer = newBuffer;
        }

        else
        {
            const auto diff = capacity - curSize;
            std::memcpy(newBuffer, buffer, curSize);
            std::memset(newBuffer + curSize, GapBuffer::emptyFiller, diff);

            delete[] buffer;
            buffer = newBuffer;
        }

        startGap = buffer + curPos;
        endGap = startGap + gapSize;
        endBuffer = buffer + capacity;
    }

    /* virtual */
    std::size_t GapBuffer::search(const std::string& text, std::size_t startPos) const
    {
        std::size_t pos = static_cast<std::size_t>(-1);

        if (startPos >= size())
        {
            return pos;
        }

        const std::size_t curPos = getCurrentPosition();
        const std::size_t endGapPos = static_cast<std::size_t>(endGap - buffer);
        const char* patternStartPtr = text.c_str();
        const char* patternEndPtr = static_cast<const char*>(patternStartPtr + text.size());
        const char* offsetPtr = static_cast<const char*>(buffer + startPos);
        const char* resultPtr;
        algorithm::BoyerMoore<const char*> algBoyerMoore(patternStartPtr, patternEndPtr);

        // Before the gap
        if (startPos < curPos)
        {
            resultPtr = algBoyerMoore.search(offsetPtr, startGap);

            if (resultPtr != startGap)
            {
                pos = static_cast<std::size_t>(resultPtr - buffer);
                return pos;
            }
        }

        // In the gap, move the start position up.
        else if (startPos >= curPos && startPos <= endGapPos)
        {
            startPos = endGapPos;
            offsetPtr = static_cast<const char*>(buffer + startPos);
        }

        // After the gap
        // else
        {
            resultPtr = algBoyerMoore.search(offsetPtr, endBuffer);
        }

        if (resultPtr != endBuffer)
        {
            pos = static_cast<std::size_t>(resultPtr - buffer);
        }

        return pos;
    }

    /* virtual */
    std::string GapBuffer::toString() const
    {
        std::ostringstream os;

        for (char* ptr = buffer; ptr != startGap; ++ptr)
        {
            os << *ptr;
        }

        // Need to be careful we don't go past the endBuffer ptr.
        if (endGap != endBuffer)
        {
            for (char* ptr = endGap + 1; ptr != endBuffer; ++ptr)
            {
                os << *ptr;
            }
        }

        return os.str();
    }

    bool GapBuffer::operator== (const GapBuffer& other) const HCLIB_NOEXCEPT
    {
        if (this == &other)
        {
            return true;
        }

        const auto len = size();

        if (gapSize != other.gapSize || len != other.size())
        {
            return false;
        }

        for (std::size_t i = 0; i < len; ++i)
        {
            if (buffer[i] != other.buffer[i])
            {
                return false;
            }
        }

        return true;
    }

    bool GapBuffer::operator!= (const GapBuffer& other) const HCLIB_NOEXCEPT
    {
        return !(*this == other);
    }

    /* virtual */
    std::size_t GapBuffer::getCurrentGapSize() const HCLIB_NOEXCEPT
    {
        const auto result = static_cast<std::size_t>(endGap - startGap);
        return result;
    }

    /* virtual */
    void GapBuffer::resizeBuffer()
    {
        const auto curPos = getCurrentPosition();
        const auto oldSize = size();
        const auto newSize = oldSize + gapSize;
        auto* newBuffer = new char[newSize];

        // Copy to the start of the gap.
        std::memcpy(newBuffer, buffer, curPos);

        // nullify the gap.
        startGap = newBuffer + curPos;
        const auto* oldEndGap = endGap;
        endGap = startGap + gapSize - 1;

        std::memset(startGap, GapBuffer::emptyFiller, gapSize);

        // Copy remaining after the gap.
        const std::size_t remaining = endBuffer - oldEndGap + 1;
        std::memcpy(endGap + 1, oldEndGap, remaining);

        // Cleanup memory and update remaining pointers.
        delete[] buffer;
        buffer = newBuffer;
        endBuffer = buffer + newSize;
    }
}

