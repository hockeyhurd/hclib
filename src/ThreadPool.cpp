/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#include <hclib/ThreadPool.h>

#include <chrono>

namespace hclib
{

    ThreadPool::ThreadPool(const u32 numThreads) : running(true)
    {
        threads.reserve(numThreads);
        init(numThreads);
    }

    ThreadPool::~ThreadPool()
    {
        if (isRunning())
        {
            // Don't wait for the job queue to empty, shutdown now!
            shutdown(false);
        }
    }

    void ThreadPool::shutdown(const bool waitForJobsToComplete)
    {
        if (waitForJobsToComplete)
            wait();

        running = false;

        {
            const UniqueLock<Mutex> guard(mutex);
            cond.broadcast();
        }

        for (auto &thread : threads)
        {
            thread.join();
        }
    }

    void ThreadPool::wait()
    {
        while (true)
        {
            const UniqueLock<Mutex> guard(mutex);
            if (jobs.empty())
                break;

            std::this_thread::yield();
        }
    }

    void ThreadPool::init(const u32 numThreads)
    {
        for (u32 i = 0; i < numThreads; ++i)
        {
            threads.emplace_back(ThreadPool::workerThread, std::ref(*this), std::ref(jobs), std::ref(mutex), std::ref(cond));
        }
    }

    /* static */
    void ThreadPool::workerThread(ThreadPool &pool, ThreadPool::Queue &jobs, Mutex &mutex, Conditional &cond)
    {
        while (pool.isRunning())
        {
            UniqueLock<Mutex> guard(mutex);
            cond.wait(guard, [&]() { return !pool.isRunning() || !jobs.empty(); });

            if (!pool.isRunning())
                break;

            auto func = jobs.front();
            jobs.pop();
            guard.unlock();

            func();
        }
    }

}
