/**
 * A class for simplifying Timers supporting periodic and not.
 *
 * @author hockeyhurd
 * @version 03/19/2023
 */

// HCLib includes
#include <hclib/Timer.h>

// std includes
#include <atomic>

namespace hclib
{
    static std::atomic<u32> uuidCounter = 0;

    Timer::Timer(s64 durationMicro, TimerEvent eventHandler, bool periodic) HCLIB_NOEXCEPT :
        durationMicro(durationMicro), eventHandler(eventHandler), periodic(periodic),
        uuid(uuidCounter++), startTime(currentTime())
    {
    }

    s64 Timer::getDuration() const HCLIB_NOEXCEPT
    {
        return durationMicro;
    }

    u32 Timer::getUUID() const HCLIB_NOEXCEPT
    {
        return uuid;
    }

    bool Timer::isPeriodic() const HCLIB_NOEXCEPT
    {
        return periodic;
    }

    bool Timer::expired() const HCLIB_NOEXCEPT
    {
        const auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime() - startTime).count();
        const bool result = elapsedTime >= durationMicro;

        return result;
    }

    void Timer::reset() HCLIB_NOEXCEPT
    {
        startTime = currentTime();
    }

    bool Timer::executeIfExpired()
    {
        // If the time is up, call the eventHandler.
        if (expired())
        {
            eventHandler();

            // If the Timer is periodic, we need to reset the startTime for the next update.
            if (periodic)
            {
                reset();
            }

            return true;
        }

        return false;
    }

    /* static */
    Timer::TimePoint Timer::currentTime() HCLIB_NOEXCEPT
    {
        return Timer::Clock::now();
    }
}

