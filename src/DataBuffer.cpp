// HCLib includes
#include <hclib/DataBuffer.h>
#include <hclib/Debug.h>

// std includes
#include <cstring>
#include <stdexcept>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

namespace hclib
{

    DataBuffer::DataBuffer(const std::size_t capacity, const EnumEndian endian, const bool doZero) :
        capacity(capacity), endian(endian), index(0), buffer(nullptr)
    {
        // If the capacity is exactly zero, enforce the caller to give a non-zero
        // value by throwing an exception back at them.
        // We do this so that we can't cheat in (most) other functions calls
        // to assume if we have capacity, the buffer isn't null and visa-versa.
        // Also, to make sure the caller isn't creating their own bugs.
        if (capacity == 0)
        {
            throw std::invalid_argument("Cannot create a new DataBuffer with a capacity of '0'");
        }

        buffer = new char[capacity];

        // Effectively calling our 'zero' function, but without a virtual call from within
        // this (base) constructor.
        if (doZero)
        {
            std::memset(buffer, 0, capacity);
        }
    }

    DataBuffer::DataBuffer(const DataBuffer& other) : capacity(other.capacity), endian(other.endian),
        index(other.index), buffer(nullptr)
    {
        if (other.buffer != nullptr)
        {
            buffer = new char[index];
            std::memcpy(buffer, other.buffer, other.size());
        }
    }

    DataBuffer::DataBuffer(DataBuffer&& other) HCLIB_NOEXCEPT : capacity(other.capacity),
        endian(other.endian), index(other.index), buffer(other.buffer)
    {
        other.capacity = 0;
        other.index = 0;
        other.buffer = nullptr;
    }

    /* virtual */
    DataBuffer::~DataBuffer()
    {
        if (buffer != nullptr)
        {
            capacity = 0;
            index = 0;
            delete[] buffer;
            buffer = nullptr;
        }
    }

    DataBuffer& DataBuffer::operator= (const DataBuffer& other)
    {
        if (this == &other)
        {
            return *this;
        }

        // Check to see if we need a bigger buffer
        if (capacity < other.capacity)
        {
            capacity = other.capacity;
            delete[] buffer;
            buffer = new char[capacity];
        }

        endian = other.endian;
        index = other.index;
        std::memcpy(buffer, other.buffer, index);

        return *this;
    }

    DataBuffer& DataBuffer::operator= (DataBuffer&& other) HCLIB_NOEXCEPT
    {
        if (this == &other)
        {
            return *this;
        }

        if (buffer != nullptr)
        {
            delete[] buffer;
        }

        capacity = other.capacity;
        other.capacity = 0;

        endian = other.endian;
        index = other.index;
        other.index = 0;

        buffer = other.buffer;
        other.buffer = nullptr;

        return *this;
    }

    /* virtual */
    std::size_t DataBuffer::getCapacity() const HCLIB_NOEXCEPT
    {
        return capacity;
    }

    /* virtual */
    bool DataBuffer::empty() const HCLIB_NOEXCEPT
    {
        return index == 0;
    }

    /* virtual */
    std::size_t DataBuffer::size() const HCLIB_NOEXCEPT
    {
        return index;
    }

    char& DataBuffer::operator[] (const std::size_t index)
    {
        return buffer[index];
    }

    const char& DataBuffer::operator[] (const std::size_t index) const
    {
        return buffer[index];
    }

    DataBuffer::iterator DataBuffer::begin() HCLIB_NOEXCEPT
    {
        return iterator(this, buffer);
    }

    DataBuffer::const_iterator DataBuffer::cbegin() const HCLIB_NOEXCEPT
    {
        return const_iterator(this, buffer);
    }

    DataBuffer::reverse_iterator DataBuffer::rbegin() HCLIB_NOEXCEPT
    {
        char* pos = &buffer[capacity + 1];
        return reverse_iterator(this, pos);
    }

    DataBuffer::const_reverse_iterator DataBuffer::crbegin() const HCLIB_NOEXCEPT
    {
        const char* pos = &buffer[capacity + 1];
        return const_reverse_iterator(this, pos);
    }

    DataBuffer::iterator DataBuffer::end() HCLIB_NOEXCEPT
    {
        char* pos = &buffer[capacity + 1];
        return iterator(this, pos);
    }

    DataBuffer::const_iterator DataBuffer::cend() const HCLIB_NOEXCEPT
    {
        const char* pos = &buffer[capacity + 1];
        return const_iterator(this, pos);
    }

    DataBuffer::reverse_iterator DataBuffer::rend() HCLIB_NOEXCEPT
    {
        return reverse_iterator(this, buffer);
    }

    DataBuffer::const_reverse_iterator DataBuffer::crend() const HCLIB_NOEXCEPT
    {
        return const_reverse_iterator(this, buffer);
    }

    /* virtual */
    char* DataBuffer::data() HCLIB_NOEXCEPT
    {
        return buffer;
    }

    /* virtual */
    const char* DataBuffer::data() const HCLIB_NOEXCEPT
    {
        return buffer;
    }

    /* virtual */
    DataBuffer DataBuffer::copy()
    {
        return DataBuffer(*this);
    }

    /* virtual */
    char* DataBuffer::copy(const std::size_t offset)
    {
        if (offset >= size())
            return nullptr;

        const std::size_t len = size() - offset + 1;
        char* outBuffer = new char[len];
        std::size_t count = 0;

        for (std::size_t i = offset; i < size(); ++i)
        {
            outBuffer[count++] = buffer[i];
        }

        return outBuffer;
    }

    /* virtual */
    std::vector<char> DataBuffer::copyVector(const std::size_t offset)
    {
        std::vector<char> vec;

        if (offset >= size())
            return vec;

        const std::size_t len = size() - offset;
        vec.reserve(len);

        for (std::size_t i = offset; i < size(); ++i)
        {
            vec.push_back(buffer[i]);
        }

        return vec;
    }

    /* virtual */
    std::unique_ptr<char[]> DataBuffer::copyUnique(const std::size_t offset)
    {
        if (offset >= size())
            return nullptr;

        const std::size_t len = size() - offset;
        std::unique_ptr<char[]> outBuffer = std::make_unique<char[]>(len);
        std::size_t count = 0;

        for (std::size_t i = offset; i < size(); ++i)
        {
            outBuffer[count++] = buffer[i];
        }

        return outBuffer;
    }

#if CPP_20
    /* virtual */
    std::shared_ptr<char[]> DataBuffer::copyShared(const std::size_t offset)
    {
        if (offset >= size())
            return nullptr;

        const std::size_t len = size() - offset;
        std::shared_ptr<char[]> outBuffer = std::make_shared<char[]>(len);
        std::size_t count = 0;

        for (std::size_t i = offset; i < size(); ++i)
        {
            outBuffer[count++] = buffer[i];
        }

        return outBuffer;
    }
#endif

    /* virtual */
    char& DataBuffer::get(const std::size_t index)
    {
        if (index >= size())
        {
            throw std::out_of_range("Index passed into DataBuffer::get is out of range");
        }

        return buffer[index];
    }

    /* virtual */
    const char& DataBuffer::get(const std::size_t index) const
    {
        if (index >= size())
        {
            throw std::out_of_range("Index passed into DataBuffer::get is out of range");
        }

        return buffer[index];
    }

    /* virtual */
    char* DataBuffer::tryGet(const std::size_t index) HCLIB_NOEXCEPT
    {
        if (index >= size())
        {
            return nullptr;
        }

        return &buffer[index];
    }

    /* virtual */
    const char* DataBuffer::tryGet(const std::size_t index) const HCLIB_NOEXCEPT
    {
        if (index >= size())
        {
            return nullptr;
        }

        return &buffer[index];
    }

    /* virtual*/
    bool DataBuffer::set(const char value, const std::size_t index)
    {
        if (index >= size())
            return false;

        buffer[index] = value;
        return true;
    }

    /* virtual */
    void DataBuffer::clear(const char defaultValue) HCLIB_NOEXCEPT
    {
        std::memset(buffer, defaultValue, size());
    }

    /* virtual */
    void DataBuffer::zero() HCLIB_NOEXCEPT
    {
        std::memset(buffer, 0, capacity);
    }

    /* virtual */
    void DataBuffer::resetPosition() HCLIB_NOEXCEPT
    {
        index = 0;
    }

    /* virtual */
    bool DataBuffer::setPosition(const std::size_t pos) HCLIB_NOEXCEPT
    {
        if (pos < capacity)
        {
            index = pos;
            return true;
        }

        return false;
    }

    /* virtual */
    void DataBuffer::reserve(const std::size_t reserveSize)
    {
        // If we are already at the reserveSize or larger, we don't need to
        // create a new buffer.
        if (capacity >= reserveSize)
        {
            return;
        }

        capacity = reserveSize;
        const auto* tempBuffer = buffer;
        buffer = new char[reserveSize];
        std::memcpy(buffer, tempBuffer, index);

        delete[] tempBuffer;
    }

    /* virtual */
    void DataBuffer::packByte(const char value)
    {
        resizeIfNeeded(sizeof(value));
        buffer[index++] = value;
    }

    /* virtual */
    void DataBuffer::packBytes(const char* data, const std::size_t len)
    {
        for (std::size_t i = 0; i < len; ++i)
        {
            packByte(data[i]);
        }
    }

    /* virtual */
    void DataBuffer::packString(const std::string& str)
    {
        if (!str.empty())
        {
            packBytes(str.data(), str.size());
        }
    }

    /* virtual */
    void DataBuffer::packU16(u16 value)
    {
        resizeIfNeeded(sizeof(value));

        switch (endian)
        {
        case EnumEndian::LITTLE:
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            break;
        case EnumEndian::BIG:
            packByte(static_cast<char>((value >> 8) & 0xFF));
            packByte(static_cast<char>(value & 0xFF));
            break;
        default:
            Debug::error("Un-expected unpack u16 with unknown endianess.");
            break;
        }
    }

    /* virtual */
    void DataBuffer::packU32(u32 value)
    {
        resizeIfNeeded(sizeof(value));

        switch (endian)
        {
        case EnumEndian::LITTLE:
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            break;
        case EnumEndian::BIG:
            packByte(static_cast<char>((value >> 24) & 0xFF));
            packByte(static_cast<char>((value >> 16) & 0xFF));
            packByte(static_cast<char>((value >> 8) & 0xFF));
            packByte(static_cast<char>(value & 0xFF));
            break;
        default:
            Debug::error("Unexpected condition in DataBuffer::set(u32, std::size_t)");
            break;
        }
    }

    /* virtual */
    void DataBuffer::packU64(u64 value)
    {
        resizeIfNeeded(sizeof(value));

        switch (endian)
        {
        case EnumEndian::LITTLE:
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            value >>= 8;
            packByte(static_cast<char>(value & 0xFF));
            break;
        case EnumEndian::BIG:
            packByte(static_cast<char>((value >> 56) & 0xFF));
            packByte(static_cast<char>((value >> 48) & 0xFF));
            packByte(static_cast<char>((value >> 40) & 0xFF));
            packByte(static_cast<char>((value >> 32) & 0xFF));
            packByte(static_cast<char>((value >> 24) & 0xFF));
            packByte(static_cast<char>((value >> 16) & 0xFF));
            packByte(static_cast<char>((value >> 8) & 0xFF));
            packByte(static_cast<char>(value & 0xFF));
            break;
        default:
            Debug::error("Unexpected condition in DataBuffer::set(u32, std::size_t)");
            break;
        }
    }

    /* virtual */
    void DataBuffer::packF32(f32 value)
    {
        const u32 asU32 = *reinterpret_cast<u32*>(&value);
        packU32(asU32);
    }

    /* virtual */
    void DataBuffer::packF64(f64 value)
    {
        const u64 asU64 = *reinterpret_cast<u64*>(&value);
        packU64(asU64);
    }

    /* virtual */
    char DataBuffer::unpackByte(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        char result = 0;

        if (iter.instance == this && iter != end())
        {
            result = *iter.pos;
            ++iter;
        }

        return result;
    }

    /* virtual */
    char DataBuffer::unpackByte(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        char result = 0;

        if (iter.instance == this && iter != cend())
        {
            result = *iter.pos;
            ++iter;
        }

        return result;
    }

    /* virtual */
    u16 DataBuffer::unpackU16(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        u16 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u16>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u16>(unpackByte(iter)) & 0xFF) << 8;
            break;
        case EnumEndian::BIG:
            result = static_cast<u16>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u16>(unpackByte(iter)) & 0xFF;
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU16(iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    u16 DataBuffer::unpackU16(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        u16 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u16>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u16>(unpackByte(iter)) & 0xFF) << 8;
            break;
        case EnumEndian::BIG:
            result = static_cast<u16>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u16>(unpackByte(iter)) & 0xFF;
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU16(const_iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    u32 DataBuffer::unpackU32(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        u32 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u32>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 8;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 16;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 24;
            break;
        case EnumEndian::BIG:
            result = static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU32(iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    u32 DataBuffer::unpackU32(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        u32 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u32>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 8;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 16;
            result |= (static_cast<u32>(unpackByte(iter)) & 0xFF) << 24;
            break;
        case EnumEndian::BIG:
            result = static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            result <<= 8;
            result |= static_cast<u32>(unpackByte(iter)) & 0xFF;
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU32(const_iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    u64 DataBuffer::unpackU64(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        u64 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u64>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 8;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 16;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 24;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 32;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 40;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 48;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 56;
            break;
        case EnumEndian::BIG:
            result = static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU64(iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    u64 DataBuffer::unpackU64(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        u64 result;

        switch (endian)
        {
        case EnumEndian::LITTLE:
            result = static_cast<u64>(unpackByte(iter)) & 0xFF;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 8;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 16;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 24;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 32;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 40;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 48;
            result |= (static_cast<u64>(unpackByte(iter)) & 0xFF) << 56;
            break;
        case EnumEndian::BIG:
            result = static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            result <<= 8;
            result |= static_cast<u64>(unpackByte(iter));
            break;
        default:
            result = 0;
            Debug::error("Unexpected endian condition in DataBuffer::unpackU64(const_iterator)");
            break;
        }

        return result;
    }

    /* virtual */
    f32 DataBuffer::unpackF32(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        const u32 temp = unpackU32(iter);
        const f32 result = *reinterpret_cast<const f32*>(&temp);

        return result;
    }

    /* virtual */
    f32 DataBuffer::unpackF32(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        const u32 temp = unpackU32(iter);
        const f32 result = *reinterpret_cast<const f32*>(&temp);

        return result;
    }

    /* virtual */
    f64 DataBuffer::unpackF64(DataBuffer::iterator& iter) HCLIB_NOEXCEPT
    {
        const u64 temp = unpackU64(iter);
        const f64 result = *reinterpret_cast<const f64*>(&temp);

        return result;
    }

    /* virtual */
    f64 DataBuffer::unpackF64(DataBuffer::const_iterator& iter) const HCLIB_NOEXCEPT
    {
        const u64 temp = unpackU64(iter);
        const f64 result = *reinterpret_cast<const f64*>(&temp);

        return result;
    }

    /* virtual */
    void DataBuffer::resizeIfNeeded(const std::size_t numMoreBytes)
    {
        if (index + numMoreBytes >= capacity)
        {
            capacity *= 2;
            const auto* tempBuffer = buffer;
            buffer = new char[capacity];

            std::memcpy(buffer, tempBuffer, index);
            delete[] tempBuffer;
        }
    }
}
