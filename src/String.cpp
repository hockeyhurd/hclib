/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#include <hclib/String.h>
#include <hclib/Math.h>

#include <functional>
#include <stdexcept>

#if CPP_VER <= 2011
#include <algorithm>
#elif CPP_VER > 2011
#include <utility>
#else
#error "Whaaaat?!?!? What sourcery is this version of C++ ???"
#endif

namespace hclib
{

    // NOTE: Max size implies the max size of both buffers.
    // Also, being an 'internal' function, it is assumed both
    // pointers are NOT nullptr AND <= 'maxSize'.
    // Lastly, any NULL termination should be handled externally
    // to this function.
    template<class T>
    static void copyString(char *dest, const char *src, const T maxSize)
    {
        for (T i = 0; i < maxSize; ++i)
        {
            dest[i] = src[i];
        }
    }

    String::String(const s32 len, char *str) : len(len), str(nullptr)
    {
        if (len < smallStringSize())
            copyString(smallStrBuffer, str, len);
        else
            this->str = str;
    }

    String::String() : len(1), str(nullptr)
    {
        smallStrBuffer[0] = '\0';
    }

    String::String(const char *cstr) : len(String::stringLength<decltype(len)>(cstr) + 1), str(nullptr)
    {
        if (len == 0)
        {
            smallStrBuffer[0] = '\0';
        }

        else if (len < smallStringSize())
        {
            copyString(smallStrBuffer, cstr, len + 1);
        }

        else
        {
            str = new char[len];
            copyString(str, cstr, len);
            str[len - 1] = '\0';
        }
    }

    String::String(const char *cstr, const s32 len) : len(len), str(nullptr)
    {
        if (len == 0)
        {
            smallStrBuffer[0] = '\0';
        }

        else if (len < smallStringSize())
        {
            copyString(smallStrBuffer, cstr, len + 1);
        }

        else
        {
            str = new char[len];
            copyString(str, cstr, len);
            str[len - 1] = '\0';
        }
    }

    String::String(const String &other) : len(other.len), str(nullptr)
    {
        if (other.isEmpty())
        {
            smallStrBuffer[0] = '\0';
        }

        else if (other.len < smallStringSize())
        {
            copyString(smallStrBuffer, other.smallStrBuffer, other.len + 1);
        }

        else
        {
            str = new char[len];
            copyString(str, other.str, len + 1);
            str[len - 1] = '\0';
        }
    }

    String::String(String &&other) : len(other.len), str(other.str)
    {
        if (other.isSmallString())
        {
            copyString(smallStrBuffer, other.smallStrBuffer, len);
        }

        else
        {
            other.str = nullptr;
            other.smallStrBuffer[0] = '\0';
            other.len = 0;
        }
    }

    String::String(const std::string &other) : len(other.size() + 1), str(nullptr)
    {
        if (other.empty())
            smallStrBuffer[0] = '\0';
        else if (len < smallStringSize())
        {
            copyString(smallStrBuffer, other.c_str(), len);
            smallStrBuffer[len - 1] = '\0';
        }

        else
        {
            const s32 minusOne = len - 1;
            str = new char[len];
            copyString(str, other.c_str(), minusOne);
            str[minusOne] = '\0';
        }
    }

    String::~String()
    {
        len = 0;

        if (str != nullptr)
        {
            delete[] str;
            str = nullptr;
        }
    }

    String &String::operator= (const char *cstr)
    {
        if (cstr != nullptr)
        {
            const s32 oldLen = len;
            len = stringLength<decltype(len)>(cstr) + 1;

            if (len < smallStringSize())
            {
                // Delete our old string
                if (oldLen >= smallStringSize())
                {
                    delete[] str;
                    str = nullptr;
                }

                const s32 lenMinusOne = len - 1;
                copyString(smallStrBuffer, cstr, lenMinusOne);
                smallStrBuffer[lenMinusOne] = '\0';
            }

            // Same size as our current buffer, re-use it!
            else if (len == oldLen)
            {
                const s32 lenMinusOne = len - 1;
                copyString(str, cstr, lenMinusOne);
                smallStrBuffer[lenMinusOne] = '\0';
            }

            // Need a bigger buffer...
            else
            {
                if (str != nullptr)
                {
                    delete[] str;
                }

                str = new char[len];

                const s32 lenMinusOne = len - 1;
                copyString(str, cstr, lenMinusOne);
                str[lenMinusOne] = '\0';
            }
        }

        return *this;
    }

    String &String::operator= (const String &other)
    {
        if (this == &other)
            return *this;

        const s32 oldLen = len;
        len = other.len;

        if (len < smallStringSize())
        {
            // Delete our old string
            if (oldLen >= smallStringSize())
            {
                delete[] str;
                str = nullptr;
            }

            const s32 lenMinusOne = len - 1;
            copyString(smallStrBuffer, other.c_str(), lenMinusOne);
            smallStrBuffer[lenMinusOne] = '\0';
        }

        // Same size as our current buffer, re-use it!
        else if (len == oldLen)
        {
            const s32 lenMinusOne = len - 1;
            copyString(str, other.c_str(), lenMinusOne);
            smallStrBuffer[lenMinusOne] = '\0';
        }

        // Need a bigger buffer...
        else
        {
            if (str != nullptr)
            {
                delete[] str;
            }

            str = new char[len];

            const s32 lenMinusOne = len - 1;
            copyString(str, other.c_str(), lenMinusOne);
            smallStrBuffer[0] = '\0';
        }

        return *this;
    }

    String &String::operator= (String &&other)
    {
        len = other.len;

        if (str != nullptr)
        {
            delete[] str;
            str = nullptr;
        }

        if (other.isSmallString())
        {
            const s32 smallStringSizeMinusOne = smallStringSize() - 1;
            copyString(smallStrBuffer, other.smallStrBuffer, smallStringSizeMinusOne);
            smallStrBuffer[smallStringSizeMinusOne] = '\0';
        }

        else
        {
            str = other.str;
            other.str = nullptr;
            other.len = 0;
        }

        other.smallStrBuffer[0] = '\0';

        return *this;
    }

    String &String::operator= (const std::string &other)
    {
        const s32 oldLen = len;
        len = other.size() + 1;

        if (len < smallStringSize())
        {
            // Delete our old string
            if (oldLen >= smallStringSize())
            {
                delete[] str;
                str = nullptr;
            }

            copyString(smallStrBuffer, other.c_str(), other.size());
            smallStrBuffer[other.size()] = '\0';
        }

        // Same size as our current buffer, re-use it!
        else if (len == oldLen)
        {
            copyString(str, other.c_str(), other.size());
            str[other.size()] = '\0';
        }

        // Need a bigger buffer...
        else
        {
            delete[] str;
            str = new char[len];

            copyString(str, other.c_str(), other.size());
            str[other.size()] = '\0';
        }

        return *this;
    }

    char &String::operator[] (s32 index)
    {
        if (isSmallString())
            return smallStrBuffer[index];

        return str[index];
    }

    const char &String::operator[] (s32 index) const
    {
        if (isSmallString())
            return smallStrBuffer[index];

        return str[index];
    }

    s32 String::size() const
    {
        return len > 0 ? len - 1 : 0;
    }

    bool String::isEmpty() const
    {
        return !size();
    }

    bool String::isSmallString() const
    {
        return len < smallStringSize();
    }

    const char *String::c_str() const
    {
        if (isSmallString())
            return smallStrBuffer;

        return str;
    }

    std::string String::toString() const
    {
        if (isEmpty())
            return std::string();
        else if (isSmallString())
            return std::string(smallStrBuffer);
        return std::string(str);
    }

    s32 String::hashCode() const
    {
        if (isSmallString())
            return hashString(smallStrBuffer);
        return hashString(str);
    }

    void String::clear()
    {
        len = 0;

        // TODO: Instead of always deleting the buffer, can we keep the buffer around
        // to reduce the number of heap allocations??
        // Note: We could do this, but not sure if this is desired.
        // Think of a case with a really big string being replaced by a
        // string of size 'HCLIB_SMALL_STRING_SIZE + 1'.
        if (str != nullptr)
        {
            delete[] str;
            str = nullptr;
        }

        smallStrBuffer[0] = '\0';
    }

    String String::copy() const
    {
        String result;
        if (isEmpty())
            return result;
        else if (isSmallString())
        {
            const s32 lenMinusOne = len - 1;
            copyString(result.smallStrBuffer, smallStrBuffer, lenMinusOne);
            result.smallStrBuffer[lenMinusOne] = '\0';
        }

        else
        {
            result.len = len;
            result.str = new char[len];
            copyString(result.str, str, len);
        }

        return result;
    }

    char &String::at(const s32 index)
    {
        if (index < 0 || index >= len)
        {
            std::ostringstream os;
            os << "Index " << index << " is out of range.";
            throw std::out_of_range(os.str());
        }

        char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        return actualStr[index];
    }

    const char &String::at(const s32 index) const
    {
        if (index < 0 || index >= len)
        {
            std::ostringstream os;
            os << "Index " << index << " is out of range.";
            throw std::out_of_range(os.str());
        }

        char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        return actualStr[index];
    }

    String &String::toLowercase()
    {
        if (isEmpty())
            return *this;

        char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        for (s32 i = 0; i < size(); ++i)
        {
            toLower(actualStr[i]);
        }

        return *this;
    }

    String &String::toUppercase()
    {
        if (isEmpty())
            return *this;

        char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        for (s32 i = 0; i < size(); ++i)
        {
            toUpper(actualStr[i]);
        }

        return *this;
    }

    bool String::contains(const char c)
    {
        if (isEmpty())
            return false;

        const char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        for (s32 i = 0; i < size(); ++i)
        {
            if (actualStr[i] == c)
                return true;
        }

        return false;
    }

    bool String::contains(const String &other)
    {
        if (isEmpty() || other.isEmpty() || size() < other.size())
            return false;

        const char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        const char *otherActualStr;
        if (isSmallString())
            otherActualStr = other.smallStrBuffer;
        else
            otherActualStr = other.str;

        s32 indexIntoOtherStr = 0;

        for (s32 i = 0; i < size(); ++i)
        {
            const char thisChar = actualStr[i];
            const char otherChar = otherActualStr[indexIntoOtherStr];

            if (thisChar == otherChar)
            {
                // Found complete match, exit.
                if (indexIntoOtherStr == other.size())
                    return true;

                // Still more chars, increment our index.
                ++indexIntoOtherStr;
            }

            else
            {
                // No match, reset index.
                indexIntoOtherStr = 0;
            }
        }

        return indexIntoOtherStr == other.size();
    }

    bool String::startsWith(const char c)
    {
        if (isEmpty())
            return false;

        else if (isSmallString())
            return smallStrBuffer[0] == c;

        return str != nullptr && str[0] == c;
    }

    bool String::endsWith(const char c)
    {
        return str != nullptr && !isEmpty() && str[size()] == c;
    }

    s64 String::indexOf(const char c)
    {
        if (isEmpty())
            return -1;

        const char *searchString;
        if (isSmallString())
            searchString = smallStrBuffer;
        else
            searchString = str;

        for (s32 i = 0; i < size(); ++i)
        {
            if (searchString[i] == c)
                return i;
        }

        return -1;
    }

    s64 String::lastIndexOf(const char c)
    {
        if (isEmpty())
            return -1;

        const char *searchString;
        if (isSmallString())
            searchString = smallStrBuffer;
        else
            searchString = str;

        for (s32 i = size(); i >= 0; --i)
        {
            if (searchString[i] == c)
                return i;
        }

        return -1;
    }

    void String::substring(String &output, const s32 start, const s32 end)
    {
        if (isEmpty() || start >= end || end > size())
            return;

        const char *referenceStr;
        if (isSmallString())
            referenceStr = smallStrBuffer;
        else
            referenceStr = str;

        const s32 dx = end - start + 1;
        char *outputStr;

        // Fits in our small string buffer.
        if (dx < smallStringSize())
            outputStr = output.smallStrBuffer;

        // Different sizes, need to re-allocate.
        else if (len != dx)
        {
            output.clear();
            output.len = dx;
            outputStr = output.str = new char[dx];
        }

        // Same size, do nothing important.
        else
            outputStr = output.str;

        for (s32 i = start; i < end; ++i)
        {
            const s32 offsetIndex = i - start;
            outputStr[offsetIndex] = referenceStr[i];
        }
    }

    String String::substring(const s32 start, const s32 end)
    {
        String string;
        substring(string, start, end);

        return string;
    }

    String &String::reverse()
    {
        if (isEmpty())
            return *this;

        char *actualStr;
        if (isSmallString())
            actualStr = smallStrBuffer;
        else
            actualStr = str;

        const s32 halfSize = len / 2;
        for (s32 i = 0; i < halfSize; ++i)
        {
            const s32 otherIndex = len - i - 1;
            char &thisChar = actualStr[i];
            char &otherChar = actualStr[otherIndex];
            std::swap(thisChar, otherChar);
        }

        return *this;
    }

    String String::reverseCopy() const
    {
        String result;
        if (isEmpty())
            return result;

        result.len = len;

        char *resultStr;
        const char *refStr;
        if (isSmallString())
        {
            resultStr = result.smallStrBuffer;
            refStr = smallStrBuffer;
        }

        else
        {
            resultStr = result.str = new char[len];
            refStr = str;
        }

        s32 thisIndex = 0;
        for (s32 i = size() - 1; i >= 0; --i)
        {
            resultStr[thisIndex++] = refStr[i];
        }

        return result;
    }

    s32 String::hashString(const char *str)
    {
        static const u32 p = 16777619u;
        u32 hash = 2166136261u;

        while (*str != '\0')
        {
            hash = (hash ^ *str) * p;
            ++str;
        }

        hash += hash << 13;
        hash ^= hash >> 7;
        hash += hash << 3;
        hash ^= hash >> 17;
        hash += hash << 5;

        return static_cast<s32>(hash);
    }

    bool String::equals(const String &other) const
    {
        if (len != other.len)
            return false;

        const char *leftStr;
        const char *rightStr;

        if (isSmallString())
        {
            leftStr = smallStrBuffer;
            rightStr = other.smallStrBuffer;
        }

        else
        {
            leftStr = str;
            rightStr = other.str;
        }

        for (s32 i = 0; i < size(); ++i)
        {
            if (leftStr[i] != rightStr[i])
                return false;
        }

        return true;
    }

    bool String::equalsIgnoreCase(const String &other) const
    {
        if (len != other.len)
            return false;

        const char *leftStr;
        const char *rightStr;

        if (isSmallString())
        {
            leftStr = smallStrBuffer;
            rightStr = other.smallStrBuffer;
        }

        else
        {
            leftStr = str;
            rightStr = other.str;
        }

        for (s32 i = 0; i < size(); ++i)
        {
            const char left = leftStr[i];
            const char right = rightStr[i];

            // Note: this is a hack to uppercase this letter.
            // It may not be the actual uppercase but we are only checking
            // for equality anyways.
            const char leftUpper = left | (1 << 6);

            if (left != right && leftUpper != right)
                return false;
        }

        return true;
    }

    void String::parseInt32(String &ref, s32 value)
    {
        if (value == 0)
        {
            ref = "0";
            return;
        }

        ref.clear();

        const bool isNegative = value < 0;

        if (isNegative)
            value = -value;

        s32 index = 0;
        do
        {
            ref.smallStrBuffer[index++] = '0' + static_cast<char>(value % 10);
            value /= 10;
        }
        while (value > 0);

        if (isNegative)
            ref.smallStrBuffer[index++] = '-';

        ref.smallStrBuffer[index++] = '\0';
        ref.len = index;
        ref.reverse();
    }

    void String::parseUInt32(String &ref, u32 value)
    {
        if (value == 0)
        {
            ref = "0";
            return;
        }

        ref.clear();

        s32 index = 0;
        do
        {
            ref.smallStrBuffer[index++] = '0' + static_cast<char>(value % 10);
            value /= 10;
        }
        while (value > 0);

        ref.smallStrBuffer[index++] = '\0';
        ref.len = index;
        ref.reverse();
    }

    void String::parseInt64(String &ref, s64 value)
    {
        if (value == 0)
        {
            ref = "0";
            return;
        }

        ref.clear();

        const bool isNegative = value < 0;

        if (isNegative)
            value = -value;

        s32 index = 0;
        do
        {
            ref.smallStrBuffer[index++] = '0' + static_cast<char>(value % 10);
            value /= 10;
        }
        while (value > 0);

        if (isNegative)
            ref.smallStrBuffer[index++] = '-';

        ref.smallStrBuffer[index++] = '\0';
        ref.len = index;
        ref.reverse();
    }

    void String::parseUInt64(String &ref, u64 value)
    {
        if (value == 0)
        {
            ref = "0";
            return;
        }

        ref.clear();

        s32 index = 0;
        do
        {
            ref.smallStrBuffer[index++] = '0' + static_cast<char>(value % 10);
            value /= 10;
        }
        while (value > 0);

        ref.smallStrBuffer[index++] = '\0';
        ref.len = index;
        ref.reverse();
    }

    String String::parseInt32(const s32 val)
    {
        String string;
        parseInt32(string, val);

        return string;
    }

    String String::parseUInt32(const u32 val)
    {
        String string;
        parseUInt32(string, val);

        return string;
    }

    String String::parseInt64(const s64 val)
    {
        String string;
        parseInt64(string, val);

        return string;
    }

    String String::parseUInt64(const u64 val)
    {
        String string;
        parseUInt64(string, val);

        return string;
    }

    String String::operator+ (const String &other)
    {
        String copy = *this;
        copy += other;
        return copy;
    }

    String String::operator+ (String &&other)
    {
        return (*this + other);
    }

    String &String::operator+= (const String &other)
    {
        if (other.isEmpty())
            return *this;

        const s32 newLen = len - 1 + other.len;
        // const s32 newLen = len + other.len;
        char *newStr;
        char *offsetStr;
        bool mustDeleteStr;

        // Need to upgrade current small string to a "proper" string.
        if (isSmallString() && newLen >= smallStringSize())
        {
            newStr = new char[newLen];
            offsetStr = newStr + size();
            copyString(newStr, smallStrBuffer, size());
            mustDeleteStr = true;
        }

        // Still room in our small string buffer.
        else if (newLen < smallStringSize())
        {
            newStr = smallStrBuffer;
            offsetStr = smallStrBuffer + size();
            mustDeleteStr = false;
        }

        // Standard reallocate case
        else
        {
            newStr = new char[newLen];
            offsetStr = newStr + size();
            copyString(newStr, str, size());
            mustDeleteStr = true;
        }

        const char *otherStr;
        if (other.isSmallString())
            otherStr = other.smallStrBuffer;
        else
            otherStr = other.str;

        for (s32 i = 0; i < other.len; ++i)
        {
            offsetStr[i] = otherStr[i];
        }

        if (mustDeleteStr)
        {
            delete[] str;
            str = newStr;
        }

        len = newLen;

        return *this;
    }

    String &String::operator+= (String &&other)
    {
        return (*this += other);
    }

    bool String::operator== (const String &other) const
    {
        return this == &other || equals(other);
    }

    bool String::operator== (String &&other) const
    {
        return (*this == std::cref(other));
    }

    bool String::operator!= (const String &other) const
    {
        return !(*this == other);
    }

    bool String::operator!= (String &&other) const
    {
        return !(*this == std::cref(other));
    }

    bool String::operator< (const String &other) const
    {
        if (len < other.len)
            return true;
        else if (len > other.len)
            return false;

        // Must be the same size, so we can just check this once and
        // call it equal conditionally.
        else if (isEmpty())
            return true;

        const char *leftStr;
        const char *rightStr;
        if (isSmallString())
        {
            leftStr = smallStrBuffer;
            rightStr = other.smallStrBuffer;
        }

        else
        {
            leftStr = str;
            rightStr = other.str;
        }

        for (s32 i = 0; i < size(); ++i)
        {
            if (leftStr[i] < rightStr[i])
                return true;
        }

        return false;
    }

    bool String::operator< (String &&other) const
    {
        return *this < std::cref(other);
    }

    bool String::operator> (const String &other) const
    {
        if (size() < other.size())
            return false;
        else if (size() > other.size())
            return true;

        // Must be the same size, so we can just check this once and
        // call it equal conditionally.
        else if (isEmpty())
            return true;

        const char *leftStr;
        const char *rightStr;
        if (isSmallString())
        {
            leftStr = smallStrBuffer;
            rightStr = other.smallStrBuffer;
        }

        else
        {
            leftStr = str;
            rightStr = other.str;
        }

        for (s32 i = 0; i < size(); ++i)
        {
            if (leftStr[i] > rightStr[i])
                return true;
        }

        return false;
    }

    bool String::operator> (String &&other) const
    {
        return *this > std::cref(other);
    }

    bool String::operator<= (const String &other) const
    {
        return *this < other || *this == other;
    }

    bool String::operator<= (String &&other) const
    {
        return *this <= std::cref(other);
    }

    bool String::operator>= (const String &other) const
    {
        return *this > other || *this == other;
    }

    bool String::operator>= (String &&other) const
    {
        return *this >= std::cref(other);
    }

    String::iterator String::begin() const
    {
        if (isSmallString())
            return String::iterator((char*) smallStrBuffer);
        return String::iterator(str);
    }

    String::iterator String::end() const
    {
        return String::iterator(str + len - 1);
    }

    String::const_iterator String::cbegin() const
    {
        if (isSmallString())
            return String::iterator((char*)smallStrBuffer);
        return String::iterator(str);
    }

    String::const_iterator String::cend() const
    {
        return String::iterator(str + len - 2);
    }

    String::reverse_iterator String::rbegin() const
    {
        return String::reverse_iterator(const_cast<String*>(this), len - 1);
    }

    String::reverse_iterator String::rend() const
    {
        return String::reverse_iterator(const_cast<String*>(this), 0);
    }

    String::const_reverse_iterator String::crbegin() const
    {
        return String::const_reverse_iterator(const_cast<String*>(this), len - 1);
    }

    String::const_reverse_iterator String::crend() const
    {
        return String::const_reverse_iterator(const_cast<String*>(this), 0);
    }

    std::ostream &operator<< (std::ostream &os, const String &string)
    {
        if (!string.isEmpty())
            os << string.c_str();

        return os;
    }

    String::iterator::iterator(char *str) : ch(str)
    {
    }

    char &String::iterator::operator*()
    {
        return *ch;
    }

    char *String::iterator::operator->()
    {
        return ch;
    }

    String::iterator String::iterator::operator++ ()
    {
        if (*ch != '\0')
            ++ch;

        return *this;
    }

    String::iterator String::iterator::operator++ (s32)
    {
        if (*ch != '\0')
            ch++;

        return *this;
    }

    String::iterator String::iterator::operator-- ()
    {
        if (*ch != '\0')
            --ch;

        return *this;
    }

    String::iterator String::iterator::operator-- (s32)
    {
        if (*ch != '\0')
            ch--;

        return *this;
    }

    bool String::iterator::operator== (const iterator &other)
    {
        return ch == other.ch;
    }

    bool String::iterator::operator!= (const iterator &other)
    {
        return !(*this == other);
    }

    String::reverse_iterator::reverse_iterator(String *str, const s32 index) : str(str), index(index)
    {
    }

    char &String::reverse_iterator::operator*()
    {
        String &theStr = *str;
        return theStr[index];
    }

    char *String::reverse_iterator::operator->()
    {
        String &theStr = *str;
        return &theStr[index];
    }

    String::reverse_iterator String::reverse_iterator::operator++ ()
    {
        if (index > 0)
            return String::reverse_iterator(str, --index);

        return *this;
    }

    String::reverse_iterator String::reverse_iterator::operator++ (s32)
    {
        if (index > 0)
            return String::reverse_iterator(str, index--);

        return *this;
    }

    String::reverse_iterator String::reverse_iterator::operator-- ()
    {
        if (index < str->size())
            return String::reverse_iterator(str, ++index);

        return *this;
    }

    String::reverse_iterator String::reverse_iterator::operator-- (s32)
    {
        if (index < str->size())
            return String::reverse_iterator(str, index++);

        return *this;
    }

    bool String::reverse_iterator::operator== (const String::reverse_iterator &other)
    {
        return str == other.str && index == other.index;
    }

    bool String::reverse_iterator::operator!= (const String::reverse_iterator &other)
    {
        return !(*this == other);
    }

}
