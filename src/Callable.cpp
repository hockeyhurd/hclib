/**
 * @author hockeyhurd
 * @version 2019-02-22
 */

#include <hclib/Callable.h>

namespace hclib
{

    Callable::Callable(std::function<void()> &&func) : func(func)
    {

    }

    void Callable::operator()()
    {
        func();
    }

}
