#include <hclib/File.h>

#include <algorithm>
#include <cstdio>
#include <functional>
#include <vector>

#if OS_APPLE || OS_UNIX
#include <sys/stat.h>
#endif

#if OS_WIN
#pragma warning(push)
#pragma warning(disable: 4996)
#endif

// struct stat {
    // dev_t     st_dev;         /* ID of device containing file */
    // ino_t     st_ino;         /* Inode number */
    // mode_t    st_mode;        /* File type and mode */
    // nlink_t   st_nlink;       /* Number of hard links */
    // uid_t     st_uid;         /* User ID of owner */
    // gid_t     st_gid;         /* Group ID of owner */
    // dev_t     st_rdev;        /* Device ID (if special file) */
    // off_t     st_size;        /* Total size, in bytes */
    // blksize_t st_blksize;     /* Block size for filesystem I/O */
    // blkcnt_t  st_blocks;      /* Number of 512B blocks allocated */

    /* Since Linux 2.6, the kernel supports nanosecond
       precision for the following timestamp fields.
       For the details before Linux 2.6, see NOTES. */

    // struct timespec st_atim;  /* Time of last access */
    // struct timespec st_mtim;  /* Time of last modification */
    // struct timespec st_ctim;  /* Time of last status change */

// #define st_atime st_atim.tv_sec      /* Backward compatibility */
// #define st_mtime st_mtim.tv_sec
// #define st_ctime st_ctim.tv_sec
// };

namespace hclib
{

    inline static void writeByte(const char data, FILE *file)
    {
        fputc(data, file);
    }

    inline static s32 readByte(FILE *file)
    {
        return static_cast<s32>(fgetc(file));
    }

    File::Buffer::Buffer(const s8 data) : data(data)
    {
    }

    bool File::Buffer::isEOF() const
    {
        return data == (s8) EOF;
    }

    std::size_t File::size() const
    {
        switch (mode)
        {
        case EnumMode::READ:
        {
            struct stat st;
            stat(path.c_str(), &st);
            return st.st_size;
        }
            break;
        case EnumMode::APPEND:
        case EnumMode::WRITE:
            return bytesWritten;
        default:
            throw std::runtime_error("Invalid mode");
        }
    }

    File::File(std::string &&path, const File::EnumMode mode) : path(std::move(path)), mode(mode),
        handle(fopen(this->path.c_str(), File::accessorMode(this->mode))), bytesWritten(0)
    {
        if (handle == nullptr)
        {
            std::ostringstream os;
            os << "File '" << path << "' doesn't exist";
            throw std::runtime_error(os.str());
        }
    }

    File::File(const std::string &path, const File::EnumMode mode) : path(path), mode(mode),
        handle(fopen(this->path.c_str(), File::accessorMode(this->mode))), bytesWritten(0)
    {
        if (handle == nullptr)
        {
            std::ostringstream os;
            os << "File '" << path << "' doesn't exist";
            throw std::runtime_error(os.str());
        }
    }

    File::File(File &&other) : path(std::move(other.path)), mode(other.mode), handle(other.handle),
        bytesWritten(other.bytesWritten)
    {
        other.handle = nullptr;
    }

    File &File::operator= (File &&other)
    {
        path = std::move(other.path);
        mode = other.mode;
        handle = other.handle;
        other.handle = nullptr;
        bytesWritten = other.bytesWritten;

        return *this;
    }

    File::~File()
    {
        closeInternal();
    }

    const std::string &File::getPath() const
    {
        return path;
    }

    std::string &File::getPath()
    {
        return path;
    }

    File::EnumMode File::getMode() const
    {
        return mode;
    }

    bool File::isEOF()
    {
        return buffer.isEOF();
    }

    /* virtual */
    s32 File::getPermissions() const
    {
        if (handle == nullptr)
            return -1;

#if OS_APPLE || OS_UNIX
        struct stat buf;
        ::stat(path.c_str(), &buf);
        return buf.st_mode;
#elif OS_WIN
        // Windows doesn't support permissions in this sense...
        // This function therefore isn't useful for this platform.
        return 0;
#else
#error "getPermissions not implemented on this platform"
        return 0;
#endif
    }

    /* virtual */
    void File::setPermissions(const s32 perms)
    {
#if OS_APPLE || OS_UNIX
        // On Linux, mode_t is apparently defined as 'unsigned int' aka s32.
        // TODO: Verify this last statement is valid for different flavors of
        // Unix/Linux as well as different architectures.  For now, we will
        // assume this is safe.

        chmod(path.c_str(), perms);
#elif OS_WIN
        // Windows doesn't support permissions in this sense...
        // This function therefore isn't useful for this platform.
#else
#error "getPermissions not implemented on this platform"
#endif
    }

    void File::flush()
    {
        if (handle != nullptr)
            fflush(handle);
    }

    void File::close()
    {
        closeInternal();
    }

    void File::write(const char data)
    {
        if (handle != nullptr)
        {
            writeByte(data, handle);
            ++bytesWritten;
        }
    }

    void File::write(const u32 data)
    {
        if (handle == nullptr)
            return;

        union U
        {
            u32 value;
            char buf[sizeof(u32)];
            U(const u32 value = 0) : value(value) {}
        };

        U v(data);

        for (u32 i = 0; i < sizeof(u32); ++i)
        {
            writeByte(v.buf[i], handle);
            ++bytesWritten;
        }
    }

    void File::write(const char *data, const std::size_t len)
    {
        if (handle == nullptr)
            return;

        for (std::size_t i = 0; i < len; ++i)
        {
            writeByte(data[i], handle);
            ++bytesWritten;
        }
    }

    void File::write(const std::string &str)
    {
        write(str.c_str(), str.size());
    }

    void File::write(std::string &&str)
    {
        write(str.c_str(), str.size());
    }

#if CPP_17
    void File::write(const std::string_view str)
    {
        write(str.data(), str.size());
    }
#endif

    s8 File::readByte()
    {
        if (!buffer.isEOF())
            buffer.data = static_cast<s8>(hclib::readByte(handle));
        return buffer.data;
    }

    s32 File::readInt()
    {
        union U
        {
            s32 value;
            char buf[sizeof(s32)];
            U(const s32 value = 0) : value(value) {}
        };

        U v(0);

        for (u32 i = 0; i < sizeof(s32); ++i)
        {
            v.buf[i] = readByte();
        }

        return v.value;
    }

    std::string File::readFile(const std::size_t expectedSize)
    {
        // TODO: Support non-ascii chars! i.e. std::wstring
        std::vector<char> buf;
        buf.reserve(expectedSize);

        while (!isEOF())
        {
            const char c = static_cast<char>(readByte());
            buf.push_back(c);
        }

        if (!buf.empty())
            return std::string(buf.begin(), buf.end());

        return "";
    }

    /* virtual */
    bool File::remove()
    {
        closeInternal();
        const s32 result = ::remove(path.c_str());
        return result == 0;
    }

    /* virtual */
    bool File::rename(const std::string &name)
    {
        if (name.empty())
            return false;

        const auto result = std::rename(path.c_str(), name.c_str());

        // Failure case
        if (result != 0)
            return false;

        // Passed if we got this far.
        this->path = name;
        return true;
    }

    /* virtual */
    bool File::rename(std::string &&name)
    {
        if (name.empty())
            return false;

        const auto result = std::rename(path.c_str(), name.c_str());

        // Failure case
        if (result != 0)
            return false;

        // Passed if we got this far.
        this->path = std::move(name);
        return true;
    }

    /* static */
    std::string File::getNameWithoutExtension(const std::string &input)
    {
        std::size_t pos = input.size();

        for (auto iter = input.rbegin(); iter != input.rend(); ++iter)
        {
            if (*iter == '.')
            {
                return input.substr(0, std::max<std::size_t>(pos - 1, 1));
            }

            --pos;
        }

        return "";
    }

    /* static */
    std::string File::getNameWithoutExtension(std::string &&input)
    {
        return getNameWithoutExtension(std::ref(input));
    }

    /* static */
    const char *File::modeName(const File::EnumMode mode)
    {
        switch (mode)
        {
        case File::EnumMode::READ:
            return "READ";
        case File::EnumMode::WRITE:
            return "WRITE";
        case File::EnumMode::APPEND:
            return "APPEND";
        default:
            return nullptr;
        }
    }

    /* static */
    const char *File::accessorMode(const File::EnumMode mode)
    {
        switch (mode)
        {
        case File::EnumMode::READ:
            return "r";
        case File::EnumMode::WRITE:
            return "w";
        case File::EnumMode::APPEND:
            return "w+";
        default:
            return nullptr;
        }
    }

#if CPP_17
    /* static */
    std::string_view File::name_s(const File::EnumMode mode)
    {
        switch (mode)
        {
        case File::EnumMode::READ:
            return "READ";
        case File::EnumMode::WRITE:
            return "WRITE";
        case File::EnumMode::APPEND:
            return "APPEND";
        default:
            throw std::runtime_error("Invalid File::EnumMode");
        }
    }

    /* static */
    std::string_view File::accessorMode_s(const File::EnumMode mode)
    {
        switch (mode)
        {
        case File::EnumMode::READ:
            return "r";
        case File::EnumMode::WRITE:
            return "w";
        case File::EnumMode::APPEND:
            return "w+";
        default:
            throw std::runtime_error("Invalid File::EnumMode");
        }
    }
#endif

    void File::closeInternal()
    {
        if (handle != nullptr)
        {
            fflush(handle);
            fclose(handle);
            handle = nullptr;
            buffer.data = EOF;
        }
    }

}

#if OS_WIN
#pragma warning(pop)
#endif
