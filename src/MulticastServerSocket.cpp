/*
 * Class for creating and using Posix style multicast server side sockets.
 *
 * @author hockeyhurd
 * @version 10/31/2021
 */

// HCLib incldues
#include <hclib/MulticastServerSocket.h>

// OS includes
#if !OS_WIN
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#endif

// std includes
#include <cstring>
#include <sstream>

namespace hclib
{
    MulticastServerSocket::MulticastServerSocket(const s32 port, const bool reuse,
        const std::string &address, const std::string &nic) :
        UDPServerSocket(port, reuse), address(address), nic(nic)
    {
    }

    MulticastServerSocket::MulticastServerSocket(const s32 port, const bool reuse,
        std::string &&address, std::string &&nic) :
        UDPServerSocket(port, reuse), address(std::move(address)), nic(std::move(nic))
    {
    }

    /* virtual */
    MulticastServerSocket::~MulticastServerSocket()
    {
        leaveGroup();
    }

    /* virtual */
    std::string &MulticastServerSocket::getGroupAddress()
    {
        return address;
    }

    /* virtual */
    const std::string &MulticastServerSocket::getGroupAddress() const
    {
        return address;
    }

    /* virtual */
    std::string &MulticastServerSocket::getNIC()
    {
        return nic;
    }

    /* virtual */
    const std::string &MulticastServerSocket::getNIC() const
    {
        return nic;
    }

    /* virtual */
    void MulticastServerSocket::close() /* override */
    {
        leaveGroup();
        UDPServerSocket::close();
    }

    /* virtual */
    void MulticastServerSocket::joinGroup()
    {
        if (isSetup())
        {
            // TODO: Consider moving this to some sort of member variable.
            ip_mreq group;
            group.imr_multiaddr.s_addr = inet_addr(address.c_str());
            group.imr_interface.s_addr = INADDR_ANY;
            const auto nicName = getIPFromNIC();
            group.imr_interface.s_addr = inet_addr(nicName.c_str()); // @@@

            const s32 code = setsockopt(mSocket.sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                    reinterpret_cast<char*>(&group), sizeof(group));

            if (code < 0)
            {
                close();
                std::ostringstream os;
                os << "Failed to set the local interface (code: " << code << ")\n";
                throw std::runtime_error(os.str());
            }
        }
    }

    /* virtual */
    void MulticastServerSocket::leaveGroup()
    {
        if (isSetup())
        {
            // TODO: Consider moving this to some sort of member variable.
            ip_mreq group;
            group.imr_multiaddr.s_addr = inet_addr(address.c_str());
            group.imr_interface.s_addr = INADDR_ANY;
            const auto nicName = getIPFromNIC();
            group.imr_interface.s_addr = inet_addr(nicName.c_str()); // @@@

            const s32 code = setsockopt(mSocket.sock, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                    reinterpret_cast<char*>(&group), sizeof(group));

            if (code < 0)
            {
                std::ostringstream os;
                os << "Failed to set the local interface (code: " << code << ")\n";
                throw std::runtime_error(os.str());
            }
        }
    }

    /* virtual */
    void MulticastServerSocket::setSocketOptions() /* override */
    {
        UDPServerSocket::setSocketOptions();
        mSocket.handle->sin_addr.s_addr = inet_addr(address.c_str());

        // TODO: Disable loopback for now.  Should make this a parameter somewhere though...
        char loopch = '\0';
        const s32 code = setsockopt(mSocket.sock, IPPROTO_IP, IP_MULTICAST_LOOP, static_cast<char*>(&loopch), sizeof(loopch));

        if (code < 0)
        {
            close();
            std::ostringstream os;
            os << "Failed to disable multicast loopback (code: " << code << ")\n";
            throw std::runtime_error(os.str());
        }

        joinGroup();
    }

    /* virtual */
    void MulticastServerSocket::setTTLInternal() /* override */
    {
        const s32 code = setsockopt(mSocket.sock, IPPROTO_IP, IP_MULTICAST_TTL, &this->ttl, sizeof(this->ttl));

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Failed to set TTL for this socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }
    }

    std::string MulticastServerSocket::getIPFromNIC()
    {
        ifreq ifr;

        // Specify that we want the IPv4 address
        ifr.ifr_addr.sa_family = AF_INET;

        // Get the IP address associated with the provided nicName.
        std::strncpy(ifr.ifr_name, nic.c_str(), IFNAMSIZ - 1);

        ioctl(mSocket.sock, SIOCGIFADDR, &ifr);

        const std::string result = inet_ntoa((reinterpret_cast<sockaddr_in*>(&ifr.ifr_addr))->sin_addr);
        return result;
    }
}
