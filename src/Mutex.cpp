/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

// HCLib includes
#include <hclib/Mutex.h>

// std includes
#include <stdexcept>

namespace hclib
{

    Mutex::Mutex()
    {
        if (pthread_mutex_init(&mutex, nullptr) != 0)
        {
            throw std::runtime_error("Failed to create mutex");
        }
    }

    /* virtual */
    Mutex::~Mutex()
    {
        pthread_mutex_destroy(&mutex);
    }

    /* virtual */
    Mutex::mutex_t *Mutex::getMutex() HCLIB_NOEXCEPT
    {
        return &mutex;
    }

    /* virtual */
    const Mutex::mutex_t *Mutex::getMutex() const HCLIB_NOEXCEPT
    {
        return &mutex;
    }

    /* virtual */
    void Mutex::lock()
    {
        if (pthread_mutex_lock(&mutex) != 0)
        {
            throw std::runtime_error("Failed to lock mutex");
        }
    }

    /* virtual */
    bool Mutex::tryLock()
    {
        return pthread_mutex_trylock(&mutex) == 0;
    }

    /* virtual */
    bool Mutex::try_lock()
    {
        return tryLock();
    }

    /* virtual */
    void Mutex::unlock()
    {
        if (pthread_mutex_unlock(&mutex) != 0)
        {
            throw std::runtime_error("Failed to unlock mutex");
        }
    }

    SharedMutex::SharedMutex()
    {
        if (pthread_rwlock_init(&mutex, nullptr) != 0)
        {
            throw std::runtime_error("Failed to create mutex");
        }
    }

    /* virtual */
    SharedMutex::~SharedMutex()
    {
        pthread_rwlock_destroy(&mutex);
    }

    /* virtual */
    SharedMutex::mutex_t *SharedMutex::getMutex() HCLIB_NOEXCEPT
    {
        return &mutex;
    }

    /* virtual */
    const SharedMutex::mutex_t *SharedMutex::getMutex() const HCLIB_NOEXCEPT
    {
        return &mutex;
    }

    /* virtual */
    void SharedMutex::lock()
    {
        writeLock();
    }

    /* virtual */
    void SharedMutex::readLock()
    {
        if (pthread_rwlock_rdlock(&mutex) != 0)
        {
            throw std::runtime_error("Failed to obtain read lock");
        }
    }

    /* virtual */
    void SharedMutex::writeLock()
    {
        if (pthread_rwlock_wrlock(&mutex) != 0)
        {
            throw std::runtime_error("Failed to obtain write lock");
        }
    }

    /* virtual */
    bool SharedMutex::tryReadLock()
    {
        return pthread_rwlock_tryrdlock(&mutex) == 0;
    }

    /* virtual */
    bool SharedMutex::tryWriteLock()
    {
        return pthread_rwlock_trywrlock(&mutex) == 0;
    }

    /* virtual */
    bool SharedMutex::try_lock()
    {
        return tryWriteLock();
    }

    /* virtual */
    void SharedMutex::unlock()
    {
        if (pthread_rwlock_unlock(&mutex) != 0)
        {
            throw std::runtime_error("Failed to unlock mutex");
        }
    }

}
