#include <hclib/Math.h>

namespace hclib
{
    namespace math
    {

        template<>
        f32 ceilPowerOfTwo<f32>(const f32 value)
        {
            using namespace std;
            return ceilf(log2f(value));
        }

        template<>
        f32 floorPowerOfTwo<f32>(const f32 value)
        {
            using namespace std;
            return floorf(log2f(value));
        }

        template<>
        f32 nearestPowerOfTwo<f32>(const f32 value)
        {
            using namespace std;
            return roundf(log2f(value));
        }
    }
}
