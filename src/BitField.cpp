#include <hclib/BitField.h>

namespace hclib
{

    BitField::BitField(const u32 data) HCLIB_NOEXCEPT : data(data), index(0)
    {
    }

    u32 BitField::getData() const HCLIB_NOEXCEPT
    {
        return data;
    }

    bool BitField::isFull() const HCLIB_NOEXCEPT
    {
        return index == capacity();
    }

    bool BitField::get(u32 index) const HCLIB_NOEXCEPT
    {
        index %= 32;
        const u32 mask = 1 << index;

        return static_cast<bool>(data & mask);
    }

    void BitField::set(u32 index, const bool value) HCLIB_NOEXCEPT
    {
        index %= 32;
        const u32 mask = 1 << index;
        const u32 fieldVal = static_cast<u32>(value) << index; 

        data &= ~mask;
        data |= fieldVal;
    }

    u32 BitField::add(const bool value) HCLIB_NOEXCEPT
    {
        if (isFull())
            return UINT32_MAX;

        const u32 pos = index++;
        set(pos, value);

        return pos;
    }

    void BitField::zero() HCLIB_NOEXCEPT
    {
        data = index = 0;
    }

    s32 BitField::hashCode() const /* override */
    {
        return static_cast<s32>(data);
    }

}
