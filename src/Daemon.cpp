/**
 * @author hockeyhurd
 * @version 2021-10-26
 */

// My includes
#include <hclib/Daemon.h>
#include <hclib/Debug.h>

// std includes
#include <csignal>

// OS specific includes
#if OS_UNIX || OS_APPLE
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#endif

namespace hclib
{

    static HCLIB_CONSTEXPR s32 INVALID_PID = -1;

    /* static */
    bool Daemon::create(const char *path, char *const argv[], char *const envp[])
    {
        if (path == nullptr)
            return false;

#if OS_WIN
        // return createProcessWindows(handle, path, argv);
#else
        s32 pid = fork();

        if (pid == INVALID_PID)
        {
            Debug::error("Failed to fork child process...");
            return false;
        }

        // Is the child process
        else if (pid == 0)
        {
            signal(SIGCHLD, SIG_IGN);
            signal(SIGHUP, SIG_IGN);

            // Fork off again to create the 'grandchild' process.
            pid = fork();

            // Since we are either child or grandchild, we only care if we
            // are the grandchild because the child process always exits.
            // Therefore we don't need to even check if an error occurred.
            if (pid == 0)
            {
                // Set file permissions
                umask(0);

                // cd to root
                chdir("/");

                // Close any open file descriptors.
                for (s32 i = sysconf(_SC_OPEN_MAX); i >= 0; --i)
                {
                    close(i);
                }

                // Execute process as grandchild (now daemon'ized) process.
                execve(path, argv, envp);
            }

            // Child process always exits here.  Grandchild never hits this
            // because of the exec call.
            std::exit(pid != INVALID_PID ? EXIT_SUCCESS : EXIT_FAILURE);
        }

        // Parent process returns success
        return true;
#endif
    }

    /* static */
    bool Daemon::create(const char *path, char *const argv[])
    {
        if (path == nullptr)
            return false;

#if OS_WIN
        // return createProcessWindows(handle, path, argv);
#else
        s32 pid = fork();

        if (pid == INVALID_PID)
        {
            Debug::error("Failed to fork child process...");
            return false;
        }

        // Is the child process
        else if (pid == 0)
        {
            signal(SIGCHLD, SIG_IGN);
            signal(SIGHUP, SIG_IGN);

            // Fork off again to create the 'grandchild' process.
            pid = fork();

            // Since we are either child or grandchild, we only care if we
            // are the grandchild because the child process always exits.
            // Therefore we don't need to even check if an error occurred.
            if (pid == 0)
            {
                // Set file permissions
                umask(0);

                // cd to root
                chdir("/");

                // Close any open file descriptors.
                for (s32 i = sysconf(_SC_OPEN_MAX); i >= 0; --i)
                {
                    close(i);
                }

                // Execute process as grandchild (now daemon'ized) process.
                execv(path, argv);
            }

            // Child process always exits here.  Grandchild never hits this
            // because of the exec call.
            std::exit(pid != INVALID_PID ? EXIT_SUCCESS : EXIT_FAILURE);
        }

        // Parent process returns success
        return true;
#endif
    }
}

