# CC=clang++
CC=g++
DEBUG=Debug
RELEASE=Release

ifdef CFG
	ifneq ($(CFG), $(DEBUG))
		CC_FLAGS=-pipe -std=c++17 -fPIC -I../include -O3 -Wall -Wextra -Wcast-qual -Wfloat-equal -Wno-unused-parameter -pedantic -fstack-protector-strong -fstack-clash-protection
		OUTFILE=libhclib.so
		OUTDIR=$(RELEASE)
	else
		CC_FLAGS=-pipe -std=c++17 -fPIC -I../include -g -O0 -Wall -Wextra -Wcast-qual -Wfloat-equal -Wno-unused-parameter -pedantic -fstack-protector-strong -fstack-clash-protection
		OUTFILE=libhclibd.so
		OUTDIR=$(DEBUG)
	endif
else
	CC_FLAGS=-pipe -std=c++17 -fPIC -I../include -O3 -Wall -Wextra -Wcast-qual -Wfloat-equal -Wno-unused-parameter -pedantic -fstack-protector-strong -fstack-clash-protection
	OUTFILE=libhclib.so
	OUTDIR=$(RELEASE)
endif

LINK_FLAGS=-pipe -shared -flto
LINK_LIBS=-pthread
all: setup link AsyncLogger.o Algorithm.o ArgParser.o BitField.o Callable.o ChildProcess.o Conditional.o Daemon.o DataBuffer.o \
	Debug.o File.o FileWriter.o Hashable.o Logger.o Math.o MemStats.o Mutex.o MulticastClientSocket.o MulticastServerSocket.o Random.o \
	TCPClientSocket.o TCPServerSocket.o Socket.o String.o StringBuilder.o ThreadPool.o \
	UDPClientSocket.o UDPServerSocket.o

setup:
	mkdir -p $(OUTDIR)

AsyncLogger.o: AsyncLogger.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/AsyncLogger.o -c AsyncLogger.cpp
Algorithm.o: Algorithm.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Algorithm.o -c Algorithm.cpp
ArgParser.o: ArgParser.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/ArgParser.o -c ArgParser.cpp
BitField.o: BitField.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/BitField.o -c BitField.cpp
Callable.o: Callable.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Callable.o -c Callable.cpp
ChildProcess.o: ChildProcess.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/ChildProcess.o -c ChildProcess.cpp
Conditional.o: Conditional.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Conditional.o -c Conditional.cpp
Daemon.o: Daemon.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Daemon.o -c Daemon.cpp
DataBuffer.o: DataBuffer.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/DataBuffer.o -c DataBuffer.cpp
Debug.o: Debug.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Debug.o -c Debug.cpp
File.o: File.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/File.o -c File.cpp
FileWriter.o: FileWriter.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/FileWriter.o -c FileWriter.cpp
Hashable.o: Hashable.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Hashable.o -c Hashable.cpp
ISerializable.o: ISerializable.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/ISerializable.o -c ISerializable.cpp
Logger.o: Logger.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Logger.o -c Logger.cpp
Math.o: Math.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Math.o -c Math.cpp
MemStats.o: MemStats.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/MemStats.o -c MemStats.cpp
Mutex.o: Mutex.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Mutex.o -c Mutex.cpp
MulticastClientSocket.o: MulticastClientSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/MulticastClientSocket.o -c MulticastClientSocket.cpp
MulticastServerSocket.o: MulticastServerSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/MulticastServerSocket.o -c MulticastServerSocket.cpp
Random.o: Random.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Random.o -c Random.cpp
Socket.o: Socket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Socket.o -c Socket.cpp
StringBuilder.o: StringBuilder.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/StringBuilder.o -c StringBuilder.cpp
String.o: String.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/String.o -c String.cpp
TCPClientSocket.o: TCPClientSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/TCPClientSocket.o -c TCPClientSocket.cpp
TCPServerSocket.o: TCPServerSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/TCPServerSocket.o -c TCPServerSocket.cpp
ThreadPool.o: ThreadPool.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/ThreadPool.o -c ThreadPool.cpp
Types.o: Types.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/Types.o -c Types.cpp
UDPClientSocket.o: UDPClientSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/UDPClientSocket.o -c UDPClientSocket.cpp
UDPServerSocket.o: UDPServerSocket.cpp
	$(CC) $(CC_FLAGS) -o $(OUTDIR)/UDPServerSocket.o -c UDPServerSocket.cpp
link: AsyncLogger.o Algorithm.o ArgParser.o BitField.o Callable.o ChildProcess.o Conditional.o Daemon.o DataBuffer.o Debug.o File.o FileWriter.o Hashable.o ISerializable.o Logger.o Math.o MemStats.o MulticastClientSocket.o MulticastServerSocket.o Mutex.o Random.o Socket.o StringBuilder.o String.o TCPClientSocket.o TCPServerSocket.o ThreadPool.o Types.o UDPClientSocket.o UDPServerSocket.o
	$(CC) $(LINK_FLAGS) -o $(OUTFILE) $(OUTDIR)/AsyncLogger.o $(OUTDIR)/Algorithm.o $(OUTDIR)/ArgParser.o $(OUTDIR)/BitField.o $(OUTDIR)/Callable.o $(OUTDIR)/ChildProcess.o $(OUTDIR)/Conditional.o $(OUTDIR)/Daemon.o $(OUTDIR)/DataBuffer.o $(OUTDIR)/Debug.o $(OUTDIR)/File.o $(OUTDIR)/FileWriter.o $(OUTDIR)/Hashable.o $(OUTDIR)/ISerializable.o $(OUTDIR)/Logger.o $(OUTDIR)/Math.o $(OUTDIR)/MemStats.o $(OUTDIR)/MulticastClientSocket.o $(OUTDIR)/MulticastServerSocket.o $(OUTDIR)/Mutex.o $(OUTDIR)/Random.o $(OUTDIR)/Socket.o $(OUTDIR)/StringBuilder.o $(OUTDIR)/String.o $(OUTDIR)/TCPClientSocket.o $(OUTDIR)/TCPServerSocket.o $(OUTDIR)/ThreadPool.o $(OUTDIR)/Types.o $(OUTDIR)/UDPClientSocket.o $(OUTDIR)/UDPServerSocket.o $(LINK_LIBS)
clean:
	rm -f $(OUTDIR)/*.o $(OUTFILE)
	rmdir $(OUTDIR)
