/*
 * Class for creating and using Posix style multicast server side sockets.
 *
 * @author hockeyhurd
 * @version 10/31/2021
 */

// HCLib incldues
#include <hclib/MulticastClientSocket.h>

// OS includes
#if !OS_WIN
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#endif

// std includes
#include <cstring>
#include <sstream>

namespace hclib
{
    MulticastClientSocket::MulticastClientSocket(const std::string &address,
        const s32 port, const u8 ttl, const std::string &nic) :
        UDPClientSocket(address, port), nic(nic)
    {
    }

    MulticastClientSocket::MulticastClientSocket(std::string &&address,
        const s32 port, const u8 ttl, std::string &&nic) :
        UDPClientSocket(address, port, ttl), nic(std::move(nic))
    {
    }

    /* virtual */
    std::string &MulticastClientSocket::getGroupAddress()
    {
        return address;
    }

    /* virtual */
    const std::string &MulticastClientSocket::getGroupAddress() const
    {
        return address;
    }

    /* virtual */
    std::string &MulticastClientSocket::getNIC()
    {
        return nic;
    }

    /* virtual */
    const std::string &MulticastClientSocket::getNIC() const
    {
        return nic;
    }

    /* virtual */
    void MulticastClientSocket::open() /* override */
    {
        if (!isSetup())
        {
            UDPClientSocket::open();

            // TODO: Move this somewhere, anywhere...
            const auto ipAddressOfNIC = getIPFromNIC();
            in_addr localInterface;
            localInterface.s_addr = inet_addr(ipAddressOfNIC.c_str());

            const s32 code = setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF,
                    reinterpret_cast<char*>(&localInterface), sizeof(localInterface));

            if (code < 0)
            {
                close();
                std::ostringstream os;
                os << "Failed to set the local interface (code: " << code << ")\n";
                throw std::runtime_error(os.str());
            }
        }
    }

    /* virtual */
    void MulticastClientSocket::close() /* override */
    {
        // Passthrough to parent class.
        UDPClientSocket::close();
    }

    /* virtual */
    void MulticastClientSocket::setTTLInternal() /* override */
    {
        const s32 code = setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &this->ttl, sizeof(this->ttl));

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Failed to set TTL for this socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }
    }

    std::string MulticastClientSocket::getIPFromNIC()
    {
        ifreq ifr;

        // Specify that we want the IPv4 address
        ifr.ifr_addr.sa_family = AF_INET;

        // Get the IP address associated with the provided nicName.
        std::strncpy(ifr.ifr_name, nic.c_str(), IFNAMSIZ - 1);

        ioctl(sock, SIOCGIFADDR, &ifr);

        const std::string result = inet_ntoa((reinterpret_cast<sockaddr_in*>(&ifr.ifr_addr))->sin_addr);
        return result;
    }
}

