/*
 * Class for creating and using Posix style UDP server side sockets.
 *
 * @author hockeyhurd
 * @version 8/22/2021
 */

// HCLib includes
#include <hclib/UDPServerSocket.h>
#include <hclib/DataBuffer.h>
#include <hclib/Debug.h>
#include <hclib/Logger.h>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

// std includes
#include <cerrno>
#include <cstring>
#include <functional>
#include <memory>
#include <sstream>

// System includes
#if !OS_WIN
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

namespace hclib
{

    UDPServerSocket::UDPServerSocket(const s32 port, const bool reuse, const u8 ttl) : port(port), reuse(reuse), ttl(ttl)
    {
        // Make sure the value is non-zero
        if (this->ttl == 0)
        {
            ++this->ttl;
        }
    }

    UDPServerSocket::UDPServerSocket(UDPServerSocket &&other) : mSocket(std::move(other.mSocket)),
        port(other.port), reuse(other.reuse), ttl(other.ttl)
    {
        other.port = -1;

        // Copy the file descriptor.
        std::memcpy(&readFileDescriptorSet, &other.readFileDescriptorSet, sizeof(readFileDescriptorSet));

        // Try and set our file descriptor.
        FD_SET(mSocket.sock, &readFileDescriptorSet);

        // Zero out the other file descriptor.
        FD_ZERO(&other.readFileDescriptorSet);
    }

    /* virtual */
    UDPServerSocket::~UDPServerSocket()
    {
        // TODO: Can we do better?  This isn't practical to call
        // a virtual function from a base class destructor.
        // Consider making this non-virtual??
        // Ensure this is disconnected
        closeInternal();
    }

    UDPServerSocket &UDPServerSocket::operator= (UDPServerSocket &&other)
    {
        // Disconnect in the event this socket is already
        // in a connected state.
        closeInternal();

        mSocket = std::move(other.mSocket);
        port = other.port;
        other.port = -1;
        reuse = other.reuse;
        ttl = other.ttl;

        // Copy the file descriptor.
        std::memcpy(&readFileDescriptorSet, &other.readFileDescriptorSet, sizeof(readFileDescriptorSet));

        // Try and set our file descriptor.
        FD_SET(mSocket.sock, &readFileDescriptorSet);

        // Zero out the other file descriptor.
        FD_ZERO(&other.readFileDescriptorSet);

        return *this;
    }

    /* virtual */
    s32 UDPServerSocket::getPort() const HCLIB_NOEXCEPT
    {
        return port;
    }

    /* virtual */
    s32 UDPServerSocket::getTTL() const HCLIB_NOEXCEPT
    {
        return ttl;
    }

    /* virtual */
    void UDPServerSocket::setTTL(const u8 ttl)
    {
        if (isSetup())
        {
            this->ttl = static_cast<s32>(ttl);

            if (this->ttl == 0)
            {
                ++this->ttl;
            }

            setTTLInternal();
        }
    }

    /* virtual */
    bool UDPServerSocket::isSetup() const HCLIB_NOEXCEPT
    {
        return mSocket.handle != nullptr;
    }

    /* virtual */
    void UDPServerSocket::open()
    {
        if (isSetup())
        {
            return;
        }

        mSocket.sock = socket(AF_INET, EnumSocketType::UDP.getType(), 0);
        mSocket.handle = std::make_unique<sockaddr_in>();

        if (mSocket.sock < 0)
        {
            std::ostringstream os;
            os << "ERROR: Opening socket\n";
            close();
            throw std::runtime_error(os.str());
        }

        setSocketOptions();

        if (reuse)
        {
            s32 temp = 1;
#if OS_WIN
            setsockopt(mSocket.sock, SOL_SOCKET, SO_REUSEADDR | SO_BROADCAST, reinterpret_cast<const char*>(&temp), sizeof(temp));
#else
            const s32 code = setsockopt(mSocket.sock, SOL_SOCKET, SO_REUSEPORT, reinterpret_cast<const char*>(&temp), sizeof(temp));

            if (code < 0)
            {
                std::ostringstream os;
                os << "ERROR: Failed to reusable for this socket (code: " << code << ")\n";
                close();
                throw std::runtime_error(os.str());
            }
#endif
        }

        setTTLInternal();
        const s32 code = bind();

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Binding to socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }

        setupFDSet();
    }

    /* virtual */
    void UDPServerSocket::close()
    {
        closeInternal();
    }

    /* virtual */
    void UDPServerSocket::makeReusable()
    {
        if (isSetup())
        {
            return;
        }

        this->reuse = true;
    }

    /* virtual */
    bool UDPServerSocket::poll(timeval *time)
    {
        FD_SET(mSocket.sock, &readFileDescriptorSet);
        const s32 errorCode = ::select(mSocket.sock + 1, &readFileDescriptorSet, nullptr, nullptr, time);

        // Unspecified error occurred
        if (errorCode == -1)
        {
            std::ostringstream os;
            os << "ERROR: Failed to select/poll the socket (error code: " << errorCode << ")\n";
            close();
            throw std::runtime_error(os.str());
        }

        // Socket is ready to be accepted if non-zero
        return errorCode > 0 && FD_ISSET(mSocket.sock, &readFileDescriptorSet);
    }

    /* virtual */
    s32 UDPServerSocket::send(const Socket *clientSocket, const char *buffer, const std::size_t len)
    {
        if (!isSetup() || clientSocket == nullptr || buffer == nullptr || len == 0)
        {
            return -1;
        }

        auto theSize = static_cast<u32>(sizeof(*clientSocket->handle));
        const s32 numBytes = static_cast<s32>(::sendto(mSocket.sock, buffer, len, 0,
                            reinterpret_cast<const sockaddr*>(clientSocket->handle.get()),
                            theSize));

        return numBytes;
    }

    /* virtual */
    s32 UDPServerSocket::send(const Socket *clientSocket, const DataBuffer &buffer)
    {
        return send(clientSocket, buffer.data(), buffer.size());
    }

    /* virtual */
    s32 UDPServerSocket::send(const Socket *clientSocket, DataBuffer &&buffer)
    {
        return send(clientSocket, std::ref(buffer));
    }

    /* virtual */
    std::unique_ptr<Socket> UDPServerSocket::receive(char *buffer, const s32 len, s32 *bytesReceived)
    {
        if (!isSetup() || buffer == nullptr || len <= 0)
            return nullptr;

        // Can't use make_unique since Socket is a protected constructor.
        std::unique_ptr<Socket> clientSocket(new Socket(-1, std::make_unique<sockaddr_in>(), EnumSocketType::UDP));

#if OS_WIN
        auto theSize = static_cast<s32>(sizeof(*clientSocket->handle));
#else
        auto theSize = static_cast<u32>(sizeof(*clientSocket->handle));
#endif

        const s32 numBytesReceived = ::recvfrom(mSocket.sock, buffer, len, MSG_WAITALL, reinterpret_cast<sockaddr*>(clientSocket->handle.get()), &theSize);

        if (bytesReceived != nullptr)
            *bytesReceived = numBytesReceived;

        return clientSocket;
    }

    /* virtual */
    std::unique_ptr<Socket> UDPServerSocket::receive(DataBuffer &buffer, const s32 len, s32 *bytesReceived)
    {
        s32 count = 0;
        auto socketResult = receive(buffer.data(), len, &count);

        if (count > 0)
        {
            buffer.setPosition(static_cast<std::size_t>(count));
        }

        if (bytesReceived != nullptr)
        {
            *bytesReceived = count;
        }

        return socketResult;
    }

    /* virtual */
    s32 UDPServerSocket::bind()
    {
        sockaddr_in *handle = mSocket.handle.get();
        return ::bind(mSocket.sock, reinterpret_cast<sockaddr*>(handle), sizeof(*handle));
    }

    /* virtual */
    void UDPServerSocket::setSocketOptions() /* override */
    {
        blockCopy(mSocket.handle.get(), sizeof(*mSocket.handle), '\0');
        mSocket.handle->sin_family = AF_INET;
        mSocket.handle->sin_addr.s_addr = INADDR_ANY;
        mSocket.handle->sin_port = htons(port);
    }

    void UDPServerSocket::closeInternal()
    {
        if (isSetup())
        {
#if OS_WIN
            const s32 code = ::closesocket(mSocket.sock);
#else
            const s32 code = ::close(mSocket.sock);
#endif
            mSocket.sock = -1;
            mSocket.handle = nullptr;
            port = -1;
            FD_ZERO(&readFileDescriptorSet);

            if (code != 0)
            {
                std::ostringstream os;
                os << "ERROR closing socket (code: " << code << ")\n";
                throw std::runtime_error(os.str());
            }
        }
    }

    /* virtual */
    void UDPServerSocket::setTTLInternal()
    {
        const s32 code = setsockopt(mSocket.sock, IPPROTO_IP, IP_TTL, &this->ttl, sizeof(this->ttl));

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Failed to set TTL for this socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }
    }

    /* virtual */
    void UDPServerSocket::setupFDSet()
    {
        FD_ZERO(&readFileDescriptorSet);
        FD_SET(mSocket.sock, &readFileDescriptorSet);
    }

}
