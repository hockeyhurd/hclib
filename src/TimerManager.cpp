/**
 * A class for manging Timers.
 *
 * @author hockeyhurd
 * @version 03/19/2023
 */

// HCLib includes
#include <hclib/TimerManager.h>
#include <hclib/ThreadPool.h>

// std includes
#include <chrono>

namespace hclib
{
    TimerManager::TimerManager(const s64 frameIntervalMicro, ThreadPool* threadPool) HCLIB_NOEXCEPT :
        frameIntervalMicro(std::abs(frameIntervalMicro)), threadPool(threadPool), overruns(0), timerQueue(),
        queueMutex(), timerSet(), setMutex(), running(true), thread(timerThreadLoop, this)
    {
    }

    TimerManager::~TimerManager()
    {
        stop();
    }

    std::size_t TimerManager::getOverrunCount() const HCLIB_NOEXCEPT
    {
        return overruns.load();
    }

    Timer* TimerManager::createTimer(u32 duration, TimerEvent eventHandler, bool periodic)
    {
        Timer* timer = new Timer(duration, eventHandler, periodic);

        // Note: we must lock both mutexes at the same time
        // so that hypothetically the background thread doesn't think it has
        // already been removed.
        std::lock(queueMutex, setMutex);
        std::lock_guard<std::mutex> queueGuard(queueMutex, std::adopt_lock);
        std::lock_guard<std::mutex> setGuard(setMutex, std::adopt_lock);

        timerQueue.push(timer);
        timerSet.insert(timer->getUUID());

        return timer;
    }

    bool TimerManager::removeTimer(Timer* timer)
    {
        if (timer != nullptr)
        {
            std::lock_guard<std::mutex> setGuard(setMutex);
            timerSet.erase(timer->getUUID());

            return true;
        }

        return false;
    }

    void TimerManager::stop()
    {
        if (!running)
        {
            return;
        }

        running = false;
        thread.join();

        // At this point we should be single threaded.
        // We will still use our locks just to be sure.
        std::lock(queueMutex, setMutex);
        std::lock_guard<std::mutex> queueGuard(queueMutex, std::adopt_lock);
        std::lock_guard<std::mutex> setGuard(setMutex, std::adopt_lock);

        // Clean-up all allocated Timers
        while (!timerQueue.empty())
        {
            Timer* timer = timerQueue.front();
            timerQueue.pop();

            delete timer;
        }

        timerSet.clear();
    }

    /* static */
    void TimerManager::timerThreadLoop(TimerManager* instance)
    {
        while (instance->running)
        {
            const auto startTime = Timer::Clock::now();

            std::unique_lock<std::mutex> queueGuard(instance->queueMutex);
            const std::size_t len = instance->timerQueue.size();

            for (std::size_t i = 0; i < len; ++i)
            {
                Timer* timerPtr = instance->timerQueue.front();
                instance->timerQueue.pop();

                // Check if the Timer was marked for removal.
                // We surround it with scope so the lock_guard releases the mutex automatically.
                {
                    std::lock_guard<std::mutex> setGuard(instance->setMutex);
                    const u32 uuid = timerPtr->getUUID();
                    const auto findResult = instance->timerSet.find(uuid);

                    // Timer is not in our set, therefore it must have been removed.
                    if (findResult == instance->timerSet.cend())
                    {
                        // Clean-up the allocated memory
                        delete timerPtr;
                        continue;
                    }
                }

                // Use the ThreadPool if we have onc
                if (instance->threadPool != nullptr)
                {
                    instance->threadPool->queueJob([](Timer* theTimerPtr, TimerManager* timeMan)
                    {
                        const bool wasExecuted = theTimerPtr->executeIfExpired();

                        if (wasExecuted && !theTimerPtr->isPeriodic())
                        {
                            timeMan->removeTimer(theTimerPtr);
                        }
                    }, timerPtr, instance);

                    // In the asynchronous case, we always add the Timer back into
                    // the queue and rely on removeTimer appropriately called.
                    instance->timerQueue.push(timerPtr);
                }

                // No ThreadPool supplied. Call it from this thread if expired.
                else
                {
                    const bool wasExecuted = timerPtr->executeIfExpired();

                    if (wasExecuted && !timerPtr->isPeriodic())
                    {
                        std::lock_guard<std::mutex> setGuard(instance->setMutex);
                        instance->timerSet.erase(timerPtr->getUUID());

                        continue;
                    }

                    instance->timerQueue.push(timerPtr);
                }
            }

            // Release the guard since we no longer need to modify the queue.
            queueGuard.unlock();

            const auto endTime = Timer::Clock::now();
            const auto deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);
            const ssize_t sleepTime = instance->frameIntervalMicro - deltaTime.count();

            // There is still some time left in this frame, so let's sleep for that amount of time.
            if (sleepTime > 0)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(deltaTime.count()));
            }

            // Overrun occurred!
            else if (sleepTime < 0)
            {
                ++instance->overruns;
            }
        }
    }
}

