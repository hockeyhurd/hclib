#include <hclib/Types.h>

namespace hclib
{

    f32 roundTwoPlaces(f32 value)
    {
        value *= 100.0F;
        value = std::round(value);
        value *= 0.01F;
        return value;
    }

    f64 roundTwoPlaces(f64 value)
    {
        value *= 100.0;
        value = std::round(value);
        value *= 0.01;
        return value;
    }

    void blockCopy(void *buf, const std::size_t size, const char value)
    {
        char *buffer = reinterpret_cast<char*>(buf);

        for (std::size_t i = 0; i < size; ++i)
        {
            buffer[i] = value;
        }
    }

}
