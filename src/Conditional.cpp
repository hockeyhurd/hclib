/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

// HCLib includes
#include <hclib/Conditional.h>

// std includes
#include <stdexcept>

namespace hclib
{

    Conditional::Conditional()
    {
        if (pthread_cond_init(&cond, nullptr) != 0)
        {
            throw std::runtime_error("Failed to create conditional variable");
        }
    }

    /* virtual */
    Conditional::~Conditional()
    {
        pthread_cond_destroy(&cond);
    }

    /* virtual */
    void Conditional::wait(UniqueLock<Mutex> &lock)
    {
        auto *theMutex = lock.getMutex().getMutex();

        if (pthread_cond_wait(&cond, theMutex) != 0)
        {
            throw std::runtime_error("Failed to wait for conditional");
        }
    }

    /* virtual */
    void Conditional::signal()
    {
        if (pthread_cond_signal(&cond) != 0)
        {
            throw std::runtime_error("Failed to signal conditional");
        }
    }

    /* virtual */
    void Conditional::broadcast()
    {
        if (pthread_cond_broadcast(&cond) != 0)
        {
            throw std::runtime_error("Failed to broadcast conditional");
        }
    }

}
