#include <hclib/Logger.h>

#include <fstream>
#include <functional>
#include <iostream>

namespace hclib
{

#if CPP_VER < 2014
    s32 ordinal(const LogLevel level)
    {
        switch (level)
        {
        case LogLevel::ERROR:
            return 0;
        case LogLevel::WARN:
            return 1;
        case LogLevel::INFO:
            return 2;
        case LogLevel::DEBUG:
            return 3;
        default:
            // Should be un-reachable.
            throw std::runtime_error("Invalid enumeration value");
        }

        // Should be un-reachable.
        return -1;
    }

    const char *getName(const LogLevel level)
    {
        HCLIB_CONSTEXPR const char *lookupTable[] = { "ERROR", "WARN", "INFO", "DEBUG" };
        const s32 index = ordinal(level);
        return lookupTable[index];
    }
#endif

    Logger::Logger(std::ostream &os, const LogLevel level) : os(&os), level(level),
        owned(false), seenEndl(true), printedSomething(false)
    {
    }

    Logger::Logger(const std::string &path, const LogLevel level) : os(new std::ofstream(path)),
                   level(level), owned(true), seenEndl(true), printedSomething(false)
    {
    }

    Logger::Logger(std::string &&path, const LogLevel level) : Logger(std::ref(path), level)
    {
    }

    Logger::Logger(Logger &&other) : os(other.os), level(other.level), owned(other.owned),
        seenEndl(other.seenEndl), printedSomething(other.printedSomething)
    {
        other.os = nullptr;
        other.owned = false;
    }

    Logger::~Logger()
    {
        if (os != nullptr && owned)
        {
            delete os;
            os = nullptr;
            owned = false;
        }
    }

    Logger &Logger::operator= (Logger &&other)
    {
        if (os != nullptr && owned)
            delete os;

        os = other.os;
        other.os = nullptr;
        level = other.level;
        owned = other.owned;
        other.owned = false;
        seenEndl = other.seenEndl;
        other.seenEndl = false;
        printedSomething = other.printedSomething;
        other.printedSomething = false;

        return *this;
    }

    Logger &Logger::stdlogger()
    {
        static Logger inst;
        return inst;
    }

    LogLevel Logger::getLevel() const
    {
        return level;
    }

    void Logger::setLevel(const LogLevel level)
    {
        this->level = level;
    }

    Logger &Logger::flush()
    {
        if (os != nullptr)
        {
            os->flush();
        }

        return *this;
    }

    Logger &Logger::endl()
    {
        if (os != nullptr && printedSomething)
        {
            *os << '\n';
            seenEndl = true;
            printedSomething = false;
        }

        return *this;
    }
}
