/**
 * @author hockeyhurd
 * @version 2020-02-22
 */

#include <hclib/Random.h>

namespace hclib
{

    RandomInt::RandomInt(const s32 lowerBound, const s32 upperBound) : lowerBound(lowerBound), upperBound(upperBound), distribution(lowerBound, upperBound)
    {
    }

    s32 RandomInt::next()
    {
        return distribution(generator);
    }

    RandomLong::RandomLong(const s64 lowerBound, const s64 upperBound) : lowerBound(lowerBound), upperBound(upperBound), distribution(lowerBound, upperBound)
    {
    }

    s64 RandomLong::next()
    {
        return distribution(generator);
    }

    RandomFloat::RandomFloat(const f32 lowerBound, const f32 upperBound) : lowerBound(lowerBound), upperBound(upperBound), distribution(lowerBound, upperBound)
    {
    }

    f32 RandomFloat::next()
    {
        return distribution(generator);
    }

    RandomDouble::RandomDouble(const f64 lowerBound, const f64 upperBound) : lowerBound(lowerBound), upperBound(upperBound), distribution(lowerBound, upperBound)
    {
    }

    f64 RandomDouble::next()
    {
        return distribution(generator);
    }
}
