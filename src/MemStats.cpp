#include <hclib/MemStats.h>
#include <hclib/Debug.h>

#include <iostream>

namespace hclib
{

    MemStats *memStatsInstance;

    MemStats::MemStats() : current(0), total(0)
    {
    }

    MemStats &MemStats::instance()
    {
        static MemStats inst;
        return inst;
    }

    std::size_t MemStats::currentUsage()
    {
        auto pair = current.get();
        return *pair.second;
    }

    std::size_t MemStats::totalAllocated()
    {
        auto pair = total.get();
        return *pair.second;
    }

    std::ostream &MemStats::print(std::ostream &os)
    {
        os << "Current usage: " << currentUsage() << '\n';
        os << "Total   usage: " << totalAllocated() << '\n';

        return os;
    }

    void *MemStats::allocate(const std::size_t size)
    {
        std::size_t actualSize = size + sizeof(std::size_t);
        current.set(currentUsage() + size);
        total.set(totalAllocated() + size);

        std::size_t *ptr = reinterpret_cast<std::size_t*>(malloc(actualSize));

        // We store the actual size of the pointer
        // in the first sizeof(std::size_t) bytes.
        ptr[0] = size;

        return reinterpret_cast<void*>(&ptr[1]);
    }

    void MemStats::deallocate(void *ptr)
    {
        std::size_t *sizePtr = reinterpret_cast<std::size_t*>(ptr) - 1;
        std::size_t size = *sizePtr;
        free(sizePtr);

        const auto cur = currentUsage();
        if (cur < size)
            Debug::warn("De-allocating untracked memory...");

        // Debug::assertTrue(cur >= size);
        current.set(cur - size);
    }
}

#if DebugMode && defined(HCLIB_MEM_STATS) && HCLIB_MEM_STATS
void *operator new (std::size_t size)
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();

    return memStatsInstance->allocate(size);
}

void *operator new[] (std::size_t size)
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();

    return memStatsInstance->allocate(size);
}

void operator delete (void *ptr) HCLIB_NOEXCEPT
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();
    memStatsInstance->deallocate(ptr);
}

void operator delete (void *ptr, const std::size_t) HCLIB_NOEXCEPT
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();
    memStatsInstance->deallocate(ptr);
}

void operator delete[] (void *ptr) HCLIB_NOEXCEPT
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();
    memStatsInstance->deallocate(ptr);
}

void operator delete[] (void *ptr, const std::size_t) HCLIB_NOEXCEPT
{
    using namespace hclib;

    if (memStatsInstance == nullptr)
        memStatsInstance = &MemStats::instance();
    memStatsInstance->deallocate(ptr);
}
#endif
