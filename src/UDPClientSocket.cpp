/*
 * Class for creating and using Posix style UDP client side sockets.
 *
 * @author hockeyhurd
 * @version 8/22/2021
 */

#include <hclib/UDPClientSocket.h>
#include <hclib/DataBuffer.h>
#include <hclib/Logger.h>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

#include <cstring>

#if OS_WIN
#include <WinSock2.h>
#include <WS2tcpip.h>

// To allow us to call 'gethostname' function
#pragma warning(push)
#pragma warning(disable: 4996)
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#endif

namespace hclib
{

    UDPClientSocket::UDPClientSocket(const std::string &address, const s32 port, const u8 ttl) :
        address(address), port(port), ttl(static_cast<s32>(ttl))
    {
    }

    UDPClientSocket::UDPClientSocket(std::string &&address, const s32 port, const u8 ttl) :
        address(std::move(address)), port(port), ttl(static_cast<s32>(ttl))
    {
    }

    /* virtual */
    const std::string &UDPClientSocket::getAddress() const
    {
        return address;
    }

    /* virtual */
    std::string &UDPClientSocket::getAddress()
    {
        return address;
    }

    /* virtual */
    void UDPClientSocket::setAddress(const std::string &address)
    {
        close();
        this->address = address;
    }

    /* virtual */
    void UDPClientSocket::setAddress(std::string &&address)
    {
        this->address = std::move(address);
    }

    /* virtual */
    s32 UDPClientSocket::getPort() const
    {
        return port;
    }

    /* virtual */
    void UDPClientSocket::setPort(const s32 port)
    {
        this->port = port;
    }

    /* virtual */
    s32 UDPClientSocket::getTTL() const HCLIB_NOEXCEPT
    {
        return ttl;
    }

    /* virtual */
    void UDPClientSocket::setTTL(const u8 ttl)
    {
        if (isSetup())
        {
            this->ttl = static_cast<s32>(ttl);

            if (this->ttl == 0)
            {
                ++this->ttl;
            }

            setTTLInternal();
        }
    }

    /* virtual */
    bool UDPClientSocket::isSetup() const
    {
        return handle != nullptr;
    }

    /* virtual */
    void UDPClientSocket::open()
    {
        if (isSetup())
            return;

        handle = std::make_unique<sockaddr_in>();

        // TODO: Revert back to this??
#if OS_WIN
        // hostent *server = gethostbyname(address.c_str());
#else
        // hostent *server = gethostbyname(address.c_str());
#endif

        /*if (server == nullptr)
        {
            close();
            throw std::runtime_error("Failed to get host\n");
        }*/

        if ((sock = socket(AF_INET, EnumSocketType::UDP.getType(), 0)) < 0)
        {
            close();
            throw std::runtime_error("Failed to create socket\n");
        }

        std::memset(handle.get(), '\0', sizeof(*handle));
        handle->sin_family = AF_INET; 
        handle->sin_port = htons(port);

        // TODO: Revert back to this??
        // handle->sin_addr = *(reinterpret_cast<in_addr*>(server->h_addr));
        handle->sin_addr.s_addr = inet_addr(address.c_str());

        setTTLInternal();
    }

    /* virtual */
    void UDPClientSocket::close() /* override */
    {
        if (!isSetup())
            return;

#if OS_WIN
        const s32 code = ::closesocket(sock);
#else
        const s32 code = ::close(sock);
#endif

        handle.reset();

        if (code != 0)
        {
            std::ostringstream os;
            os << "Error closing socket (code: " << std::to_string(code) << ")\n";
            throw std::runtime_error(os.str());
        }
    }

    /* virtual */
    s32 UDPClientSocket::send(const char *buffer, const std::size_t len) const /* override */
    {
        if (handle == nullptr)
            return -1;

#if OS_WIN
        return ::sendto(sock, buffer, len, 0, reinterpret_cast<sockaddr*>(handle.get()), sizeof(*handle));
#else
        return ::sendto(sock, static_cast<const void*>(buffer), len, 0,
                reinterpret_cast<sockaddr*>(handle.get()), sizeof(*handle));
#endif
    }

    /* virtual */
    s32 UDPClientSocket::send(const DataBuffer &buffer) const /* override */
    {
        return send(buffer.data(), buffer.size());
    }

    /* virtual */
    s32 UDPClientSocket::send(DataBuffer &&buffer) const /* override */
    {
        return send(buffer.data(), buffer.size());
    }

    /* virtual */
    s32 UDPClientSocket::receive(char *buffer, const s32 len) const /* override */
    {
        if (handle == nullptr || buffer == nullptr || len <= 0)
            return -1;

        s32 count = 0;

        // TODO: This is UDP and not TCP.  So does this make sense to sit in a while loop??
        do
        {
            const s32 returnCode = recvfrom(sock, buffer + count, len - count, 0, nullptr, nullptr);

            if (returnCode < 0)
                break;

            count += returnCode;
        }
        while (count < len);

        if (count < 0)
        {
            std::ostringstream os;
            os << "ERROR: Reading from socket (code: "
                << std::to_string(count) << ")\n";
            throw std::runtime_error(os.str());
        }

        return count;
    }

    /* virtual */
    s32 UDPClientSocket::receive(DataBuffer &buffer, const s32 len) const /* override */
    {
        const s32 maxBufferSize = min<s32>(INT32_MAX, buffer.size());
        if (handle == nullptr || len <= 0 || len >= maxBufferSize)
            return -1;

        // Make sure the buffer is empty.
        buffer.clear();

        const s32 count = receive(buffer.data(), len);
        return count;
    }

    /* virtual */
    void UDPClientSocket::setTTLInternal()
    {
        const s32 code = ::setsockopt(sock, IPPROTO_IP, IP_TTL, &this->ttl, sizeof(this->ttl));

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Failed to set TTL for this socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }
    }
}

