/**
 * @author hockeyhurd
 * @version 2022-01-28
 */

#include <hclib/AsyncLogger.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <mutex>

namespace hclib
{

    AsyncLogger::AsyncLogger(std::ostream &os, const LogLevel level) : logger(os, level)
    {
    }

    AsyncLogger::AsyncLogger(const std::string &path, const LogLevel level) : logger(path, level)
    {
    }

    AsyncLogger::AsyncLogger(std::string &&path, const LogLevel level) : logger(std::move(path), level)
    {
    }

    AsyncLogger::AsyncLogger(AsyncLogger &&other)
    {
        std::lock(mutex, other.mutex);
        logger = std::move(other.logger);
    }

    AsyncLogger::~AsyncLogger()
    {
    }

    AsyncLogger &AsyncLogger::operator= (AsyncLogger &&other)
    {
        std::lock(mutex, other.mutex);
        logger = std::move(other.logger);
        return *this;
    }

    AsyncLogger &AsyncLogger::stdlogger()
    {
        static AsyncLogger inst;
        return inst;
    }

    LogLevel AsyncLogger::getLevel() const
    {
        const SharedLock<mutex_t> guard(mutex);
        return logger.getLevel();
    }

    void AsyncLogger::setLevel(const LogLevel level)
    {
        const UniqueLock<mutex_t> guard(mutex);
        logger.setLevel(level);
    }

    AsyncLogger &AsyncLogger::flush()
    {
        const UniqueLock<mutex_t> guard(mutex);
        logger.flush();
        return *this;
    }

    AsyncLogger &AsyncLogger::endl()
    {
        const UniqueLock<mutex_t> guard(mutex);
        logger.endl();
        return *this;
    }

}

