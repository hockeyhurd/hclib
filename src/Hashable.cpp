/**
 * @author hockeyhurd
 * @version 2019-02-16
 */

#include <hclib/Hashable.h>

namespace hclib
{

    s32 Hashable::operator% (const s32 val) const
    {
        return hashCode() % val;
    }

}
