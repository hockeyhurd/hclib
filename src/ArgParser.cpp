/**
 * @author hockeyhurd
 * @version 2020-12-22
 */

#include <hclib/ArgParser.h>

namespace hclib
{

    IArg::IArg(const s32 order, const s32 numValues) : order(max<s32>(-1, order)), numValues(max<s32>(0, numValues))
    {
    }

    /* virtual */
    s32 IArg::getOrder() const
    {
        return numValues;
    }

    /* virtual */
    s32 IArg::getNumberOfValues() const
    {
        return numValues;
    }

    /* virtual */
    bool IArg::isOrderDependent() const
    {
        return order >= 0;
    }

    StringArg::StringArg(const s32 order, const u32 numValues) : IArg(order, numValues)
    {
    }

    /* virtual */
    bool StringArg::parse(StringCacheType &stringCache, const std::vector<StringType> &options) /* override */
    {
        // Make sure we have enough args...
        // NOTE: options.size() is an std::size_t type
        // but ArgParser will gurantee this value is bounded
        // by the range of [0, INT32_MAX).
        if (static_cast<s32>(options.size()) != getNumberOfValues())
            return false;

        // Make sure the collection is empty
        results.clear();

        for (auto &str : options)
        {
            // const auto &actualString = str.string();
            results.push_back(str);
        }

        return true;
    }

    /* virtual */
    u32 ArgParser::expectedParseResult() const
    {
#if 0
        u32 count = 0;
        for (const auto &argHandler : flagMap)
        {
            count += argHandler.second->getNumberOfValues();
        }

        return count;
#else
        return static_cast<u32>(flagMap.size());
#endif
    }

    /* virtual */
    s32 ArgParser::parse(const s32 argc, char *argv[])
    {
        if (argc <= 1 || flagMap.empty())
            return 0;

        s32 parseCount = 0;
        s32 argCount = 1; // start at 1, to offset arg[0] (see comment below).
        std::vector<StringType> argsBuffer;

        // Most arguments/flags will only need [1-7] but 8 is a nice power of 2...
        // ... and we really like powers of 2.
        argsBuffer.reserve(8);

        // We start at 1 since we expect arg[0] to be the name of the program.
        for (s32 i = 1; i < argc; ++i)
        {
            auto cachedString = stringCache.intern(argv[i]);

            // Check to see if an arg must be at this position
            IArg *currentArg;
            auto orderFindResultIter = orderMap.find(argCount);
            if (orderFindResultIter != orderMap.end())
            {
                currentArg = orderFindResultIter->second;
            }

            else
            {
                auto findResultIter = flagMap.find(cachedString);

                // If not found in the map, abort.
                if (findResultIter == flagMap.end())
                    return -1;

                currentArg = findResultIter->second;
            }

            ++argCount;

            // Now we found the arg handler.  Make sure
            // we can supply it with the correct number of args...
            const auto argsNeeded = currentArg->getNumberOfValues();
            if (!enoughArgsLeft(i, argsNeeded, argc))
                return -1;

            // Now that we are sure we have enough args for this flag, collect them in a vector.
            // Clear the buffer in case there is old information there...
            argsBuffer.clear();

            for (s32 j = 0; j < argsNeeded; ++j)
            {
                // Again, using '++i' should be safe since we checked
                // it by calling the 'enoughArgsLeft' function.
                cachedString = stringCache.intern(argv[++i]);
                argsBuffer.push_back(cachedString);
            }

            // Args are now collected. Send them to our arg handler.
            const bool parseResult = currentArg->parse(stringCache, argsBuffer);

            // If the parse result is 'false', something failed and we should abort.
            if (!parseResult)
                return -1;

            // Else successfully parsed everything. Increment our counter and move
            // onto the next arg.
            ++parseCount;
        }

        // Check if any arg was missed that is in the orderMap
        // We check in reverse since this is the higher chance
        // of an error and we can early exit.
        for (auto iter = orderMap.rbegin(); iter != orderMap.rend(); ++iter)
        {
            const auto orderNum = iter->first;
            if (argCount < orderNum)
                return -1;
        }

        return parseCount;
    }

    /* static */
    bool ArgParser::enoughArgsLeft(const s32 index, const s32 requestedArgs, const s32 argc)
    {
        return index + requestedArgs < argc;
    }

    /* virtual */
    bool ArgParser::registerFlag(const std::string &flag, IArg *arg)
    {
        if (flag.empty() || arg == nullptr)
            return false;

        StringType internalFlag = stringCache.intern(flag);
        return registerFlag(internalFlag, arg);
    }

    /* virtual */
    bool ArgParser::registerFlag(std::string &&flag, IArg *arg)
    {
        if (flag.empty() || arg == nullptr)
            return false;

        StringType internalFlag = stringCache.intern(std::move(flag));
        return registerFlag(internalFlag, arg);
    }

    /* virtual */
    bool ArgParser::registerFlag(StringType flag, IArg *arg)
    {
        // Check if the arg has an order component.
        if (arg->isOrderDependent())
        {
            const auto index = arg->getOrder();
            orderMap[index] = arg;
        }

        flagMap[flag] = arg;

        return true;
    }
}
