// HCLib includes
#include <hclib/TCPServerSocket.h>
#include <hclib/DataBuffer.h>
#include <hclib/Debug.h>
#include <hclib/Logger.h>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

// std includes
#include <cstring>
#include <functional>
#include <sstream>

// System includes
#if OS_APPLE || OS_UNIX
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

namespace hclib
{

    TCPServerSocket::TCPServerSocket(const s32 port, const bool reuse) : port(port), reuse(reuse)
    {
    }

    TCPServerSocket::TCPServerSocket(TCPServerSocket &&other) : mSocket(std::move(other.mSocket)),
        port(other.port), reuse(other.reuse)
    {
        other.port = -1;

        // Copy the file descriptor.
        std::memcpy(&readFileDescriptorSet, &other.readFileDescriptorSet, sizeof(readFileDescriptorSet));

        // Try and set our file descriptor.
        FD_SET(mSocket.sock, &readFileDescriptorSet);

        // Zero out the other file descriptor.
        FD_ZERO(&other.readFileDescriptorSet);
    }

    TCPServerSocket::~TCPServerSocket()
    {
        // TODO: Can we do better?  This isn't practical to call
        // a virtual function from a base class destructor.
        // Consider making this non-virtual??
        // Ensure this is disconnected
        closeInternal();
    }

    TCPServerSocket &TCPServerSocket::operator= (TCPServerSocket &&other)
    {
        // Disconnect in the event this socket is already
        // in a connected state.
        closeInternal();

        mSocket = std::move(other.mSocket);
        port = other.port;
        other.port = -1;
        reuse = other.reuse;

        // Copy the file descriptor.
        std::memcpy(&readFileDescriptorSet, &other.readFileDescriptorSet, sizeof(readFileDescriptorSet));

        // Try and set our file descriptor.
        FD_SET(mSocket.sock, &readFileDescriptorSet);

        // Zero out the other file descriptor.
        FD_ZERO(&other.readFileDescriptorSet);

        return *this;
    }

    s32 TCPServerSocket::getPort() const
    {
        return port;
    }

    bool TCPServerSocket::isSetup() const
    {
        return mSocket.handle != nullptr;
    }

    void TCPServerSocket::open()
    {
        mSocket.sock = socket(AF_INET, EnumSocketType::TCP.getType(), 0);
        mSocket.handle = std::make_unique<sockaddr_in>();

        if (mSocket.sock < 0)
        {
            std::ostringstream os;
            os << "ERROR: Opening socket\n";
            close();
            throw std::runtime_error(os.str());
        }

        std::memset(mSocket.handle.get(), '\0', sizeof(*mSocket.handle));
        mSocket.handle->sin_family = AF_INET;
        mSocket.handle->sin_addr.s_addr = INADDR_ANY;
        mSocket.handle->sin_port = htons(port);

        if (reuse)
        {
            s32 temp = 1;
#if OS_WIN
            setsockopt(mSocket.sock, SOL_SOCKET, SO_REUSEADDR | SO_BROADCAST, reinterpret_cast<const char*>(&temp), sizeof(temp));
#else
            const s32 code = setsockopt(mSocket.sock, SOL_SOCKET, SO_REUSEPORT, reinterpret_cast<const char*>(&temp), sizeof(temp));
            if (code < 0)
            {
                std::ostringstream os;
                os << "ERROR: Failed to reusable for this socket (code: " << code << ")\n";
                close();
                throw std::runtime_error(os.str());
            }
#endif
        }

        const s32 code = bind();

        if (code < 0)
        {
            std::ostringstream os;
            os << "ERROR: Binding to socket (code: " << code << ")\n";
            close();
            throw std::runtime_error(os.str());
        }

        // Set-up read descriptors
        setupFDSet();

        // Listen to the socket.
        listen(mSocket.sock, 5);
    }

    void TCPServerSocket::close()
    {
        closeInternal();
    }

    void TCPServerSocket::makeReusable()
    {
        if (isSetup())
        {
            return;
        }

        this->reuse = true;
    }

    std::unique_ptr<Socket> TCPServerSocket::accept()
    {
        // Note: This constructor is protected, so we can't use 'make_unique' here.
        std::unique_ptr<Socket> clientSocket(new Socket(-1, std::make_unique<sockaddr_in>(), EnumSocketType::TCP));

#if OS_WIN
        s32 clientLen;
#else
        socklen_t clientLen;
#endif
        auto &handle = clientSocket->handle;
        clientSocket->sock = ::accept(mSocket.sock, reinterpret_cast<sockaddr*>(handle.get()), &clientLen);

        if (clientSocket->sock < 0)
        {
            std::ostringstream os;
            os << "ERROR: Accepting client connection (code: " << clientSocket->sock << ")\n";
            close();
            throw std::runtime_error(os.str());
        }

        return clientSocket;
    }

    bool TCPServerSocket::poll(timeval *time)
    {
        FD_SET(mSocket.sock, &readFileDescriptorSet);
        const s32 errorCode = ::select(mSocket.sock + 1, &readFileDescriptorSet, nullptr, nullptr, time);

        // Unspecified error occurred
        if (errorCode == -1)
        {
            Debug::error("polling the socket");
            return false;
        }

        // Socket is ready to be accepted if non-zero
        return errorCode > 0 && FD_ISSET(mSocket.sock, &readFileDescriptorSet);
    }

    s32 TCPServerSocket::send(const Socket *clientSocket, const char *buffer, const std::size_t len)
    {
        if (clientSocket == nullptr || buffer == nullptr || len == 0)
        {
            return -1;
        }

        return clientSocket->send(buffer, len);
    }

    s32 TCPServerSocket::send(const Socket *clientSocket, const DataBuffer &buffer)
    {
        return clientSocket != nullptr ? clientSocket->send(buffer) : -1;
    }

    s32 TCPServerSocket::send(const Socket *clientSocket, DataBuffer &&buffer)
    {
        return clientSocket != nullptr ? clientSocket->send(std::move(buffer)) : -1;
    }

    s32 TCPServerSocket::receive(const Socket *clientSocket, char *buffer, const s32 len)
    {
        if (clientSocket == nullptr || buffer == nullptr || len <= 0)
        {
            return -1;
        }

        return clientSocket->receive(buffer, len);
    }

    s32 TCPServerSocket::receive(const Socket *clientSocket, DataBuffer &buffer, const s32 len)
    {
        if (clientSocket == nullptr || len <= 0)
        {
            return -1;
        }

        return clientSocket->receive(buffer, len);
    }

    s32 TCPServerSocket::bind()
    {
        sockaddr_in *handle = mSocket.handle.get();
        return ::bind(mSocket.sock, reinterpret_cast<sockaddr*>(handle), sizeof(*handle));
    }

    void TCPServerSocket::setupFDSet()
    {
        FD_ZERO(&readFileDescriptorSet);
        FD_SET(mSocket.sock, &readFileDescriptorSet);
    }

    void TCPServerSocket::closeInternal()
    {
        if (mSocket.handle == nullptr)
            return;
#if OS_WIN
        ::shutdown(mSocket.sock, 1);
        const s32 code = ::closesocket(mSocket.sock);
#else
        ::shutdown(mSocket.sock, SHUT_RDWR);
        const s32 code = ::close(mSocket.sock);
#endif

        mSocket.sock = -1;
        mSocket.handle = nullptr;
        port = -1;
        FD_ZERO(&readFileDescriptorSet);

        if (code != 0)
            throw std::runtime_error("ERROR closing socket\n");
    }
}
