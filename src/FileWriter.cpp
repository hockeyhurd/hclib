#include <hclib/FileWriter.h>

namespace hclib
{

    FileWriter::FileWriter(const std::string &path, const std::ios_base::openmode mode) : path(path), os(path, mode)
    {
    }

    FileWriter::FileWriter(std::string &&path, const std::ios_base::openmode mode) : path(std::move(path)), os(this->path, mode)
    {
    }

    FileWriter::~FileWriter()
    {
        os.close();
    }

    const std::string &FileWriter::getPath() const
    {
        return path;
    }

    FileWriter &FileWriter::flush()
    {
        os.flush();
        return *this;
    }

    FileWriter &FileWriter::endl()
    {
        os << '\n';
        return *this;
    }
}
