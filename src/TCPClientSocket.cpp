#include <hclib/TCPClientSocket.h>
#include <hclib/DataBuffer.h>
#include <hclib/Logger.h>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

#include <cstring>

#if OS_WIN
#include <WinSock2.h>
#include <WS2tcpip.h>

// To allow us to call 'gethostname' function
#pragma warning(push)
#pragma warning(disable: 4996)
#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#endif

namespace hclib
{

    TCPClientSocket::TCPClientSocket(const std::string &address, const s32 port) : address(address), port(port)
    {
    }

    TCPClientSocket::TCPClientSocket(std::string &&address, const s32 port) : address(std::move(address)), port(port)
    {
    }

    /* virtual */
    const std::string &TCPClientSocket::getAddress() const
    {
        return address;
    }

    /* virtual */
    std::string &TCPClientSocket::getAddress()
    {
        return address;
    }

    /* virtual */
    void TCPClientSocket::setAddress(const std::string &address)
    {
        disconnect();
        this->address = address;
    }

    /* virtual */
    void TCPClientSocket::setAddress(std::string &&address)
    {
        this->address = std::move(address);
    }

    /* virtual */
    s32 TCPClientSocket::getPort() const
    {
        return port;
    }

    /* virtual */
    void TCPClientSocket::setPort(const s32 port)
    {
        this->port = port;
    }

    /* virtual */
    bool TCPClientSocket::isConnected() const
    {
        return handle != nullptr;
    }

    /* virtual */
    void TCPClientSocket::connect()
    {
        if (isConnected())
            return;

        handle = std::make_unique<sockaddr_in>();

#if OS_WIN
        hostent *server = gethostbyname(address.c_str());
#else
        hostent *server = gethostbyname(address.c_str());
#endif

        if (server == nullptr)
        {
            disconnect();
            throw std::runtime_error("Failed to get host\n");
        }

        if ((sock = socket(AF_INET, EnumSocketType::TCP.getType(), 0)) < 0)
        {
            disconnect();
            throw std::runtime_error("Failed to create socket\n");
        }

        std::memset(handle.get(), '\0', sizeof(*handle));
        handle->sin_family = AF_INET; 
        handle->sin_port = htons(port);
        handle->sin_addr = *(reinterpret_cast<struct in_addr*>(server->h_addr));

        s32 code = ::connect(sock, reinterpret_cast<struct sockaddr*>(handle.get()), sizeof(*handle.get()));

        if (code < 0)
        {
            const auto err = errno;
            std::ostringstream os;
            os << err << '\n';
            os << "Connection failed, code: " << std::to_string(code) << '\n';
            disconnect();
            throw std::runtime_error(os.str());
        }
    }

    /* virtual */
    void TCPClientSocket::disconnect()
    {
        if (!isConnected())
            return;

#if OS_WIN
        ::shutdown(sock, 1);
        const s32 code = ::closesocket(sock);
#else
        shutdown(sock, SHUT_RDWR);
        const s32 code = ::close(sock);
#endif

        handle = nullptr;

        if (code != 0)
        {
            std::ostringstream os;
            os << "Error closing socket (code: " << std::to_string(code) << ")\n";
            throw std::runtime_error(os.str());
        }
    }

    /* virtual */
    void TCPClientSocket::close() /* override */
    {
        disconnect();
    }
}

