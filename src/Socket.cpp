#include <hclib/Socket.h>
#include <hclib/DataBuffer.h>

#if CPP_VER == 2011
#include <hclib/make_unique.h>
#endif

#include <sstream>

#if OS_WIN
#include <WinSock2.h>
#include <WS2tcpip.h>

// To allow us to call 'gethostname' function
#pragma warning(push)
#pragma warning(disable: 4996)
#else
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#endif

namespace hclib
{

    static std::string TCP_NAME = "TCP";
    static std::string UDP_NAME = "UDP";
    static std::string RAW_NAME = "RAW";

    /* static */
    EnumSocketType EnumSocketType::TCP = EnumSocketType(SOCK_STREAM, &TCP_NAME);
    EnumSocketType EnumSocketType::UDP = EnumSocketType(SOCK_DGRAM, &UDP_NAME);
    EnumSocketType EnumSocketType::RAW = EnumSocketType(SOCK_RAW, &RAW_NAME);

    EnumSocketType::EnumSocketType(const s32 type, std::string *name) :
        type(type), name(name)
    {
    }

    s32 EnumSocketType::getType() const
    {
        return type;
    }

    const std::string &EnumSocketType::getName() const
    {
        return *name;
    }

    Socket::Socket(const s32 sock, std::unique_ptr<sockaddr_in> &&handle,
            const EnumSocketType &socketType) : sock(sock), handle(std::move(handle)), socketType(socketType)
    {
    }

    Socket::Socket(Socket &&other) : sock(other.sock), handle(std::move(other.handle)), socketType(other.socketType)
    {
        other.sock = -1;
    }

    Socket &Socket::operator= (Socket &&other)
    {
        sock = other.sock;
        other.sock = -1;
        handle = std::move(other.handle);

        return *this;
    }

    /* virtual */
    EnumSocketType Socket::getSocketType() const
    {
        return socketType;
    }

    /* virtual */
    void Socket::setSocketType(const EnumSocketType &socketType)
    {
        this->socketType = socketType;
    }

    /* virtual */
    void Socket::close()
    {
        if (handle != nullptr)
        {
#if OS_WIN
            ::closesocket(sock);
#else
            ::close(sock);
#endif
            sock = -1;
            handle.reset();
        }
    }

    /* virtual */
    s32 Socket::send(const char *buffer, const std::size_t len) const
    {
        if (handle == nullptr)
            return -1;

#if OS_WIN
        return ::send(sock, buffer, len, 0);
#else
        return ::send(sock, static_cast<const void*>(buffer), len, 0);
#endif
    }

    /* virtual */
    s32 Socket::send(const DataBuffer &buffer) const
    {
        return send(buffer.data(), buffer.size());
    }

    /* virtual */
    s32 Socket::send(DataBuffer &&buffer) const
    {
        return send(buffer.data(), buffer.size());
    }

    /* virtual */
    s32 Socket::receive(char *buffer, const s32 len) const
    {
        if (handle == nullptr || buffer == nullptr || len <= 0)
            return -1;

        const s32 returnCode = recv(sock, buffer, len, 0);

        if (returnCode < 0)
        {
            std::ostringstream os;
            os << "ERROR: Reading from socket (code: "
                << std::to_string(returnCode) << ")\n";
            throw std::runtime_error(os.str());
        }

        return returnCode;
    }

    /* virtual */
    s32 Socket::receive(DataBuffer &buffer, const s32 len) const
    {
        const s32 maxBufferSize = min<s32>(INT32_MAX, buffer.getCapacity());
        if (handle == nullptr || len <= 0 || len > maxBufferSize)
            return -1;

        // Make sure the buffer is empty.
        buffer.clear();

        const s32 count = receive(buffer.data(), len);
        buffer.setPosition(count);
        return count;
    }
}

#if OS_WIN
#pragma warning(pop)
#endif

