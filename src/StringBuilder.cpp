#include <hclib/StringBuilder.h>
#include <hclib/String.h>

#include <algorithm>
#include <cstring>
#include <functional>

namespace hclib
{

    StringBuilder::StringBuilder(const std::size_t capacity)
    {
        vec.reserve(capacity);
    }

    StringBuilder::StringBuilder(const StringBuilder &other) : vec(other.vec)
    {
    }

    StringBuilder::StringBuilder(StringBuilder &&other) : vec(std::move(other.vec))
    {
    }

    StringBuilder::StringBuilder(const std::string &str)
    {
        vec.reserve(str.size());

        for (const auto ch : str)
        {
            vec.push_back(ch);
        }
    }

    StringBuilder::StringBuilder(std::string &&str)
    {
        vec.reserve(str.size());

        for (const auto ch : str)
        {
            vec.push_back(ch);
        }
    }

#if CPP_VER >= 2017
    StringBuilder::StringBuilder(const std::string_view str)
    {
        vec.reserve(str.size());

        for (const auto ch : str)
        {
            vec.push_back(ch);
        }
    }
#endif

    StringBuilder &StringBuilder::operator= (const StringBuilder &other)
    {
        vec = other.vec;
        return *this;
    }

    StringBuilder &StringBuilder::operator= (StringBuilder &&other)
    {
        vec = std::move(other.vec);
        return *this;
    }

    StringBuilder &StringBuilder::operator= (const std::string &other)
    {
        vec.clear();
        vec.reserve(other.size());

        for (const auto ch : other)
        {
            vec.push_back(ch);
        }

        return *this;
    }

    StringBuilder &StringBuilder::operator= (std::string &&other)
    {
        vec.clear();
        vec.reserve(other.size());

        for (const auto ch : other)
        {
            vec.push_back(ch);
        }

        return *this;
    }

#if CPP_VER >= 2017
    StringBuilder &StringBuilder::operator= (const std::string_view str)
    {
        vec.clear();
        vec.reserve(str.size());

        for (const auto ch : str)
        {
            vec.push_back(ch);
        }

        return *this;
    }
#endif

    StringBuilder::size_type StringBuilder::size() const
    {
        return vec.size();
    }

    bool StringBuilder::isEmpty() const
    {
        return vec.empty();
    }

    void StringBuilder::reserve(const StringBuilder::size_type capacity)
    {
        vec.reserve(capacity);
    }

    void StringBuilder::append(const value_type c)
    {
        vec.push_back(c);
    }

    void StringBuilder::append(const value_type *str)
    {
        if (str != nullptr)
        {
            for (const value_type *c = str; *c != '\0'; ++c)
                vec.push_back(*c);
        }
    }

    void StringBuilder::append(const std::string &str)
    {
        if (!str.empty())
        {
            for (const auto c : str)
            {
                vec.push_back(c);
            }
        }
    }

    void StringBuilder::append(std::string &&str)
    {
        append(std::ref(str));
    }

    void StringBuilder::append(const String &str)
    {
        if (!str.isEmpty())
        {
            for (const auto c : str)
            {
                vec.push_back(c);
            }
        }
    }

    void StringBuilder::append(String &&str)
    {
        append(std::ref(str));
    }

    void StringBuilder::append(const StringBuilder &str)
    {
        if (!str.isEmpty())
        {
            for (const auto c : str.vec)
            {
                vec.push_back(c);
            }
        }
    }

    /* virtual */
    void StringBuilder::append(const bool value)
    {
        if (value)
        {
            vec.push_back('t');
            vec.push_back('r');
            vec.push_back('u');
            vec.push_back('e');
        }

        else
        {
            vec.push_back('f');
            vec.push_back('a');
            vec.push_back('l');
            vec.push_back('s');
            vec.push_back('e');
        }
    }

    /* virtual */
    void StringBuilder::append(const s16 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const u16 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const s32 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const u32 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const s64 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const u64 value)
    {
        primitiveAppend(value);
    }

#if OS_APPLE
    /* virtual */
    void StringBuilder::append(const std::size_t value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const ssize_t value)
    {
        primitiveAppend(value);
    }
#endif

    /* virtual */
    void StringBuilder::append(const f32 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    void StringBuilder::append(const f64 value)
    {
        primitiveAppend(value);
    }

    /* virtual */
    StringBuilder StringBuilder::substring(const StringBuilder::size_type length)
    {
        const StringBuilder::size_type actualLen = std::min<StringBuilder::size_type>(size(), length);

        StringBuilder result(actualLen);

        for (StringBuilder::size_type i = 0; i < actualLen; ++i)
        {
            result.vec.push_back(vec[i]);
        }

        return result;
    }

    /* virtual */
    StringBuilder StringBuilder::substring(const StringBuilder::size_type start, const StringBuilder::size_type length)
    {
        const auto thisSize = size();
        const StringBuilder::size_type actualLen = std::min<StringBuilder::size_type>(thisSize, length);
        StringBuilder result(actualLen);

        for (StringBuilder::size_type i = 0; i < length; ++i)
        {
            auto pos = start + i;

            // Check to make sure we are still in the bounds of the underlying vector.
            // If this is true, we want to immediately break out of this loop.
            if (pos >= thisSize)
                break;

            result.vec.push_back(vec[pos]);
        }

        return result;
    }

    /* virtual */
    void StringBuilder::clear()
    {
        vec.clear();
    }

    /* virtual */
    String StringBuilder::toString() const
    {
        return String(vec.data(), vec.size());
    }

    /* virtual */
    std::string StringBuilder::toSTDString() const
    {
        return std::string(vec.cbegin(), vec.cend());
    }

    /* virtual */
    /*const char *StringBuilder::c_str() const
    {
        return vec.data();
    }*/

    /* virtual */
    char *StringBuilder::copy() const
    {
        char *result = new char[vec.size() + 1];
        result[vec.size()] = '\0';
        std::memcpy(result, vec.data(), vec.size());

        return result;
    }

#if CPP_VER >= 2020
    /* virtual */
    std::shared_ptr<char[]> StringBuilder::copyShared() const
    {
        auto result = std::make_shared<char[]>(vec.size() + 1);
        result[vec.size()] = '\0';
        std::memcpy(result.get(), vec.data(), vec.size());

        return result;
    }
#endif

#if CPP_VER >= 2014
    /* virtual */
    std::unique_ptr<char[]> StringBuilder::copyUnique() const
    {
        auto result = std::make_unique<char[]>(vec.size() + 1);
        result[vec.size()] = '\0';
        std::memcpy(result.get(), vec.data(), vec.size());

        return result;
    }
#endif

    /* virtual */
    std::vector<char> StringBuilder::copyVector() const
    {
        std::vector<char> result(vec.size() + 1);
        result[vec.size()] = '\0';
        std::memcpy(result.data(), vec.data(), vec.size());

        return result;
    }

    /* virtual */
    void StringBuilder::reverse()
    {
        std::reverse(vec.begin(), vec.end());
    }

}

