#include <hclib/ChildProcess.h>
#include <hclib/Debug.h>

// std include
#include <csignal>
#include <functional>

// OS specific includes
#if OS_WIN
#include <cwchar>

// to remove std::mbsrtowcs warning
#pragma warning(push)
#pragma warning(disable: 4996)

#ifndef WIFEXITED
#define WIFEXITED(x) ((x) != 3)
#endif // !WIFEXITED

#ifndef WEXITSTATUS
#define WEXITSTATUS(x) (x)
#endif // !WEXITSTATUS

// TODO: Figure this out...
#ifndef WIFSTOPPED
#define WIFSTOPPED(x) (x)
#endif // !WIFSTOPPED

#else
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#endif

namespace hclib
{

    static HCLIB_CONSTEXPR s32 INVALID_PID = -1;

#if OS_WIN

    /**
     * Converts a c-string to LPWSTR (Windows version
     * of (std) wchar_t.  We need this conversion
     * for some of the Windows API calls.
     *
     * NOTE:  It is up to the caller, to call
     *        'delete[] variable'.
     *
     * @param str c-string to convert from.
     *
     * @return LPWSTR (wchar_t).
     */
    static LPWSTR toWinStr(const char *str)
    {
        // Call the first time to see how many bytes we
        // need to allocate.
        std::size_t size = std::mbsrtowcs(nullptr, &str, 0, nullptr);

        // NOTE: LPWSTR is a typedef of std::wchar_t
        wchar_t *buffer = new wchar_t[size + 1];

        // Load our c-string into the buffer.
#if CPP_VER >= 2017
        [[maybe_unused]]
#endif
        size = std::mbsrtowcs(buffer, &str, size + 1, nullptr);

        // Return the buffer.
        return buffer;
    }

    s32 ChildProcess::createProcessWindows(Handle &handle, const char *path, char *const argv[])
    {
        LPWSTR winPath = toWinStr(path);

        if (!CreateProcess(nullptr, winPath, nullptr, nullptr,
                           NULL, 0, nullptr, nullptr,
                           &handle.si, &handle.pid))
        {
            Debug::error("Failed to fork child process...");
            return 0;
        }

        // Per 'toWinStr' documentation, it is up to
        // us (the caller) to release memory to prevent
        // any sort of leakage!!
        delete[] winPath;

        const s32 pid = handle.getPID();

        if (pid != 0)
            handle.valid = true;

        return pid;
    }
#endif

#if OS_WIN
    ChildProcess::Handle::Handle() : valid(false)
    {
        ZeroMemory(&pid, sizeof(pid));
        ZeroMemory(&si, sizeof(si));
#else
    ChildProcess::Handle::Handle() : pid(INVALID_PID)
    {
#endif
    }

    s32 ChildProcess::Handle::getPID() const
    {
#if OS_WIN
        return GetProcessId(pid.hProcess);
#else
        return pid;
#endif
    }

    bool ChildProcess::Handle::isValid() const
    {
#if OS_WIN
        return valid;
#else
        return pid != INVALID_PID;
#endif
    }

    s32 ChildProcess::getPID() const
    {
#if OS_WIN
        return GetProcessId(handle.pid.hProcess);
#else
        return handle.pid;
#endif
    }

    void ChildProcess::setupSignalHandler(std::initializer_list<s32> signals, ChildProcess::SignalHandler signalHandler)
    {
#if OS_WIN
        for (auto &sig : signals)
            signal(sig, signalHandler);
#else
        struct sigaction sigAct;
        blockCopy(&sigAct, sizeof(sigAct), '\0');
        sigAct.sa_handler = signalHandler;
        sigAct.sa_flags = SA_RESTART;
        sigfillset(&sigAct.sa_mask);

        for (auto &sig : signals)
        {
            if (sigaction(sig, &sigAct, NULL) == -1)
            {
                Debug::error("Could not setup signal handler...");
            }
        }
#endif   
    }

    s32 ChildProcess::create(const char *path, char *const argv[], char *const envp[])
    {
        if (handle.isValid())
        {
            Debug::warn("Child process already created...");
            return handle.getPID();
        }

        if (path == nullptr)
            return INVALID_PID;

        // Debug::assertTrue(argv != nullptr);
        // Debug::assertTrue(envp != nullptr);

#if OS_WIN
        return createProcessWindows(handle, path, argv);
#else
        handle.pid = fork();

        if (handle.pid == INVALID_PID)
        {
            Debug::error("Failed to fork child process...");
            return handle.pid;
        }

        // Is the child process
        else if (handle.pid == 0)
        {
            const auto result = execve(path, argv, envp);

            if (result == -1)
            {
                Debug::error("execve failed...");
                std::exit(result);
            }
        }

        // Is the parent process
        // else
        // {
            // Do nothing...
        // }

        // Debug::info("handle.pid: " + std::to_string(handle.pid));
        return handle.pid;
#endif
    }

    s32 ChildProcess::create(const char *path, char *const argv[])
    {
        if (handle.isValid())
        {
            Debug::warn("Child process already created...");
            return handle.getPID();
        }

        Debug::assertTrue(path != nullptr);
        // Debug::assertTrue(argv != nullptr);
        // Debug::assertTrue(envp != nullptr);

#if OS_WIN
        return createProcessWindows(handle, path, argv);
#else
        handle.pid = fork();

        if (handle.pid == INVALID_PID)
        {
            Debug::error("Failed to fork child process...");
            std::exit(handle.pid);
        }

        // Is the child process
        else if (handle.pid == 0)
        {
            const auto result = execv(path, argv);

            if (result == -1)
            {
                Debug::error("execve failed...");
                std::exit(result);
            }
        }

        // Is the parent process
        // else
        // {
            // Do nothing...
        // }

        // Debug::info("handle.pid: " + std::to_string(handle.pid));
        return handle.pid;
#endif
    }

#if OS_UNIX || OS_APPLE
    /* virtual */
    s32 ChildProcess::daemonize()
    {
        s32 pid = fork();

        if (pid == INVALID_PID)
        {
            Debug::error("Failed to daemonize child process after first fork...");
            std::exit(pid);
        }

        // Is the child process
        else if (pid == 0)
        {
            signal(SIGCHLD, SIG_IGN);
            signal(SIGHUP, SIG_IGN);

            pid = fork();

            if (pid == INVALID_PID)
            {
                Debug::error("Failed to daemonize child process after second fork...");
                std::exit(pid);
            }

            // Is the child (grandchild) process
            else if (pid == 0)
            {
                // Set file permissions
                umask(0);

                // cd to root
                chdir("/");

                // Close any open file descriptors.
                for (s32 i = sysconf(_SC_OPEN_MAX); i >= 0; --i)
                {
                    close(i);
                }
            }

            // Is the parent (child) process
            else
            {
                std::exit(EXIT_SUCCESS);
            }
        }

        return pid;
    }
#endif

    /* virtual */
    s32 ChildProcess::kill(const s32 signal)
    {
        s32 returnValue;

#if OS_WIN
        HANDLE winHandle = OpenProcess(PROCESS_ALL_ACCESS, false, handle.getPID());

        // TODO: Probably don't want to use the 'signal' as the exitcode...
        // but good enough for now???
        // NOTE: Win32 uses non-zero to indicate call succeeded,
        // but this API will return zero on success.
        returnValue = !TerminateProcess(winHandle, signal);
#else
        if (::kill(handle.getPID(), signal) < 0)
        {
            returnValue = errno;
        }
        else
        {
            returnValue = 0;
        }
#endif

        return returnValue;
    }

    /* virtual */
    EnumProcessState ChildProcess::status() const
    {
        s32 waitStatus;

#if OS_WIN
        const s32 result = GetExitCodeProcess(handle.pid.hProcess, reinterpret_cast<LPDWORD>(&waitStatus));

        if (!result)
        {
            Debug::error("Failed to obtain child process status");
            std::exit(result);
        }
#else
        const s32 result = waitpid(handle.pid, &waitStatus, WNOHANG);

        if (result == -1)
        {
            Debug::error("Failed to obtain child process status");
            return EnumProcessState::UNKNOWN;
        }
#endif
        const EnumProcessState returnValue = EnumProcessState::getState(waitStatus);
        return returnValue;
    }

    s32 ChildProcess::wait(const u32 waitType)
    {
        s32 waitStatus;

        try
        {
#if OS_WIN
            const s32 result = WaitForSingleObject(handle.pid.hProcess, waitType);

            if (!result)
            {
                Debug::error("Failed to obtain child process status");
                std::exit(result);
            }

            // TODO: Will this work??
            const auto theStatus = status();
            waitStatus = theStatus.ordinal();
#else
            const auto result = waitpid(handle.pid, &waitStatus, waitType);

            if (result == -1)
            {
                Debug::error("Failed to obtain child process status");
                std::exit(result);
            }
#endif
        }

        catch (const std::exception &e)
        {
            Debug::error("Failed to wait for child process to exit");
            return -1;
        }

        return waitStatus;
    }

}

#if OS_WIN
// to remove std::mbsrtowcs warning
#pragma warning(pop)
#endif
