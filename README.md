# HCLib

Hockeyhurd's C++ library.

# Building:
================================================================================

Debug:
cmake -DCMAKE_BUILD_TYPE=Debug -B build_debug
cd build_debug
make

Release:
cmake -DCMAKE_BUILD_TYPE=Release -B build_release
cd build_release
make

# Unit testing:
================================================================================

bash unitTest.sh <build dir>

# Running CppCheck:
================================================================================

bash check.sh <job count>

This will create the file 'cppcheck.xml' to store the static analysis results.

# Generating documentation:
================================================================================
doxygen Doxyfile

